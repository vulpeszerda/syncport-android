# Syncport android client #

Syncport (File transfer service through personal devices) android client
Syncport는 "내가 가지고 있는 기기에 설치만 하면 언제 어디서든 이 기기의 파일에 접근할 수 있게 하는" 서비스 입니다.

## DEMO ##
http://www.youtube.com/watch?v=SjVdWPhhrh0

## Service concept ##
https://www.youtube.com/watch?v=H7CoeMMF-zg

## 사용된 기술 ##


### <디렉토리 싱크 부분> ###

syncport는 연결된 기기들의 file의 binary는 sync하지 않지만 directory tree는 모두 sync하여 서버에서 동기화 합니다. 이는 각 기기에서 ftp의 형태로 directory를 streaming하여 보여주는 것보다 아래와 같은 장점이 있습니다.

1) 기기가 꺼져있더라도 유저가 해당 기기의 파일 목록을 볼 수 있게 함
2) 유저가 다른 기기의 디렉토리를 보고있는 상황에서 새로고침을 하지 않더라도 변경내역이 있을 경우 변경된 부분만을 클라이언트에서 받아와서 실시간으로 적용가능

이를 구현하기 위해서 저희는 클라이언트A에서 
변경 내역이 있을 경우 
addition  : [ /a/b/c/,  /a/b/d]
deletion : [/a/b/e/, /a/b/f]
의 형태로 경로를 받아오면 서버에서는 이를 버전관리 시스템 git의 하나의 커밋과 비슷한 형태로 commit이라 저장을 하게 됩니다.

그러면 유저의 다른 클라이언트 B가 보는 A의 디렉토리 리스트도 업데이트를 해주어야 하는데, 

1) 만일 B가 켜져있다면 B에게 이 commit을 polling connection을 통해 뿌려주게 됩니다. 그러면 B가 가지고 있던 disk에 cache된 A의 디렉토리 리스트와 commit을 합쳐 새로운 최신 형태의 A의 디렉토리 리스트를 만들 수 있습니다.

2) B가 꺼져있다면, B가 켜지는 순간 B는 B가 기억하는 A의 마지막 커밋을 서버에게 날려 최신 커밋이 맞는지 확인하고, 최신 커밋이 아니라면 그 사이의 모든 커밋 리스트를 받아와서 1)과 같이 A의 최신본 디렉토리 리스트를 만듭니다.

1)과 2)를 통해서 저희는 실시간으로 변경되는 디렉토리 리스트를 모든 다른 디바이스에서 볼 수 있도록 만들었습니다.



### <파일 전송 부분> ###

유저간에 p2p를 하기위해 hole punching을 하여야 하는데 TCP hole punching은 지원되지 않는 공유기가 50%정도 되기 때문에 

syncport가 설치된 두 기기의 경우에는
1) TCP hole punching을 1차로 시도
2) 실패시 wave를 사용한  UDP hole punching, 
3) 그리고 이것도 실패했을때에는 http relay방식

syncport가 설치된 한 기기에 웹클라이언트로 접속하는 경우
0) web p2p를 1차 시도
1) 그실패했을때에는 http relay방식

을 사용하여 최대한도로 저희 서버 리소스를 적게먹으면서 유저 입장에서는 가장 빠른 데이터 전송이 가능하도록 개발하였습니다.
그리고 당연하게도 이 모든 시도는 클라이언트가 알아서 뒷단에서 처리합니다.


- wave p2p protocol
p2p로 데이터를 전송하기 위해 현재 개발중인 데스크탑 네이티브 클라이언트와 안드로이드에 추가될 프로토콜입니다. 
torrent protocol과  tsunami udp protocol을 참조하여 wave라는 reliable udp protocol을 개발하였습니다.

참조 논문
http://www.bittorrent.org/beps/bep_0029.html
http://tsunami-udp.sourceforge.net/


- http relay
피어간의 p2p전송이 불가능 할때 저희 서버는 각각의 peer가 데이터를 제대로 전송할 수 있도록 relay를 해줍니다.  http로 전송받은 post form형태의 데이터를 다른 피어에게 http get의 형태로 받을 수 있도록 실시간으로 http를 파싱하여 전달해줍니다.

- web p2p
유저가 웹 클라이언트로 전송하려는 기기와 같은 nat안에 있을 경우에 저희 서버를 거치지 않더라도 p2p로 데이터를 전송할수 있게 하기 위하여 web p2p를 구현하였습니다.  

예를 들어 유저가 웹 클라이언트로 파일을 다운받는다면 syncport가 깔린 기기는 server socket을 열어 웹 클라이언트의 http연결을 기다립니다.  웹 클라이언트는 해당 기기의 열린 socket으로 http protocol로 파일을 전송하고, 기기는 accept된 client socket의 데이터를 http로 파싱하여 데이터를 보내거나 받게 됩니다.

### <기타 사용된 기술> ### 

- long polling
저희 서비스에서 모든 기기들은 실시간으로 서버에서 업데이트를 받는 것이 중요한데, 이를 구현하기 위해 push와 long polling등의 선택에서 모든 클라이언트에서 구현이 쉬운 long polling으로 구현하였습니다.
모든 클라이언트는 이 long polling으로 서버에서 업데이트를 받고 한 기기가 다른 기기와 커뮤니케이션해야하는 경우에는 저희 서버가 중계자 역할을 하여 polling response로 메시지를 주고 받습니다.

- android wakelock
android와 같은 기기는 sleep mode라는 것이 존재하여 cpu가 잠들어버리는 경우가 있습니다.  이를 방지하기 위해서 android는 wakelock을 잡아 특정 일이 끝나기 전까지는 cpu가 잠들지 못하게 하는 기능을 제공합니다. 

저희 서비스의 경우 A라는 클라이언트에서 안드로이드 B라는 클라이언트의 파일을 옮길때 polling을 통해 메시지를 주고받는데 polling의 경우 request를 날리고 response를 기다리는 동안 cpu가 잠들어버려 제대로 기기간의 통신을 하는것이 불가능 합니다. 

이를 해결하기 위해 저희는 사용자가 다른 클라이언트로 A라는  안드로이드 기기와 통신해야 할 일이 있을 경우 안드로이드 기기에 google cloud message (GCM)을 통해 push 를 보내고, push를 받은 안드로이드 클라이언트는 그 순간부터 wakelock을 잡고 다시 polling을 시작합니다.