package io.syncport.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public abstract class BaseChildService extends Service {

    public static interface ReadyToDestroyListener {
        void onReadyToDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        IBinder iBinder = new ChildServiceBinder() {

            @Override
            public void prepareDestroy(ReadyToDestroyListener listener) {
                onPrepareDestroy(listener);
            }

            @Override
            public Service getService() {
                return BaseChildService.this.getService();
            }
        };
        return iBinder;
    }

    protected abstract void onPrepareDestroy(ReadyToDestroyListener listener);

    protected abstract Service getService();
}
