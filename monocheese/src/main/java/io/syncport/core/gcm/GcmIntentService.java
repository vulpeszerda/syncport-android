package io.syncport.core.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import io.syncport.R;
import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.ui.NavigationDrawerActivity;
import io.syncport.utils.Log;

public class GcmIntentService extends IntentService {
    private static final String TAG = "GcmIntentService";
    private static int sNotiIndex = 1;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) { // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that
             * GCM will be extended in the future with new message types, just
             * ignore any message types you're not interested in, or that you
             * don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                    .equals(messageType)) {
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                    .equals(messageType)) {
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                    .equals(messageType)) {
                String response = extras.getString("message");
                if (response == null) {
                    return;
                }
                Log.d(TAG, "got push response " + response);
                try {
                    JSONObject data = new JSONObject(response);
                    String type = data.getString("type");
                    String sender = data.optString("sender");
                    SyncportAccount account = SyncportAccount
                            .getAuthenticatedUser(getApplicationContext());

                    if (account == null) {
                        return;
                    }
                    if (type.equals("CPR")) {
                        if (sender == null) {
                            return;
                        }
                        RootService.extendWatcher(this, sender);
                    } else if (type.equals("RCPR")) {
                        if (sender == null) {
                            return;
                        }
                        RootService.expireWatcher(this, sender);
                    } else if (type.equals("notice")) {
                        generateNotice(data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Gson gson = SyncportGson.getInstance();
                //
                // try {
                // GeneralPollingInfo info = gson.fromJson(response,
                // GeneralPollingInfo.class);
                // if (info != null) {
                // String action = GeneralPollerService
                // .buildPollerAction(info.getType());
                //
                // Intent pollIntent = new Intent(action);
                // Class<? extends GeneralPollingInfo> dataClass = info
                // .getKlass();
                // if (dataClass != null) {
                // pollIntent.putExtra(
                // GeneralPollerService.PARAM_DATA,
                // gson.fromJson(response, dataClass));
                // }
                // Log.d(TAG, "sent broadcast action:" + action);
                // Log.d(TAG, response.toString());
                //
                // LocalBroadcastManager.getInstance(this)
                // .sendBroadcastSync(intent);
                // }
                // } catch (JsonSyntaxException e) {
                // Log.d(TAG, "failed to parse push response:" + e.toString());
                // e.printStackTrace();
                // }
            }
        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void generateNotice(JSONObject data) {

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                NavigationDrawerActivity.createIntent(this),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notiManager = (NotificationManager)
                        getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder compatBuilder = new NotificationCompat
                .Builder(this);
        compatBuilder.setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(data.optString("title", "Syncport notice"))
                .setContentText(data.optString("text"))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        if (data.optString("ticker") != null) {
            compatBuilder.setTicker(data.optString("ticker"));
        }
        Notification noti = compatBuilder.build();
        notiManager.notify(++sNotiIndex, noti);
    }
}
