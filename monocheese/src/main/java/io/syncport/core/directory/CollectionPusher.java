package io.syncport.core.directory;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.os.Process;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Authenticator;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.remote.DeltaResources;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

/**
 * Created by vulpes on 15. 2. 26..
 */
public class CollectionPusher extends Thread implements
    InternetConnectivityManager.Observer {

    public static interface Observer {
        void onPusherException(Exception e);
    }
    public static interface CollectionType {
        String IMAGE = "image";
        String DOCUMENT = "document";
        String VIDEO = "video";
        String MUSIC = "music";
    }

    public static final String[] COLLECTION_TYPES = new String[]{
            CollectionType.IMAGE,
            CollectionType.DOCUMENT,
            CollectionType.VIDEO,
            CollectionType.MUSIC
    };

    private static final String TAG = "CollectionPusher";
    private static final int PUSH_RETRY_DELAY = 3000;
    private static final int POP_DELAY = 3000;
    private static final int MAX_PUSH_RETRY_COUNT = 3;

    private Context mContext;
    private Object mInternetLock;
    private InternetConnectivityManager mConnManager;
    private Map<String, Set<String>> mCachedCollectionDirSet;
    private List<CollectionPatch> mQueue;
    private List<String> mCurrentDirList;
    private boolean mIsReadyToPush;
    private Object mLoadLock;
    private List<Observer> mObservers;

    public CollectionPusher(Context context) {
        mContext = context;
        mQueue = new ArrayList<>();
        mInternetLock = new Object();
        mConnManager = InternetConnectivityManager.getInstance(context);
        mConnManager.addObserver(this);

        mCachedCollectionDirSet = new HashMap<>();
        mObservers = new ArrayList<>();
        mIsReadyToPush = false;
        mLoadLock = new Object();
    }

    public void addAddition(String path) {
        CollectionPatch patch = new CollectionPatch();

        String parentPath = new File(path).getParent();
        if (!parentPath.endsWith("/")) {
            parentPath += "/";
        }

        if (FileUtils.isImagePath(path)) {
            patch.addAddition(CollectionType.IMAGE, parentPath);
        } else if (FileUtils.isAudioPath(path)) {
            patch.addAddition(CollectionType.MUSIC, parentPath);
        } else if (FileUtils.isVideoPath(path)) {
            patch.addAddition(CollectionType.VIDEO, parentPath);
        } else if (FileUtils.isDocumentPath(path)) {
            patch.addAddition(CollectionType.DOCUMENT, parentPath);
        }
        synchronized (mQueue) {
            mQueue.add(patch);
            mQueue.notify();
        }
    }

    private CollectionPatch createDiffPatchFromLastUplodedCache() {
        Map<String, Set<String>> currCache = new HashMap<>();
        for (String colType : COLLECTION_TYPES) {
            currCache.put(colType, new HashSet<String>());
        }

        for (String path : mCurrentDirList) {

            String parentPath = new File(path).getParent();
            if (!parentPath.endsWith("/")) {
                parentPath += "/";
            }

            Set<String> set = null;
            if (FileUtils.isImagePath(path)) {
                set = currCache.get(CollectionType.IMAGE);
            } else if (FileUtils.isAudioPath(path)) {
                set = currCache.get(CollectionType.MUSIC);
            } else if (FileUtils.isVideoPath(path)) {
                set = currCache.get(CollectionType.VIDEO);
            } else if (FileUtils.isDocumentPath(path)) {
                set = currCache.get(CollectionType.DOCUMENT);
            }
            if (set != null) {
                set.add(parentPath);
            }
        }

        CollectionPatch patch = null;
        boolean hasData = false;

        for (String colType : COLLECTION_TYPES) {
            Set<String> cached = mCachedCollectionDirSet.get(colType);
            Set<String> curr = currCache.get(colType);
            Set<String> delDiff = new HashSet<>(cached);
            Set<String> addDiff = new HashSet<>(curr);
            addDiff.removeAll(cached);
            delDiff.removeAll(curr);
            if (addDiff.size() > 0 || delDiff.size() > 0) {
                if (patch == null) {
                    patch = new CollectionPatch();
                }
                patch.addAdditions(colType, addDiff);
                patch.addDeletions(colType, delDiff);
            }
            if (addDiff.size() > 0 || delDiff.size() > 0) {
                hasData = true;
            }
        }
        return hasData ? patch : null;
    }

    public void setCurrentDirList(List<String> currentPaths) {
        synchronized (mLoadLock) {
            mCurrentDirList = currentPaths;
            mIsReadyToPush = true;
            mLoadLock.notify();
        }
    }

    public void addDeletion(String path) {
        CollectionPatch patch = new CollectionPatch();

        String parentPath = new File(path).getParent();
        if (!parentPath.endsWith("/")) {
            parentPath += "/";
        }

        if (FileUtils.isImagePath(path)) {
            patch.addDeletion(CollectionType.IMAGE, parentPath);
        } else if (FileUtils.isAudioPath(path)) {
            patch.addDeletion(CollectionType.MUSIC, parentPath);
        } else if (FileUtils.isVideoPath(path)) {
            patch.addDeletion(CollectionType.VIDEO, parentPath);
        } else if (FileUtils.isDocumentPath(path)) {
            patch.addDeletion(CollectionType.DOCUMENT, parentPath);
        }
        synchronized (mQueue) {
            mQueue.add(patch);
            mQueue.notify();
        }
    }

    private boolean isCollectionPathValid(String colType, String path) {
        File file = new File(path);
        if (!file.exists() || !file.isDirectory()) {
            return false;
        } else {
            File[] files = file.listFiles();
            if (files != null) {
                for (File child : files) {
                    String childName = child.getName();
                    boolean isValid = (colType.equals(CollectionType.IMAGE) && FileUtils.isImagePath(childName)) ||
                            (colType.equals(CollectionType.DOCUMENT) && FileUtils.isDocumentPath(childName)) ||
                            (colType.equals(CollectionType.MUSIC) && FileUtils.isAudioPath(childName)) ||
                            (colType.equals(CollectionType.VIDEO) && FileUtils.isVideoPath(childName));

                    if (isValid) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private CollectionPatch createSecurePatch(CollectionPatch patch) {
        boolean hasData = false;
        for(String colType : COLLECTION_TYPES) {
            SingleCollectionPatch p = patch.data.get(colType);
            Set<String> cache = mCachedCollectionDirSet.get(colType);

            boolean hasChange = false;
            Set<String> additions = new HashSet<>(p.additions);
            Set<String> deletions = new HashSet<>(p.deletions);
            for (String path : p.additions) {
                if (!isCollectionPathValid(colType, path) ||
                        cache.contains(path)) {
                    hasChange = true;
                    additions.remove(path);
                }
            }

            for (String path : p.deletions) {
                if (isCollectionPathValid(colType, path) ||
                        !cache.contains(path)) {
                    hasChange = true;
                    deletions.remove(path);
                }
            }
            if (hasChange) {
                p.additions = additions;
                p.deletions = deletions;
            }
            if (p.additions.size() > 0 || p.deletions.size() > 0) {
                hasData = true;
            }
        }
        return hasData ? patch : null;
    }

    private void readCollectionCaches() throws AuthenticatorException{
        for (String colType : COLLECTION_TYPES) {
            readCollectionCache(colType);
        }
    }

    private void readCollectionCache(String colType)
            throws AuthenticatorException{
        Set<String> list = null;
        if (DirectoryUtils.isSavedCollectionCacheExists(mContext, colType)) {
            try {
                list = new HashSet<>(DirectoryUtils.readCollectionCache(mContext,
                        colType));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (list == null) {
            list = new HashSet<>();
        }
        mCachedCollectionDirSet.put(colType, list);
    }

    private CollectionPatch compressAndPopFromQueue() {
        List<CollectionPatch> patches = new ArrayList<>();
        synchronized (mQueue) {
            patches.addAll(mQueue);
            mQueue.clear();
        }
        CollectionPatch merged = null;
        for (CollectionPatch patch : patches) {
            if (merged == null) {
                merged = patch;
                continue;
            }
            merged.merge(patch);
        }
        return createSecurePatch(merged);
    }

    private void saveCollectionCache(CollectionPatch patch) throws
            IOException, AuthenticatorException{
        for (String colType : patch.data.keySet()) {
            Set<String> cachedPaths = mCachedCollectionDirSet.get(colType);
            SingleCollectionPatch p = patch.data.get(colType);
            boolean needSave = false;
            for (String path : p.additions) {
                needSave |= cachedPaths.add(path);
            }
            for (String path : p.deletions) {
                needSave |= cachedPaths.remove(path);
            }
            if (needSave) {
                List<String> paths = new ArrayList<>(cachedPaths);
                if (paths.size() > 1) {
                    Collections.sort(paths);
                }
                DirectoryUtils.writeCollectionCache(mContext, colType, paths);
            }
        }
    }

    public synchronized void addObserver(Observer ob) {
        mObservers.add(ob);
    }

    public synchronized void removeObserver(Observer ob) {
        mObservers.remove(ob);
    }

    public synchronized void notifyPusherException(Exception e) {
        for (Observer ob : mObservers) {
            ob.onPusherException(e);
        }
    }

    private boolean request(CollectionPatch patch) throws ExecutionException,
            InterruptedException{
        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        try {
            JSONObject params = new JSONObject();
            params.put("data", patch.toJSON());
            DeltaResources.push(mContext, "collection", params, future, future);

            JSONObject response = future.get();
            return response.optBoolean("success");
        } catch(JSONException e) {
            // not reach
        }
        return false;
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        boolean pushImmediatly = true;
        int remainRetryCount = MAX_PUSH_RETRY_COUNT;

        try {
            readCollectionCaches();
            synchronized (mLoadLock) {
                while (!mIsReadyToPush) {
                    mLoadLock.wait();
                }
            }
            CollectionPatch patch = createDiffPatchFromLastUplodedCache();
            if (patch != null) {
                mQueue.add(0, patch);
            }
        } catch (AuthenticatorException e) {
            return;
        } catch (InterruptedException e) {
            // do nothing
        }

        while(!isInterrupted()) {
            try {
                if (mQueue.size() == 0) {
                    synchronized (mQueue) {
                        while (mQueue.size() == 0) {
                            mQueue.wait();
                        }
                    }
                }
                if (!pushImmediatly) {
                    sleep(POP_DELAY);
                }
                if (mConnManager.isConnected()) {
                    Log.d(TAG, "server upload started");
                    CollectionPatch patch = compressAndPopFromQueue();
                    if (patch != null) {
                        try {
                            boolean success = request(patch);
                            if (success) {
                                remainRetryCount = MAX_PUSH_RETRY_COUNT;
                                saveCollectionCache(patch);
                            } else {
                                notifyPusherException(
                                        new Exception("push result is not success"));
                                return;
                            }
                        } catch (IOException | AuthenticatorException e) {
                            notifyPusherException(e);
                            return;
                        } catch (ExecutionException e) {
                            VolleyError error = (VolleyError) e.getCause();
                            if ((error instanceof TimeoutError ||
                                    error instanceof NoConnectionError ||
                                    error instanceof NetworkError) &&
                                    remainRetryCount > 0) {
                                remainRetryCount -= 1;
                                mQueue.add(0, patch);
                                sleep(PUSH_RETRY_DELAY);
                            } else {
                                notifyPusherException(e);
                                return;
                            }
                        }
                    }
                }
            } catch (InterruptedException e) {
                // do nothing
            }
            if (!mConnManager.isConnected()) {
                Log.d(TAG, "wait push queue until internet reconnect");
                synchronized (mInternetLock) {
                    try {
                        while (!mConnManager.isConnected()) {
                            mInternetLock.wait();
                        }
                        Log.d(TAG, "internet reconnected. go push quque");
                        pushImmediatly = true;
                    } catch (InterruptedException e) {
                        // do nothing
                    }
                }
            }
        }
    }

    @Override
    public void onInternetConnected() {
        synchronized (mInternetLock) {
            mInternetLock.notify();
        }
    }

    @Override
    public void onInternetDisconnected() {

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {
    }

    private class CollectionPatch {

        Map<String, SingleCollectionPatch> data;


        public CollectionPatch() {
            data = new HashMap<>();

            for (String colType : COLLECTION_TYPES) {
                data.put(colType, new SingleCollectionPatch());
            }
        }

        public void addAddition(String colType, String addition) {
            SingleCollectionPatch patch = data.get(colType);
            if (patch != null) {
                patch.additions.add(addition);
            }
        }

        public void addAdditions(String colType, Collection<String> additions) {
            SingleCollectionPatch patch = data.get(colType);
            if (patch != null) {
                patch.additions.addAll(additions);
            }
        }

        public void addDeletion(String colType, String deletion) {
            SingleCollectionPatch patch = data.get(colType);
            if (patch != null) {
                patch.deletions.add(deletion);
            }
        }

        public void addDeletions(String colType, Collection<String> deletions) {
            SingleCollectionPatch patch = data.get(colType);
            if (patch != null) {
                patch.deletions.addAll(deletions);
            }
        }

        public JSONObject toJSON() throws JSONException{
            JSONObject dataObj = new JSONObject();
            for (String key : data.keySet()) {
                SingleCollectionPatch patch = data.get(key);

                JSONObject obj = new JSONObject();

                JSONArray additionArray = new JSONArray();
                JSONArray deletionArray = new JSONArray();

                ArrayList<String> additions = new ArrayList<>(patch.additions);
                ArrayList<String> deletions = new ArrayList<>(patch.deletions);

                Collections.sort(additions);
                Collections.sort(deletions);

                for (String path : additions) {
                    additionArray.put(path);
                }

                for (String path : deletions) {
                    deletionArray.put(path);
                }

                obj.put("addition", additionArray);
                obj.put("deletion", deletionArray);

                dataObj.put(key, obj);
            }
            return dataObj;
        }

        public CollectionPatch merge(CollectionPatch p) {
            boolean hasData = false;

            for (String colType : data.keySet()) {
                SingleCollectionPatch p1 = data.get(colType);
                SingleCollectionPatch p2 = p.data.get(colType);

                Set<String> additions = new HashSet<>(p1.additions);
                Set<String> deletions = new HashSet<>(p1.deletions);

                Set<String> nextAdditions = new HashSet<>(p2.additions);
                Set<String> nextDeletions = new HashSet<>(p2.deletions);

                deletions.removeAll(nextAdditions);
                additions.addAll(nextAdditions);

                additions.removeAll(nextDeletions);
                deletions.addAll(nextDeletions);

                p1.additions = additions;
                p1.deletions = deletions;
                if (p1.additions.size() > 0 || p1.deletions.size() > 0) {
                    hasData = true;
                }
            }
            return hasData ? this : null;
        }
    }

    private class SingleCollectionPatch {
        Set<String> additions;
        Set<String> deletions;
        public SingleCollectionPatch() {
            additions = new HashSet<>();
            deletions = new HashSet<>();
        }
    }
}
