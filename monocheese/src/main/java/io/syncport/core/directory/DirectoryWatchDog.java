package io.syncport.core.directory;

import io.syncport.R;
import io.syncport.core.polling.model.CollectionPollingInfo;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * WatchDog for directory change tracking. When watcher start, it compare
 * current directory with last committed dirlist. If any change, it will notice
 * JSONObject type patch to listener.
 *
 * @author vulpes
 *
 */
public class DirectoryWatchDog {

    public static interface ErrorListener {
        void onFailure(WatchDogException e);
    }

    public static class WatchDogException extends Exception {
        private static final long serialVersionUID = 1L;

        public WatchDogException(String msg) {
            super(msg);
        }

        public WatchDogException(Exception e) {
            super(e);
        }

        public WatchDogException(String msg, Exception e) {
            super(msg, e);
        }
    }

    private InitalizerThread mInitThread;

    private ThumbnailPusher mThumbnailPusher;
    private CollectionPusher mCollectionPusher;
    private DirectoryPatchPusher mPusher;

    private DirectoryPatchPusher.Queue mQueue;

    private RecursiveFileObserver mObserver;

    private Context mContext;
    private ErrorListener mErrorListener;
    private List<DirectoryPatch.FileDict> mLastCommittedDirList;
    private AtomicBoolean mIsRunning = new AtomicBoolean(false);

    private DirectoryPatchPusher.Observer mDirPusherObserver = new
            DirectoryPatchPusher.Observer() {

                @Override
                public void onDirectoryPatchPushed(DirectoryPatch patch) {
                    DirectoryWatchDog.this.onDirectoryPatchPushed(patch);
                }

                @Override
                public void onPusherException(Exception e) {
                    DirectoryWatchDog.this.onPusherException(e);
                }
            };

    private CollectionPusher.Observer mCollectionPusherObserver = new
            CollectionPusher.Observer() {

                @Override
                public void onPusherException(Exception e) {
                    DirectoryWatchDog.this.onPusherException(e);
                }
            };

    public DirectoryWatchDog(Context context, ErrorListener errorListener) {
        mContext = context;
        mPusher = new DirectoryPatchPusher(context);
        mPusher.addObserver(mDirPusherObserver);
        mQueue = mPusher.getQueue();

        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        String thumbnailKey = context
                .getString(R.string.preference_key__thumbnail_upload);
        if (pref.getBoolean(thumbnailKey, true)) {
            mThumbnailPusher = new ThumbnailPusher(context);
        }

        mCollectionPusher = new CollectionPusher(context);
        mCollectionPusher.addObserver(mCollectionPusherObserver);

        mErrorListener = errorListener;
    }

    public synchronized void start() {
        if (mIsRunning.get()) {
            return;
        }
        mIsRunning.set(true);

        if (mThumbnailPusher != null) {
            mThumbnailPusher.start();
        }
        mCollectionPusher.start();
        mPusher.start();
        final Handler handler = new Handler();
        mInitThread = new InitalizerThread(handler);
        mInitThread.start();
    }

    public synchronized void stop() {
        if (!mIsRunning.get()) {
            return;
        }
        mIsRunning.set(false);
        stopObserver();
        mPusher.quit();
        if (mThumbnailPusher != null) {
            mThumbnailPusher.interrupt();
        }
        mCollectionPusher.interrupt();
        if (mInitThread != null && mInitThread.isAlive()) {
            mInitThread.interrupt();
        }
    }

    public boolean isRunning() {
        return mIsRunning.get();
    }

    private void startObserver() {
        String rootPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath();
        mObserver = new RecursiveFileObserver(rootPath,
                RecursiveFileObserver.ALL_EVENTS, DirectoryUtils.ignorePaths()) {

            @Override
            public void onEvent(int event, String path) {
                DirectoryPatch patch = null;
                switch (event) {
                case MOVED_TO:
                case CREATE:

//                  Log.d("observer", "<create>" + path);
                    patch = DirectoryPatch.createPatch(mContext, path,
                                DirectoryPatch.CREATE);
                    if (event == FileObserver.MOVED_TO
                            && mThumbnailPusher != null) {
                        pushPatchThumbnails(patch);
                    }
                    mCollectionPusher.addAddition(path);
                    break;
                case MOVED_FROM:
                case DELETE:
//                  Log.d("observer", "<delete>" + path);
                    patch = DirectoryPatch.createPatch(mContext, path,
                                DirectoryPatch.DELETE);

                    if (mThumbnailPusher != null) {
                        pushPatchThumbnails(patch);
                    }
                    mCollectionPusher.addDeletion(path);
                    break;
                case CLOSE_WRITE:
                    if (FileUtils.isSyncportLogPath(path)) {
                        break;
                    }
                    Log.d("observer", "<close_write>" + path);
                    patch = DirectoryPatch.createPatch(mContext, path,
                                DirectoryPatch.CREATE);
                    if (mThumbnailPusher != null) {
                        if (FileUtils.isImagePath(path)
                                || FileUtils.isAudioPath(path)
                                || FileUtils.isVideoPath(path)) {
                            mThumbnailPusher.addPath(path);
                        }
                    }
                    break;
                case MODIFY:
                case ACCESS:
                case ATTRIB:
                case DELETE_SELF:
                case CLOSE_NOWRITE:
                case MOVE_SELF:
                case OPEN:
                    break;
                }
                if (patch != null) {
                    mQueue.add(patch);
                }
            }
        };

        mObserver.startWatching();
    }

    private void stopObserver() {
        if (mObserver != null) {
            mObserver.stopWatching();
        }
    }

    private void pushPatchThumbnails(DirectoryPatch patch) {
        List<String> additionImagePaths = new ArrayList<>();
        List<String> deletionImagePaths = new ArrayList<>();
        for (DirectoryPatch.FileDict dict : patch.getAddition()) {
            String path = dict.getPath();
            if (FileUtils.isImagePath(path) || FileUtils.isAudioPath(path)
                    || FileUtils.isVideoPath(path)) {
                additionImagePaths.add(path);
            }
        }
        for (DirectoryPatch.FileDict dict : patch.getDeletion()) {
            String path = dict.getPath();
            if (FileUtils.isImagePath(path) || FileUtils.isAudioPath(path)
                    || FileUtils.isVideoPath(path)) {
                deletionImagePaths.add(path);
            }
        }
        mThumbnailPusher.addPaths(additionImagePaths);
        mThumbnailPusher.removePathsFromAll(deletionImagePaths);
    }

    public synchronized void onDirectoryPatchPushed(DirectoryPatch patch) {
        try {
            mLastCommittedDirList = DirectoryPatch.doPatch
                    (mLastCommittedDirList, patch);
            List<String> serializedDirList = new ArrayList<>();
            for (DirectoryPatch.FileDict dict : mLastCommittedDirList) {
                serializedDirList.add(dict.toJSON().toString());
            }
            DirectoryUtils.saveDirList(mContext, serializedDirList);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AuthenticatorException e) {
            // do nothing.
        }
    }

    public void onPusherException(final Exception e) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                DirectoryWatchDog.this.stop();
                mErrorListener.onFailure(new WatchDogException(e));
            }

        });
    }

    private class InitalizerThread extends Thread {

        private Handler mHandler;

        public InitalizerThread(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            startObserver();

            try {
                List<String> dirList = DirectoryUtils.getCurrentDirList();

                mLastCommittedDirList = new ArrayList<>();
                for (String dir : dirList) {
                    DirectoryPatch.FileDict dict;
                    dict = DirectoryPatch.FileDict.buildAdditionFromPath
                            (mContext, dir);
                    mLastCommittedDirList.add(dict);
                }

                mCollectionPusher.setCurrentDirList(dirList);

                DirectoryPatch patch = null;

                List<String> savedDir = DirectoryUtils.readSavedDirList(mContext);
                List<DirectoryPatch.FileDict> savedFileDicts = new ArrayList<>();


                try {
                    for (String dir : savedDir) {
                        DirectoryPatch.FileDict dict;
                        dict = DirectoryPatch.FileDict.fromJSON(new JSONObject(dir));
                        if (dict != null) {
                            savedFileDicts.add(dict);
                        }
                    }

                    patch = DirectoryPatch.createPatch(savedFileDicts,
                            mLastCommittedDirList);
                } catch(JSONException e) {
                    // not reach
                }



                if (patch != null) {
                    mQueue.insertAt(0, patch);

                    if (mThumbnailPusher != null) {
                        pushPatchThumbnails(patch);
                    }
                }
                mQueue.setReady();
            } catch (final IllegalAccessException | IOException |
                    AuthenticatorException e) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        DirectoryWatchDog.this.stop();
                        mErrorListener.onFailure(new WatchDogException(e));
                    }

                });
            }
        }
    };
}
