package io.syncport.core.directory;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

public class DirectoryPatch implements Parcelable {

    private static Comparator<FileDict> sComparator = new Comparator<FileDict>
            () {

        @Override
        public int compare(FileDict lhs, FileDict rhs) {
            String lpath = lhs.getPath();
            String rpath = rhs.getPath();
            return lpath.compareTo(rpath);
        }

    };

    private String mDeviceId;
    private String mCommit;
    private List<FileDict> mAddition;
    private List<FileDict> mDeletion;

    /**
     * Constructor
     *
     * @param deviceId
     * @param addition
     *            (should be sorted)
     * @param deletion
     *            (should be sorted)
     */
    public DirectoryPatch(String deviceId, String commit,
                          List<FileDict> addition, List<FileDict> deletion) {
        mDeviceId = deviceId;
        mCommit = commit;

        mAddition = new ArrayList<>(addition);
        mDeletion = new ArrayList<>(deletion);
        if (mAddition.size() > 1) {
            Collections.sort(mAddition, sComparator);
        }
        if (mDeletion.size() > 1) {
            Collections.sort(mDeletion, sComparator);
        }
    }

    public DirectoryPatch(Context context, String deviceId, String commit,
            HashSet<String> additionSet, HashSet<String> deletionSet) {
        mDeviceId = deviceId;
        mCommit = commit;

        mAddition = new ArrayList<>();
        mDeletion = new ArrayList<>();

        for (String addition : additionSet) {
            FileDict dict = FileDict.buildAdditionFromPath(context, addition);
            if (dict != null) {
                mAddition.add(dict);
            }
        }
        for (String deletion : deletionSet) {
            FileDict dict =  FileDict.buildDeletionFromPath(deletion);
            if (dict != null) {
                mDeletion.add(dict);
            }
        }
        if (mAddition.size() > 1) {
            Collections.sort(mAddition, sComparator);
        }
        if (mDeletion.size() > 1) {
            Collections.sort(mDeletion, sComparator);
        }
    }

    @SuppressWarnings("unchecked")
    public DirectoryPatch(Parcel in) throws JSONException{
        mDeviceId = in.readString();
        mCommit = in.readString();
        String[] additionJSONs = in.createStringArray();
        String[] deletionJSONs = in.createStringArray();

        mAddition = new ArrayList<>();
        mDeletion = new ArrayList<>();
        for (String json : additionJSONs) {
            JSONObject obj = new JSONObject(json);
            mAddition.add(FileDict.fromJSON(obj));
        }
        for (String json : deletionJSONs) {
            JSONObject obj = new JSONObject(json);
            mDeletion.add(FileDict.fromJSON(obj));
        }
    }

    public DirectoryPatch(String deviceId, JSONObject obj) throws
            JSONException {
        mDeviceId = deviceId;
        JSONObject raw = obj.getJSONObject("raw");
        mCommit = obj.getString("commit");

        mAddition = new ArrayList<>();
        JSONArray additionObj = raw.getJSONArray("addition");
        for (int i = 0; i < additionObj.length(); i++) {
            mAddition.add(FileDict.fromJSON(additionObj.getJSONObject(i)));
        }

        mDeletion = new ArrayList<>();
        JSONArray deletionObj = raw.getJSONArray("deletion");
        for (int i = 0; i < deletionObj.length(); i++) {
            mDeletion.add(FileDict.fromJSON(deletionObj.getJSONObject(i)));
        }

        if (mAddition.size() > 1) {
            Collections.sort(mAddition, sComparator);
        }
        if (mDeletion.size() > 1) {
            Collections.sort(mDeletion, sComparator);
        }
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public String getCommit() {
        return mCommit;
    }

    public List<FileDict> getAddition() {
        return mAddition;
    }

    public List<FileDict> getDeletion() {
        return mDeletion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDeviceId);
        dest.writeString(mCommit);

        List<String> additions = new ArrayList<>();
        List<String> deletions = new ArrayList<>();

        try {
            for (FileDict dict : mAddition) {
                additions.add(dict.toJSON().toString());
            }
            for (FileDict dict : mDeletion) {
                deletions.add(dict.toJSON().toString());
            }
            dest.writeStringArray(additions.toArray(new String[additions.size()]));
            dest.writeStringArray(deletions.toArray(new String[deletions.size()]));
        } catch (JSONException e) {
            Log.e("DirectoryPatch", "Failed to serialize patch");
        }
    }

    public static final int CREATE = 1;
    public static final int DELETE = 2;

    public static DirectoryPatch createPatch(Context context, String path,
                                             int type) {
        File newFile = new File(path);
        HashSet<String> addition = new HashSet<>();
        HashSet<String> deletion = new HashSet<>();

        switch (type) {
        case CREATE:
            if (newFile.isDirectory()) {
                addition.add(newFile.getAbsolutePath() + "/");
            } else {
                addition.add(newFile.getAbsolutePath());
            }
            break;
        case DELETE:
            deletion.add(path);
            break;
        default:
            return null;
        }
        return new DirectoryPatch(context, null, null, addition, deletion);
    }

    public static DirectoryPatch createPatch(List<FileDict> prevList,
                                             List<FileDict> currList) throws
            JSONException{

        ArrayList<FileDict> additions = new ArrayList<>();
        ArrayList<FileDict> deletions = new ArrayList<>();

        if (prevList.size() > 1) {
            Collections.sort(prevList, sComparator);
        }
        if (currList.size() > 1) {
            Collections.sort(currList, sComparator);
        }

        int prevLen = prevList.size();
        int currLen = currList.size();

        int prevLineNum = 0;
        int currLineNum = 0;
        int compareVal;

        while (prevLineNum < prevLen && currLineNum < currLen) {
            FileDict prevDict = prevList.get(prevLineNum);
            FileDict currDict = currList.get(currLineNum);

            String prevPath = prevDict.getPath();
            String currPath = currDict.getPath();

            compareVal = prevPath.compareTo(currPath);
            if (compareVal > 0) { // prevLine should be next to currLine
                additions.add(currDict);
                ++currLineNum;
            } else if (compareVal < 0) {
                FileDict del = new FileDict(prevPath, null);
                deletions.add(del);
                ++prevLineNum;
            } else {
                if (!prevDict.areEquals(currDict)) {
                    additions.add(currDict);
                }
                ++prevLineNum;
                ++currLineNum;
            }
        }

        if (prevLineNum < prevLen) {
            for (int i = prevLineNum; i < prevLen; i++) {
                FileDict del = new FileDict(prevList.get(i).getPath(), null);
                deletions.add(del);
            }
        }
        if (currLineNum < currLen) {
            for (int i = currLineNum; i < currLen; i++) {
                additions.add(currList.get(i));
            }
        }

        if (additions.isEmpty() && deletions.isEmpty()) {
            return null;
        }

        return new DirectoryPatch(null, null, additions, deletions);
    }

    public static List<String> doPatchAsString(List<String> previous,
                                     DirectoryPatch patch)
            throws JSONException{
        Map<String, FileDict> set = new HashMap<>();
        for(String s : previous) {
            FileDict dict = FileDict.fromJSON(new JSONObject(s));
            set.put(dict.getPath(), dict);
        }
        for(FileDict dict : patch.getAddition()) {
            set.put(dict.getPath(), dict);
        }

        for(FileDict dict : patch.getDeletion()) {
            set.remove(dict.getPath());
        }

        List<FileDict> result = new ArrayList<>(set.values());
        if (result.size() > 1) {
            Collections.sort(result, sComparator);
        }
        List<String> resultString = new ArrayList<>();
        for (FileDict dict : result) {
            resultString.add(dict.toJSON().toString());
        }
        return resultString;
    }

    public static List<FileDict> doPatch(List<FileDict> previous,
                                       DirectoryPatch patch)
            throws JSONException{
        Map<String, FileDict> set = new HashMap<>();
        for (FileDict dict : previous) {
            set.put(dict.getPath(), dict);
        }
        for(FileDict dict : patch.getAddition()) {
            set.put(dict.getPath(), dict);
        }

        for(FileDict dict : patch.getDeletion()) {
            set.remove(dict.getPath());
        }

        List<FileDict> result = new ArrayList<>(set.values());
        if (result.size() > 1) {
            Collections.sort(result, sComparator);
        }
        return result;
    }

    public static DirectoryPatch merge(DirectoryPatch prevPatch,
            DirectoryPatch nextPatch) {
        String prevPatchDeviceId = prevPatch.getDeviceId();
        String nextPatchDeviceId = nextPatch.getDeviceId();
        if ((prevPatchDeviceId != null || nextPatchDeviceId != null)
                && (
                    (prevPatchDeviceId == null || nextPatchDeviceId == null) ||
                    (!prevPatchDeviceId.equals(nextPatchDeviceId)))) {
            return null;
        }


        Map<String, FileDict> additions = new HashMap<>();
        for (FileDict dict : prevPatch.mAddition) {
            additions.put(dict.getPath(), dict);
        }

        Map<String, FileDict> deletions = new HashMap<>();
        for (FileDict dict : prevPatch.mDeletion) {
            deletions.put(dict.getPath(), dict);
        }

        Map<String, FileDict> nextAdditions = new HashMap<>();
        for (FileDict dict : nextPatch.mAddition) {
            nextAdditions.put(dict.getPath(), dict);
        }

        Map<String, FileDict> nextDeletions = new HashMap<>();
        for (FileDict dict : nextPatch.mDeletion) {
            nextDeletions.put(dict.getPath(), dict);
        }

        // Delete next added path from previous deletion
        for (String key : nextAdditions.keySet()) {
            deletions.remove(key);
            additions.put(key, nextAdditions.get(key));
        }

        // remove addition which is just deleted by next patch
        for (String key : nextDeletions.keySet()) {
            additions.remove(key);
            deletions.put(key, nextDeletions.get(key));
        }

        ArrayList<FileDict> mergedAdditions = new ArrayList<>(additions.values());
        ArrayList<FileDict> mergedDeletions = new ArrayList<>(deletions.values());

        if (mergedAdditions.size() > 1) {
            Collections.sort(mergedAdditions, sComparator);
        }
        if (mergedDeletions.size() > 1) {
            Collections.sort(mergedDeletions, sComparator);
        }

        return new DirectoryPatch(prevPatch.getDeviceId(),
                    nextPatch.getCommit(), mergedAdditions, mergedDeletions);
    }

    public static class FileDict {
        private String mPath;
        private Map<String, String> mMeta;

        private FileDict(String path, Map<String, String> meta) {
            mPath = path;
            mMeta = meta;
        }

        public String getPath() {
            return mPath;
        }

        public Map<String, String> getMeta() {
            return mMeta;
        }

        public static FileDict fromJSON(JSONObject obj) throws JSONException{
            String path = obj.getString("p");
            HashMap<String, String> meta = null;
            JSONObject metaObj = obj.optJSONObject("m");
            if (metaObj != null) {
                meta = new HashMap<>();
                Iterator<String> keys = metaObj.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    meta.put(key, metaObj.getString(key));
                }
            }
            return new FileDict(path, meta);
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject obj = new JSONObject();
            obj.put("p", mPath);

            if (mMeta != null) {
                JSONObject metaObj = new JSONObject();

                for (String key : mMeta.keySet()) {
                    metaObj.put(key, mMeta.get(key));
                }

                obj.put("m", metaObj);
            }
            return obj;
        }

        public boolean areEquals(FileDict dict) {
            if (!mPath.equals(dict.getPath())) {
                return false;
            }
            Map<String, String> lhsMeta = mMeta;
            Map<String, String> rhsMeta = dict.getMeta();
            if (lhsMeta == null && rhsMeta == null) {
                return true;
            } else if (lhsMeta != null &&  rhsMeta != null) {
                Set<String> lhsKeys = lhsMeta.keySet();
                Set<String> rhsKeys = rhsMeta.keySet();

                Set<String> keys = new HashSet<>(lhsKeys);
                keys.addAll(rhsKeys);

                for (String key : keys) {
                    String lhsVal = lhsMeta.get(key);
                    String rhsVal = rhsMeta.get(key);
                    if (lhsVal == null && rhsVal == null) {
                        continue;
                    }
                    if ((lhsVal == null && rhsVal != null)
                            || (lhsVal != null && rhsVal == null)) {
                        return false;
                    }
                    if (!lhsVal.equals(rhsVal)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        @SuppressLint("NewApi")
        public static FileDict buildAdditionFromPath(Context context,
                                                     String path) {

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Map<String, String> meta = null;

            if (!path.endsWith("/")) {
                meta = new HashMap<>();
                File file = new File(path);
                if (!file.exists()) {
                    return null;
                }

                Date modifiedAt = new Date(file.lastModified());
                String modifiedAtStr = formatter.format(modifiedAt);

                if (FileUtils.isImagePath(path) || FileUtils.isDocumentPath
                        (path)) {
                    meta.put("ma", modifiedAtStr);
                    String p = path.toLowerCase();
                    if (p.endsWith(".jpg") || p.endsWith(".jpeg")) {
                        try {
                            ExifInterface exif = new ExifInterface(path);
                            int exifOrientation = exif.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            int rotate = 0;
                            switch (exifOrientation) {
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    rotate += 90;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                rotate += 90;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    rotate += 90;
                            }
                            rotate = (360 - rotate) % 360;
                            meta.put("or", String.valueOf((int) (rotate /
                                    90)));

                        } catch (IOException e) {
                            // do nothing
                        }
                    }
                } else if (FileUtils.isAudioPath(path)) {
                    try {
                        if (FileUtils.isAudioPath(file.getAbsolutePath()) &&
                                Build.VERSION.SDK_INT >= 10) {
                            MediaMetadataRetriever mediaMetadataRetriever =
                                    new MediaMetadataRetriever();
                            Uri uri = Uri.fromFile(file);
                            mediaMetadataRetriever.setDataSource(context, uri);
                            String title = mediaMetadataRetriever.extractMetadata(
                                    MediaMetadataRetriever.METADATA_KEY_TITLE);
                            String album = mediaMetadataRetriever.extractMetadata(
                                    MediaMetadataRetriever.METADATA_KEY_ALBUM);
                            String artist = mediaMetadataRetriever.extractMetadata(
                                    MediaMetadataRetriever.METADATA_KEY_ARTIST);
                            String time = mediaMetadataRetriever.extractMetadata(
                                    MediaMetadataRetriever.METADATA_KEY_DURATION);
                            mediaMetadataRetriever.release();
                            meta.put("tt", title);
                            meta.put("at", artist);
                            meta.put("ab", album);
                            meta.put("dr", time);
                        }
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                } else if (FileUtils.isVideoPath(path)) {
                    meta.put("ma", modifiedAtStr);
                    meta.put("sz", String.valueOf(file.length()));
                }
            }
            return new FileDict(path, meta);
        }

        public static FileDict buildDeletionFromPath(String path) {
            return new FileDict(path, null);
        }
    }
}
