package io.syncport.core.directory;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import io.syncport.core.account.SyncportAccount;

public class DirectoryUtils {

    public static final String DIR_CACHE = "dir_cache";
    private static final String ANDROID_DATA_DIR_PATH = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/Android";
    private static final String ANDROID_TEMP_DIR_PATH = Environment
            .getExternalStorageDirectory().getAbsolutePath() + "/temp";
    private static final String ANDROID_THUMBNAIL_DIR_PATH = Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            .getAbsolutePath()
            + "/.thumbnails";

    public static File getAccountExternalFilesDir(Context context)
            throws AuthenticatorException{
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            throw new AuthenticatorException("No account exists");
        }
        return new File(context.getExternalFilesDir(null), account.getName());
    }

    public static String[] ignorePaths() {
        return new String[] { ANDROID_DATA_DIR_PATH, ANDROID_TEMP_DIR_PATH,
                ANDROID_THUMBNAIL_DIR_PATH };
    }

    public static boolean isSdcardAvailable() {
        return android.os.Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public static void saveDirList(Context context, List<String> dirList)
            throws IOException, AuthenticatorException{
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        appDataDir.mkdirs();
        File myDirListFile = new File(appDataDir, DIR_CACHE);
        if (!myDirListFile.exists()) {
            myDirListFile.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(myDirListFile);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
        for (String line : dirList) {
            writer.write(line);
            writer.newLine();
        }
        writer.flush();
        writer.close();
        fos.close();
    }

    public static boolean isSavedDirListExists(Context context)
            throws AuthenticatorException{
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        File myDirListFile = new File(appDataDir, DIR_CACHE);
        return myDirListFile.exists();
    }

    public static void removeSavedDirList(Context context)
            throws AuthenticatorException {
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        File myDirListFile = new File(appDataDir, DIR_CACHE);
        if (myDirListFile.exists()) {
            myDirListFile.delete();
        }
    }

    public static List<String> readSavedDirList(Context context)
            throws IOException, IllegalAccessException, AuthenticatorException {
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        appDataDir.mkdirs();
        File myDirListFile = new File(appDataDir, DIR_CACHE);
        if (!myDirListFile.exists()) {
            throw new IllegalAccessException("file not found");
        }

        FileInputStream fis = new FileInputStream(myDirListFile);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
        List<String> dirs = new ArrayList<>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            dirs.add(line);
        }
        reader.close();
        fis.close();

        return dirs;
    }

    public static File getSyncRoot() {
        File sdcardRootFile = Environment.getExternalStorageDirectory();
        if (sdcardRootFile == null || !sdcardRootFile.exists()
                || !sdcardRootFile.isDirectory()) {
            throw new RuntimeException("Failed to access r/w sdcard");
        }
        return sdcardRootFile;
    }

    public static List<String> getCurrentDirList() throws RuntimeException {
        File sdcardRootFile = Environment.getExternalStorageDirectory();
        if (sdcardRootFile == null || !sdcardRootFile.exists()
                || !sdcardRootFile.isDirectory()) {
            throw new RuntimeException("Failed to access r/w sdcard");
        }
        return getChildDirList(sdcardRootFile);
    }

    public static List<String> getChildDirList(File currentDir) {
        ArrayList<String> dirList = new ArrayList<>();
        for (String ignorePath : ignorePaths()) {
            if (currentDir.getAbsolutePath().equals(ignorePath)) {
                return dirList;
            }
        }

        File[] childFiles = currentDir.listFiles();
        if (childFiles == null) {
            return dirList;
        }

        for (File file : childFiles) {
            if (!file.getName().startsWith(".")) {
                if (file.isDirectory()) {
                    dirList.add(file.getAbsolutePath() + "/");
                    dirList.addAll(getChildDirList(file));
                } else {
                    dirList.add(file.getAbsolutePath());
                }
            }
        }
        return dirList;
    }

    public static boolean isSavedCollectionCacheExists(
            Context context, String colType) throws AuthenticatorException{
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        String filename = "collection_" + colType + "_cache";
        File file = new File(appDataDir, filename);
        return file.exists();
    }

    public static void removeCollectionCache(Context context, String colType)
            throws AuthenticatorException{
        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        String filename = "collection_" + colType + "_cache";
        File file = new File(appDataDir, filename);
        if (file.exists()) {
            file.delete();
        }
    }

    public static List<String> readCollectionCache(
            Context context, String colType)
            throws IllegalAccessException, IOException, AuthenticatorException{

        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        String filename = "collection_" + colType + "_cache";
        File file = new File(appDataDir, filename);
        if (!file.exists()) {
            throw new IllegalAccessException("file not found");
        }

        FileInputStream fis = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
        List<String> dirs = new ArrayList<>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            dirs.add(line);
        }
        reader.close();
        fis.close();

        return dirs;
    }

    public static void writeCollectionCache(
            Context context, String colType, List<String> dirs)
            throws IOException, AuthenticatorException {

        File appDataDir = new File(getAccountExternalFilesDir(context),
                "directory");
        appDataDir.mkdirs();
        String filename = "collection_" + colType + "_cache";
        File file = new File(appDataDir, filename);
        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
        for (String line : dirs) {
            writer.write(line);
            writer.newLine();
        }
        writer.flush();
        writer.close();
        fos.close();
    }
}
