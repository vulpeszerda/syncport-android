package io.syncport.core.directory;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.utils.L;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import io.syncport.core.account.SyncportAccount;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.CancelTapingPollingInfo;
import io.syncport.core.polling.model.RemoteTapingPollingInfo;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;

/**
 * Created by vulpes on 15. 2. 3..
 */
public class FileCompressUpadaterService extends IntentService{

    private static class Receiver extends WakefulBroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            RemoteTapingPollingInfo info = (RemoteTapingPollingInfo)
                    intent.getSerializableExtra(GeneralPollerService
                            .PARAM_DATA);
            Intent serviceIntent = createIntent(context, info);
            sIsCancelledMap.put(info.getId(), new AtomicBoolean(false));
            startWakefulService(context, serviceIntent);
        }
    }

    private static final Map<String, AtomicBoolean> sIsCancelledMap = new
            HashMap<>();

    private static class CancelReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            CancelTapingPollingInfo info = (CancelTapingPollingInfo)
                    intent.getSerializableExtra(GeneralPollerService
                            .PARAM_DATA);
            AtomicBoolean isCancelled = sIsCancelledMap.get(info.getId());
            if (isCancelled != null) {
                isCancelled.set(true);
            }
        }
    }

    public static BroadcastReceiver registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter(
                GeneralPollerService.PollerAction.REMOTE_TAPING);
        BroadcastReceiver receiver = new Receiver();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (context);
        manager.registerReceiver(receiver, filter);
        return receiver;
    }

    public static BroadcastReceiver registerCancelReceiver(Context context) {
        IntentFilter cancelFilter = new IntentFilter(GeneralPollerService
                .PollerAction.CANCEL_REMOTE_TAPING);
        BroadcastReceiver receiver = new CancelReceiver();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (context);
        manager.registerReceiver(receiver, cancelFilter);
        return receiver;
    }

    public static Intent createIntent(Context context,
                                      RemoteTapingPollingInfo info) {
        Intent intent = new Intent(context, FileCompressUpadaterService.class);
        intent.putExtra(PARAM_INFO, info);
        return intent;
    }

    private static String PARAM_INFO = SyncportKeyBuilder.build("info");
    private static String TAG = "FileCompressUpdaterService";

    private static long MIN_PUBLISH_DIFF_TIME = 500;

    private AtomicInteger mOnTheFlyRequestCount = new AtomicInteger(0);
    private long mTotalSize;
    private long mProgressSize;
    private long mLastPublishedTime;
    private boolean mPublishFinished;
    private String mPeerDeviceId;
    private String mTaskId;
    private String mCompressFilePath;
    private AtomicBoolean mIsCancelled;
    private Set<String> mRelativePaths;

    public FileCompressUpadaterService() {
        super("FileCompressUpdaterpService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RemoteTapingPollingInfo info = (RemoteTapingPollingInfo)
                intent.getSerializableExtra(PARAM_INFO);
        mIsCancelled = sIsCancelledMap.get(info.getId());

        mPeerDeviceId = info.getSender();
        mTaskId = info.getId();

        String[] paths = info.getPaths();
        if (paths.length == 0) {
            return;
        }

        ArrayList<String> parentPaths = null;
        for (String path : paths) {
            String[] subPaths = path.split("/");
            if (parentPaths == null) {
                parentPaths = new ArrayList<>();
                for (int i = 0; i < subPaths.length - 1; i++) {
                    parentPaths.add(subPaths[i]);
                }
                continue;
            }
            int removeFromIdx = -1;
            for (int i = 0; i < parentPaths.size(); i++) {
                if (!parentPaths.get(i).equals(subPaths[i])) {
                    removeFromIdx = i;
                    break;
                }
            }
            if (removeFromIdx > -1) {
                // pop all elements i to end
                for (int j = parentPaths.size() - 1; j >= removeFromIdx; j--) {
                    parentPaths.remove(j);
                }
            }
        }
        String parentPath = "";
        for (String path : parentPaths) {
            parentPath += path + "/";
        }
        File parentDir = new File(parentPath);
        if (!parentDir.exists()) {
            return;
        }

        Context context = getApplicationContext();
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        File accountDir = new File(context.getExternalCacheDir(),
                account.getName());
        File compressCacheDir = new File(accountDir, "compressed");
        compressCacheDir.mkdirs();

        String zipFileName;
        if (paths.length > 1) {
            zipFileName = parentDir.getName();
        } else {
            String name = new File(paths[0]).getName();
            int idx = name.lastIndexOf(".");
            if (idx > -1) {
                name = name.substring(0, idx);
            }
            zipFileName = name;
        }
        File compressCacheFile = new File(compressCacheDir,
                zipFileName + ".zip");
        compressCacheFile = FileUtils.getAlternativeFile(compressCacheFile);
        mCompressFilePath = compressCacheFile.getAbsolutePath();

        mTotalSize = 0;
        mProgressSize = 0;
        mPublishFinished = false;

        try {
            FileOutputStream dest = new FileOutputStream(compressCacheFile);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));

            List<File> sourceFileList = new ArrayList<>();
            mRelativePaths = new HashSet<>();

            for (int i = 0; i < paths.length; i++) {
                File sourceFile = new File(paths[i]);
                if (sourceFile.exists()) {
                    sourceFileList.add(sourceFile);
                    mTotalSize += getFileSize(sourceFile, info.getFilter());
                }
            }
            File[] files = sourceFileList.toArray(
                    new File[sourceFileList.size()]);

            int parentDirPathLen = parentDir.toString().length();
            zipFiles(out, files, parentDirPathLen, info.getFilter());
            out.close();

            if (mIsCancelled.get()) {
                publishCancelled();
                compressCacheFile.delete();
            } else if (!mPublishFinished) {
                publishProgress(100);
            }

            while(mOnTheFlyRequestCount.get() > 0) {
                synchronized (this) {
                    wait();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sIsCancelledMap.remove(info.getId());
        Receiver.completeWakefulIntent(intent);
    }



    private void zipFiles(ZipOutputStream out, File[] fileList,
                              int basePathLength,
                              String filter) throws IOException {

        final int BUFFER = 2048;

        BufferedInputStream origin = null;
        for (File file : fileList) {
            if (file.isDirectory()) {
                zipFiles(out, file.listFiles(), basePathLength, filter);
                continue;
            }

            boolean isValidFile = isCompressTarget(file, filter);
            if (!isValidFile) {
                continue;
            }

            byte data[] = new byte[BUFFER];
            String unmodifiedFilePath = file.getPath();
            String relativePath;

            if (TextUtils.isEmpty(filter)) {
                relativePath = unmodifiedFilePath.substring(basePathLength);
            } else {
                relativePath = file.getName();
            }
            relativePath = getAlternativePath(mRelativePaths, relativePath);
            mRelativePaths.add(relativePath);

            FileInputStream fi = new FileInputStream(unmodifiedFilePath);
            origin = new BufferedInputStream(fi, BUFFER);
            ZipEntry entry = new ZipEntry(relativePath);
            out.putNextEntry(entry);

            int count;
            long currTime;
            float percent;
            while ((count = origin.read(data, 0,
                    BUFFER)) != -1 && !mIsCancelled.get()) {
                out.write(data, 0, count);

                // publish progress
                mProgressSize += count;
                percent = mProgressSize * 100 / mTotalSize;
                currTime = (new Date()).getTime();
                if (mLastPublishedTime == 0 ||
                        currTime - mLastPublishedTime > MIN_PUBLISH_DIFF_TIME ||
                        mProgressSize == mTotalSize) {
                    mLastPublishedTime = currTime;
                    publishProgress(percent);
                }
            }
            origin.close();

            if (mIsCancelled.get()) {
                break;
            }
        }
    }

    private boolean isCompressTarget(File file, String filter) {
        if (!file.isFile()) {
            return false;
        }
        boolean isValidFile = false;
        if (filter != null) {
            if (filter.equals("image")) {
                isValidFile = FileUtils.isImagePath(file.getName());
            } else if (filter.equals("document")) {
                isValidFile = FileUtils.isDocumentPath(file.getName());
            } else if (filter.equals("music")) {
                isValidFile = FileUtils.isAudioPath(file.getName());
            } else if (filter.equals("video")) {
                isValidFile = FileUtils.isVideoPath(file.getName());
            }
        } else {
            isValidFile = true;
        }
        return isValidFile;
    }

    private long getFileSize(File file, String filter) {
        if (file.isDirectory()) {
            File[] fileList = file.listFiles();
            if (fileList == null) {
                return 0;
            }
            long size = 0;
            for (File subfile : fileList) {
                size += getFileSize(subfile, filter);
            }
            return size;
        } else if (file.isFile()) {
            boolean isValidFile = isCompressTarget(file, filter);
            return isValidFile ? file.length() : 0;
        }
        return 0;
    }

    private void publishCancelled() {
        publishProgress(-1);
    }

    private void publishProgress(float percent) {
        Response.Listener<JSONObject> responseListener =
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mOnTheFlyRequestCount.decrementAndGet();
                synchronized (FileCompressUpadaterService.this) {
                    FileCompressUpadaterService.this.notifyAll();
                }
            }
        };

        RobustResponseErrorListener errorListener =
                new RobustResponseErrorListener(getApplicationContext()) {
            @Override
            public void onSkipped(VolleyError error) {
                // not reach
            }

            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                Log.e(TAG, msg);
                mOnTheFlyRequestCount.decrementAndGet();
                synchronized (FileCompressUpadaterService.this) {
                    FileCompressUpadaterService.this.notifyAll();
                }
            }
        };

        try {
            mOnTheFlyRequestCount.incrementAndGet();
            JSONObject params = new JSONObject();
            params.put("receiver", mPeerDeviceId);
            params.put("progress", percent);
            params.put("id", mTaskId);
            if (percent == 100) {
                mPublishFinished = true;
                params.put("path", mCompressFilePath);
            }
            DeltaResources.push(getApplicationContext(), "taping_progress",
                    params, responseListener, errorListener);

        } catch (JSONException e) {
        }

    }

    private String getAlternativePath(Set<String> paths, String path) {
        String p;
        if (path.endsWith("/")) {
            p = path.substring(0, path.length() - 1);
        } else {
            p = path;
        }

        String parentPath;
        String name;
        int idx = p.lastIndexOf("/");
        if (idx > -1) {
            name = p.substring(idx + 1);
            parentPath = p.substring(0, idx + 1);
        } else {
            name = p;
            parentPath = "";
        }

        int dotIdx = path.lastIndexOf(".");
        String extension = "";

        if (dotIdx > 0 && name.length() > dotIdx + 1) {
            extension = name.substring(dotIdx + 1);
            name = name.substring(0, dotIdx);
        }

        int count = 0;
        String alternativePath = path;
        while (paths.contains(alternativePath)) {
            ++count;
            alternativePath = parentPath + name + "(" + count + ")." +
                    extension;
        }
        return alternativePath;
    }
}
