package io.syncport.core.directory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import android.os.FileObserver;
import android.util.Pair;

/**
 * Recursive file observer. FileObserver in Android sdk only observes top level
 * dir change.
 *
 * @see https
 *      ://github.com/owncloud/android/blob/master/src/com/owncloud/android/
 *      utils/RecursiveFileObserver.java#L1
 * @author vulpes
 *
 */
public class RecursiveFileObserver extends FileObserver {

    public static int CHANGES_ONLY = CLOSE_WRITE | MOVE_SELF | MOVED_FROM;
    // public static int DIRECTORY_CREATE = 1073742080;
    // public static int DIRECTORY_DELETE = 1073742336;
    // public static int DIRECTORY_MOVE_TO = 1073741952;
    // public static int DIRECTORY_MOVE_FROM = 1073741888;
    private static int DIRECTORY_EVENT = 1073741824;

    Map<String, SingleFileObserver> mObservers;
    String mPath;
    int mMask;
    String[] mIgnorePaths;

    public RecursiveFileObserver(String path) {
        this(path, ALL_EVENTS);
    }

    public RecursiveFileObserver(String path, int mask) {
        this(path, mask, new String[] {});
    }

    public RecursiveFileObserver(String path, int mask, String[] ignorePaths) {
        super(path, mask);
        mPath = path;
        mMask = mask;
        mIgnorePaths = ignorePaths;
    }

    @Override
    public synchronized void startWatching() {
        if (mObservers != null)
            return;
        mObservers = new HashMap<>();
        Stack<String> stack = new Stack<>();
        stack.push(mPath);

        while (!stack.empty()) {
            String parent = stack.pop();
            boolean ignore = false;
            for (String ignorePath : mIgnorePaths) {
                if (ignorePath.equals(parent)) {
                    ignore = true;
                    break;
                }
            }
            if (ignore) {
                continue;
            }
            mObservers.put(parent, new SingleFileObserver(parent, mMask));
            File path = new File(parent);
            File[] files = path.listFiles();
            if (files == null)
                continue;
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory() && !files[i].getName().equals(".")
                        && !files[i].getName().equals("..")
                        && !files[i].getName().startsWith(".")) {
                    stack.push(files[i].getPath());
                }
            }
        }
        for (Entry<String, SingleFileObserver> entry : mObservers.entrySet()) {
            if (entry.getValue() != null) {
                entry.getValue().startWatching();
            }
        }
    }

    @Override
    public synchronized void stopWatching() {
        if (mObservers == null)
            return;

        for (Entry<String, SingleFileObserver> entry : mObservers.entrySet()) {
            if (entry.getValue() != null) {
                entry.getValue().stopWatching();
            }
        }

        mObservers.clear();
        mObservers = null;
    }

    @Override
    public void onEvent(int event, String path) {

    }

    private class SingleFileObserver extends FileObserver {
        private String mPath;

        public SingleFileObserver(String path, int mask) {
            super(path, mask);
            mPath = path;
        }

        @Override
        public void onEvent(int event, String path) {
            List<Pair<Integer, String>> childEvents = new ArrayList<Pair<Integer, String>>();
            // XXX: this code added for proper directory observe
            boolean isDirectory = (event & DIRECTORY_EVENT) > 0;
            event &= FileObserver.ALL_EVENTS;

            String newPath = path != null ? mPath + "/" + path : mPath;

            for (String ignorePath : mIgnorePaths) {
                if (ignorePath.equals(newPath)) {
                    return;
                }
            }

            // Release observers which were deleted
            if (event == FileObserver.DELETE_SELF) {
                mObservers.get(mPath).stopWatching();
                mObservers.remove(mPath);
            }

            // case: moved from path file is dir
            if (event == FileObserver.MOVED_FROM
                    && mObservers.get(newPath) != null) {
                SingleFileObserver observer;
                String[] keys = mObservers.keySet().toArray(
                        new String[mObservers.keySet().size()]);
                for (String key : keys) {
                    if (key.startsWith(newPath)) {
                        observer = mObservers.remove(key);
                        if (observer != null) {
                            observer.stopWatching();
                        }
                    }
                }
            }

            if ((event == FileObserver.MOVED_TO || event == FileObserver.CREATE)
                    && new File(newPath).isDirectory()) {
                Stack<String> stack = new Stack<String>();
                stack.push(newPath);

                while (!stack.empty()) {
                    String parent = stack.pop();
                    SingleFileObserver observer = new SingleFileObserver(
                            parent, mMask);
                    observer.startWatching();
                    mObservers.put(parent, observer);
                    File file = new File(parent);
                    File[] files = file.listFiles();
                    if (files == null)
                        continue;
                    for (int i = 0; i < files.length; ++i) {
                        if (files[i].isDirectory()
                                && !files[i].getName().equals(".")
                                && !files[i].getName().equals("..")
                                && !files[i].getName().startsWith(".")) {
                            stack.push(files[i].getPath());
                        }
                        if (!files[i].getName().startsWith(".")) {
                            if (files[i].isDirectory()) {
                                childEvents.add(new Pair<Integer, String>(
                                        event, files[i]
                                                .getAbsolutePath() + "/"));
                            } else {
                                childEvents.add(new Pair<Integer, String>(
                                        event, files[i].getAbsolutePath()));
                            }
                        }
                    }
                }

            }

            if (isDirectory && !newPath.endsWith("/")) {
                newPath += "/";
            }
            File effectedFile = new File(newPath);
            if (!effectedFile.getName().startsWith(".")) {
                RecursiveFileObserver.this.onEvent(event, newPath);
                int[] observeEvents = new int[] { SingleFileObserver.CREATE,
                        SingleFileObserver.DELETE, SingleFileObserver.MOVED_TO,
                        SingleFileObserver.MOVED_FROM, };
                if (Arrays.asList(observeEvents).contains(event)) {
                }
            }
            for (Pair<Integer, String> pair : childEvents) {
                RecursiveFileObserver.this.onEvent(pair.first, pair.second);
            }
        }
    }
}
