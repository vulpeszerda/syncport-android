package io.syncport.core.directory;

import android.accounts.AuthenticatorException;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import io.syncport.core.BaseChildService;
import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.directory.DirectoryWatchDog.WatchDogException;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;

public class DirectoryPusherService extends BaseChildService implements
        DirectoryWatchDog.ErrorListener, InternetConnectivityManager.Observer {

    public static final String ACTION_RESTART_DIRECTORY_WATCHDOG =
            SyncportKeyBuilder
            .build("restart_directory_watchdog");

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, DirectoryPusherService.class);
        return intent;
    }


    private static final String TAG = "DirectoryPusherService";
    private static final int MAX_RETRY_COUNT = 3;

    private int mRetryCount = MAX_RETRY_COUNT;
    private DirectoryWatchDog mWatchDog;
    private Handler mHandler;
    private AtomicBoolean mNeedInitOnConnect = new AtomicBoolean(false);
    private Runnable mRestartWatchDogRunnable = new Runnable() {

        @Override
        public void run() {
            secureInitWatchDog();
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
        IBinder iBinder = super.onBind(intent);
        mHandler = new Handler();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                stopWatchDog();
                secureInitWatchDog();
            }
        }, new IntentFilter(ACTION_RESTART_DIRECTORY_WATCHDOG));

        InternetConnectivityManager connManager = InternetConnectivityManager
                .getInstance(this);
        connManager.addObserver(this);
        if (connManager.isConnected()) {
            secureInitWatchDog();
        } else {
            mNeedInitOnConnect.set(true);
        }
        return iBinder;
    }

    private void secureInitWatchDog() {
        if (mWatchDog != null && mWatchDog.isRunning()) {
            return;
        }
        try {
            boolean isSavedDirExists;
            isSavedDirExists = DirectoryUtils.isSavedDirListExists(this);
            for (String colType : CollectionPusher.COLLECTION_TYPES) {
                isSavedDirExists &= DirectoryUtils
                        .isSavedCollectionCacheExists(this, colType);
            }
            if (!isSavedDirExists) {
                rsyncAndInitWatchDog();
                return;
            }
            SyncportAccount account = SyncportAccount.getAuthenticatedUser
                    (this);
            if (account == null) {
                throw new AuthenticatorException("need account");
            }

            Response.Listener<String> listener =
                    new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String prevSyncRoot = response;
                    String currSyncRoot = DirectoryUtils.getSyncRoot()
                            .getAbsolutePath();
                    if (!currSyncRoot.endsWith("/")) {
                        currSyncRoot += "/";
                    }
                    if (!TextUtils.isEmpty(prevSyncRoot) && prevSyncRoot.equals
                            (currSyncRoot)) {
                        initWatchDog();
                    } else {
                        try {
                            rsyncAndInitWatchDog();
                        } catch (AuthenticatorException e) {
                            return;
                        }
                    }
                }
            };

            RobustResponseErrorListener errorListener = new
                    RobustResponseErrorListener(this) {
                        @Override
                        public void onErrorResponse(VolleyError error, String msg) {
                            Log.e(TAG, msg);
                            NetworkResponse response = error.networkResponse;
                            if (response != null && response.statusCode ==
                                    400) {
                                try {
                                    rsyncAndInitWatchDog();
                                } catch (AuthenticatorException e) {
                                    // not reach
                                }
                            }
                        }

                        @Override
                        public void onSkipped(VolleyError error) {
                            // not reach
                        }

                        @Override
                        protected boolean onNoConnectionError(VolleyError error) {
                            mNeedInitOnConnect.set(true);
                            return true;
                        }
                    };

            DeltaResources.getSyncRoot(this, account.getDeviceId(),
                    listener, errorListener);
        } catch (AuthenticatorException e) {
            return;
        }
    }

    private void initWatchDog() {
        Log.d(TAG, "init watchdog");
        mWatchDog = new DirectoryWatchDog(this, this);
        mWatchDog.start();
    }

    private void stopWatchDog() {
        if (mWatchDog != null && mWatchDog.isRunning()) {
            Log.d(TAG, "stop watchdog");
            mWatchDog.stop();
        }
    }

    private void rsyncAndInitWatchDog() throws AuthenticatorException{
        final SyncportAccount account = SyncportAccount.getAuthenticatedUser
                (this);
        if (account == null) {
            return;
        }
        Log.d(TAG, "remove dir cache");
        DirectoryUtils.removeSavedDirList(this);
        for (String colType : CollectionPusher.COLLECTION_TYPES) {
            DirectoryUtils.removeCollectionCache(this, colType);
        }

        final RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(this) {
                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        Log.e(TAG, msg);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {
                        // not reach
                    }

                    @Override
                    protected boolean onNoConnectionError(VolleyError error) {
                        mNeedInitOnConnect.set(true);
                        return true;
                    }
                };

        final Response.Listener<JSONObject> setSyncRootListener = new Response
                .Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    DirectoryUtils.saveDirList(
                            DirectoryPusherService.this,
                            new ArrayList<String>());
                    initWatchDog();
                } catch (AuthenticatorException e) {
                    // do nothing and service will exit
                } catch (IOException e) {
                    // not reach
                }
            }
        };

        Response.Listener<JSONObject> rsyncListener = new Response
                .Listener<JSONObject>()  {
            @Override
            public void onResponse(JSONObject response) {
                File syncRoot = DirectoryUtils.getSyncRoot();
                String path = syncRoot.getAbsolutePath();
                if (!path.endsWith("/")) {
                    path += "/";
                }
                DeltaResources.setSyncRoot(DirectoryPusherService.this,
                        account.getDeviceId(),
                        path, setSyncRootListener, errorListener);
            }
        };

        DeltaResources.rsync(this, account.getDeviceId(), rsyncListener,
                errorListener);
    }


    @Override
    public boolean onUnbind(Intent intent) {
        stopWatchDog();
        mHandler.removeCallbacks(mRestartWatchDogRunnable);
        return false;
    }

    @Override
    public void onFailure(WatchDogException e) {
        e.printStackTrace();
        if (e.getCause() instanceof IllegalAccessException) {
            Log.e(TAG, "Failed to find dir_cache file.");
            // when case mycommit file not exist
            if (mRetryCount > 0) {
                mRetryCount -= 1;
                mHandler.postDelayed(mRestartWatchDogRunnable, 5000);
            }
            return;
        }
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        Intent intent = new Intent(RootService.ACTION_STOP_SELF);
        manager.sendBroadcast(intent);
    }

    @Override
    protected void onPrepareDestroy(ReadyToDestroyListener listener) {
        if (listener != null) {
            listener.onReadyToDestroy();
        }
    }

    @Override
    protected Service getService() {
        return this;
    }

    @Override
    public void onInternetConnected() {
        if (mNeedInitOnConnect.get()) {
            mNeedInitOnConnect.set(false);
            secureInitWatchDog();
        }
    }

    @Override
    public void onInternetDisconnected() {

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {

    }
}
