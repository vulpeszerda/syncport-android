package io.syncport.core.directory;

import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.database.model.ThumbnailDbModel;
import io.syncport.core.database.queryset.ThumbnailQuery;
import io.syncport.core.remote.AWSResources;
import io.syncport.core.remote.DeltaResources;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Process;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

public class ThumbnailPusher extends Thread implements
        InternetConnectivityManager.Observer,
        AWSResources.UploadFinishedListener {

    private static final int THUMBNAIL_MAX_WIDTH = 160;
    private static final int THUMBNAIL_MAX_HEIGHT = 160;
    private static final double MIN_ASPECT_RATIO = 0.25;
    private static final double MAX_ASPECT_RATIO = 4;
    private static final int ON_THE_FLY_MAX = 5;
    private static final int UPLOAD_SHIP_MAX_SLOT = 200;
    private static final int UPLOAD_SHIP_MAX_WAIT = 5000;
    private static final int UPLOAD_TICK_MAX_INTERVAL = 3000;
    private static final int REMOVE_MIN_INTERVAL = 3000;
    private static final String TAG = "ThumbnailPusher";

    private Context mContext;
    private SyncportAccount mAccount;
    private Queue mQueue;
    private DeleteQueue mDeleteQueue;
    private Object mQueueLock;
    private Object mInternetLock;
    private Object mUploadShipLock;
    private int mOnTheFlyCount;
    private InternetConnectivityManager mConnManager;
    private List<File> mUploadShip;

    private Timer mDeleteWaitTimer;
    private Timer mUploadShipWaitTimer;
    private Timer mUploadTickWaitTimer;

    public ThumbnailPusher(Context context) {
        mContext = context;
        mAccount = SyncportAccount.getAuthenticatedUser(context);
        mQueueLock = new Object();
        mInternetLock = new Object();
        mUploadShipLock = new Object();
        mQueue = new Queue();
        mDeleteQueue = new DeleteQueue();
        mOnTheFlyCount = 0;
        mUploadShip = new ArrayList<File>();
        mConnManager = InternetConnectivityManager.getInstance(context);
        mConnManager.addObserver(this);

        List<ThumbnailDbModel> models = ThumbnailQuery.pullAllPaths(context);
        if (models.size() > 0) {
            for (ThumbnailDbModel model : models) {
                if (model.isAdd()) {
                    mQueue.add(model.getPath());
                } else {
                    mDeleteQueue.add(model.getPath());
                }
            }
        }
    }

    public synchronized void decreaseOnTheFlyCount() {
        if (mOnTheFlyCount > 0) {
            mOnTheFlyCount -= 1;
        }
        notify();
    }

    public synchronized void increaseOnTheFlyCount() {
        mOnTheFlyCount += 1;
    }

    public synchronized int getOnTheFlyCount() {
        return mOnTheFlyCount;
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        Queue.UploadablePath uploadable;
        while (!isInterrupted()) {
            try {
                synchronized (mQueueLock) {
                    while ((uploadable = mQueue.peek()) == null
                            && mDeleteQueue.size() == 0) {
                        Log.d(TAG, "wait pusher queue push..");
                        mQueueLock.wait();
                    }
                }


                while (getOnTheFlyCount() >= ON_THE_FLY_MAX) {
                    synchronized (this) {
                        wait();
                    }
                }

                if (mConnManager.isConnected()) {
                    if (uploadable != null) {
                        uploadPathFile(uploadable);
                    } else {
                        deletePathFiles();
                    }
                } else {
                    Log.d(TAG, "wait push queue until internet reconnect");
                    synchronized (mInternetLock) {
                        while (!mConnManager.isConnected()) {
                            mInternetLock.wait();
                        }
                        Log.d(TAG, "internet reconnected. go push quque");
                    }
                }
            } catch (InterruptedException e) {
                // do nothing
            }
        }
        AWSResources.releaseTransferManager();
        mConnManager.removeObserver(this);
    }

    @Override
    public void onInternetConnected() {
        synchronized (mInternetLock) {
            mInternetLock.notifyAll();
        }
    }

    @Override
    public void onInternetDisconnected() {

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {

    }

    private class PathNotUsableException extends Exception {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public PathNotUsableException(String msg) {
            super(msg);
        };
    }

    public void deletePathFiles() {
        if (mDeleteWaitTimer == null) {
            mDeleteWaitTimer = new Timer();
            mDeleteWaitTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    mDeleteWaitTimer = null;

                    List<String> paths = mDeleteQueue.popAll();
                    List<String> hashFilenames = new ArrayList<String>();
                    for (String path : paths) {
                        hashFilenames.add(getHashFileName(path));
                    }
                    if (paths.size() > 0) {
                        boolean success = AWSResources
                                .deleteFilesSync(hashFilenames);
                        if (success) {
                            ThumbnailQuery.removePaths(mContext, paths);
                        }
                    }
                }

            }, REMOVE_MIN_INTERVAL);
        }
    }

    public void uploadPathFile(final Queue.UploadablePath uploadablePath) {
        if (uploadablePath.isOnTheFly()) {
            return;
        }
        uploadablePath.setIsOnTheFly(true);
        File file = new File(uploadablePath.getPath());
        try {
            if (!file.exists() || !file.isFile()) {
                throw new PathNotUsableException("path not exist");
            }

            File accountDir = new File(mContext.getExternalCacheDir(),
                    mAccount.getName());
            final File cachedImageDir = new File(accountDir, "thumbnails");
            File deviceImageDir = new File(cachedImageDir, mAccount.getDeviceId());
            File cachedImageFile = new File(deviceImageDir,
                    getHashFileName(uploadablePath.getPath()));

            deviceImageDir.mkdirs();
            if (cachedImageFile.exists()) {
                cachedImageFile.delete();
                // if (uploadablePath.getCacheFileName() == null) {
                // uploadablePath
                // .setCacheFileName(getHashFileName(uploadablePath
                // .getPath()));
                // }
            }
            deviceImageDir.createNewFile();

            // get bitmap form file
            Bitmap bitmap = FileUtils.getThumbnail(mContext,
                    file.getAbsolutePath());
            Bitmap croppedBitmap;
            Bitmap scaledBitmap;
            if (bitmap == null
                    && FileUtils.isImagePath(uploadablePath.getPath())) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(uploadablePath.getPath(), options);
                int imageWidth = options.outWidth;
                int imageHeight = options.outHeight;
                int scaleFactor = Math.min(imageWidth / THUMBNAIL_MAX_WIDTH,
                        imageHeight / THUMBNAIL_MAX_HEIGHT);
                options.inJustDecodeBounds = false;
                options.inSampleSize = scaleFactor;
                bitmap = BitmapFactory.decodeFile(uploadablePath.getPath(),
                        options);
            }
            if (bitmap == null) {
                cachedImageFile.delete();
                throw new PathNotUsableException("failed to get bitmap image");
            }
            int oriWidth = bitmap.getWidth();
            int oriHeight = bitmap.getHeight();
            if (oriWidth == 0 || oriHeight == 0) {
                cachedImageFile.delete();
                throw new PathNotUsableException("bitmap height or width is 0");
            }
            double aspectRatio = ((double) oriWidth) / oriHeight;
            int scaledWidth;
            int scaledHeight;

            if (aspectRatio > 1) {
                scaledHeight = THUMBNAIL_MAX_HEIGHT;
                scaledWidth = (int) (scaledHeight * aspectRatio);
            } else {
                scaledWidth = THUMBNAIL_MAX_WIDTH;
                scaledHeight = (int) (scaledWidth / aspectRatio);
            }
            scaledBitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth,
                    scaledHeight, true);

            if (aspectRatio > 1) {
                croppedBitmap = Bitmap.createBitmap(scaledBitmap,
                        (scaledWidth - scaledHeight) / 2, 0, scaledHeight,
                        scaledHeight);
            } else {
                croppedBitmap = Bitmap.createBitmap(scaledBitmap, 0,
                        (scaledHeight - scaledWidth) / 2, scaledWidth,
                        scaledWidth);
            }

            FileOutputStream fos = new FileOutputStream(cachedImageFile);
            croppedBitmap.compress(CompressFormat.JPEG, 70, fos);
            fos.flush();
            fos.close();
            bitmap.recycle();
            scaledBitmap.recycle();
            croppedBitmap.recycle();
            uploadablePath.setCacheFileName(getHashFileName(uploadablePath
                    .getPath()));

            synchronized (mUploadShipLock) {
                mUploadShip.add(cachedImageFile);

                if (mUploadShip.size() >= UPLOAD_SHIP_MAX_SLOT) {
                    if (mUploadShipWaitTimer != null) {
                        mUploadShipWaitTimer.cancel();
                        mUploadShipWaitTimer = null;
                    }
                    increaseOnTheFlyCount();
                    AWSResources.uploadFilesAsync(cachedImageDir, mUploadShip,
                            this);
                    mUploadShip.clear();
                    return;
                } else if (mUploadShipWaitTimer == null) {
                    mUploadShipWaitTimer = new Timer();
                    mUploadShipWaitTimer.schedule(new TimerTask() {

                        @Override
                        public void run() {
                            synchronized (mUploadShipLock) {
                                mUploadShipWaitTimer = null;
                                increaseOnTheFlyCount();
                                AWSResources.uploadFilesAsync(cachedImageDir,
                                        mUploadShip, ThumbnailPusher.this);
                                mUploadShip.clear();
                            }
                        }

                    }, UPLOAD_SHIP_MAX_WAIT);
                }
            }
        } catch (PathNotUsableException e) {
            Log.d(TAG, "upload failed: " + uploadablePath.getPath() + " reason: " + e.getMessage());
            String path = uploadablePath.getPath();
            removePathFromQueue(path);
            decreaseRetryCountOrRemoveFromDb(path);
            return;
        } catch (Exception e) {
            Log.d(TAG, "upload failed: " + uploadablePath.getPath()
                    + " reason: " + e.getMessage());
            uploadablePath.setIsOnTheFly(false);
            if (!uploadablePath.increaseFailureCount()) {
                removePathExceptS3(uploadablePath.getPath());
            }
            return;
        }
    }

    @Override
    public void onSuccess(List<File> files) {
        List<String> hashs = new ArrayList<>();

        decreaseOnTheFlyCount();
        Queue.UploadablePath path;
        String hash;
        for (File file : files) {
            hash = file.getName();
            hashs.add(hash.substring(0, 32));
            path = mQueue.getWithCacheFileName(hash);
            if (path == null || !path.isOnTheFly()) {
                continue;
            }
            path.setIsOnTheFly(false);
            removePathExceptS3(path.getPath());
        }

        // if (mUploadTickWaitTimer == null) {
        // mUploadTickWaitTimer = new Timer();
        // mUploadTickWaitTimer.schedule(new TimerTask(){
        //
        // @Override
        // public void run() {
        //
        // }
        //
        // }, UPLOAD_TICK_MAX_INTERVAL);
        // }
        pushThumbnailUploadTick(hashs);
    }

    private void pushThumbnailUploadTick(List<String> hashs) {
        JSONObject params = new JSONObject();
        try {
            params.put("hashes", new JSONArray(hashs));
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            DeltaResources.push(mContext, "thumbnail", params, future, future);
            JSONObject response = future.get();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            VolleyError error = (VolleyError) e.getCause();
            NetworkResponse response = error.networkResponse;
            if (response != null && response.data != null
                    && response.data.length > 0) {
                Log.e(TAG, new String(response.data));
            }
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(List<File> files) {
        decreaseOnTheFlyCount();
        Queue.UploadablePath path;
        String hash;
        for (File file : files) {
            hash = file.getName();
            path = mQueue.getWithCacheFileName(hash);
            if (path == null || !path.isOnTheFly()) {
                continue;
            }
            path.setIsOnTheFly(false);

            if (!path.increaseFailureCount()) {
                removePathExceptS3(path.getPath());
            }
            Log.e(TAG, "upload failed: " + path.getPath()
                    + " try again later..");
        }

    }

    public void removePathFromQueue(String path) {
        mQueue.remove(path);
    }

    public void removePathFromQueueAndDb(String path) {
        ThumbnailQuery.removePath(mContext, path);
        removePathFromQueue(path);
    }

    public void removePathExceptS3(String path) {
        removePathFromQueueAndDb(path);
        String filename = getHashFileName(path);
        File accountDir = new File(mContext.getExternalCacheDir(),
                mAccount.getName());
        File cachedImageFile = new File(accountDir,
                "thumbnails/" + mAccount.getDeviceId() + "/" + filename);
        if (cachedImageFile.exists()) {
            cachedImageFile.delete();
        }
    }

    public void decreaseRetryCountOrRemoveFromDb(String path) {
        ThumbnailQuery.decreasePathRetryCount(mContext, path);
        // ThumbnailQuery.decreaseRetryCountOrRemove(path);
    }

    public String getHashFileName(String path) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((mAccount.getDeviceId() + ":" + path).getBytes());
            byte[] byteData = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            return sb.toString() + ".jpg";
        } catch (NoSuchAlgorithmException e) {
            // not reach
        }
        return null;
    }

    public void addPath(String path) {
        mQueue.add(path);
    }

    public void addPaths(List<String> paths) {
        ThumbnailQuery.pushPaths(mContext, paths, true);
        for (String path : paths) {
            mQueue.add(path);
        }
    }

    public void removePathsFromAll(List<String> paths) {
        ThumbnailQuery.pushPaths(mContext, paths, false);
        for (String path : paths) {
            removePathFromQueue(path);
        }
        boolean needNotify = mQueue.peek() == null || mDeleteQueue.size() == 0;
        mDeleteQueue.addAll(paths);
        if (needNotify) {
            notifyQueueAdded();
        }
    }

    public void notifyQueueAdded() {
        synchronized (mQueueLock) {
            mQueueLock.notifyAll();
        }
    }

    private class Queue {
        private Map<String, UploadablePath> mPaths;
        private Object mLock;

        public class UploadablePath {
            private static final int MAX_FAILURE_COUNT = 3;
            private String mPath;
            private boolean mIsOnTheFly;
            private int mFailureCount;
            private String mCacheFileName;

            public UploadablePath(String path) {
                mPath = path;
                mFailureCount = 0;
                mIsOnTheFly = false;
            }

            public String getPath() {
                return mPath;
            }

            public void setIsOnTheFly(boolean isOnTheFly) {
                synchronized (mLock) {
                    mIsOnTheFly = isOnTheFly;
                }
            }

            public boolean isOnTheFly() {
                synchronized (mLock) {
                    return mIsOnTheFly;
                }
            }

            public boolean increaseFailureCount() {
                synchronized (mLock) {
                    ++mFailureCount;
                    return mFailureCount < MAX_FAILURE_COUNT;
                }
            }

            public void setCacheFileName(String name) {
                mCacheFileName = name;
            }

            public String getCacheFileName() {
                return mCacheFileName;
            }
        }

        public Queue() {
            mPaths = new HashMap<String, UploadablePath>();
            mLock = new Object();
        }

        public UploadablePath peek() {
            synchronized (mLock) {
                if (mPaths.size() == 0) {
                    return null;
                }
                for (Entry<String, UploadablePath> entry : mPaths.entrySet()) {
                    if (!entry.getValue().isOnTheFly()) {
                        return entry.getValue();
                    }
                }
            }
            return null;
        }

        public void add(String path) {
            UploadablePath uploadablePath = new UploadablePath(path);
            boolean needNotify;
            synchronized (mLock) {
                needNotify = peek() == null;
                if (mPaths.get(path) != null) {
                    UploadablePath existingPath = mPaths.get(path);
                    uploadablePath.setIsOnTheFly(existingPath.isOnTheFly());
                }
                mPaths.put(uploadablePath.getPath(), uploadablePath);
            }
            if (needNotify) {
                notifyQueueAdded();
            }
        }

        public void remove(String path) {
            synchronized (mLock) {
                mPaths.remove(path);
            }
        }

        public UploadablePath getWithCacheFileName(String name) {
            for (UploadablePath path : mPaths.values()) {
                String cacheName = path.getCacheFileName();
                if (cacheName != null && cacheName.equals(name)) {
                    return path;
                }
            }
            return null;
        }
    }

    private class DeleteQueue {
        private Set<String> mPaths;
        private Object mLock;

        public DeleteQueue() {
            mPaths = new HashSet<String>();
            mLock = new Object();
        }

        public int size() {
            synchronized (mLock) {
                return mPaths.size();
            }
        }

        public void add(String path) {
            boolean needNotify;
            synchronized (mLock) {
                needNotify = size() == 0;
                mPaths.add(path);
            }
            if (needNotify) {
                notifyQueueAdded();
            }
        }

        public void addAll(List<String> paths) {
            boolean needNotify;
            synchronized (mLock) {
                needNotify = size() == 0;
                mPaths.addAll(paths);
            }
            if (needNotify) {
                notifyQueueAdded();
            }
        }

        public List<String> popAll() {
            synchronized (mLock) {
                List<String> array = new ArrayList<String>(mPaths);
                mPaths.clear();
                return array;
            }
        }

        public void remove(String path) {
            synchronized (mLock) {
                mPaths.remove(path);
            }
        }
    }
}
