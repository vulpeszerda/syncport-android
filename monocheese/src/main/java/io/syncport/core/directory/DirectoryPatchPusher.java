package io.syncport.core.directory;

import android.content.Context;
import android.os.Process;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.remote.DeltaResources;
import io.syncport.utils.Log;

public class DirectoryPatchPusher extends Thread implements
        InternetConnectivityManager.Observer {
    public static interface Observer {
        void onDirectoryPatchPushed(DirectoryPatch patch);
        void onPusherException(Exception e);
    }

    private static final String TAG = "DirectoryPatchPusher";
    private static final String PUSH_TYPE = "directory";
    private static final int PUSH_RETRY_COUNT = 3;
    private static final int PUSH_RETRY_DELAY = 3000;
    private static final int POP_DELAY = 3000;

    private Context mContext;
    private Queue mQueue;
    private Object mQueueLock;
    private Object mInternetLock;
    private ArrayList<Observer> mObservers;
    private boolean mQuit;
    private InternetConnectivityManager mConnManager;

    public DirectoryPatchPusher(Context context) {
        super(TAG);
        mContext = context;
        mQueueLock = new Object();
        mInternetLock = new Object();
        mObservers = new ArrayList<>();
        mQueue = new Queue();
        mQuit = false;
        mConnManager = InternetConnectivityManager.getInstance(context);
        mConnManager.addObserver(this);
    }

    public synchronized void addObserver(Observer ob) {
        mObservers.add(ob);
    }

    public synchronized void removeObserver(Observer ob) {
        mObservers.remove(ob);
    }

    public synchronized void notifyPatchPushed(DirectoryPatch patch) {
        for (Observer ob : mObservers) {
            ob.onDirectoryPatchPushed(patch);
        }
    }

    public synchronized void notifyPusherException(Exception e) {
        for (Observer ob : mObservers) {
            ob.onPusherException(e);
        }
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

        boolean pushImmediatly = true;
        int remainRetryCount = PUSH_RETRY_COUNT;

        while (!mQuit) {
            try {
                synchronized (mQueueLock) {
                    while (!mQueue.isReady() || mQueue.peek() == null) {
                        if (!mQueue.isReady()) {
                            Log.d(TAG, "wait pusher queue ready..");
                            pushImmediatly = true;
                        } else {
                            Log.d(TAG, "wait pusher queue push..");
                            pushImmediatly = false;
                        }
                        mQueueLock.wait();
                    }
                    if (!pushImmediatly) {
                        sleep(POP_DELAY);
                    }
                }
                if (mConnManager.isConnected()) {
                    Log.d(TAG, "server upload started");
                    DirectoryPatch patch = mQueue.compressAndPop();
                    if (patch != null) {
                        try {
                            boolean success = request(patch);
                            if (success) {
                                remainRetryCount = PUSH_RETRY_COUNT;
                                notifyPatchPushed(patch);
                            } else {
                                notifyPusherException(
                                        new Exception("push result is not success"));
                                quit();
                            }
                        } catch (ExecutionException e) {
                            VolleyError error = (VolleyError) e.getCause();
                            if ((error instanceof TimeoutError ||
                                error instanceof NoConnectionError ||
                                error instanceof NetworkError) &&
                                remainRetryCount > 0){

                                remainRetryCount -= 1;
                                mQueue.insertAt(0, patch);
                                sleep(PUSH_RETRY_DELAY);
                            } else {
                                notifyPusherException(e);
                                quit();
                                return;
                            }
                        }
                    }
                }
            } catch (InterruptedException e) {
                // do nothing
            }
            if (!mConnManager.isConnected()) {
                Log.d(TAG, "wait push queue until internet reconnect");
                synchronized (mInternetLock) {
                    try {
                        while (!mConnManager.isConnected()) {
                            mInternetLock.wait();
                        }
                        Log.d(TAG, "internet reconnected. go push quque");
                        pushImmediatly = true;
                    } catch (InterruptedException e) {
                        // do nothing
                    }
                }
            }
        }
        mConnManager.removeObserver(this);
    }

    public void quit() {
        mQuit = true;
        interrupt();
    }

    public Queue getQueue() {
        return mQueue;
    }

    private void onQueueReady() {
        synchronized (mQueueLock) {
            Log.d(TAG, "notify pusher queue ready");
            mQueueLock.notifyAll();
        }
    }

    private void onNotifyPatchArrived() {
        synchronized (mQueueLock) {
            Log.d(TAG, "notify pusher queue push");
            mQueueLock.notifyAll();
        }
    }

    private boolean request(DirectoryPatch patch) throws
            InterruptedException, ExecutionException{
        try {
            JSONObject params = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray additionArray = new JSONArray();
            for (DirectoryPatch.FileDict dict : patch.getAddition()) {
                additionArray.put(dict.toJSON());
            }
            data.put("addition", additionArray);
            JSONArray deletionArray = new JSONArray();
            for (DirectoryPatch.FileDict dict : patch.getDeletion()) {
                deletionArray.put(dict.toJSON());
            }
            data.put("deletion", deletionArray);
            params.put("data", data);

            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            DeltaResources.push(mContext, PUSH_TYPE, params, future, future);
            JSONObject response = future.get();
            return response.optBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public class Queue {

        private Object mLock;
        private ArrayList<DirectoryPatch> mPatches;
        private boolean mIsReadyToPush;

        public Queue() {
            mPatches = new ArrayList<>();
            mIsReadyToPush = false;
            mLock = new Object();
        }

        public boolean isReady() {
            return mIsReadyToPush;
        }

        public void setReady() {
            synchronized (mLock) {
                if (mIsReadyToPush == false) {
                    mIsReadyToPush = true;
                    onQueueReady();
                }
            }
        }

        public void insertAt(int position, DirectoryPatch patch) {
            synchronized (mLock) {
                mPatches.add(position, patch);
                if (mPatches.size() == 1 && mIsReadyToPush) {
                    onNotifyPatchArrived();
                }
            }
        }

        public DirectoryPatch compressAndPop() {
            List<DirectoryPatch> mergeTargets;
            synchronized (mLock) {
                mergeTargets = new ArrayList<>(mPatches);
                mPatches.clear();
            }
            DirectoryPatch merged = null;
            for (DirectoryPatch patch : mergeTargets) {
                if (merged == null) {
                    merged = patch;
                    continue;
                }
                try {
                    merged = DirectoryPatch.merge(merged, patch);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (merged.getAddition().size() > 0
                    || merged.getDeletion().size() > 0) {
                return merged;
            }
            return null;
        }

        public DirectoryPatch peek() {
            synchronized (mLock) {
                if (mPatches.size() > 0) {
                    return mPatches.get(0);
                }
            }
            return null;
        }

        public DirectoryPatch pop() {
            synchronized (mLock) {
                if (mPatches.size() > 0) {
                    return mPatches.remove(0);
                }
            }
            return null;
        }

        public void add(DirectoryPatch patch) {
            synchronized (mLock) {
                mPatches.add(patch);
                if (mPatches.size() == 1 && mIsReadyToPush) {
                    onNotifyPatchArrived();
                }
            }
        }
    }

    @Override
    public void onInternetConnected() {
        synchronized (mInternetLock) {
            mInternetLock.notifyAll();
        }
    }

    @Override
    public void onInternetDisconnected() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {
        // do nothing. timer will handle this case
    }
}
