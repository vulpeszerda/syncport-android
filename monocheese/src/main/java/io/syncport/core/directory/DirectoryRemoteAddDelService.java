package io.syncport.core.directory;

import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.DirectoryRemoteAddDelPollingInfo;
import io.syncport.utils.SyncportKeyBuilder;

import java.io.File;
import java.util.Stack;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;

public class DirectoryRemoteAddDelService extends IntentService {

    private static class Receiver extends WakefulBroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            DirectoryRemoteAddDelPollingInfo info = (DirectoryRemoteAddDelPollingInfo) intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            Intent service = createIntent(context, info);
            startWakefulService(context, service);
        }
    }

    public static BroadcastReceiver registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter(
                GeneralPollerService.PollerAction.DIRECTORY_REMOTE_ADDDEL);
        BroadcastReceiver receiver = new Receiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver,
                filter);
        return receiver;
    }

    public static Intent createIntent(Context context) {
        return createIntent(context, null);
    }

    public static Intent createIntent(Context context,
            DirectoryRemoteAddDelPollingInfo info) {
        Intent intent = new Intent(context, DirectoryRemoteAddDelService.class);
        intent.putExtra(PARAM_INFO, info);
        return intent;
    }

    private static final String PARAM_INFO = SyncportKeyBuilder.build("info");

    public DirectoryRemoteAddDelService() {
        super("DirectorySelfApplyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DirectoryRemoteAddDelPollingInfo info = (DirectoryRemoteAddDelPollingInfo) intent
                .getSerializableExtra(PARAM_INFO);
        if (info == null) {
            return;
        }
        for (String addition : info.getAddition()) {
            try {
                if (!addition.endsWith("/")) {
                    continue;
                }
                File willBeCreatedFile = new File(addition);
                if (!willBeCreatedFile.exists()) {
                    willBeCreatedFile.mkdirs();
                } else if (willBeCreatedFile.isDirectory()) {
                    // skip
                } else {
                    // skip
                }
            } catch (Exception e) {
                // skip exception for permission issues
            }

        }
        for (String deletion : info.getDeletion()) {
            try {
                File willBeDeletedFile = new File(deletion);
                if (!willBeDeletedFile.exists()) {
                    continue;
                } else {
                    Stack<File> stack = new Stack<File>();
                    stack.push(willBeDeletedFile);

                    while (!stack.isEmpty()) {
                        File file = stack.pop();
                        if (file.isDirectory()) {
                            File[] childFiles = file.listFiles();
                            if (childFiles.length > 0) {
                                stack.push(file);
                                for (File childFile : file.listFiles()) {
                                    stack.push(childFile);
                                }
                            } else {
                                file.delete();
                            }
                        } else {
                            file.delete();
                        }
                    }
                }
            } catch (Exception e) {
                // skip exception for permission issues
            }
        }
    }
}
