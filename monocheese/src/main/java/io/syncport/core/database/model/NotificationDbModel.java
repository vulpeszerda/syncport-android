package io.syncport.core.database.model;

import java.util.Date;

import com.daftshady.superandroidkit.database.BaseDbModel;
import com.daftshady.superandroidkit.database.DbColumn;
import com.daftshady.superandroidkit.database.DbEnum;

public class NotificationDbModel extends BaseDbModel {

    public static final String TABLE_NAME = "syncport_notification";

    public static final DbColumn COLUMN_ID = new DbColumn("id",
            DbEnum.Type.INTEGER);

    public static final DbColumn COLUMN_USER_EMAIL = new DbColumn(
            "user_email", DbEnum.Type.STRING);

    public static final DbColumn COLUMN_PACKAGE_NAME = new DbColumn(
            "package_name", DbEnum.Type.STRING);

    public static final DbColumn COLUMN_ICON_URL = new DbColumn("icon_url",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_ARRIVED_AT = new DbColumn("arrived_at",
            DbEnum.Type.DATE);

    public static final DbColumn COLUMN_TITLE = new DbColumn("title",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_TEXT = new DbColumn("text",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_TEXT_LINES = new DbColumn("text_lines",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_SUB_TEXT = new DbColumn("sub_text",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_DEVICE_ID = new DbColumn("device_id",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_DEVICE_NAME = new DbColumn(
            "device_name", DbEnum.Type.STRING);

    public static final DbColumn COLUMN_IS_NEW = new DbColumn(
            "is_new", DbEnum.Type.BOOLEAN);

    @Override
    public DbColumn[] getColumns() {
        return new DbColumn[] { 
                COLUMN_ID, 
                COLUMN_USER_EMAIL,
                COLUMN_PACKAGE_NAME,
                COLUMN_ICON_URL, 
                COLUMN_ARRIVED_AT,
                COLUMN_TITLE, 
                COLUMN_TEXT, 
                COLUMN_TEXT_LINES,
                COLUMN_SUB_TEXT, 
                COLUMN_DEVICE_ID,
                COLUMN_DEVICE_NAME,
                COLUMN_IS_NEW
            };
    }

    private Integer id;
    private String packageName;
    private String iconUrl;
    private Date arrivedAt;
    private String title;
    private String text;
    private String textLines;
    private String subText;
    private String deviceId;
    private String deviceName;
    private Boolean isNew;

    public Integer getId() {
        return id;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getTextLines() {
        return textLines;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public Date getArrivedAt() {
        return arrivedAt;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getSubText() {
        return subText;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }
    
    public Boolean isNew() {
        return isNew;
    }
}
