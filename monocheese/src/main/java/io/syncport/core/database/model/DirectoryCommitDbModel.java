package io.syncport.core.database.model;

import com.daftshady.superandroidkit.database.BaseDbModel;
import com.daftshady.superandroidkit.database.DbColumn;
import com.daftshady.superandroidkit.database.DbEnum;

public class DirectoryCommitDbModel extends BaseDbModel {

    public static final String TABLE_NAME = "syncport_directory_commit";

    public static final DbColumn COLUMN_ID = new DbColumn("id",
            DbEnum.Type.INTEGER);

    public static final DbColumn COLUMN_USER_EMAIL = new DbColumn("user_email",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_DEVICE_ID = new DbColumn("device_id",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_COMMIT = new DbColumn("commit_text",
            DbEnum.Type.STRING);

    @Override
    public DbColumn[] getColumns() {
        return new DbColumn[] {
                COLUMN_ID, 
                COLUMN_USER_EMAIL,
                COLUMN_DEVICE_ID,
                COLUMN_COMMIT
        };
    }
    
    private Integer id;
    private String deviceId;
    private String commitText;
    
    public Integer getId() {
        return id;
    }
    
    public String getDeviceId() {
        return deviceId;
    }
    
    public String getCommit() {
        return commitText;
    }

}
