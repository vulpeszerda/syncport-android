package io.syncport.core.database.migrations;

import io.syncport.core.database.model.ThumbnailDbModel;

import com.b50.migrations.AbstractMigration;

public class DBVersion2 extends AbstractMigration {

    public void up() {
        String query;
        query = "ALTER TABLE " + ThumbnailDbModel.TABLE_NAME + " ADD COLUMN "
                + ThumbnailDbModel.COLUMN_RETRY_COUNT.getName()
                + " integer not null default "
                + ThumbnailDbModel.DEFAULT_RETRY_COUNT + ";";
        execSQL(query);
    }

    public void down() {
    }

}
