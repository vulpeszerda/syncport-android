package io.syncport.core.database.model;

import com.daftshady.superandroidkit.database.BaseDbModel;
import com.daftshady.superandroidkit.database.DbColumn;
import com.daftshady.superandroidkit.database.DbEnum;

public class ThumbnailDbModel extends BaseDbModel {
    
    public static final int DEFAULT_RETRY_COUNT = 3;

    public static final String TABLE_NAME = "syncport_thumbnail";

    public static final DbColumn COLUMN_ID = new DbColumn("id",
            DbEnum.Type.INTEGER);

    public static final DbColumn COLUMN_USER_EMAIL = new DbColumn("user_email",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_PATH = new DbColumn("path",
            DbEnum.Type.STRING);

    public static final DbColumn COLUMN_IS_ADD = new DbColumn("is_add",
            DbEnum.Type.BOOLEAN);

    public static final DbColumn COLUMN_RETRY_COUNT = new DbColumn(
            "retry_count", DbEnum.Type.INTEGER);

    @Override
    public DbColumn[] getColumns() {
        return new DbColumn[] { COLUMN_ID, COLUMN_USER_EMAIL, COLUMN_PATH,
                COLUMN_IS_ADD, COLUMN_RETRY_COUNT };
    }

    private Integer id;
    private String path;
    private String userEmail;
    private Boolean isAdd;
    private Integer retryCount;

    public Integer getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public Boolean isAdd() {
        return isAdd;
    }
    
    public Integer getRetryCount() {
        return retryCount;
    }
}
