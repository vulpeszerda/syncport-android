package io.syncport.core.database.migrations;

import io.syncport.core.database.model.DirectoryCommitDbModel;
import io.syncport.core.database.model.NotificationDbModel;
import io.syncport.core.database.model.ThumbnailDbModel;

import java.util.ArrayList;
import java.util.List;

import com.b50.migrations.AbstractMigration;

public class DBVersion1 extends AbstractMigration {

    public void up() {
        List<String> queries = new ArrayList<String>();
        
        String notiDbQuery = 
                "create table " + NotificationDbModel.TABLE_NAME + "(" +
                NotificationDbModel.COLUMN_ID.getName() + " integer primary key autoincrement , " +
                NotificationDbModel.COLUMN_USER_EMAIL.getName() + " text not null, " +
                NotificationDbModel.COLUMN_PACKAGE_NAME.getName() + " text not null, " +
                NotificationDbModel.COLUMN_DEVICE_ID.getName() + " text not null, " + 
                NotificationDbModel.COLUMN_DEVICE_NAME.getName() + " text not null, " +
                NotificationDbModel.COLUMN_ARRIVED_AT.getName() + " text not null, " +
                NotificationDbModel.COLUMN_IS_NEW.getName() + " integer not null default 1, " +
                NotificationDbModel.COLUMN_ICON_URL.getName() + " text, " +
                NotificationDbModel.COLUMN_TITLE.getName() + " text not null, " +
                NotificationDbModel.COLUMN_TEXT.getName() + " text," + 
                NotificationDbModel.COLUMN_SUB_TEXT.getName() + " text, " +
                NotificationDbModel.COLUMN_TEXT_LINES.getName() + " text);";
        
        queries.add(notiDbQuery);
        
        
        String dirCommitDbQuery = 
                "create table " + DirectoryCommitDbModel.TABLE_NAME + "(" +
                DirectoryCommitDbModel.COLUMN_ID.getName() + " integer primary key autoincrement , " +
                DirectoryCommitDbModel.COLUMN_USER_EMAIL.getName() + " text not null, " +
                DirectoryCommitDbModel.COLUMN_DEVICE_ID.getName() + " text not null, " + 
                DirectoryCommitDbModel.COLUMN_COMMIT.getName() + " text not null, " +
                "unique(" + 
                    DirectoryCommitDbModel.COLUMN_USER_EMAIL.getName() + ", " + 
                    DirectoryCommitDbModel.COLUMN_DEVICE_ID.getName() + 
                ") on conflict replace);";

        queries.add(dirCommitDbQuery);
    
        String thumbnailDbQuery = 
                "create table " + ThumbnailDbModel.TABLE_NAME + "(" + 
                ThumbnailDbModel.COLUMN_ID.getName() + " integer primary key autoincrement , " +
                ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + " text not null, " +
                ThumbnailDbModel.COLUMN_PATH.getName() + " text not null, " +
                ThumbnailDbModel.COLUMN_IS_ADD.getName() + " integer not null default 1, " +
                "unique(" + 
                    ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + ", " + 
                    ThumbnailDbModel.COLUMN_PATH.getName() +
                ") on conflict replace);";

        queries.add(thumbnailDbQuery);
        
        
        for (String query : queries) {
            execSQL(query);
        }
    }

    public void down() {
        execSQL("drop table " + NotificationDbModel.TABLE_NAME + ";");
        execSQL("drop table " + DirectoryCommitDbModel.TABLE_NAME + ";");
        execSQL("drop table " + ThumbnailDbModel.TABLE_NAME + ";");
    }

}

