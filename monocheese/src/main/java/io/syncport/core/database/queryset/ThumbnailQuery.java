package io.syncport.core.database.queryset;

import io.syncport.core.account.SyncportAccount;
import io.syncport.core.database.migrations.DatabaseHelper;
import io.syncport.core.database.model.ThumbnailDbModel;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.daftshady.superandroidkit.database.BaseCursorManager;
import com.daftshady.superandroidkit.database.DbManager;

public class ThumbnailQuery {
    public static List<ThumbnailDbModel> pullAllPaths(Context context) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return new ArrayList<ThumbnailDbModel>();
        }

        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_READABLE_DATABASE);

        String selection = null;
        String[] selectionArgs = null;

        List<String> selections = new ArrayList<String>();
        selections.add(ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + "='"
                + account.getName() + "'");
        selection = TextUtils.join(" and ", selections);
        selectionArgs = new String[] {};

        Cursor cursor = manager.select(ThumbnailDbModel.TABLE_NAME, null,
                selection, selectionArgs, null, null, null, null);
        BaseCursorManager cursorManager = new BaseCursorManager(cursor);
        List<ThumbnailDbModel> models = cursorManager
                .retreiveData(ThumbnailDbModel.class);
        cursor.close();
//      manager.close();
        return models;
    }

    public static void decreasePathRetryCount(Context context, String path) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }
        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_WRITABLE_DATABASE);

        String selection = null;
        String[] selectionArgs = null;

        selection = ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + "='"
                + account.getName() + "'";
        selection += " and " + ThumbnailDbModel.COLUMN_PATH.getName() + "=\""
                + path + "\" and " + ThumbnailDbModel.COLUMN_IS_ADD.getName()
                + "=1";
        selectionArgs = new String[] {};

        Cursor cursor = manager.select(ThumbnailDbModel.TABLE_NAME, null,
                selection, selectionArgs, null, null, null, null);
        BaseCursorManager cursorManager = new BaseCursorManager(cursor);
        List<ThumbnailDbModel> models = cursorManager
                .retreiveData(ThumbnailDbModel.class);
        cursor.close();

        if (models.size() == 0) {
//          manager.close();
            return;
        }
        ThumbnailDbModel model = models.get(0);
        if (model.getRetryCount() > 0) {
            ContentValues params = new ContentValues();
            params.put(ThumbnailDbModel.COLUMN_USER_EMAIL.getName(),
                    model.getUserEmail());
            params.put(ThumbnailDbModel.COLUMN_PATH.getName(), model.getPath());
            params.put(ThumbnailDbModel.COLUMN_IS_ADD.getName(), 1);
            params.put(ThumbnailDbModel.COLUMN_RETRY_COUNT.getName(),
                    model.getRetryCount() - 1);
            manager.insert(ThumbnailDbModel.TABLE_NAME, params);
        } else {
            manager.delete(ThumbnailDbModel.TABLE_NAME, "id=" + model.getId(),
                    new String[] {});
        }
//      manager.close();
    }

    public static void pushPath(Context context, String path, boolean isAdd) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }

        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_WRITABLE_DATABASE);
        ContentValues params = new ContentValues();
        params.put(ThumbnailDbModel.COLUMN_USER_EMAIL.getName(),
                account.getName());
        params.put(ThumbnailDbModel.COLUMN_PATH.getName(), path);
        params.put(ThumbnailDbModel.COLUMN_IS_ADD.getName(), isAdd ? 1 : 0);
        params.put(ThumbnailDbModel.COLUMN_RETRY_COUNT.getName(),
                ThumbnailDbModel.DEFAULT_RETRY_COUNT);

        manager.insert(ThumbnailDbModel.TABLE_NAME, params);
//      manager.close();
    }

    public static void pushPaths(Context context, List<String> paths,
            boolean isAdd) {

        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }

        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_WRITABLE_DATABASE);
        List<ContentValues> paramList = new ArrayList<ContentValues>();
        ContentValues params;
        for (String path : paths) {
            params = new ContentValues();
            params.put(ThumbnailDbModel.COLUMN_USER_EMAIL.getName(),
                    account.getName());
            params.put(ThumbnailDbModel.COLUMN_PATH.getName(), path);
            params.put(ThumbnailDbModel.COLUMN_IS_ADD.getName(), isAdd ? 1 : 0);
            params.put(ThumbnailDbModel.COLUMN_RETRY_COUNT.getName(),
                    ThumbnailDbModel.DEFAULT_RETRY_COUNT);
            paramList.add(params);
        }
        manager.insert(ThumbnailDbModel.TABLE_NAME, paramList);
//      manager.close();
    }

    public static void removePath(Context context, String path) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }

        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_WRITABLE_DATABASE);

        String selection = null;
        String[] selectionArgs = null;

        selection = ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + "='"
                + account.getName() + "'";
        selection += " and " + ThumbnailDbModel.COLUMN_PATH.getName() + "=\""
                + path + "\"";
        selectionArgs = new String[] {};

        manager.delete(ThumbnailDbModel.TABLE_NAME, selection, selectionArgs);
//      manager.close();
    }

    public static void removePaths(Context context, List<String> paths) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }

        DbManager manager = new DbManager(DatabaseHelper.getInstance(context));
        manager.open(DbManager.FLAG_OPEN_WRITABLE_DATABASE);

        String selection = null;
        String[] selectionArgs = new String[] {};
        String args = TextUtils.join("\",\"", paths);

        selection = ThumbnailDbModel.COLUMN_USER_EMAIL.getName() + "='"
                + account.getName() + "'";
        selection += " and " + ThumbnailDbModel.COLUMN_PATH.getName()
                + " in (\"" + args + "\")";

        manager.delete(ThumbnailDbModel.TABLE_NAME, selection, selectionArgs);
//      manager.close();
    }
}
