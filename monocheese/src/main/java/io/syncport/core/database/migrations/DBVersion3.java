package io.syncport.core.database.migrations;

import com.b50.migrations.AbstractMigration;

import io.syncport.core.database.model.NotificationDbModel;
import io.syncport.core.database.model.ThumbnailDbModel;

public class DBVersion3 extends AbstractMigration {

    public void up() {
        execSQL("drop table " + NotificationDbModel.TABLE_NAME + ";");
    }

    public void down() {
    }

}
