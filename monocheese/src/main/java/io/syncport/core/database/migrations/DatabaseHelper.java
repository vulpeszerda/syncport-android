package io.syncport.core.database.migrations;

import io.syncport.R;
import android.content.Context;
import com.b50.migrations.MigrationsDatabaseHelper;

public class DatabaseHelper extends MigrationsDatabaseHelper {
    
    private static DatabaseHelper sHelper;
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (sHelper == null) {
            sHelper = new DatabaseHelper(context);
        }
        return sHelper;
    }
    
    private DatabaseHelper(Context context) {
        super(context, context.getString(R.string.database_name), null, context.getResources().getInteger(
                R.integer.database_version), context.getString(R.string.package_name));
    }

}

