package io.syncport.core.model;

import io.syncport.core.database.model.NotificationDbModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.Date;

import android.util.Base64;

public class NotificationModel {

    private static String serializeStringArray(String[] array) {
        String serializedString = null;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            new ObjectOutputStream(out).writeObject(array);
            serializedString = Base64.encodeToString(out.toByteArray(),
                    Base64.DEFAULT);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return serializedString;
    }

    private static String[] deserializeStringArray(String serializedString) {
        String[] array = null;
        if (serializedString != null) {
            try {
                ByteArrayInputStream in = new ByteArrayInputStream(
                        Base64.decode(serializedString, Base64.DEFAULT));
                array = (String[]) new ObjectInputStream(in).readObject();
                in.close();
            } catch (OptionalDataException e) {
                e.printStackTrace();
            } catch (StreamCorruptedException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return array;
    }

    public static NotificationModel fromDbModel(NotificationDbModel model) {

        return new NotificationModel(
                model.getPackageName(), 
                model.getTitle(),
                model.getText(), 
                model.getSubText(),
                deserializeStringArray(model.getTextLines()),
                model.getIconUrl(), 
                model.getDeviceId(), 
                model.getDeviceName(),
                model.getArrivedAt(),
                model.isNew());
    }

    private String title;
    private String text;
    private String subText;
    private String[] textLines;
    private String packageName;
    private String iconUrl;
    private String deviceId;
    private String deviceName;
    private Date arrivedAt;
    private boolean isNew;

    private NotificationModel(String packageName, String title, String text,
            String subText, String[] textLines, String iconUrl,
            String deviceId, String deviceName, Date arrivedAt, boolean isNew) {
        this.packageName = packageName;
        this.title = title;
        this.text = text;
        this.subText = subText;
        this.textLines = textLines;
        this.iconUrl = iconUrl;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.arrivedAt = arrivedAt;
        this.isNew = isNew;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getSubText() {
        return subText;
    }

    public String[] getTextLines() {
        return textLines;
    }
    
    public String getSerializedTextLines() {
        return serializeStringArray(textLines);
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public Date getArrivedAt() {
        return arrivedAt;
    }
    
    public boolean isNew() {
        return isNew;
    }
}
