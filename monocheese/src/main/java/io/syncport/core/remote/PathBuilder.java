package io.syncport.core.remote;

import android.net.Uri;

public class PathBuilder {
    private String mHost;
    private String mPrefixPath;

    public PathBuilder(String host) {
        mHost = host;
    }

    public PathBuilder(String host, String prefixPath) {
        mHost = host;
        mPrefixPath = prefixPath;
    }

    public String build(String path) {
        Uri.Builder builder = Uri.parse(mHost).buildUpon();
        if (mPrefixPath != null) {
            for (String segment : mPrefixPath.split("/")) {
                builder.appendPath(segment);
            }
        }
        for (String segment : path.split("/")) {
            builder.appendPath(segment);
        }
        String url = builder.build().toString();
        if (!url.endsWith("/")) {
            url += "/";
        }
        return url;
    }
}
