package io.syncport.core.remote;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.daftshady.superandroidkit.authentication.AbstractAccount.OnGetAuthTokenListener;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import io.syncport.core.account.SyncportAccount;

public abstract class AuthGenernalRequestHelper<T> implements Response
        .ErrorListener {

    /** Charset for request. */
    private static final String PROTOCOL_CHARSET = "utf-8";

    /** Content type for request. */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private static final String HEADER_COOKIE = "Cookie";

    private Context mContext;
    private SyncportAccount mAccount;
    private boolean mRenewTokenWhenUnauthurized = true;

    private String mToken;
    private boolean mShouldCache;

    // Request params
    private int mRequestMethod;
    private String mRequestPath;
    private Response.Listener<T> mResponseListener;
    private Response.ErrorListener mErrorListener;

    private RequestQueue mQueue;

    public AuthGenernalRequestHelper(Context context) {
        mContext = context;
        mAccount = SyncportAccount.getAuthenticatedUser(context);
        mRequestMethod = Method.GET;
        mShouldCache = true;
    }

    protected abstract T parseNetworkResponse(NetworkResponse response)
            throws ParseError;

    protected abstract byte[] getBody();

    public AuthGenernalRequestHelper setToken(String token) {
        mToken = token;
        return this;
    }

    public AuthGenernalRequestHelper setMethod(int method) {
        mRequestMethod = method;
        return this;
    }

    public AuthGenernalRequestHelper setPath(String path) {
        mRequestPath = path;
        return this;
    }

    public AuthGenernalRequestHelper setRequestQueue(RequestQueue q) {
        mQueue = q;
        return this;
    }

    public AuthGenernalRequestHelper setShouldCache(boolean shouldCache) {
        mShouldCache = shouldCache;
        return this;
    }

    public AuthGenernalRequestHelper setResponseListener(
            Response.Listener<T> listener) {
        mResponseListener = listener;
        return this;
    }

    public AuthGenernalRequestHelper setErrorListener(Response.ErrorListener listener) {
        mErrorListener = listener;
        return this;
    }

    public void sendRequest() {
        if (mToken != null) {
            Request<T> request = buildRequest(mToken);
            request.setShouldCache(mShouldCache);
            mQueue.add(request);
            return;
        }

        if (mAccount == null) {
            mErrorListener.onErrorResponse(new VolleyError("need signin"));
            return;
        }
        requestAuthToken();
    }

    private void requestAuthToken() {
        mAccount.getAuthToken(new OnGetAuthTokenListener() {
            @Override
            public void onGetAuthToken(String token) {
                Request<T> request = buildRequest(token);
                mQueue.add(request);
            }

            @Override
            public void onFailure(Exception e) {
                mErrorListener.onErrorResponse(new VolleyError(
                        "failed to get auth token", e));
            }
        });
    }

    private Request<T> buildRequest(final String token) {
        Request<T> request = new Request<T>(mRequestMethod,
                mRequestPath, this) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put(HEADER_COOKIE,
                        String.format("sessionid=%s;", token));
                return headers;
            }

            @Override
            protected void deliverResponse(T response) {
                mResponseListener.onResponse(response);
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return AuthGenernalRequestHelper.this.getBody();
            }

            @Override
            public String getBodyContentType() {
                return AuthGenernalRequestHelper.this.getBodyContentType();
            }

            @Override
            protected Response<T> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    T res = AuthGenernalRequestHelper.this
                            .parseNetworkResponse(response);
                    return Response.success(res,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (ParseError e) {
                    return Response.error(e);
                }
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    @Override
    public void onErrorResponse(final VolleyError error) {
        NetworkResponse response = error.networkResponse;
        boolean needAuth = response != null
                && response.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED;
        needAuth = needAuth
                || (error.getMessage() != null && error
                        .getMessage()
                        .equals("java.io.IOException: No authentication challenges found"));
        if (needAuth && mRenewTokenWhenUnauthurized) {
            mRenewTokenWhenUnauthurized = false;
            mAccount.invalidateAuthToken();
            mToken = null;
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    mAccount = SyncportAccount.getOrCreateAccount(mContext);
                    if (mAccount != null) {
                        return true;
                    }
                    return false;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                    if (aBoolean) {
                        sendRequest();
                    } else {
                        mErrorListener.onErrorResponse(error);
                    }
                }
            }.execute();
        } else {
            mErrorListener.onErrorResponse(error);
        }
    }

    protected String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

}
