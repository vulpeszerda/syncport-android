package io.syncport.core.remote;

import io.syncport.ui.BaseActivity;
import android.support.v4.app.Fragment;

import com.android.volley.Response;

public abstract class RobustResponseListener<T> implements Response.Listener<T> {

    private BaseActivity mActivity;
    private Fragment mFragment;

    public RobustResponseListener(BaseActivity activity) {
        this(activity, null);
    }

    public RobustResponseListener(BaseActivity activity, Fragment fragment) {
        mActivity = activity;
        mFragment = fragment;
    }

    @Override
    public void onResponse(T response) {
        mActivity.getSupportFragmentManager().executePendingTransactions();
        if ((mActivity == null || !mActivity.isAlive())
                || (mFragment != null && !mFragment.isAdded())) {
            String reason;
            if (mActivity == null || !mActivity.isAlive()) {
                reason = "activity not alive";
            } else {
                reason = "fragment detached;";
            }
            System.out.println("Skipped response: reason = " + reason);
            try {
                onSkipped(response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        onSecureResponse(response);
    }

    public abstract void onSkipped(T response);

    public abstract void onSecureResponse(T response);

}
