package io.syncport.core.remote;

import io.syncport.R;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

public class DeltaResources {

    private static RequestQueueManager sManager = RequestQueueManager
            .getInstance("delta");
    private static PathBuilder sBuilder = null;

    public static RequestQueue getQ(Context context) {
        return sManager.getQ(context);
    }

    public static String getPath(Context context, String resource) {
        if (sBuilder == null) {

            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(context);
            boolean isDebugMode = pref.getBoolean(
                    context.getString(R.string.preference_key__debug), false);
            sBuilder = new PathBuilder(isDebugMode ?
                    DeltaPaths.DEBUG_HOST :
                    DeltaPaths.HOST, DeltaPaths.VERSION);
        }
        return sBuilder.build(resource);
    }

    public static void resetPathBuilder() {
        sBuilder = null;
    }

    public static void pull(Context context, String type,
            Map<String, String> params, Listener<JSONObject> listener,
            ErrorListener errorListener) {

        String path = getPath(context, DeltaPaths.PULL) + "?type=" + type;

        if (params != null) {
            for (Entry<String, String> entry : params.entrySet()) {
                path += "&" + entry.getKey() + "=" + entry.getValue();
            }
        }
        System.out.println("pull request:" + path);

        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setPath(path)
            .setResponseListener(listener)
            .setErrorListener(errorListener)
            .setShouldCache(false)
            .setRequestQueue(getQ(context))
            .sendRequest();
    }

    public static void push(Context context, String type, JSONObject params,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        try {
            if (params == null) {
                params = new JSONObject();
            }
            params.put("type", type);
            JSONObject delta = new JSONObject();
            delta.put("delta", params);

            System.out.println("push request:" + delta.toString());
            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setPath(getPath(context, DeltaPaths.PUSH))
                .setMethod(Method.POST)
                .setParams(delta)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setShouldCache(false)
                .setRequestQueue(getQ(context))
                .sendRequest();
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }

    }

    public static void startFilePullTransaction(Context context,
            String parentPathFrom, String filename, String peerId,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        startFilePullTransaction(context, parentPathFrom, "", filename, peerId,
                listener, errorListener);
    }

    public static void startFilePullTransaction(Context context,
            String parentPathFrom, String parentPathTo, String filename,
            String peerId, Listener<JSONObject> listener,
            ErrorListener errorListener) {
        startFileTransaction(context, parentPathFrom, parentPathTo, filename,
                peerId, "pull", listener, errorListener);
    }

    public static void startFilePushTransaction(Context context,
            String parentPathFrom, String parentPathTo, String filename,
            String peerId, Listener<JSONObject> listener,
            ErrorListener errorListener) {
        startFileTransaction(context, parentPathFrom, parentPathTo, filename,
                peerId, "push", listener, errorListener);
    }

    private static void startFileTransaction(Context context,
            String parentPathFrom, String parentPathTo, String filename,
            String peerId, String type, Listener<JSONObject> listener,
            ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();

            params.put("filename", filename);
            params.put("path_from", parentPathFrom);
            params.put("path_to", parentPathTo);

            params.put("type", type);
            params.put("peer_id", peerId);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setPath(getPath(context, DeltaPaths.START_FILE_TRANSACTION))
                .setParams(params)
                .setMethod(Method.POST)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setShouldCache(false)
                .setRequestQueue(getQ(context))
                .sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void endFileTransaction(Context context,
            String transactionId, Listener<JSONObject> listener,
            ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("transaction_id",
                    transactionId);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setPath(getPath(context, DeltaPaths.END_FILE_TRANSACTION))
                .setParams(params)
                .setMethod(Method.POST)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setShouldCache(false)
                .setRequestQueue(getQ(context))
                .sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void rsync(Context context, String deviceId,
                             Listener<JSONObject> listener,
                             ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject().put("device_id", deviceId);
            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setPath(getPath(context, DeltaPaths.RSYNC))
                    .setParams(params)
                    .setMethod(Method.POST)
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setShouldCache(false)
                    .setRequestQueue(getQ(context))
                    .sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void hasCommit(Context context, String deviceId,
                                 Listener<JSONObject> listener,
                                 ErrorListener errorListener) {
        listener.onResponse(null);
        return;
//        try {
//            JSONObject params = new JSONObject().put("device_id", deviceId);
//            AuthRequestHelper helper = new AuthRequestHelper(context);
//            helper.setPath(getPath(context, DeltaPaths.RSYNC))
//                    .setParams(params)
//                    .setMethod(Method.POST)
//                    .setResponseListener(listener)
//                    .setErrorListener(errorListener)
//                    .setShouldCache(false)
//                    .setRequestQueue(getQ(context))
//                    .sendRequest();
//        } catch (JSONException e) {
//        }
    }

    public static void setSyncRoot(Context context,
                                   String deviceId, String rootPath,
                                   Listener<JSONObject> listener,
                                   ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject()
                    .put("data", rootPath)
                    .put("device_id", deviceId);
            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setPath(getPath(context, DeltaPaths.SYNCROOT))
                    .setParams(params)
                    .setMethod(Method.POST)
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setRequestQueue(getQ(context))
                    .sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void getSyncRoot(Context context, String deviceId,
                                   Listener<String> listener,
                                   ErrorListener errorListener) {
        String path = getPath(context, DeltaPaths.SYNCROOT) +
                "?device_id=" + deviceId;

        AuthStringRequestHelper helper = new AuthStringRequestHelper(context);

        helper.setPath(path)
                .setMethod(Method.GET)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setRequestQueue(getQ(context))
                .sendRequest();
    }
}
