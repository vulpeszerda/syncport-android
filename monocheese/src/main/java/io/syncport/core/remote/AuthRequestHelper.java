package io.syncport.core.remote;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daftshady.superandroidkit.authentication.AbstractAccount.OnGetAuthTokenListener;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import io.syncport.core.account.SyncportAccount;

public class AuthRequestHelper implements Response.ErrorListener {

    private static final String HEADER_COOKIE = "Cookie";

    private SyncportAccount mAccount;
    private boolean mRenewTokenWhenUnauthurized = true;

    private Context mContext;
    private String mToken;
    private boolean mShouldCache;

    // Request params
    private int mRequestMethod;
    private String mRequestPath;
    private JSONObject mRequestParams;
    private Response.Listener<JSONObject> mResponseListener;
    private Response.ErrorListener mErrorListener;

    private RequestQueue mQueue;

    public AuthRequestHelper(Context context) {
        mContext = context;
        mAccount = SyncportAccount.getAuthenticatedUser(context);
        mRequestMethod = Method.GET;
        mRequestParams = null;
        mShouldCache = true;
    }

    public AuthRequestHelper setToken(String token) {
        mToken = token;
        return this;
    }

    public AuthRequestHelper setMethod(int method) {
        mRequestMethod = method;
        return this;
    }

    public AuthRequestHelper setPath(String path) {
        mRequestPath = path;
        return this;
    }

    public AuthRequestHelper setParams(JSONObject params) {
        mRequestParams = params;
        return this;
    }

    public AuthRequestHelper setRequestQueue(RequestQueue q) {
        mQueue = q;
        return this;
    }

    public AuthRequestHelper setShouldCache(boolean shouldCache) {
        mShouldCache = shouldCache;
        return this;
    }

    public AuthRequestHelper setResponseListener(
            Response.Listener<JSONObject> listener) {
        mResponseListener = listener;
        return this;
    }

    public AuthRequestHelper setErrorListener(Response.ErrorListener listener) {
        mErrorListener = listener;
        return this;
    }

    public void sendRequest() {
        if (mToken != null) {
            JsonObjectRequest request = buildRequest(mToken);
            request.setShouldCache(mShouldCache);
            mQueue.add(request);
            return;
        }

        if (mAccount == null) {
            mErrorListener.onErrorResponse(new VolleyError("need signin"));
            return;
        }
        requestAuthToken();
    }

    private void requestAuthToken() {
        mAccount.getAuthToken(new OnGetAuthTokenListener() {
            @Override
            public void onGetAuthToken(String token) {
                JsonObjectRequest request = buildRequest(token);
                mQueue.add(request);
            }

            @Override
            public void onFailure(Exception e) {
                mErrorListener.onErrorResponse(new VolleyError(
                        "failed to get auth token", e));
            }
        });
    }

    private JsonObjectRequest buildRequest(final String token) {
        JsonObjectRequest request = new JsonObjectRequest(mRequestMethod,
                mRequestPath, mRequestParams, mResponseListener, this) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put(HEADER_COOKIE,
                        String.format("sessionid=%s;", token));
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    if (response.data.length == 0) {
                        byte[] responseData = "{}".getBytes("UTF8");
                        response = new NetworkResponse(response.statusCode,
                                responseData, response.headers,
                                response.notModified);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    @Override
    public void onErrorResponse(final VolleyError error) {
        NetworkResponse response = error.networkResponse;
        boolean needAuth = response != null
                && response.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED;
        needAuth = needAuth
                || (error.getMessage() != null && error
                        .getMessage()
                        .equals("java.io.IOException: No authentication challenges found"));
        if (needAuth && mRenewTokenWhenUnauthurized) {
            mRenewTokenWhenUnauthurized = false;
            mAccount.invalidateAuthToken();
            mToken = null;
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    mAccount = SyncportAccount.getOrCreateAccount(mContext);
                    if (mAccount != null) {
                        return true;
                    }
                    return false;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);
                    if (aBoolean) {
                        sendRequest();
                    } else {
                        mErrorListener.onErrorResponse(error);
                    }
                }
            }.execute();
        } else {
            mErrorListener.onErrorResponse(error);
        }
    }
}
