package io.syncport.core.remote;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.Service;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestQueueManager {

    private static Map<String, RequestQueue> queues = new HashMap<String, RequestQueue>();

    public static RequestQueueManager getInstance(String key) {
        return new RequestQueueManager(key);
    }
    
    private String mName;
    private RequestQueue mQueue;

    private RequestQueueManager(String name) {
        mName = name;
    }

    public RequestQueue getQ(Context context) {
        mQueue = queues.get(mName);
        if (mQueue == null) {
            if (context instanceof Activity) {
                context = ((Activity) context).getApplicationContext();
            } else if (context instanceof Service) {
                context = ((Service) context).getApplicationContext();
            }
            mQueue = Volley.newRequestQueue(context);
            queues.put(mName, mQueue);
        }
        return mQueue;
    }
}
