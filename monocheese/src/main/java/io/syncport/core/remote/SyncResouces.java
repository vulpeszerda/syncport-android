package io.syncport.core.remote;

import io.syncport.R;
import io.syncport.utils.SyncportKeyBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

public class SyncResouces {

    private static RequestQueueManager sManager = RequestQueueManager
            .getInstance("sync");
    private static PathBuilder sBuilder = null;


    public static RequestQueue getQ(Context context) {
        return sManager.getQ(context);
    }

    public static String getPath(Context context, String resource) {
        if (sBuilder == null) {
            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(context);
            boolean isDebugMode = pref.getBoolean(
                    context.getString(R.string.preference_key__debug), false);
            sBuilder = new PathBuilder(isDebugMode ? SyncPaths.DEBUG_HOST
                    : SyncPaths.HOST, SyncPaths.VERSION);
        }
        return sBuilder.build(resource);
    }

    public static void resetPathBuilder() {
        sBuilder = null;
    }


    public static void connect(Context context, String deviceType,
            String deviceName, String connectionState, Listener<JSONObject>
            listener, ErrorListener errorListener) {

        try {
            JSONObject params = new JSONObject()
                    .put("device_type", deviceType)
                    .put("device_name", deviceName)
                    .put("mobile_state", connectionState);
            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST)
                .setPath(getPath(context, SyncPaths.CONNECT))
                .setParams(params)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setShouldCache(false)
                .setRequestQueue(getQ(context))
                .sendRequest();
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void disconnect(Context context, String deviceType,
            String deviceName, Listener<JSONObject> listener,
            ErrorListener errorListener) {
        if (errorListener == null) {
            errorListener = new ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            };
        }

        try {
            JSONObject params = new JSONObject().put("device_type", deviceType)
                    .put("device_name", deviceName);
            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST)
                .setPath(getPath(context, SyncPaths.DISCONNECT))
                .setParams(params)
                .setResponseListener(listener)
                .setErrorListener(errorListener)
                .setShouldCache(false)
                .setRequestQueue(getQ(context))
                .sendRequest();
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError(e));
        }
    }

    public static void activeDevices(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setMethod(Method.GET)
            .setPath(getPath(context, SyncPaths.DEVICE))
            .setResponseListener(listener)
            .setErrorListener(errorListener)
            .setShouldCache(false)
            .setRequestQueue(getQ(context))
            .sendRequest();
    }

    public static void sendHeartbeat(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setMethod(Method.GET)
            .setPath(getPath(context, SyncPaths.HEARTBEAT))
            .setResponseListener(listener)
            .setErrorListener(errorListener)
            .setShouldCache(false)
            .setRequestQueue(getQ(context))
            .sendRequest();
    }
}
