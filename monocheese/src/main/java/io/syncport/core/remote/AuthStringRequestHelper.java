package io.syncport.core.remote;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;

/**
 * Created by vulpes on 15. 3. 9..
 */
public class AuthStringRequestHelper extends AuthGenernalRequestHelper<String>{

    public AuthStringRequestHelper(Context context) {
        super(context);
    }

    @Override
    protected String parseNetworkResponse(NetworkResponse
                                                  response) throws ParseError {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return parsed;
    }

    @Override
    protected byte[] getBody() {
        return null;
    }
}
