package io.syncport.core.remote;

import io.syncport.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SyncPaths {

    public static final String HOST = "http://sync.syncport.io/";
    public static final String DEBUG_HOST = "http://sync.coroutine.io/";
    public static final String VERSION = "0";

    public static final String CONNECT = "connect";
    public static final String DISCONNECT = "disconnect";
    public static final String DEVICE = "devices";
    public static final String HEARTBEAT = "heartbeat";

    private static final String POLLING = "long_poll";

    private static PathBuilder sPathBuilder = null;

    public static String getPollingPath(Context context) {
        if (sPathBuilder == null) {
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            if (prefs.getBoolean(
                    context.getString(R.string.preference_key__debug), false)) {
                sPathBuilder = new PathBuilder(DEBUG_HOST, VERSION);
            } else {
                sPathBuilder = new PathBuilder(HOST, VERSION);
            }
        }
        return sPathBuilder.build(POLLING);
    }

    public static void clearCache() {
        sPathBuilder = null;
    }
}
