package io.syncport.core.remote;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;

public class ApiResources {

    private static RequestQueueManager sManager = RequestQueueManager
            .getInstance("api");
    private static PathBuilder sBuilder = null;

    public static RequestQueue getQ(Context context) {
        return sManager.getQ(context);
    }

    public static String getPath(Context context, String resource) {
        if (sBuilder == null) {

            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(context);
            boolean isDebugMode = pref.getBoolean(
                    context.getString(R.string.preference_key__debug), false);
            sBuilder = new PathBuilder(isDebugMode ? ApiPaths.DEBUG_HOST
                    : ApiPaths.HOST, "api/" + ApiPaths.VERSION);
        }
        return sBuilder.build(resource);
    }

    public static void resetPathBuilder() {
        sBuilder = null;
    }

    public static void getClientVersion(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        JsonObjectRequest request = new JsonObjectRequest(getPath(context,
                ApiPaths.CLIENT_VERSION), null, listener, errorListener);
        getQ(context).add(request);
    }

    public static void signin(Context context, String tag, JSONObject params,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        String path = getPath(context, ApiPaths.SIGNIN);
        JsonObjectRequest request = new JsonObjectRequest(Method.POST, getPath(
                context, ApiPaths.SIGNIN), params, listener, errorListener);
        request.setTag(tag);
        getQ(context).add(request);
    }

    public static String syncSignin(Context context, JSONObject params) {
        try {
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            signin(context, "syncSignin", params, future, future);
            JSONObject response = future.get();
            return response.getString("session_key");

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void signup(Context context, JSONObject params,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        JsonObjectRequest request = new JsonObjectRequest(Method.POST, getPath(
                context, ApiPaths.SIGNUP), params, listener, errorListener);
        getQ(context).add(request);
    }

    public static void findpasswd(Context context, String email,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        try {
            JSONObject params = new JSONObject();
            params.put("email", email);

            JsonObjectRequest request = new JsonObjectRequest(Method.POST,
                    getPath(context, ApiPaths.FINDPWD), params, listener,
                    errorListener);
            request.setRequestQueue(getQ(context));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getUserSelf(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setMethod(Method.POST)
                .setPath(getPath(context, ApiPaths.USER_SELF))
                .setResponseListener(listener).setErrorListener(errorListener)
                .setRequestQueue(getQ(context)).sendRequest();
    }

    public static void getDeviceSelf(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setMethod(Method.GET)
                .setPath(getPath(context, ApiPaths.DEVICE_SELF))
                .setResponseListener(listener).setErrorListener(errorListener)
                .setRequestQueue(getQ(context)).sendRequest();
    }

    public static void getDeviceSelf(Context context, String token,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setToken(token);
        helper.setMethod(Method.GET)
                .setPath(getPath(context, ApiPaths.DEVICE_SELF))
                .setResponseListener(listener).setErrorListener(errorListener)
                .setRequestQueue(getQ(context)).sendRequest();
    }

    public static void getAllDevices(Context context,
            Listener<JSONObject> listener, ErrorListener errorListener) {

        AuthRequestHelper helper = new AuthRequestHelper(context);
        helper.setMethod(Method.GET)
                .setPath(getPath(context, ApiPaths.DEVICE_ALL))
                .setResponseListener(listener).setErrorListener(errorListener)
                .setRequestQueue(getQ(context)).sendRequest();
    }

    public static void changeMyDeviceName(Context context, String deviceName,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        changeDeviceName(context, account.getDeviceId(), deviceName, listener,
                errorListener);
    }

    public static void changeDeviceName(Context context, String deviceId,
            String deviceName, Listener<JSONObject> listener,
            ErrorListener errorListener){

        try {
            JSONObject params = new JSONObject();
            JSONArray changes = new JSONArray();

            JSONObject change = new JSONObject();
            change.put("device_id", deviceId);
            change.put("name", deviceName);

            changes.put(change);
            params.put("changes", changes);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST)
                    .setParams(params)
                    .setPath(getPath(context, ApiPaths.DEVICE_ALL))
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setRequestQueue(getQ(context))
                    .sendRequest();
        } catch (JSONException e) {
            // not reach
        }

    }

    public static void registerPushKey(Context context, String pushKey,
            Listener<JSONObject> listener, ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            params.put("push_key", pushKey);

            String path = getPath(context, ApiPaths.DEVICE_PUSH_KEY);
            System.out.println("registerPushKey:" + path + ", " + pushKey);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST).setParams(params)
                    .setPath(getPath(context, ApiPaths.DEVICE_PUSH_KEY))
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setRequestQueue(getQ(context)).sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void requestCpr(Context context, List<String> targetDeviceIds,
                                  Listener<JSONObject> listener,
                                  ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            for (String deviceId : targetDeviceIds) {
                array.put(deviceId);
            }
            params.put("device_ids", array);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST).setParams(params)
                    .setPath(getPath(context, ApiPaths.CPR))
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setRequestQueue(getQ(context)).sendRequest();
        } catch (JSONException e) {
        }
    }

    public static void requestRcpr(Context context,
                                   List<String> targetDeviceIds,
                                  Listener<JSONObject> listener,
                                  ErrorListener errorListener) {
        try {
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();
            for (String deviceId : targetDeviceIds) {
                array.put(deviceId);
            }
            params.put("device_ids", array);

            AuthRequestHelper helper = new AuthRequestHelper(context);
            helper.setMethod(Method.POST).setParams(params)
                    .setPath(getPath(context, ApiPaths.RCPR))
                    .setResponseListener(listener)
                    .setErrorListener(errorListener)
                    .setRequestQueue(getQ(context)).sendRequest();
        } catch (JSONException e) {
        }
    }

}
