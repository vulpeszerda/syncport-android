package io.syncport.core.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;

import com.amazonaws.org.apache.http.client.ClientProtocolException;

public class WebResources {

    /**
     * Use {@link ApiResources.getClientVersion} instead
     * @param context
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Deprecated
    public static String getAppVersion(Context context)
            throws ClientProtocolException, IOException {

        String path = WebPaths.getAppVersionPath(context);

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httppost = new HttpGet(path);
        HttpResponse response = httpclient.execute(httppost);
        HttpEntity ht = response.getEntity();

        BufferedHttpEntity buf = new BufferedHttpEntity(ht);

        InputStream is = buf.getContent();

        BufferedReader r = new BufferedReader(new InputStreamReader(is));

        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line + "\n");
        }
        r.close();
        return total.toString().trim();
    }
}
