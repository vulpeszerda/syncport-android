package io.syncport.core.remote;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.MultipleFileUpload;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.MultiObjectDeleteException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.syncport.utils.Log;

public class AWSResources {

    public static final String ACCESS_KEY_ID = "AKIAI3FYDRDI3XLL7HVQ";
    public static final String SECRET_KEY = "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";
    public static final String BUCKET_NAME = "syncport-thumbnail";

    private static final String TAG = "AWSResources";

    private static TransferManager sTransferManager = null;
    private static Object sTransferManagerLock = new Object();

    public static class UploadPassenger {
        private File mFile;
        private ProgressListener mListener;

        public UploadPassenger(File file, ProgressListener listener) {
            mFile = file;
            mListener = listener;
        }

        public File getFile() {
            return mFile;
        }

        public ProgressListener getListener() {
            return mListener;
        }
    }

    public static void uploadSync(String filename, long length, InputStream in) {

        AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials(
                ACCESS_KEY_ID, SECRET_KEY));
        ObjectMetadata metaData = new ObjectMetadata();
        metaData.setContentLength(length);
        PutObjectRequest por = new PutObjectRequest(BUCKET_NAME, filename, in,
                metaData);
        s3Client.putObject(por);
    }

    public static void uploadAsync(String filename, long length,
            InputStream in, ProgressListener listener) {
        synchronized (sTransferManagerLock) {
            if (sTransferManager == null) {
                sTransferManager = new TransferManager(new BasicAWSCredentials(
                        ACCESS_KEY_ID, SECRET_KEY));
            }
            ObjectMetadata metaData = new ObjectMetadata();
            metaData.setContentLength(length);
            Upload upload = sTransferManager.upload(BUCKET_NAME, filename, in,
                    metaData);
            upload.addProgressListener(listener);
        }
    }

    public static interface UploadFinishedListener {
        void onSuccess(List<File> files);

        void onFailure(List<File> files);
    }

    public static void uploadFilesAsync(File parentCommonDir, final List<File> files,
            final UploadFinishedListener listener) {
        synchronized (sTransferManagerLock) {
            if (sTransferManager == null) {
                sTransferManager = new TransferManager(new BasicAWSCredentials(
                        ACCESS_KEY_ID, SECRET_KEY));
            }

            final List<File> uploadedFiles = new ArrayList<File>(files);
            final MultipleFileUpload upload = sTransferManager.uploadFileList(
                    BUCKET_NAME, null, parentCommonDir, uploadedFiles);
            final AtomicInteger successCount = new AtomicInteger(0);
            upload.addProgressListener(new ProgressListener() {
                private boolean didFire = false;

                @Override
                public void progressChanged(ProgressEvent event) {
                    if (didFire) {
                        return;
                    }
                    if (event.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {
                        if (successCount.incrementAndGet() == uploadedFiles.size()) {
                            didFire = true;
                            listener.onSuccess(uploadedFiles);
                        }
                    } else if (event.getEventCode() == ProgressEvent.FAILED_EVENT_CODE
                            || event.getEventCode() == ProgressEvent.CANCELED_EVENT_CODE) {
                        didFire = true;
                        listener.onFailure(uploadedFiles);
                    }
                }

            });
		}
	}

	public static boolean deleteFilesSync(List<String> keys) {
		try {
			ArrayList<KeyVersion> keyVersions = new ArrayList<KeyVersion>();
			for (String key : keys) {
				keyVersions.add(new KeyVersion(key));
			}
            if (keys.size() == 0) {
                return true;
            }
			AmazonS3Client s3Client = new AmazonS3Client(
					new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY));
			DeleteObjectsRequest request = new DeleteObjectsRequest(BUCKET_NAME)
					.withQuiet(true).withKeys(keyVersions);
			// request.withKeys(keys.toArray(new String[keys.size()]));
			DeleteObjectsResult result = s3Client.deleteObjects(request);
			return true;
		} catch (MultiObjectDeleteException e) {
			e.printStackTrace();
			return false;
		} catch (AmazonS3Exception e) {
            e.printStackTrace();
            return false;
        } catch(AmazonClientException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void releaseTransferManager() {
        synchronized (sTransferManagerLock) {
            if (sTransferManager != null) {
                sTransferManager.shutdownNow();
                sTransferManager = null;
            }
        }
    }
}
