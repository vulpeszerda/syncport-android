package io.syncport.core.remote;

import io.syncport.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class RelayPaths {
    private static final int PORT = 19004;

    private static final int DEBUG_PORT = 19004;

    private static int sCachedPort = -1;

    public static int getPort(Context context) {
        if (sCachedPort < 0) {
            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(context);
            boolean isDebugMode = pref.getBoolean(
                    context.getString(R.string.preference_key__debug), false);
            sCachedPort = isDebugMode ? DEBUG_PORT : PORT;
        }
        return sCachedPort;
    }
    
    public static void clearCache() {
        sCachedPort = -1;
    }
}
