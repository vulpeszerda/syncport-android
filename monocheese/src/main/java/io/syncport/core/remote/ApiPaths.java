package io.syncport.core.remote;

public class ApiPaths {

    public static final String DEBUG_HOST = "http://api.coroutine.io";
    public static final String HOST = "http://api.syncport.io";
    public static final String VERSION = "v1";

    public static final String CLIENT_VERSION = "meta";

    // User Resources
    public static final String USER_SELF = "users/self";

    public static final String SIGNIN = "users/signin";
    public static final String SIGNUP = "users/signup";
    public static final String FINDPWD = "users/findpwd";

    // Device Resources
    public static final String DEVICE_SELF = "devices/self";
    public static final String DEVICE_ALL = "devices/all";
    public static final String DEVICE_PUSH_KEY = "devices/push_key";
    public static final String CPR = "devices/cpr";
    public static final String RCPR = "devices/rcpr";

}
