package io.syncport.core.remote;

public class DeltaPaths {
    public static final String HOST = "http://delta.syncport.io/";
    public static final String DEBUG_HOST = "http://delta.coroutine.io/";
    public static final String VERSION = "0";

    public static final String PULL = "delta_pull";
    public static final String PUSH = "delta_push";

    public static final String SYNCROOT = "syncroot";

    public static final String START_FILE_TRANSACTION = "start_file_transaction";
    public static final String END_FILE_TRANSACTION = "end_file_transaction";

    public static final String RSYNC = "rsync";

}
