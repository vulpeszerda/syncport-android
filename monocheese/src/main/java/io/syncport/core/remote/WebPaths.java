package io.syncport.core.remote;

import io.syncport.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class WebPaths {

    private static final String DEBUG_HOST = "http://coroutine.io";
    private static final String HOST = "http://syncport.io";

    private static final String APP_VERSION = "static/android__ver";
    private static final String TERM_URL = "terms";

    public static String getTermPath(Context context) {
        return getPathBuilder(context).build(TERM_URL);
    }

    /**
     * @param context
     * @return
     */
    @Deprecated
    public static String getAppVersionPath(Context context) {
        return getPathBuilder(context).build(APP_VERSION);
    }

    private static PathBuilder getPathBuilder(Context context) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        PathBuilder pathBuilder;
        if (prefs.getBoolean(context.getString(R.string.preference_key__debug),
                false)) {
            pathBuilder = new PathBuilder(DEBUG_HOST);
        } else {
            pathBuilder = new PathBuilder(HOST);
        }
        return pathBuilder;

    }

}
