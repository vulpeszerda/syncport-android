package io.syncport.core;

import io.syncport.core.account.SyncportAccount;
import io.syncport.utils.Log;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RestartServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            return;
        }
        if (!RootService.isRunning(context)) {
            Log.d("RestartServiceReceiver", "restart");
            context.startService(RootService.createIntent(context));
        } else {
            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(System.currentTimeMillis() + 30 * 1000);

            AlarmManager am = (AlarmManager) context
                    .getSystemService(Context.ALARM_SERVICE);
            Intent restartIntent = new Intent(context,
                    RestartServiceReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                    0, restartIntent, PendingIntent.FLAG_ONE_SHOT);
            am.cancel(pendingIntent);
            am.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
        }
    }

}
