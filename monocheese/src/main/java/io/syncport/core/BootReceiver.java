package io.syncport.core;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.utils.VersionComparator;
import roboguice.receiver.RoboBroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class BootReceiver extends RoboBroadcastReceiver {

    @Override
    protected void handleReceive(Context context, Intent intent) {
        if (SyncportAccount.getAuthenticatedUser(context) != null) {
            startBackgroundService(context);
        }
    }

    private void startBackgroundService(Context context) {
        if (!RootService.isRunning(context)) {

            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(context);
            String remoteAppVersionKey = context.getString(R.string
                    .preference_key__remote_app_version);
            String currVersion = context.getString(R.string.app_version);
            String recentVersion = prefs.getString(remoteAppVersionKey, null);
            if (recentVersion != null && VersionComparator.compare
                    (currVersion, recentVersion) < 0) {
                return;
            }
            context.startService(RootService.createIntent(context));
        }
    }
}
