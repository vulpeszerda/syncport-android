package io.syncport.core;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import io.syncport.BuildConfig;
import io.syncport.R;
import io.syncport.core.BaseChildService.ReadyToDestroyListener;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.DeviceStateUpdateService;
import io.syncport.core.directory.DirectoryPusherService;
import io.syncport.core.directory.DirectoryRemoteAddDelService;
import io.syncport.core.directory.FileCompressUpadaterService;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.transfer.FileTransferRecevier;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;
import io.syncport.utils.VersionComparator;
import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import main.java.com.mindscapehq.android.raygun4android.messages.RaygunUserInfo;

/**
 * Background service is basic always running service of syncport. This service
 * listen user signin / signout event with broadcast for bind / unbind child
 * service
 *
 * @author vulpes
 *
 */
public class RootService extends Service {

    public static final String ACTION_STOP_SELF = SyncportKeyBuilder.build
            ("stop_self");

    public static Intent createIntent(Context context) {
        return createIntent(context, false);
    }

    public static Intent createIntent(Context context, boolean useWakeLock) {
        Intent intent = new Intent(context, RootService.class);
        intent.putExtra(PARAM_USE_WAKELOCK, useWakeLock);
        return intent;
    }

    public static boolean isRunning(Context context) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (RootService.class.getName().equals(
                    service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void extendWatcher(final Context context,
                                        final String receiver) {
        synchronized (sWatchers) {
            Timer timer = sWatchers.get(receiver);
            if (timer != null) {
                timer.cancel();
            }
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (sWatchers) {
                        Timer timer = sWatchers.get(receiver);
                        if (timer != null) {
                            timer.cancel();
                            sWatchers.remove(receiver);
                        }
                        if (wakeLock != null && sWatchers.size() == 0 &&
                                wakeLock.isHeld()) {
                            context.startService(RootService.createIntent
                                    (context, false));
                        }
                    }
                }
            }, WAKE_LOCK_TIMEOUT);
            sWatchers.put(receiver, timer);

            Log.d("TEST10", "extend watcher size:" + sWatchers.size());
            if (!isRunning(context) ||
                    (sWatchers.size() > 0 &&
                    (wakeLock == null || !wakeLock.isHeld()))) {
                context.startService(RootService.createIntent
                        (context, true));
            }
        }
    }

    public static void expireWatcher(Context context, String receiver) {
        synchronized (sWatchers) {
            Timer timer = sWatchers.get(receiver);
            if (timer != null) {
                timer.cancel();
                sWatchers.remove(receiver);
                Log.d("TEST10", "expire watcher size:" + sWatchers.size());
                if (sWatchers.size() == 0 && isRunning(context) &&
                        wakeLock != null && wakeLock.isHeld()) {
                    context.startService(RootService.createIntent(context,
                            false));
                }
            }
        }
    }

    private static final String TAG = "RootService";
    private static final String PARAM_USE_WAKELOCK = SyncportKeyBuilder
            .build("use_wakelock");
    private static final long WAKE_LOCK_TIMEOUT = 1 * 60 * 1000 + 20 * 1000;
    private static final Map<String, Timer> sWatchers = new HashMap<>();
    private static PowerManager.WakeLock wakeLock;
    //private PowerManager.WakeLock wakeLock;

    private Intent mIntent;
    private boolean mUseWakeLock;
    private BroadcastReceiver mAuthReceiver;
    private Map<String, ChildServiceBinder> mChildServiceBinders = new HashMap<String, ChildServiceBinder>();

    @Override
    public void onCreate() {
        super.onCreate();
        // startForeground(NotificationFactory.NOTI_ID_FOREGROUND,
        // NotificationFactory.createForegroundNoti(this));
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException(
                "This service should be called with 'startService', not 'bindService'");
    }

    private void registerAlarmManager(long timeAfter) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(System.currentTimeMillis() + timeAfter);

        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent restartIntent = new Intent(this, RestartServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                restartIntent, PendingIntent.FLAG_ONE_SHOT);
        am.cancel(pendingIntent);
        am.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
    }

    private void unregisterAlarmManager() {
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent restartIntent = new Intent(this, RestartServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                restartIntent, PendingIntent.FLAG_ONE_SHOT);
        am.cancel(pendingIntent);
        pendingIntent.cancel();
    }


    private boolean isRunnableAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            String currentVersion = packageInfo.versionName;
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            ApiResources.getClientVersion(this, future, future);
            JSONObject response = future.get();
            String recentVersion = response.getString("android_version");
            if (!recentVersion.matches("\\d+\\.\\d+\\.\\d+")) {
                throw new Exception("version is not proper format");
            }
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            String remoteAppVersionKey = getString(R.string
                    .preference_key__remote_app_version);
            editor.putString(remoteAppVersionKey, recentVersion);
            editor.commit();
            int cmp = VersionComparator.compare(currentVersion, recentVersion);
            if (cmp < 0) {
                return false;
            }
            return true;
        } catch (ExecutionException e) {
        } catch (JSONException e) {
        } catch (PackageManager.NameNotFoundException e) {
            // skip
        } catch (InterruptedException e) {
            return false;
        } catch (Exception e) {
            return false;
        }

        return false;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mIntent = intent;
        mUseWakeLock = mIntent.getBooleanExtra(PARAM_USE_WAKELOCK, false);
        startListenAuthEvent();
        registerAlarmManager(30* 1000);
        clearExternalFileCaches();

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        String remoteAppVersionKey = getString(R.string
                .preference_key__remote_app_version);
        String currVersion = getString(R.string.app_version);
        String recentVersion = prefs.getString(remoteAppVersionKey, null);
        if (recentVersion != null && VersionComparator.compare(currVersion,
                recentVersion) < 0) {
            stopSelf();
            return START_NOT_STICKY;
        }

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                return BuildConfig.DEBUG || isRunnableAppVersion();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    SyncportAccount account = SyncportAccount
                            .getAuthenticatedUser(RootService.this);
                    if (account != null) {
                        onStartRootService(account);

                        if (!BuildConfig.DEBUG) {
                            RaygunClient.Init(getApplicationContext());
                            RaygunClient.AttachExceptionHandler();
                            RaygunUserInfo user = new RaygunUserInfo();
                            user.Identifier = account.getDeviceId();
                            user.FullName = account.getDeviceName();
                            user.Email = account.getName();
                            user.Uuid = account.getDeviceId();
                            user.IsAnonymous = false;

                            RaygunClient.SetUser(user);
                        }
                    }
                    return;
                } else {
                    unregisterAlarmManager();
                    stopSelf();
                }
            }
        }.execute();
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        stopListenAuthEvent();
        onFinishRootService();

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            Log.d(TAG, "released wakelock");
        }
        // stopForeground(true);
        // sendBroadcast(new Intent(
        // getString(R.string.broadcast_action__rootservice_destroyed)));

        super.onDestroy();
    }

    private void clearExternalFileCaches() {
        Context context = getApplicationContext();
        File cacheDir = context.getExternalCacheDir();

        Stack<File> stack = new Stack<>();
        SimpleDateFormat formater = new SimpleDateFormat
                ("yyyy/MM/dd'T'HH:mm:ss");
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        stack.add(cacheDir);
        while (stack.size() > 0) {
            File file = stack.pop();
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                if (files != null) {
                    File[] childFiles = file.listFiles();
                    if (childFiles != null) {
                        for (File child : childFiles) {
                            stack.push(child);
                        }
                    }
                }
                continue;
            }
            long lastModifiedAt = file.lastModified();
            long diffTime = new Date().getTime() - lastModifiedAt;
            int dateAfter = (int)(diffTime / (1000 * 60 * 60 * 24));
            if (dateAfter > 0) {
                file.delete();
            }
        }
    }

    private void startListenAuthEvent() {
        IntentFilter filter = new IntentFilter(SyncportAccount.ACTION_SIGNIN);
        filter.addAction(SyncportAccount.ACTION_SIGNOUT_SYNC);

        mAuthReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(SyncportAccount.ACTION_SIGNIN)) {
                    SyncportAccount account = SyncportAccount
                            .getAuthenticatedUser(RootService.this);
                    onAccountSignedIn(account);
                } else if (intent.getAction().equals(
                        SyncportAccount.ACTION_SIGNOUT_SYNC)) {
                    onAccountSignedOut();
                }

            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mAuthReceiver,
                filter);
    }

    private void stopListenAuthEvent() {
        if (mAuthReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    mAuthReceiver);
        }
    }


    private void onAccountSignedIn(SyncportAccount account) {
        onStartRootService(account);
    }

    private void onAccountSignedOut() {
        unregisterAlarmManager();
        onFinishRootService();
        stopSelf();
    }

    private synchronized void onStartRootService(SyncportAccount account) {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (wakeLock == null) {
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "BackgroundService");
        }
        if (mUseWakeLock && !wakeLock.isHeld()) {
            wakeLock.acquire();
            Log.d(TAG, "acquired wakelock");
            Log.d(TAG, "root service is running with wakelock:" + wakeLock.isHeld());
        } else if (!mUseWakeLock && wakeLock.isHeld()) {
            wakeLock.release();
            Log.d(TAG, "released wakelock");
            Log.d(TAG, "root service is running with wakelock:" + wakeLock.isHeld());
        }

        bindChildServices(mIntent);

        registerPollReceivers();
        registerScreenReceiver();
        registerStopSelfReceiver();

    }

    private synchronized void onFinishRootService() {
        unregisterScreenReceiver();
        unregisterPollReceivers();
        unregisterStopSelfReceiver();
        if (Looper.myLooper() == Looper.getMainLooper()) {
            // unbind asynchronously
            unbindChildServices(null);
        } else {
            // unbind synchronously
            final CountDownLatch latch = new CountDownLatch(1);
            unbindChildServices(new ReadyToDestroyListener() {

                @Override
                public void onReadyToDestroy() {
                    latch.countDown();
                }
            });
            try {
                latch.await(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void bindChildServices(final Intent intent) {
        if (mChildServiceBinders.get("polling") == null) {
            bindPollingService(intent);
        } else {
            LocalBroadcastManager manager = LocalBroadcastManager
                    .getInstance(RootService.this);
            manager.sendBroadcast(new Intent(
                    GeneralPollerService.ACTION_RESTART_POLLING));
        }
        if (mChildServiceBinders.get("directory") == null) {
            bindDirectoryService(intent);
        }
    }

    private void unbindChildServices(final ReadyToDestroyListener listener) {
        unbindPollingService(new ReadyToDestroyListener() {

            @Override
            public void onReadyToDestroy() {
                unbindDirectoryService(listener);

            }
        });
    }

    private void bindPollingService(Intent intent) {
        Intent generalPollingIntent = GeneralPollerService.createIntent(this);
        mPollingServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                mChildServiceBinders
                        .put("polling", (ChildServiceBinder) binder);
            }
        };

        bindService(generalPollingIntent, mPollingServiceConnection,
                Service.BIND_AUTO_CREATE);
    }

    private void unbindPollingService(final ReadyToDestroyListener listener) {
        ChildServiceBinder binder = mChildServiceBinders.get("polling");
        if (binder != null) {
            binder.prepareDestroy(new ReadyToDestroyListener() {

                @Override
                public void onReadyToDestroy() {
                    if (mPollingServiceConnection != null) {
                        try {
                            unbindService(mPollingServiceConnection);
                        } catch (IllegalArgumentException e) {
                            // XXX: Have to handle later..
                            e.printStackTrace();
                            Log.e("TEST", e.toString());
                        }
                    }
                    mChildServiceBinders.remove("polling");
                    if (listener != null) {
                        listener.onReadyToDestroy();

                    }
                }
            });
        } else if (listener != null) {
            listener.onReadyToDestroy();
        }
    }

    private void bindDirectoryService(Intent intent) {
        Intent dirPusherIntent = DirectoryPusherService.createIntent(this);
        mDirectoryServiceConnection = new ServiceConnection() {

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                mChildServiceBinders.put("directory",
                        (ChildServiceBinder) binder);
            }
        };
        bindService(dirPusherIntent, mDirectoryServiceConnection,
                Service.BIND_AUTO_CREATE);
    }

    private void unbindDirectoryService(final ReadyToDestroyListener listener) {
        ChildServiceBinder binder = mChildServiceBinders.get("directory");
        if (binder != null) {
            binder.prepareDestroy(new ReadyToDestroyListener() {

                @Override
                public void onReadyToDestroy() {
                    if (mDirectoryServiceConnection != null) {
                        try {
                            unbindService(mDirectoryServiceConnection);
                        } catch (IllegalArgumentException e) {
                            // XXX: Have to handle later..
                            e.printStackTrace();
                            Log.e("TEST", e.toString());
                        }
                    }
                    mChildServiceBinders.remove("directory");
                    if (listener != null) {
                        listener.onReadyToDestroy();
                    }
                }
            });
        } else if (listener != null) {
            listener.onReadyToDestroy();
        }
    }

    private ServiceConnection mPollingServiceConnection;

    private ServiceConnection mDirectoryServiceConnection;

    // Polling receivers
    private List<BroadcastReceiver> mPollingReceivers = new ArrayList<>();
    private AtomicBoolean mIsPollReceiverRegistered = new AtomicBoolean(false);

    private synchronized void registerPollReceivers() {
        if (mIsPollReceiverRegistered.get()) {
            return;
        }
        Context context = this;
        mPollingReceivers.add(DeviceStateUpdateService
                .registerReceiver(context));
        mPollingReceivers.add(DirectoryRemoteAddDelService
                .registerReceiver(context));
        mPollingReceivers.add(FileCompressUpadaterService
                .registerReceiver(context));
        mPollingReceivers.add(FileCompressUpadaterService
                .registerCancelReceiver(context));

        BroadcastReceiver fileTransferReceiver = new FileTransferRecevier();
        LocalBroadcastManager.getInstance(context).registerReceiver(fileTransferReceiver, FileTransferRecevier.getFilter());
        mPollingReceivers.add(fileTransferReceiver);

        mIsPollReceiverRegistered.set(true);
    }

    private synchronized void unregisterPollReceivers() {
        if (!mIsPollReceiverRegistered.get()) {
            return;
        }
        LocalBroadcastManager manager = LocalBroadcastManager
                .getInstance(this);
        for (BroadcastReceiver receiver : mPollingReceivers) {
            manager.unregisterReceiver(receiver);
        }
        mIsStopSelfReceiverRegistered.set(false);
    }

    // ScreenonReceiver
    private BroadcastReceiver mScreenOnReceiver;
    private AtomicBoolean mIsScreenReceiverRegistered = new AtomicBoolean
            (false);

    private synchronized void registerScreenReceiver() {
        if (!mIsScreenReceiverRegistered.get()) {
            mScreenOnReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                        LocalBroadcastManager manager = LocalBroadcastManager
                                .getInstance(RootService.this);
                        manager.sendBroadcast(new Intent(
                                GeneralPollerService.ACTION_RESTART_POLLING));
                    }
                }
            };
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            registerReceiver(mScreenOnReceiver, filter);
            mIsScreenReceiverRegistered.set(true);
        }
    }

    private synchronized void unregisterScreenReceiver() {
        if (mIsScreenReceiverRegistered.get()) {
            // XXX: why this exception throw?
            try {
                unregisterReceiver(mScreenOnReceiver);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            mIsScreenReceiverRegistered.set(false);
        }
    }

    private BroadcastReceiver mStopSelfReceiver;
    private AtomicBoolean mIsStopSelfReceiverRegistered = new AtomicBoolean
            (false);

    private synchronized void registerStopSelfReceiver() {
        if (mIsStopSelfReceiverRegistered.get()) {
            return;
        }
        mStopSelfReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                registerAlarmManager(60 * 60 * 1000);
                onFinishRootService();
                stopSelf();
            }
        };
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance
                (this);
        manager.registerReceiver(mStopSelfReceiver,
                new IntentFilter(ACTION_STOP_SELF));
        mIsStopSelfReceiverRegistered.set(true);
    }

    private synchronized void unregisterStopSelfReceiver() {
        if (mIsStopSelfReceiverRegistered.get()) {
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
            manager.unregisterReceiver(mStopSelfReceiver);
            mIsStopSelfReceiverRegistered.set(false);
        }
    }
}
