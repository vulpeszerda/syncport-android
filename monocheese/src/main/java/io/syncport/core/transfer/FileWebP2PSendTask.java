package io.syncport.core.transfer;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.transfer.webp2p.Http;
import io.syncport.ui.NavigationDrawerActivity;
import io.syncport.ui.NavigationDrawerFragment;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

public class FileWebP2PSendTask extends BaseFileTask {

    private static final String TAG = "FileWebP2PFileSendTask";

    private static final long MAX_SENDABLE_FILE_SIZE = (long) Math
            .pow(2, 4 * 8) - 1;
    private static final int SOCKET_READ_TIMEOUT = 3000;

    protected TaskType mTaskType = TaskType.SEND;

    private String mNotiTitleFormat;
    private String mNotiSendProgressFormat;
    private boolean mForceDownload;

    private Socket mConn;
    private String mDestFilename;

    public FileWebP2PSendTask(Context context, Transaction trans, Socket conn,
            String filename, boolean showNoti, boolean forceDl) {
        super(context, trans, showNoti);
        mConn = conn;
        mDestFilename = filename;
        mForceDownload = forceDl;
    }

    @Override
    protected TaskType taskType() {
        return TaskType.SEND;
    }

    @Override
    protected String getNotiTitleText() {
        if (mNotiSendProgressFormat == null) {
            mNotiTitleFormat = getContext().getString(
                    R.string.desc_file_send__format);
        }
        return String.format(mNotiTitleFormat, getTransaction().getFilename());
    }

    @Override
    protected String getNotiPrepareText() {
        return getContext().getString(R.string.desc_file_send_prepare);
    }

    protected String getNotiProgressText(Progress prog) {
        if (mNotiSendProgressFormat == null) {
            mNotiSendProgressFormat = getContext().getString(
                    R.string.desc_file_send_progress__format);
        }
        return String.format(mNotiSendProgressFormat,
                FileUtils.readableFileSize(prog.doneLength),
                FileUtils.readableFileSize(prog.fileLength));
    }

    @Override
    protected Intent getNotiIntent() {
        return NavigationDrawerActivity.createIntent(getContext(),
                NavigationDrawerFragment.MenuType.FILE_TRANSFER);
    }

    @Override
    protected Result doInBackground() {
        Transaction trans = getTransaction();
        File file = new File(trans.getPathFrom(), trans.getFilename());
        if (!file.exists() || !file.isFile()) {
            // TODO
            // need to abort transaction.
            // this case will not be handled on PROTO TYPING
            return new FailureResult(new UnsupportedOperationException(
                    "file not exist."));
        }

        BufferedInputStream fileInputStream = null;

        try {

            long fileLength = file.length();
            String resp;
            if (mForceDownload) {
                resp = Http.getDownloadHeader(mDestFilename, String.valueOf(fileLength));
            } else {
                resp = Http.getInlineHeader(mDestFilename, String.valueOf(fileLength));
            }
            OutputStream os = mConn.getOutputStream();

            os.write(resp.getBytes());

            fileInputStream = new BufferedInputStream(new FileInputStream(file));

            // write file
            int bufLen = 65536;
            byte[] buffer = new byte[bufLen];
            int readByteLen = 0;
            long sentByteLen = 0;
            while (!isCancelled()
                    && (readByteLen = fileInputStream.read(buffer)) != -1) {
                sentByteLen += readByteLen;
                os.write(buffer, 0, readByteLen);
                int percent = (int) (100 * sentByteLen / fileLength);
                publishProgress(new Progress(fileLength, sentByteLen, percent));
            }

            if (!isCancelled()) {
                Log.d(TAG, String.format("file <%s> send complete",
                        file.getName()));
            } else {
                Log.d(TAG, String.format("file <%s> send cancelled", file.getName()));
            }
            return new Result(true);
        } catch (IOException e) {
            e.printStackTrace();
            return new FailureResult(new RuntimeException(
                    "Failed to send file", e));

        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                mConn.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public synchronized void cancel() {
        super.cancel();
        if (mConn != null) {
            try {
                mConn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        if (!result.isSuccess()) {
            Transaction trans = getTransaction();
            try {
                JSONObject params = new JSONObject();
                params.put("receiver", trans.getReceiver());
                params.put("transaction", trans.getId());
                DeltaResources.push(getContext(), "notify_fail", params,
                        new Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                            }

                        }, new ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }

                        });
            } catch (JSONException e) {
            }
        }
        super.onPostExecute(result);
    }
}
