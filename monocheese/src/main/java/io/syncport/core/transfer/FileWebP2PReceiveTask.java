package io.syncport.core.transfer;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.transfer.webp2p.Http;
import io.syncport.ui.NavigationDrawerActivity;
import io.syncport.ui.NavigationDrawerFragment;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

public class FileWebP2PReceiveTask extends BaseFileTask {

    private static final String TAG = "FileWebP2PReceiveTask";
    private String mNotiTitleTextFormat;
    private String mNotiProgressTextFormat;
    private long mFilesize;
    private Socket mConn;
    private String mBoundary;

    public FileWebP2PReceiveTask(Context context, Transaction trans,
            Socket conn, long filesize, String boundary, boolean showNoti) {
        super(context, trans, showNoti);
        mConn = conn;
        mFilesize = filesize;
        mBoundary = boundary;
    }

    @Override
    protected TaskType taskType() {
        return TaskType.RECEIVE;
    }

    protected String getNotiTitleText() {
        if (mNotiTitleTextFormat == null) {
            mNotiTitleTextFormat = getContext().getString(
                    R.string.desc_file_receive__format);
        }
        return String.format(mNotiTitleTextFormat, getTransaction()
                .getFilename());
    }

    @Override
    protected String getNotiProgressText(Progress prog) {
        if (mNotiProgressTextFormat == null) {
            mNotiProgressTextFormat = getContext().getString(
                    R.string.desc_file_receive_progress__format);
        }

        return String.format(mNotiProgressTextFormat,
                FileUtils.readableFileSize(prog.doneLength),
                FileUtils.readableFileSize(prog.fileLength));
    }

    @Override
    protected String getNotiPrepareText() {
        return getContext().getString(R.string.desc_file_receive_prepare);
    }

    @Override
    protected Intent getNotiIntent() {
        return NavigationDrawerActivity.createIntent(getContext(),
                NavigationDrawerFragment.MenuType.FILE_TRANSFER);
    }

    @Override
    protected Result doInBackground() {
        Transaction trans = getTransaction();
        String filename = trans.getFilename();

        File file;
        File parentDir;
        if (TextUtils.isEmpty(trans.getPathTo())) {
            parentDir = new File(
                    FileUtils.getDefaultDownloadPath(getContext()));
        } else {
            parentDir = new File(trans.getPathTo());
        }
        int mkdirTryCount = 10;
        while (!parentDir.exists() && mkdirTryCount-- > 0) {
            parentDir.mkdirs();
        }

        file = new File(parentDir, filename);
        if (file.exists()) {
            file = FileUtils.getAlternativeFile(file);
        }
//      try {
//          file.createNewFile();
//      } catch (IOException e) {
//          Log.e(TAG, "Failed to create file " + file.getAbsolutePath()
//                  + ", transaction : " + getTransaction().getId());
//          e.printStackTrace();
//          try {
//              mConn.close();
//          } catch (IOException e2) {
//          }
//          return new FailureResult(e);
//      }

        BufferedOutputStream fileOutputStream = null;
        Exception exception = null;

        try {
            InputStream is = mConn.getInputStream();
            OutputStream os = mConn.getOutputStream();

            // Go reading with recv timeout 3.0 sec. (see `ready` method)
            long bytesRead = 0;
            int chunkBytesRead;
            int bufSize = 65536;
            byte[] buf;
            fileOutputStream = null;
            fileOutputStream = new BufferedOutputStream(new FileOutputStream(
                    file));

            while (bytesRead < mFilesize && !isCancelled()) {
                if (mFilesize - bytesRead < bufSize) {
                    bufSize = (int) (mFilesize - bytesRead);
                }
                buf = new byte[bufSize];
                if ((chunkBytesRead = is.read(buf)) < 0) {
                    throw new SocketException(
                            "socket closed while write to file");
                }
                bytesRead += chunkBytesRead;
                fileOutputStream.write(buf, 0, chunkBytesRead);
                int progPercent = (int) ((bytesRead * 100) / mFilesize);
                publishProgress(new Progress(mFilesize, bytesRead, progPercent));
            }

            if (!isCancelled()) {

                fileOutputStream.flush();
                fileOutputStream.close();

                // We should read last form delimeter ('--boundary--')
                // 1 bytes read is suitable here because boundary is not that
                // long.
                bytesRead = 0;
                String lastRead = "";
                while (bytesRead < (mBoundary.length() + 8)) {
                    buf = new byte[1];
                    int read = is.read(buf);
                    if (read < 0) {
                        break;
                    }
                    bytesRead += read;
                    lastRead += new String(buf);
                }

                // Write HTTP response.
                String successHeader = Http.getSuccessHeader();
                os.write(successHeader.getBytes());
            }
            is.close();
            os.close();

            mConn.close();
            Log.d(TAG, "transid:" + trans.getId() + " finished");
        } catch (IOException e) {
            exception = e;
            Log.d(TAG, "transid:" + trans.getId() + " finished with exception "
                    + e.toString());
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (mConn != null) {
                    mConn.close();
                }
            } catch (IOException e1) {
            }
            if (exception != null) {
                file.delete();
                return new FailureResult(new RuntimeException(
                        "Failed to receive file", exception));
            }
        }

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        DeltaResources.endFileTransaction(getContext(), getTransaction()
                .getId(), future, future);
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (isCancelled()) {
            file.delete();
            return new FailureResult(new Exception("cancelled"));
        }
        return new SuccessResult(file);
    }

    @Override
    public synchronized void cancel() {
        super.cancel();
        if (mConn != null) {
            try {
                mConn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        if (!result.isSuccess()) {
            Transaction trans = getTransaction();
            try {
                JSONObject params = new JSONObject();
                params.put("receiver", trans.getSender());
                params.put("transaction", trans.getId());
                DeltaResources.push(getContext(), "notify_fail", params,
                        new Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                            }

                        }, new ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }

                        });
            } catch (JSONException e) {
            }
        } else {
            SuccessResult sResult = (SuccessResult) result;

            File cacheDirPath = getContext().getExternalCacheDir();
            File received = sResult.getFile();

            String path = received.getAbsolutePath();
            if (!path.startsWith(cacheDirPath.getAbsolutePath())) {
                new SingleMediaScanner(getContext(), sResult.getFile());
            }
        }
        super.onPostExecute(result);
    }
}
