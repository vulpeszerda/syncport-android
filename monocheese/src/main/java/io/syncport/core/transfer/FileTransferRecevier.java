package io.syncport.core.transfer;

import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.device.DeviceManager.OnGetDeviceListener;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.FileTransferPollingInfo;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.transfer.WebP2PServerThread.FailureListener;
import io.syncport.core.transfer.WebP2PServerThread.ReadyListener;
import io.syncport.utils.Log;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AuthenticatorException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.android.volley.Response;
import com.android.volley.VolleyError;

public class FileTransferRecevier extends BroadcastReceiver {

    private static final String TAG = "FileTransferReceiver";

    public static IntentFilter getFilter() {
        return new IntentFilter(
                GeneralPollerService.PollerAction.FILE_TRANSFER);
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        FileTransferPollingInfo info = (FileTransferPollingInfo) intent
                .getSerializableExtra(GeneralPollerService.PARAM_DATA);
        final Transaction transaction = info.getTransaction();

        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        final String myDeviceId = account.getDeviceId();

        String peerId;
        if (transaction.getSender().equals(myDeviceId)) {
            peerId = transaction.getReceiver();
        } else {
            peerId = transaction.getSender();
        }

        DeviceManager manager;
        try {
            manager = DeviceManager.from(context);
            manager.getDeviceWithId(peerId, new OnGetDeviceListener() {

                @Override
                public void onGetDevice(Device device) {
                    if (device.getType().equals("WEB")) {
                        tryWebP2P(context, transaction, myDeviceId);
                    } else {
                        doRelay(context, transaction, myDeviceId);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    // pass
                }
            });
        } catch (AuthenticatorException e1) {
            // pass
        }
    }

    private void doRelay(Context context, Transaction transaction,
            String myDeviceId) {
        if (transaction.getSender().equals(myDeviceId)) {
            startRelaySendFile(context, transaction);
        } else if (transaction.getReceiver().equals(myDeviceId)) {
            startRelayReceiveFile(context, transaction);
        } else {
            // not reach
        }
    }

    private void startRelaySendFile(Context context, Transaction trans) {
        String key = BaseFileTask.generateTaskKey(trans,
                BaseFileTask.TaskType.SEND);
        BaseFileTask task = new FileRelaySendTask(context, trans, false);
        FileTaskManager.getInstance(context).putTask(key, task);
    }

    private void startRelayReceiveFile(Context context, Transaction trans) {
        String key = FileRelayReceiveTask.generateTaskKey(trans,
                BaseFileTask.TaskType.RECEIVE);

        BaseFileTask task = new FileRelayReceiveTask(context, trans, false);
        FileTaskManager.getInstance(context).putTask(key, task);
    }

    private void tryWebP2P(final Context context,
            final Transaction transaction, final String myDeviceId) {
        final String peerId = transaction.getSender().equals(myDeviceId) ? transaction
                .getReceiver() : transaction.getSender();

        final Response.Listener<JSONObject> emptyResListener = new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // do noting
            }
        };

        final Response.ErrorListener emptyErrorListener = new RobustResponseErrorListener(
                context) {

            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                Log.e(TAG, msg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                // TODO Auto-generated method stub

            }
        };

        ReadyListener listener = new ReadyListener() {

            @Override
            public void onReady(String publicIp, String privateIp) {
                Log.d(TAG, "P2P server is ready with public:" + publicIp
                        + ", private: " + privateIp);
                try {
                    JSONObject params = new JSONObject();
                    params.put("public", publicIp);
                    params.put("private", privateIp);
                    DeltaResources.push(context, "address", params,
                            emptyResListener, emptyErrorListener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };
        WebP2PServerThread thread = WebP2PServerThread
                .getOrCreateInstance(context);
        thread.addTransaction(peerId, transaction, listener,
                new FailureListener() {
                    @Override
                    public void onFailure(List<Transaction> transactions,
                            boolean doNegotiate, Exception e) {
                        Log.d(TAG, "Failed to open server " + e.toString());
                        if (doNegotiate) {
                            try {
                                JSONObject params = new JSONObject();
                                params.put("protocol", "relaying");
                                params.put("receiver", peerId);
                                DeltaResources.push(context, "negotiation",
                                        params, emptyResListener,
                                        emptyErrorListener);
                            } catch (JSONException e2) {
                                // not reach
                            }
                        }
                        for (Transaction trans : transactions) {
                            doRelay(context, trans, myDeviceId);
                        }
                    }
                });
        synchronized (thread) {
            if (!thread.isAlive() && thread.getState() == Thread.State.NEW) {
                thread.start();
            }
        }
    }
}
