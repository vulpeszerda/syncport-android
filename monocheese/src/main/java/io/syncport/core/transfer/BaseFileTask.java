package io.syncport.core.transfer;

import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.ui.addon.NotificationFactory;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

public abstract class BaseFileTask {

    public static interface TaskProgressListener {
        void onPrepare();

        void onProgressUpdated(Progress prog);

        void onFinished(Result result);

        void onCancelled();
    }
    
    public static interface PublishObserver {
        void publishProgress(long fileLen, long sentByteLen, int percent);
    }

    public static enum State {
        PREPARE, PROGRESS, FINISHED, CANCELLED
    }

    public static enum TaskType {
        RECEIVE, SEND
    }

    public static class Progress {
        long fileLength;
        long doneLength;
        int percent;

        public Progress(long fileLength, long doneLength, int percent) {
            this.fileLength = fileLength;
            this.doneLength = doneLength;
            this.percent = percent;
        }

        public int getPercent() {
            return percent;
        }

        public long getFileLength() {
            return fileLength;
        }

        public long getDoneLength() {
            return doneLength;
        }
    }

    public static class Result {
        private final boolean success;

        public Result(boolean success) {
            this.success = success;
        }

        public boolean isSuccess() {
            return success;
        }
    }

    public static class SuccessResult extends Result {
        final File file;

        public SuccessResult(File file) {
            super(true);
            this.file = file;
        }

        public File getFile() {
            return file;
        }
    }

    public static class FailureResult extends Result {
        private final Exception exception;

        public FailureResult(Exception e) {
            super(false);
            this.exception = e;
        }

        public Exception getException() {
            return exception;
        }
    }

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "BaseRelayFileTask #"
                    + mCount.getAndIncrement());
        }
    };

    private static final AtomicInteger sCount = new AtomicInteger(1);

    private static final int MESSAGE_POST_PROGRESS = 1;
    private static final int MESSAGE_POST_RESULT = 2;

    private static final int MIN_DISPLAY_PROGRESS_PERCENT_DIFF = 5;
    private static final long MIN_DISPLAY_PROGRESS_FILELEN_DIFF = 1000 * 1000;
    private static final long MIN_DISPLAY_PROGRESS_TIME_DIFF = 500;

    private final Thread mTaskThread;
    private final Handler mHandler;

    private final Context mContext;
    private final Transaction mTransaction;

    private boolean mIsCancelled;
    private int mWakeLockId;

    private TaskType mTaskType;
    private State mTaskState;
    private Progress mProgress;
    private Result mResult;
    private List<TaskProgressListener> mObservers;

    private boolean mShowNoti;
    private NotificationCompat.Builder mNotificationBuilder;
    private NotificationManagerCompat mNotiManager;
    private int mNotiId;

    private int mLastPublishProgPercent = 0;
    private long mLastPublishProgFileLen = 0;
    private long mLastPublishProgTime = -1;

    protected abstract TaskType taskType();

    public BaseFileTask(Context context, Transaction trans,
            boolean showNoti) {
        mTaskThread = sThreadFactory.newThread(mTaskRunnable);
        mHandler = new Handler(Looper.getMainLooper(), new Callback() {

            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                case MESSAGE_POST_PROGRESS:
                    onProgressUpdate((Progress) msg.obj);
                    break;
                case MESSAGE_POST_RESULT:
                    onFinish((Result) msg.obj);
                    break;
                }
                return false;
            }
        });

        mIsCancelled = false;

        mTaskState = State.PREPARE;
        mTaskType = taskType();
        mContext = context;
        mTransaction = trans;
        mObservers = new ArrayList<TaskProgressListener>();

        mShowNoti = showNoti;
        mNotiManager = NotificationManagerCompat.from(context);
    }
    
    private final Runnable mTaskRunnable = new Runnable() {
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            postResult(doInBackground());
        }
    };

    protected void publishProgress(Progress prog) {
        if (!isCancelled()) {
            long currentTime = (new Date()).getTime();
            if (prog.percent - mLastPublishProgPercent > MIN_DISPLAY_PROGRESS_PERCENT_DIFF
                    || prog.doneLength - mLastPublishProgFileLen > MIN_DISPLAY_PROGRESS_FILELEN_DIFF
                    || mLastPublishProgTime < 0
                    || currentTime - mLastPublishProgTime > MIN_DISPLAY_PROGRESS_TIME_DIFF) {

                mLastPublishProgPercent = prog.percent;
                mLastPublishProgFileLen = prog.doneLength;
                mLastPublishProgTime = currentTime;
                mHandler.obtainMessage(MESSAGE_POST_PROGRESS, prog)
                        .sendToTarget();
            }
        }
    }

    private void postResult(Result result) {
        Message message = mHandler.obtainMessage(MESSAGE_POST_RESULT, result);
        message.sendToTarget();
    }

    public synchronized void cancel() {
        mIsCancelled = true;
    }

    public synchronized boolean isCancelled() {
        return mIsCancelled;
    }

    private void onFinish(Result result) {
        if (isCancelled()) {
            onCancelled();
        } else {
            onPostExecute(result);
        }
    }

    public void execute() {
        onPreExecute();
        mTaskThread.start();
    }

    protected abstract String getNotiTitleText();

    protected abstract String getNotiProgressText(Progress prog);

    protected abstract String getNotiPrepareText();

    protected abstract Intent getNotiIntent();

    protected abstract Result doInBackground();

    public void setWakeLockId(int id) {
        mWakeLockId = id;
    }

    public int getWakeLockId() {
        return mWakeLockId;
    }

    protected Context getContext() {
        return mContext;
    }

    public Transaction getTransaction() {
        return mTransaction;
    }

    public synchronized State getState() {
        return mTaskState;
    }

    public synchronized Progress getProgress() {
        return mProgress;
    }

    public Result getResult() {
        return mResult;
    }

    public TaskType getTaskType() {
        return mTaskType;
    }

    protected void onPreExecute() {
        synchronized (mObservers) {
            for (TaskProgressListener listener : mObservers) {
                listener.onPrepare();
            }
        }

        String title = getNotiTitleText();
        Intent intent = getNotiIntent();
        String prepareText = getNotiPrepareText();

        if (mShowNoti) {
            mNotificationBuilder = NotificationFactory
                    .createDefaultNotiBuilder(getContext(),
                            NotificationFactory.REQUEST_CODE_TRANSFER, intent,
                            title, prepareText, null, false);
            mNotificationBuilder.setProgress(0, 0, true).setOngoing(true)
                    .setAutoCancel(false);
            mNotiId = NotificationFactory.getNewTransferId();
            mNotiManager.notify(mNotiId, mNotificationBuilder.build());
        }
    }

    protected void onProgressUpdate(Progress value) {
        mProgress = value;
        if (mTaskState != State.PROGRESS) {
            mTaskState = State.PROGRESS;
        }
        synchronized (mObservers) {
            for (TaskProgressListener listener : mObservers) {
                listener.onProgressUpdated(mProgress);
            }
        }

        if (mShowNoti) {
            int progPercent = (int) (mProgress.doneLength * 100 / mProgress.fileLength);
            String text = getNotiProgressText(mProgress);
            mNotificationBuilder.setContentText(text)
                    .setProgress(100, progPercent, false).setTicker(null);
            mNotiManager.notify(mNotiId, mNotificationBuilder.build());
        }
    }

    protected void onPostExecute(Result result) {
        mResult = result;
        mTaskState = State.FINISHED;
        synchronized (mObservers) {
            List<TaskProgressListener> observers = new ArrayList<TaskProgressListener>(
                    mObservers);
            for (TaskProgressListener listener : observers) {
                listener.onFinished(result);
            }
        }
        if (mShowNoti) {
            mNotiManager.cancel(mNotiId);
        }
    }

    protected void onCancelled() {
        mResult = null;
        mTaskState = State.CANCELLED;
        synchronized (mObservers) {
            List<TaskProgressListener> observers = new ArrayList<TaskProgressListener>(
                    mObservers);
            for (TaskProgressListener listener : observers) {
                listener.onCancelled();
            }
        }
        if (mShowNoti) {
            mNotiManager.cancel(mNotiId);
        }
    }

    public void registerTaskObserver(TaskProgressListener listener) {
        synchronized (mObservers) {
            mObservers.add(listener);
        }
    }

    public void unregisterTaskObserver(TaskProgressListener listener) {
        synchronized (mObservers) {
            mObservers.remove(listener);
        }
    }

    public static String generateTaskKey(String peerId, String parentPathFrom,
            String filename) {
        return generateTaskKey(peerId, parentPathFrom, "", filename);
    }

    public static String generateTaskKey(Transaction trans, TaskType type) {
        String peerId = type == TaskType.RECEIVE ? trans.getSender() : trans
                .getReceiver();
        return generateTaskKey(peerId, trans.getPathFrom(), trans.getPathTo(),
                trans.getFilename());
    }

    public static String generateTaskKey(String peerId, String parentPathFrom,
            String parentPathTo, String filename) {
        String key = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(peerId.getBytes());
            md.update(parentPathFrom.getBytes());
            md.update(parentPathTo.getBytes());
            md.update(filename.getBytes());
            StringBuffer hexString = new StringBuffer();
            byte[] hash = md.digest();

            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0"
                            + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            key = hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return key;
    }
}
