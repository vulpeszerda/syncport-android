package io.syncport.core.transfer;

import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.polling.model.NegotiationPollingInfo;
import io.syncport.core.transfer.webp2p.Http;
import io.syncport.core.transfer.webp2p.SockAddr;
import io.syncport.utils.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.content.LocalBroadcastManager;

public class WebP2PServerThread extends Thread implements
        InternetConnectivityManager.Observer {

    public static interface ReadyListener {
        void onReady(String publicIp, String privateIp);
    }

    public static interface FailureListener {
        void onFailure(List<Transaction> transactions, boolean doNegotiate,
                Exception e);
    }

    private static final String TAG = "WebP2PSeverThread";
    private static final int SERVER_SOCKET_TIMEOUT = 0;
    private static final int SOCKET_TIMEOUT = 20000;
    private static final int TRANSACTION_WAIT_TIMEOUT = 20000;
    private static final String PUBLIC_IP_CHECK_URL = "http://checkip.amazonaws.com/";
    private static WebP2PServerThread sThread;
    private static Object sThreadLock = new Object();

    private Object mServerReadyLock = new Object();
    private ServerSocket mSock;
    private SockAddr mOurAddr;
    private String mPrivateIp;
    private String mPublicIp;
    private boolean mServerReady = false;
    private Map<String, TransactionMap> mWaiters;
    private Context mContext;
    private Handler mHandler;
    private InternetConnectivityManager mInternetManager;
    private LocalBroadcastManager mBroadcastManager;

    private WakeLock mWakeLock;

    private BroadcastReceiver mHandshakeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            NegotiationPollingInfo info = (NegotiationPollingInfo) intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            String peerId = info.getSender();
            synchronized (mWaiters) {
                TransactionMap map = mWaiters.get(peerId);
                if (map != null) {
                    map.fireFailureAndRemove(false,
                            new Exception("Negotiation received with protocol "
                                    + info.getProtocol()));
                }
            }
        }
    };

    public static WebP2PServerThread getOrCreateInstance(Context context) {
        synchronized (sThreadLock) {
            if (sThread == null || sThread.isInterrupted()
                    || !sThread.isAlive()) {
                sThread = new WebP2PServerThread(context);
            }
        }
        return sThread;
    }

    public WebP2PServerThread(Context context) {
        mContext = context;
        mHandler = new Handler(Looper.getMainLooper());
        mWaiters = new HashMap<String, TransactionMap>();

        PowerManager pm = (PowerManager) context
                .getSystemService(Context.POWER_SERVICE);
        if (mWakeLock == null) {
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "WebP2PServerWakeLock");
        }

        mInternetManager = InternetConnectivityManager.getInstance(context);

        IntentFilter filter = new IntentFilter(
                GeneralPollerService.PollerAction.NEGOTIATION);
        mBroadcastManager = LocalBroadcastManager.getInstance(context);
        mBroadcastManager.registerReceiver(mHandshakeReceiver, filter);
    }

    private void notifyFailure(final Exception e) {
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                synchronized (mWaiters) {
                    ArrayList<TransactionMap> maps = new ArrayList<TransactionMap>(
                            mWaiters.values());
                    for (TransactionMap map : maps) {
                        map.fireFailureAndRemove(true, e);
                    }
                }
            }
        });
    }

    private final AtomicBoolean mIsStarted = new AtomicBoolean(false);

    @Override
    public synchronized void start() {
        if (!mIsStarted.getAndSet(true)) {
            super.start();
        }
    }

    @Override
    public void run() {
        mWakeLock.acquire();
        try {
            if (mInternetManager.isWifiConnected()) {
                String privateIp = getWifiIPAddress(mContext);
                if (privateIp == null) {
                    throw new IOException("failed to get private ip");
                }
                mOurAddr = new SockAddr(privateIp, 61001);
                URL whatismyip = new URL(PUBLIC_IP_CHECK_URL);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        whatismyip.openStream()));

                String publicIp = in.readLine();
                in.close();
                mPublicIp = publicIp;
                mPrivateIp = mOurAddr.getIp();
                synchronized (mServerReadyLock) {
                    mServerReady = true;
                }
            } else {
                throw new IOException("wifi is not connected");
            }
            mSock = new ServerSocket(mOurAddr.getPort(), 1, mOurAddr.getAddr());
            mSock.setReuseAddress(true);
            mSock.setSoTimeout(SERVER_SOCKET_TIMEOUT);
            Log.d(TAG, "server opened");
            synchronized (mWaiters) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        synchronized (mWaiters) {
                            ArrayList<TransactionMap> maps = new ArrayList<TransactionMap>(
                                    mWaiters.values());
                            for (TransactionMap map : maps) {
                                map.fireReady();
                            }
                        }
                    }
                });
            }
            while (!isInterrupted()) {
                synchronized (mWaiters) {
                    if (mWaiters.size() == 0) {
                        break;
                    }
                }
                final Socket conn = mSock.accept();
                new WakefulThread(mContext, new Runnable() {

                    @Override
                    public void run() {
                        ready(conn);
                    }

                }).start();
            }
            mSock.close();
        } catch (IOException e) {
            if (mSock != null) {
                try {
                    mSock.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            notifyFailure(e);
        }
        Log.d(TAG, "server closed");
        mBroadcastManager.unregisterReceiver(mHandshakeReceiver);
        mWakeLock.release();
    }

    private void ready(Socket conn) {
        String readBuf = "";
        try {
            byte[] buf = null;
            InputStream is = null;
            OutputStream os = null;

            conn.setSoTimeout(SOCKET_TIMEOUT);
            is = conn.getInputStream();

            int read;
            while (!readBuf.contains("\r\n\r\n")) {
                // Read full header.
                buf = new byte[1];
                read = is.read(buf);
                if (read < 0) {
                    throw new SocketException("socket closed while read header");
                }
                readBuf += new String(buf);
            }

            String maybeMethod = readBuf;

            if (maybeMethod.contains("OPTIONS")) {
                Log.d(TAG, "received OPTIONS");
                // XXX: handle CORS.
                os = conn.getOutputStream();
                os.write(Http.getCORSHeader().getBytes());
                conn.close();
                return;
            }

            if (maybeMethod.contains("HEAD")) {
                Log.d(TAG, "received HEAD");
                os = conn.getOutputStream();
                os.write(Http.getSuccessHeader().getBytes());
                conn.close();
                return;
            }

            if (!maybeMethod.contains("GET") && !maybeMethod.contains("POST")) {
                // Invalid request.
                conn.close();
                return;
            }

            Transaction trans;
            if (maybeMethod.contains("GET")) {
                String transIdVal = Http.retreiveGETParams(readBuf,
                        "transaction");
                if (transIdVal.length() != 36) {
                    conn.close();
                    return;
                }
                String transId = transIdVal.substring(4);

                boolean forceDl = true;
                String forceDlVal = Http.retreiveGETParams(readBuf, "force_dl");
                if (forceDlVal != null) {
                    forceDlVal = forceDlVal.trim();
                    try {
                        int dl = Integer.parseInt(forceDlVal);
                        if (dl == 0) {
                            forceDl = false;
                        }
                    } catch (NumberFormatException e) {
                        // do nothing
                    }
                }

                Log.d(TAG, "received GET with transId:" + transId);
                if ((trans = popTransaction(transId)) == null) {
                    // Invalid transaction id.
                    Log.e(TAG, "invalid transaction id : " + transId);
                    conn.close();
                    return;
                }
                String filename = Http.retreiveGETParams(readBuf, "filename");
                filename = filename.trim();
                filename = URLDecoder.decode(filename, "UTF-8");
                final FileWebP2PSendTask task = new FileWebP2PSendTask(
                        mContext, trans, conn, filename, false, forceDl);
                final String key = BaseFileTask.generateTaskKey(trans,
                        BaseFileTask.TaskType.SEND);

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        FileTaskManager.getInstance(mContext)
                                .putTask(key, task);

                    }

                });
            } else {
                String boundary = Http.getHeaderValue(readBuf, "Content-Type")
                        .split("boundary=")[1];

                // Start reading until actual file content starts.
                // HTTP POST request has two form keys (key, filesize)
                // XXX: Reading with bufsize 1 makes sense here.

                while (Http.countHTTPEscape(readBuf) < 4) {
                    buf = new byte[1];
                    read = is.read(buf);
                    if (read < 0) {
                        throw new SocketException(
                                "socket closed while read post");
                    }
                    readBuf += new String(buf);
                }

                Matcher m = Pattern.compile("\\r\\n\\r\\n(.*)\\r\\n--")
                        .matcher(readBuf);
                // XXX: Skip transactionId, filesize validation here
                // Because parsing POST form is too tiresome.
                m.find();
                String transIdVal = m.group(1);
                m.find();
                String fileSize = m.group(1);

                if (transIdVal.length() != 36) {
                    conn.close();
                    return;
                }
                String transId = transIdVal.substring(4);
                Log.d(TAG, "received POST with transId:" + transId);

                if ((trans = popTransaction(transId)) == null) {
                    // Invalid transaction id.
                    Log.d(TAG, "invalid transaction id : " + transId);
                    conn.close();
                    return;
                }
                long fileLen = Long.valueOf(fileSize);

                final FileWebP2PReceiveTask task = new FileWebP2PReceiveTask(
                        mContext, trans, conn, fileLen, boundary, false);
                final String key = BaseFileTask.generateTaskKey(trans,
                        BaseFileTask.TaskType.RECEIVE);

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        FileTaskManager.getInstance(mContext)
                                .putTask(key, task);

                    }

                });

            }
        } catch (IOException e) {
            e.printStackTrace();
            return;

        }
    }

    public void addTransaction(String peerId, Transaction trans,
            ReadyListener readyListener, FailureListener listener) {
        synchronized (mWaiters) {
            TransactionMap map = mWaiters.get(peerId);
            if (map == null) {
                map = new TransactionMap(peerId, listener);
                map.setReadyListener(readyListener);
                mWaiters.put(peerId, map);
            }
            map.put(trans.getId(), trans);
        }
    }

    public void removeTransactionsWithDeviceId(String deviceId) {
        synchronized (mWaiters) {
            TransactionMap list = mWaiters.remove(deviceId);
            if (list != null) {
                list.cancelConnectionTimeout();
                list.cancelDestroyTimeout();
            }
            if (mWaiters.size() == 0 && mSock != null) {
                try {
                    mSock.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public Transaction popTransaction(String transId) {
        int retryCount = 6;
        try {
            while (retryCount-- > 0) {
                synchronized (mWaiters) {
                    ArrayList<TransactionMap> maps = new ArrayList<TransactionMap>(
                            mWaiters.values());
                    for (TransactionMap map : maps) {
                        if (map.get(transId) != null) {
                            Transaction trans = map.remove(transId);
                            map.cancelConnectionTimeout();
                            return trans;
                        }
                    }
                }
                sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onInternetConnected() {

    }

    @Override
    public void onInternetDisconnected() {

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {
        if (!isWifi) {
            interrupt();
        }
    }

    private static String getWifiIPAddress(Context context) {
        WifiManager wm = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wm.getConnectionInfo();
        int ipAddress = wifiInf.getIpAddress();
        if (ipAddress == 0) {
            return null;
        }
        return String.format("%d.%d.%d.%d", (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
    }

    private class TransactionMap extends HashMap<String, Transaction> {
        /**
         *
         */
        private static final int MAX_CONCURRENT_TRANSACTION_WAIT_TIMEOUT = 10000;
        private static final long serialVersionUID = 1L;
        private ReadyListener mReadyListener;
        private FailureListener mFailureListener;
        private String mPeerId;
        private Timer mTimer;
        private Timer mDestroyTimer;

        public TransactionMap(String peerId, FailureListener listener) {
            super();
            mPeerId = peerId;
            mFailureListener = listener;
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    fireFailureAndRemove(true, new TimeoutException("Timeout"));
                }
            }, TRANSACTION_WAIT_TIMEOUT);
        }

        public void cancelConnectionTimeout() {
            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }
        }

        public void cancelDestroyTimeout() {
            if (mDestroyTimer != null) {
                mDestroyTimer.cancel();
                mDestroyTimer = null;
            }
        }

        public void setReadyListener(ReadyListener listener) {
            mReadyListener = listener;
        }

        @Override
        public synchronized int size() {
            return super.size();
        }

        @Override
        public Transaction put(String key, Transaction value) {
            synchronized (mServerReadyLock) {
                if (mServerReady) {
                    fireReady();
                }
            }
            cancelDestroyTimeout();
            synchronized (this) {
                return super.put(key, value);
            }
        }

        @Override
        public synchronized Transaction remove(Object key) {
            Transaction trans = super.remove(key);
            if (trans != null && size() == 0) {
                cancelDestroyTimeout();
                mDestroyTimer = new Timer();
                mDestroyTimer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        boolean isEmpty = size() == 0;
                        if (isEmpty) {
                            removeTransactionsWithDeviceId(mPeerId);
                        }
                    }

                }, MAX_CONCURRENT_TRANSACTION_WAIT_TIMEOUT);
            }
            return trans;
        }

        public void fireReady() {
            if (mReadyListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        mReadyListener.onReady(mPublicIp, mPrivateIp);
                    }

                });
            }
        }

        public void fireFailureAndRemove(final boolean doNegotiate,
                final Exception e) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    ArrayList<Transaction> transactions = new ArrayList<Transaction>(
                            TransactionMap.this.values());
                    mFailureListener.onFailure(transactions, doNegotiate, e);
                }
            });
            removeTransactionsWithDeviceId(mPeerId);
        }

        public String getPeerId() {
            return mPeerId;
        }
    }

    private class WakefulThread extends Thread {

        private final WakeLock mWakeLock;
        private final Runnable mRunnable;

        public WakefulThread(Context context, Runnable runnable) {
            PowerManager pm = (PowerManager) context
                    .getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "WebP2PServerWakeLock-request");
            mRunnable = runnable;
        }

        @Override
        public void run() {
            mWakeLock.acquire(30 * 1000);
            mRunnable.run();
            mWakeLock.release();
        }
    }
}
