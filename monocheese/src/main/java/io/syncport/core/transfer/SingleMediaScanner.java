package io.syncport.core.transfer;

import java.io.File;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;

public class SingleMediaScanner implements MediaScannerConnectionClient {

    private MediaScannerConnection mConn;
    private File mFile;

    public SingleMediaScanner(Context context, File file) {
        mFile = file;
        mConn = new MediaScannerConnection(context, this);
        mConn.connect();
    }

    @Override
    public void onMediaScannerConnected() {
        mConn.scanFile(mFile.getAbsolutePath(), null);
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        mConn.disconnect();
    }
}
