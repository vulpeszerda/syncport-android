package io.syncport.core.transfer;

import io.syncport.core.transfer.BaseFileTask.Progress;
import io.syncport.core.transfer.BaseFileTask.Result;
import io.syncport.core.transfer.BaseFileTask.TaskProgressListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.PowerManager;
import android.util.SparseArray;

public class FileTaskManager {

    public static interface Observer {
        void onAddTask(String tag, BaseFileTask task);

        void onRemoveTask(String tag, BaseFileTask task);
    }

    public static FileTaskManager getInstance(Context context) {
        if (manager == null) {
            manager = new FileTaskManager(context);
        }
        return manager;
    }


    private static FileTaskManager manager;

    private Context mContext;
    private HashMap<String, BaseFileTask> mTasks;
    private List<Observer> mObservers;
    
    // wake lock variables
    private final SparseArray<PowerManager.WakeLock> mActiveWakeLocks;
    private int mNextId;

    private FileTaskManager(Context context) {
        mContext = context;
        mTasks = new HashMap<String, BaseFileTask>();
        mObservers = new ArrayList<Observer>();
        mActiveWakeLocks = new SparseArray<PowerManager.WakeLock>();
        mNextId = 1;
    }
    
    public void addObserver(Observer ob) {
        synchronized (mObservers) {
            mObservers.add(ob);
        }
    }

    public void removeObserver(Observer ob) {
        synchronized (mObservers) {
            mObservers.remove(ob);
        }
    }
    
    public void putTask(final String tag, final BaseFileTask task) {
        task.registerTaskObserver(new TaskProgressListener() {

            @Override
            public void onProgressUpdated(Progress prog) {
                // do nothing
            }

            @Override
            public void onPrepare() {
                // do nothing
            }

            @Override
            public void onFinished(Result result) {
                onTaskFinished(tag, result);
                task.unregisterTaskObserver(this);
            }

            @Override
            public void onCancelled() {
                onTaskFinished(tag, null);
                task.unregisterTaskObserver(this);
            }
            
            
        });
        synchronized (mTasks) {
            BaseFileTask prevTask = mTasks.get(tag);
            if (prevTask != null) {
                // do nothing
                return;
            }
            mTasks.put(tag, task);
        }

        synchronized (mObservers) {
            ArrayList<Observer> observers = new ArrayList<Observer>(mObservers);
            for (Observer ob : observers) {
                ob.onAddTask(tag, task);
            }
        }
        synchronized (mActiveWakeLocks) {
            int id = mNextId;
            mNextId++;
            if (mNextId <= 0) {
                mNextId = 1;
            }
            task.setWakeLockId(id);
            task.execute();

            PowerManager pm = (PowerManager) mContext
                    .getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(
                    PowerManager.PARTIAL_WAKE_LOCK, "wake:"
                            + task.getClass().getSimpleName());
            wl.setReferenceCounted(false);
            wl.acquire();
            mActiveWakeLocks.put(id, wl);
        }
    }

    public BaseFileTask getTask(String tag) {
        synchronized (mTasks) {
            return mTasks.get(tag);
        }
    }

    public List<BaseFileTask> getAllTasks() {
        synchronized (mTasks) {
            return new ArrayList<BaseFileTask>(mTasks.values());
        }
    }


    public void onTaskFinished(String tag, Result result) {
        BaseFileTask task;
        synchronized (mTasks) {
            task = mTasks.remove(tag);
        }
        synchronized (mObservers) {
            ArrayList<Observer> observers = new ArrayList<Observer>(mObservers);
            for (Observer ob : observers) {
                ob.onRemoveTask(tag, task);
            }
        }
        releaseWakeLock(task.getWakeLockId());
    }

    private boolean releaseWakeLock(int id) {
        synchronized (mActiveWakeLocks) {
            PowerManager.WakeLock wl = mActiveWakeLocks.get(id);
            if (wl != null) {
                wl.release();
                mActiveWakeLocks.remove(id);
                return true;
            }
            return false;
        }
    }
}
