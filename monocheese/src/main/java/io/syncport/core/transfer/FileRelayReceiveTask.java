package io.syncport.core.transfer;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutionException;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.ui.NavigationDrawerActivity;
import io.syncport.ui.NavigationDrawerFragment;
import io.syncport.utils.FileUtils;

public class FileRelayReceiveTask extends BaseFileTask {

    private static final String TAG = "RelayFileReceiverTask";

    private static final int SOCKET_READ_TIMEOUT = 500000;
    private static final int MAX_RETRY_COUNT_ON_SOCKET_TIMEOUT = 0;

    private String mNotiTitleTextFormat;
    private String mNotiProgressTextFormat;

    private Socket mSocket;
    private int mRetryCountOnSocketTimeout = MAX_RETRY_COUNT_ON_SOCKET_TIMEOUT;

    @Override
    protected TaskType taskType() {
        return TaskType.RECEIVE;
    }

    public FileRelayReceiveTask(Context context, Transaction trans) {
        this(context, trans, false);
    }

    public FileRelayReceiveTask(Context context, Transaction trans,
            boolean showNoti) {
        super(context, trans, showNoti);
    }

    @Override
    protected String getNotiTitleText() {
        if (mNotiTitleTextFormat == null) {
            mNotiTitleTextFormat = getContext().getString(
                    R.string.desc_file_receive__format);
        }
        return String.format(mNotiTitleTextFormat, getTransaction()
                .getFilename());
    }

    @Override
    protected String getNotiProgressText(Progress prog) {
        if (mNotiProgressTextFormat == null) {
            mNotiProgressTextFormat = getContext().getString(
                    R.string.desc_file_receive_progress__format);
        }

        return String.format(mNotiProgressTextFormat,
                FileUtils.readableFileSize(prog.doneLength),
                FileUtils.readableFileSize(prog.fileLength));
    }

    @Override
    protected String getNotiPrepareText() {
        return getContext().getString(R.string.desc_file_receive_prepare);
    }

    @Override
    protected Intent getNotiIntent() {
        return NavigationDrawerActivity.createIntent(getContext(),
                NavigationDrawerFragment.MenuType.FILE_TRANSFER);
    }

    @Override
    protected Result doInBackground() {
        Transaction trans = getTransaction();
        String peerDeviceId = trans.getSender();
        String filename = trans.getFilename();

        File parentFile;
        File file;
        if (TextUtils.isEmpty(trans.getPathTo())) {
            parentFile = new File(
                    FileUtils.getDefaultDownloadPath(getContext()));
        } else {
            parentFile = new File(trans.getPathTo());
        }

        int mkdirTryCount = 10;
        while (!parentFile.exists() && mkdirTryCount-- > 0) {
            parentFile.mkdirs();
        }
        file = new File(parentFile, filename);
        if (file.exists()) {
            file = FileUtils.getAlternativeFile(file);
        }

        mSocket = null;
        BufferedOutputStream sockOutputStream = null;
        BufferedInputStream sockInputStream = null;
        BufferedOutputStream fileOutputStream = null;

        int retryConnectCount = 5;
        boolean socketConnected = false;
        while (retryConnectCount-- > 0) {
            try {
                mSocket = new Socket();
                mSocket.connect(new InetSocketAddress(trans.getRelayNodeHost(),
                        trans.getRelayNodePort()), 3000);
                sockOutputStream = new BufferedOutputStream(
                        mSocket.getOutputStream());
                socketConnected = true;
                break;
            } catch (SocketTimeoutException e) {
                // skip
            } catch (UnknownHostException e) {
                file.delete();
                return new FailureResult(new RuntimeException(
                        "Failed to connect relay server", e));
            } catch (IOException e) {
                file.delete();
                return new FailureResult(new RuntimeException(
                        "Failed to create socket", e));
            }
        }
        if (!socketConnected) {
            file.delete();
            return new FailureResult(new SocketTimeoutException(
                    "Failed to connect to server"));
        }

        // write header
        Exception exception = null;
        boolean retryOnSocketTimeout = true;

        try {
            mSocket.setSoTimeout(SOCKET_READ_TIMEOUT);

            String header = String.format("recv%s", getTransaction().getId());
            sockOutputStream.write(header.getBytes("UTF-8"));
            sockOutputStream.flush();

            sockInputStream = new BufferedInputStream(mSocket.getInputStream());

            // read file len
            byte[] fileLenBytes = new byte[8];
            int readCount = sockInputStream.read(fileLenBytes, 4, 4);
            retryOnSocketTimeout = false;
            if (readCount == -1) {
                throw new SocketException("file length is 0");
            }

            long fileLen = ByteBuffer.wrap(fileLenBytes).getLong();

            fileOutputStream = new BufferedOutputStream(new FileOutputStream(
                    file));

            byte[] buffer = new byte[4096];
            long readFileLen = 0;
            int readByteLen = 0;
            while (!isCancelled()
                    && (readByteLen = sockInputStream.read(buffer)) != -1) {
                readFileLen += readByteLen;
                fileOutputStream.write(buffer, 0, readByteLen);
                int progPercent = (int) (readFileLen * 100 / fileLen);
                publishProgress(new Progress(fileLen, readFileLen, progPercent));

                if (readFileLen == fileLen) {
                    break;
                }
            }
            fileOutputStream.flush();
            if (!isCancelled() && readFileLen != fileLen) {
                throw new SocketException(
                        "Socket disconnected while receiving file");
            }
        } catch (IOException e) {
            e.printStackTrace();
            exception = e;
        } finally {
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (sockOutputStream != null) {
                    sockOutputStream.close();
                }
                if (sockInputStream != null) {
                    sockInputStream.close();
                }
                if (mSocket != null) {
                    mSocket.close();
                }
            } catch (IOException e1) {
            }
            if (exception != null) {
                file.delete();
                if (exception instanceof SocketTimeoutException
                        && retryOnSocketTimeout
                        && --mRetryCountOnSocketTimeout > 0) {
                    return doInBackground();
                }
                return new FailureResult(new RuntimeException(
                        "Failed to receive file", exception));
            }
        }

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        DeltaResources.endFileTransaction(getContext(), getTransaction()
                .getId(), future, future);
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (isCancelled()) {
            file.delete();
            return new FailureResult(new Exception("cancelled"));
        }
        return new SuccessResult(file);
    }

    @Override
    public synchronized void cancel() {
        super.cancel();
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        if (!result.isSuccess()) {
            Transaction trans = getTransaction();
            try {
                JSONObject params = new JSONObject();
                params.put("receiver", trans.getSender());
                params.put("transaction", trans.getId());
                DeltaResources.push(getContext(), "notify_fail", params,
                        new Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                            }

                        }, new ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }

                        });
            } catch (JSONException e) {
            }
        } else {
            SuccessResult sResult = (SuccessResult) result;

            File cacheDirPath = getContext().getExternalCacheDir();
            File received = sResult.getFile();

            String path = received.getAbsolutePath();
            if (!path.startsWith(cacheDirPath.getAbsolutePath())) {
                new SingleMediaScanner(getContext(), sResult.getFile());
            }
        }
        super.onPostExecute(result);
    }

}
