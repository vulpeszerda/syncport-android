package io.syncport.core.transfer.webp2p;

import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;

public class Http {
    public static String getDownloadHeader(String filename, String contentLength) {
        String contentType = URLConnection.guessContentTypeFromName(filename);
        if (TextUtils.isEmpty(contentType)) {
            contentType = "application/octet-stream";
        }
        return "HTTP/1.1 200 OK\r\n" + "Access-Control-Allow-Origin: *\r\n"
                + "Content-Disposition: attachment; filename=" + filename
                + "\r\n" + "Content-Type: " + contentType + "\r\n"
                + "Content-Length: " + contentLength + "\r\n\r\n";
    }

    public static String getInlineHeader(String filename,
                                         String contentLength) {
        String contentType = URLConnection.guessContentTypeFromName(filename);
        if (TextUtils.isEmpty(contentType)) {
            contentType = "application/octet-stream";
        }
        return "HTTP/1.1 200 OK\r\n" + "Access-Control-Allow-Origin: *\r\n"
                + "Content-Disposition: inline; filename=" + filename + "\r\n"
                + "Content-Type: " + contentType + "\r\n"
                + "Content-Length: " + contentLength + "\r\n\r\n";
    }

    public static String getSuccessHeader() {
        return "HTTP/1.1 200 OK\r\n" + "Access-Control-Allow-Origin: *\r\n"
                + "Content-Type: text/plain\r\n\r\n";
    }

    public static String retreiveGETParams(String rawHeader, String key) {
        String regex = "GET \\/\\?(.*)HTTP";
        Matcher m = Pattern.compile(regex).matcher(rawHeader);
        if (!m.find()) {
            return null;
        }
        String query = m.group(1);
        String[] params = query.split("&");
        for (String param : params) {
            String[] kv = param.split("=");
            if (kv[0].equals(key)) {
                return kv[1];
            }
        }
        return null;
    }

    public static String getCORSHeader() {
        return "HTTP/1.1 200 OK\r\n"
                + "Access-Control-Allow-Origin: *\r\n"
                + "Access-Control-Allow-Methods: GET, POST\r\n"
                + "Access-Control-Allow-Headers: Access-Control-Request-Methods\r\n"
                + "Content-Type: text/html; charset=utf-8\r\n\r\n";
    }

    public static String getHeaderValue(String rawHeader, String key) {
        String withoutEscape = rawHeader.split("\r\n\r\n")[0];
//      System.out.println(withoutEscape);

        for (String kv : withoutEscape.split("\r\n")) {
            if (kv.toLowerCase().startsWith(key.toLowerCase())) {
                return kv.split(": ")[1];
            }
        }
        return null;
    }

    public static Integer countHTTPEscape(String raw) {
        Integer i = 0;
        Integer count = 0;
        String escape = "\r\n\r\n";
        while ((i = raw.indexOf(escape, i++)) != -1) {
            count++;
            i += escape.length();
        }
        return count;
    }
}
