package io.syncport.core.transfer;

import android.content.Context;
import android.content.Intent;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.ui.NavigationDrawerActivity;
import io.syncport.ui.NavigationDrawerFragment;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

public class FileRelaySendTask extends BaseFileTask {

    private static final String TAG = "RelayFileSendTask";

    private static final long MAX_SENDABLE_FILE_SIZE = (long) Math
            .pow(2, 4 * 8) - 1;
    private static final int SOCKET_READ_TIMEOUT = 3000;

    protected TaskType mTaskType = TaskType.SEND;

    private String mNotiTitleFormat;
    private String mNotiSendProgressFormat;

    private Socket mSocket;

    public FileRelaySendTask(Context context, Transaction trans,
            boolean showNoti) {
        super(context, trans, showNoti);
    }

    @Override
    protected TaskType taskType() {
        return TaskType.SEND;
    }

    @Override
    protected String getNotiTitleText() {
        if (mNotiSendProgressFormat == null) {
            mNotiTitleFormat = getContext().getString(
                    R.string.desc_file_send__format);
        }
        return String.format(mNotiTitleFormat, getTransaction().getFilename());
    }

    @Override
    protected String getNotiPrepareText() {
        return getContext().getString(R.string.desc_file_send_prepare);
    }

    protected String getNotiProgressText(Progress prog) {
        if (mNotiSendProgressFormat == null) {
            mNotiSendProgressFormat = getContext().getString(
                    R.string.desc_file_send_progress__format);
        }
        return String.format(mNotiSendProgressFormat,
                FileUtils.readableFileSize(prog.doneLength),
                FileUtils.readableFileSize(prog.fileLength));
    }

    @Override
    protected Intent getNotiIntent() {
        return NavigationDrawerActivity.createIntent(getContext(),
                NavigationDrawerFragment.MenuType.FILE_TRANSFER);
    }

    @Override
    protected Result doInBackground() {
        Transaction trans = getTransaction();
        File file = new File(trans.getPathFrom(), trans.getFilename());
        if (!file.exists() || !file.isFile()) {
            // TODO
            // need to abort transaction.
            // this case will not be handled on PROTO TYPING
            return new FailureResult(new UnsupportedOperationException(
                    "file not exist."));
        }

        BufferedOutputStream sockOutputStream = null;
        BufferedInputStream fileInputStream = null;

        int retryConnectCount = 5;
        boolean socketConnected = false;
        while (retryConnectCount-- > 0) {
            try {
                mSocket = new Socket();
                mSocket.connect(new InetSocketAddress(trans.getRelayNodeHost(),
                        trans.getRelayNodePort()), 3000);
                // sock.setSoTimeout(SOCKET_READ_TIMEOUT);
                sockOutputStream = new BufferedOutputStream(
                        mSocket.getOutputStream());
                fileInputStream = new BufferedInputStream(new FileInputStream(
                        file));
                socketConnected = true;
                break;
            } catch (SocketTimeoutException e) {
                // skip
            } catch (UnknownHostException e) {
                return new FailureResult(new RuntimeException(
                        "Failed to connect relay server", e));
            } catch (IOException e) {
                return new FailureResult(new RuntimeException(
                        "Failed to create socket", e));
            }
        }
        if (!socketConnected) {
            return new FailureResult(new SocketTimeoutException(
                    "Failed to connect to server"));
        }

        try {

            // write header
            String header = String.format("send%s", trans.getId());
            sockOutputStream.write(header.getBytes("UTF-8"));

            // write file length
            long fileLen = file.length();
            if (fileLen > MAX_SENDABLE_FILE_SIZE) {
                fileInputStream.close();
                sockOutputStream.close();
                mSocket.close();
                throw new UnsupportedOperationException("exceed max file size");
            }

            byte[] fileLengthByte = ByteBuffer.allocate(8).putLong(fileLen)
                    .array();
            sockOutputStream.write(fileLengthByte, 4, 4);

            // write file
            byte[] buffer = new byte[4096];
            int readByteLen = 0;
            long sentByteLen = 0;
            String fileName = file.getName();
            while (!isCancelled()
                    && (readByteLen = fileInputStream.read(buffer)) != -1) {
                sentByteLen += readByteLen;
                sockOutputStream.write(buffer, 0, readByteLen);
                int percent = (int) (100 * sentByteLen / fileLen);
                publishProgress(new Progress(fileLen, sentByteLen, percent));
            }
            sockOutputStream.flush();
            if (!isCancelled()) {
                Log.d(TAG, String.format("file <%s> send complete", fileName));
            } else {
                Log.d(TAG, String.format("file <%s> send cancelled", fileName));
            }
            return new Result(true);
        } catch (IOException e) {
            e.printStackTrace();
            return new FailureResult(new RuntimeException(
                    "Failed to send file", e));

        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                if (sockOutputStream != null) {
                    sockOutputStream.close();
                }
                mSocket.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public synchronized void cancel() {
        super.cancel();
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        if (!result.isSuccess()) {
            Transaction trans = getTransaction();
            try {
                JSONObject params = new JSONObject();
                params.put("receiver", trans.getReceiver());
                params.put("transaction", trans.getId());
                DeltaResources.push(getContext(), "notify_fail", params,
                        new Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                            }

                        }, new ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }

                        });
            } catch (JSONException e) {
            }
        }
        super.onPostExecute(result);
    }
}
