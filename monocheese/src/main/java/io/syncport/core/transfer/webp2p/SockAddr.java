package io.syncport.core.transfer.webp2p;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SockAddr {
    private String ip;
    private Integer port;

    public SockAddr(String ip, Integer port) {
        this.setIp(ip);
        this.setPort(port);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public InetAddress getAddr() {
        try {
            return InetAddress.getByName(this.ip);
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
