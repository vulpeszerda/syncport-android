package io.syncport.core.transfer.webp2p;

import java.net.ServerSocket;
import java.net.Socket;

public class SockWrap {
    private ServerSocket sock = null;
    private Socket conn = null;

    public SockWrap(ServerSocket sock, Socket conn) {
        this.setSock(sock);
        this.setConn(conn);
    }

    public ServerSocket getSock() {
        return sock;
    }

    public void setSock(ServerSocket sock) {
        this.sock = sock;
    }

    public Socket getConn() {
        return conn;
    }

    public void setConn(Socket conn) {
        this.conn = conn;
    }
}