package io.syncport.core;

import io.syncport.core.BaseChildService.ReadyToDestroyListener;
import android.app.Service;
import android.os.Binder;

public abstract class ChildServiceBinder extends Binder {
    public abstract Service getService();

    public abstract void prepareDestroy(ReadyToDestroyListener listener);
}
