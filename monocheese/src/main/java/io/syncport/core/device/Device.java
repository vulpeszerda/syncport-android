package io.syncport.core.device;

import android.content.Context;

import com.android.volley.toolbox.RequestFuture;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.syncport.core.remote.DeltaResources;

public class Device {

    public static interface Type {
        String WEB = "WEB";
        String ANDROID = "ANDROID";
        String WINDOWS = "WINDOWS";
        String MAC = "MAC";
    }

    public static interface DeviceStateListener {
        void onTurnOn();

        void onTurnOff();

        void onNameChanged(String name);
    }

    private @SerializedName("name") String mName;
    private @SerializedName("uid") String mId;
    private @SerializedName("type") String mType;
    private String mConnectionState;
    private String mSyncRootPath;

    private boolean mIsTurnOn;
    private boolean mIsMyDevice;
    private Object mSyncRootLock;
    private List<DeviceStateListener> mListeners;

    public Device() {
        mIsTurnOn = false;
        mIsMyDevice = false;
        mListeners = new ArrayList<>();
        mSyncRootLock = new Object();
        mSyncRootPath = null;
    }

    public Device(String name, String type, String id) {
        mName = name;
        mType = type;
        mId = id;
        mIsTurnOn = false;
        mIsMyDevice = false;
        mListeners = new ArrayList<>();
        mSyncRootLock = new Object();
        mSyncRootPath = null;
    }

    public synchronized void addObserver(DeviceStateListener ob) {
        if (!mListeners.contains(ob)) {
            mListeners.add(ob);
        }
    }

    public synchronized void removeObserver(DeviceStateListener ob) {
        mListeners.remove(ob);
    }

    public void setMyDevice() {
        mIsMyDevice = true;
    }

    public synchronized void setTurnOn() {
        mIsTurnOn = true;
        for (DeviceStateListener ob : mListeners) {
            ob.onTurnOn();
        }
    }

    public synchronized void setTurnOff() {
        mIsTurnOn = false;
        for (DeviceStateListener ob : mListeners) {
            ob.onTurnOff();
        }
    }

    public synchronized void setName(String name) {
        mName = name;
        for (DeviceStateListener ob : mListeners) {
            ob.onNameChanged(name);
        }
    }

    public String getName() {
        return mName;
    }

    public String getId() {
        return mId;
    }

    public String getType() {
        return mType;
    }

    public boolean isTurnOn() {
        return mIsTurnOn;
    }

    public boolean isMyDevice() {
        return mIsMyDevice;
    }

    public void setConnectionState(String state) {
        mConnectionState = state;
    }

    public boolean isChargedConnectedState() {
        if (mConnectionState == null) {
            return false;
        }
        return mConnectionState.equals("charged");
    }

    /**
     * This method is blocking method.
     * Therefore it should be called on non UI thread.
     * @param forceRefresh
     * @return
     */
    public String getSyncRootPath(
            Context context, boolean forceRefresh)
            throws InterruptedException, ExecutionException{
        if (forceRefresh || mSyncRootPath == null) {
            synchronized (mSyncRootLock) {
                if (forceRefresh || mSyncRootPath == null) {
                    RequestFuture<String> future = RequestFuture.newFuture();
                    DeltaResources.getSyncRoot(context, mId, future, future);
                    mSyncRootPath = future.get();
                }
            }
        }
        return mSyncRootPath;
    }

}
