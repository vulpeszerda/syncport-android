package io.syncport.core.device;


import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.SyncResouces;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportGson;

public class DeviceManager implements InternetConnectivityManager.Observer {

    public static interface Observer {
        void onNewDeviceAdded(Device device);

        void onDeviceTurnOn(Device device);

        void onDeviceTurnOff(Device device);

        void onDeviceNameChanged(Device device, String name);
    }

    public static interface OnGetDevicesListener {
        void onGetDevices(List<Device> devices);

        void onFailure(Exception e);
    }

    public static interface OnGetDeviceListener {
        void onGetDevice(Device device);

        void onFailure(Exception e);
    }

    public static interface SyncListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    private static class OnGetDevicesDelivery {

        private final Executor mResponsePoster;
        private final OnGetDevicesListener mListener;
        private final OnGetDeviceListener mSingleListener;

        public OnGetDevicesDelivery(final OnGetDevicesListener listener,
                final Handler handler) {
            mSingleListener = null;
            mListener = listener;
            mResponsePoster = new Executor() {
                @Override
                public void execute(Runnable command) {
                    handler.post(command);
                }
            };
        }

        public OnGetDevicesDelivery(final OnGetDeviceListener listener,
                final Handler handler) {
            mListener = null;
            mSingleListener = listener;
            mResponsePoster = new Executor() {
                @Override
                public void execute(Runnable command) {
                    handler.post(command);
                }
            };
        }

        private void postDevices(final Device device) {
            List<Device> devices = new ArrayList<>();
            devices.add(device);
            postDevices(devices);
        }

        private void postDevices(final List<Device> devices) {
            mResponsePoster.execute(new Runnable() {

                @Override
                public void run() {
                    if (mListener != null) {
                        mListener.onGetDevices(devices);
                        return;
                    }
                    mSingleListener.onGetDevice(devices.get(0));
                }
            });
        }

        private void postFailure(final Exception e) {
            mResponsePoster.execute(new Runnable() {

                @Override
                public void run() {
                    if (mListener != null) {
                        mListener.onFailure(e);
                        return;
                    }
                    mSingleListener.onFailure(e);
                }
            });
        }
    }

    private static class DeviceSyncResult {
        public static DeviceSyncResult SUCCESS = new DeviceSyncResult();

        private boolean mSuccess;
        private Exception mException;

        private DeviceSyncResult() {
            mSuccess = true;
        }
        public DeviceSyncResult(Exception e) {
            mException = e;
            mSuccess = false;
        }

        public boolean isSuccess() {
            return mSuccess;
        }

        public Exception getException() {
            return mException;
        }
    }

    private class DeviceSyncTask extends AsyncTask<Void, Void, DeviceSyncResult> {
        private boolean mForceResync;
        private SyncListener mListener;

        public DeviceSyncTask(boolean forceResync, SyncListener listener) {
            mForceResync = forceResync;
            mListener = listener;
        }

        @Override
        protected DeviceSyncResult doInBackground(Void... params) {
            if (mForceResync || !mIsSynced.get()) {
                try {
                    sync(mForceResync);
                } catch (InterruptedException e) {
                    return new DeviceSyncResult(e);
                } catch (final ExecutionException e) {
                    return new DeviceSyncResult(e);
                }
            }
            return DeviceSyncResult.SUCCESS;
        }

        @Override
        protected void onPostExecute(DeviceSyncResult result) {
            super.onPostExecute(result);
            if (result.isSuccess()) {
                mListener.onSuccess();
            } else {
                mListener.onFailure(result.getException());
            }
        }
    }
    private static final Comparator<Device> sDeviceComparator = new Comparator<Device>() {

        @Override
        public int compare(Device lhs, Device rhs) {
            if (lhs.isMyDevice()) {
                if (rhs.isMyDevice()) {
                    return lhs.getName().compareTo(rhs.getName());
                }
                return -1;
            }
            return lhs.getName().compareTo(rhs.getName());
        }

    };

    private static final String TAG = "DeviceManager";
    private static DeviceManager manager;

    public static synchronized DeviceManager from(Context context)
            throws AuthenticatorException {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(context);
        if (account == null) {
            throw new AuthenticatorException("signin needed");
        }
        if (manager == null
                || !manager.getAccount().getName().equals(account.getName())) {
            clearSelf(context);
            if (context instanceof Activity) {
                context = ((Activity) context).getApplicationContext();
            } else if (context instanceof Service) {
                context = ((Service) context).getApplicationContext();
            }

            manager = new DeviceManager(context, account);
        }
        return manager;
    }

    private static synchronized void clearSelf(Context context) {
        if (manager != null) {
            InternetConnectivityManager connManager = InternetConnectivityManager
                    .getInstance(context);
            connManager.removeObserver(manager);
            manager.unregisterSignoutReceiver();
            manager = null;
            Log.d(TAG, "Device manager cleared");
        }
    };


    private Context mContext;
    private SyncportAccount mAccount;
    private Device mMyDevice;
    private HashMap<String, Device> mDevices;
    private Object mSyncLock;
    private AtomicBoolean mIsSynced;

    private InternetConnectivityManager mConnManager;

    private ArrayList<Observer> mObservers;
    private BroadcastReceiver mSignoutReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            clearSelf(context);
        }
    };

    private DeviceManager(Context context, SyncportAccount account) {
        mContext = context;
        mDevices = new HashMap<>();
        mAccount = account;
        LocalBroadcastManager.getInstance(context).registerReceiver(
                mSignoutReceiver,
                new IntentFilter(SyncportAccount.ACTION_SIGNOUT_SYNC));

        mMyDevice = new Device(mAccount.getDeviceName(),
                mAccount.getDeviceType(), mAccount.getDeviceId());
        mMyDevice.setMyDevice();
        mMyDevice.setTurnOn();

        mIsSynced = new AtomicBoolean(false);
        mSyncLock = new Object();
        mObservers = new ArrayList<>();
        mConnManager = InternetConnectivityManager.getInstance(context);
        mConnManager.addObserver(this);
    }

    private void unregisterSignoutReceiver() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
                mSignoutReceiver);
    }

    private SyncportAccount getAccount() {
        return mAccount;
    }

    /**
     * Fetch device info from server. This should not be called on MainUIThread
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void sync(boolean forceRefresh) throws InterruptedException,
            ExecutionException {
        synchronized (mSyncLock) {
            if (mIsSynced.get() && ! forceRefresh) {
                return;
            }
            RequestFuture<JSONObject> future;
            JSONObject response;

            future = RequestFuture.newFuture();
            ApiResources.getAllDevices(mContext, future, future);
            response = future.get();

            JSONArray devicesObj = response.optJSONArray("devices");
            int devicesNum = devicesObj.length();
            JSONObject deviceObj = null;

            for (int i = 0; i < devicesNum; i++) {
                deviceObj = devicesObj.optJSONObject(i);
                Device device = SyncportGson.getInstance().fromJson(
                        deviceObj.toString(), Device.class);
                if (device.getId().equals(mMyDevice.getId())) {
                    if (!mMyDevice.getName().equals(device.getName())) {
                        mMyDevice.setName(device.getName());
                        mAccount.setDeviceName(device.getName());
                    }
                    continue;
                }
                Device preExistingDevice = mDevices.get(device.getId());
                if (preExistingDevice == null) {
                    addDevice(device);
                } else if (!preExistingDevice.getName()
                        .equals(device.getName())) {
                    preExistingDevice.setName(device.getName());
                }
            }

            future = RequestFuture.newFuture();
            SyncResouces.activeDevices(mContext, future, future);
            response = future.get();
            Log.d("TEST", "active devices:" + response);

            JSONArray deviceObjs = response.optJSONArray("devices");
            if (deviceObjs != null) {

                ArrayList<Device> turnOffDevices = new ArrayList<>();
                turnOffDevices.addAll(mDevices.values());

                int length = deviceObjs.length();
                String deviceId;

                for (int i = 0; i < length; i++) {
                    deviceObj = deviceObjs.optJSONObject(i);
                    deviceId = deviceObj.optString("device_id");

                    Device preExistDevice = mDevices.get(deviceId);
                    if (preExistDevice != null) {
                        String mobileState = deviceObj.optString("mobile_state");
                        preExistDevice.setConnectionState(mobileState);
                        turnOffDevices.remove(preExistDevice);
                        if (!preExistDevice.isTurnOn()) {
                            preExistDevice.setTurnOn();
                        }
                    }
                }

                for (Device device : turnOffDevices) {
                    if (device.isTurnOn()) {
                        device.setTurnOff();
                    }
                }
            }

            mIsSynced.set(true);
        }
    }

    public void addObserver(Observer ob) {
        synchronized (mObservers) {
            if (!mObservers.contains(ob)) {
                mObservers.add(ob);
            }
        }
    }

    public void removeObserver(Observer ob) {
        synchronized (mObservers) {
            mObservers.remove(ob);
        }
    }

    public void getAllDevices(boolean forceResync, OnGetDevicesListener listener) {
        getAllDevices(false, forceResync, listener);
    }

    public void getAllDevices(final boolean includeMine,
            final boolean forceResync, final OnGetDevicesListener listener) {
        final OnGetDevicesDelivery delivery = new OnGetDevicesDelivery(
                listener, new Handler(Looper.getMainLooper()));

        SyncListener syncListener = new SyncListener() {

            @Override
            public void onSuccess() {

                final ArrayList<Device> devices = new ArrayList<>();
                devices.addAll(mDevices.values());
                if (includeMine) {
                    devices.add(0, mMyDevice);
                }
                if (devices.size() > 1) {
                    Collections.sort(devices, sDeviceComparator);
                }
                delivery.postDevices(devices);
            }

            @Override
            public void onFailure(Exception e) {
                delivery.postFailure(e);
            }
        };

        if (mIsSynced.get() && !forceResync) {
            syncListener.onSuccess();
            return;
        }
        new DeviceSyncTask(forceResync, syncListener).execute();
    }

    public void getOnDevices(OnGetDevicesListener listener) {
        getOnDevices(false, listener);
    }

    public void getOnDevices(final boolean includeMine,
            final OnGetDevicesListener listener) {
        final OnGetDevicesDelivery delivery = new OnGetDevicesDelivery(
                listener, new Handler(Looper.getMainLooper()));
        SyncListener syncListener = new SyncListener() {

            @Override
            public void onSuccess() {
                final ArrayList<Device> onDevices = new ArrayList<Device>();
                for (Device device : mDevices.values()) {
                    if (device.isTurnOn()) {
                        onDevices.add(device);
                    }
                }
                if (includeMine) {
                    onDevices.add(0, mMyDevice);
                }
                if (onDevices.size() > 1) {
                    Collections.sort(onDevices, sDeviceComparator);
                }
                delivery.postDevices(onDevices);
            }

            @Override
            public void onFailure(Exception e) {
                delivery.postFailure(e);
            }
        };

        if (mIsSynced.get()) {
            syncListener.onSuccess();
            return;
        }
        new DeviceSyncTask(false, syncListener).execute();
    }

    public Device getMyDevice() {
        return mMyDevice;
    }

    public void getDeviceWithId(final String id,
            final OnGetDeviceListener listener) {
        final OnGetDevicesDelivery delivery = new OnGetDevicesDelivery(
                listener, new Handler(Looper.getMainLooper()));

        SyncListener syncListener = new SyncListener() {

            @Override
            public void onSuccess() {
                final Device found = mMyDevice.getId().equals(id) ? mMyDevice
                        : mDevices.get(id);
                if (found != null) {
                    delivery.postDevices(found);
                }
            }

            @Override
            public void onFailure(Exception e) {
                delivery.postFailure(e);
            }
        };

        boolean needSync = !mIsSynced.get()
                || (!mMyDevice.getId().equals(id) && mDevices.get(id) == null);
        if (!needSync) {
            syncListener.onSuccess();
            return;
        }
        new DeviceSyncTask(true, syncListener).execute();
    }

    public Device getCachedDeviceWithId(String id) {
        if (mMyDevice.getId().equals(id)) {
            return mMyDevice;
        }
        return mDevices.get(id);
    }

    public List<Device> getCachedDevices() {
        return getCachedDevices(false);
    }

    public List<Device> getCachedDevices(boolean includeMine) {
        ArrayList<Device> devices = new ArrayList<>();
        devices.addAll(mDevices.values());
        if (includeMine) {
            devices.add(0, mMyDevice);
        }
        if (devices.size() > 1) {
            Collections.sort(devices, sDeviceComparator);
        }
        return devices;
    }

    private void addDevice(final Device device) {
        device.addObserver(new Device.DeviceStateListener() {

            @Override
            public void onTurnOn() {
                onDeviceTurnOn(device);
            }

            @Override
            public void onTurnOff() {
                onDeviceTurnOff(device);
            }

            @Override
            public void onNameChanged(String name) {
                onDeviceNameChanged(device, name);
            }
        });
        synchronized (mSyncLock) {
            mDevices.put(device.getId(), device);
        }
        notifyNewDevice(device);
    }

    private void notifyNewDevice(final Device device) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (Observer ob : mObservers) {
                    ob.onNewDeviceAdded(device);
                }
            }
        });
    }

    private void onDeviceTurnOn(final Device device) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                for (Observer ob : mObservers) {
                    ob.onDeviceTurnOn(device);
                }
            }

        });
    }

    private void onDeviceTurnOff(final Device device) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                for (Observer ob : mObservers) {
                    ob.onDeviceTurnOff(device);
                }
            }
        });
    }

    private void onDeviceNameChanged(final Device device, final String name) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {

            @Override
            public void run() {
                for (Observer ob : mObservers) {
                    ob.onDeviceNameChanged(device, name);
                }
            }
        });
    }

    private Thread mResyncOnConnectThread;

    private void startResyncOnInternetConnect() {
        if (mResyncOnConnectThread != null) {
            mResyncOnConnectThread.interrupt();
        }
        mResyncOnConnectThread = new Thread() {
            @Override
            public void run() {
                try {
                    sync(true);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            };
        };
        mResyncOnConnectThread.start();
    }

    @Override
    public void onInternetConnected() {
        startResyncOnInternetConnect();
    }

    @Override
    public void onInternetDisconnected() {

    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {
        startResyncOnInternetConnect();
    }

}
