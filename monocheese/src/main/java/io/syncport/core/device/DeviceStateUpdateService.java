package io.syncport.core.device;

import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.DeviceManager.OnGetDeviceListener;
import io.syncport.core.device.DeviceManager.OnGetDevicesListener;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.DevicePollingInfo;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import android.accounts.AuthenticatorException;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;

public class DeviceStateUpdateService extends IntentService {

    private static class Receiver extends WakefulBroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            DevicePollingInfo info = (DevicePollingInfo) intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            Intent service = createIntent(context, info);
            startWakefulService(context, service);
        }
    }

    public static BroadcastReceiver registerReceiver(Context context) {
        IntentFilter filter = new IntentFilter(
                GeneralPollerService.PollerAction.DEVICE);
        BroadcastReceiver receiver = new Receiver();
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver,
                filter);
        return receiver;
    }

    public static Intent createIntent(Context context, DevicePollingInfo info) {
        Intent intent = new Intent(context, DeviceStateUpdateService.class);
        intent.putExtra(GeneralPollerService.PARAM_DATA, info);
        return intent;
    }

    public DeviceStateUpdateService() {
        super("DeviceStateUpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SyncportAccount account = SyncportAccount
                .getAuthenticatedUser(getApplicationContext());
        if (account == null) {
            return;
        }

        DevicePollingInfo info = (DevicePollingInfo) intent
                .getSerializableExtra(GeneralPollerService.PARAM_DATA);
        if (info == null) {
            return;
        }
        final String deviceId = info.getDeviceId();
        final String state = info.getState();

        if (deviceId.equals(account.getDeviceId())) {
            return;
        }

        try {
            final CountDownLatch latch = new CountDownLatch(1);
            final DeviceManager manager = DeviceManager
                    .from(getApplicationContext());

            manager.getAllDevices(true, new OnGetDevicesListener() {
                @Override
                public void onGetDevices(List<Device> devices) {
                    latch.countDown();
                }

                @Override
                public void onFailure(Exception e) {
                    latch.countDown();
                }
            });
            latch.await();
        } catch (AuthenticatorException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        Receiver.completeWakefulIntent(intent);
    }

}
