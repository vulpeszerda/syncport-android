package io.syncport.core.account;

import io.syncport.R;
import io.syncport.core.RootService;
import io.syncport.core.remote.ApiResources;
import io.syncport.ui.account.AuthActivity;
import io.syncport.ui.form.Form;
import io.syncport.ui.form.SigninFormBuilder;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.daftshady.superandroidkit.authentication.BaseAuthenticator;

public class AccountAuthenticator extends BaseAuthenticator {

    private Context mContext;

    protected AccountAuthenticator(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected String signIn(String userName, String userPass) {
        Form signinForm = new SigninFormBuilder(mContext)
                .setEmailValue(userName).setPasswdValue(userPass).build();
        return ApiResources.syncSignin(mContext, signinForm.getParamsAsJson());
    }

    @Override
    protected Intent createActivityIntent(AccountAuthenticatorResponse response) {
        return AuthActivity.createIntent(mContext);
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response,
            String accountType, String authTokenType,
            String[] requiredFeatures, Bundle options)
            throws NetworkErrorException {
        if (AccountUtils.isThereAccount(mContext, accountType, authTokenType)) {
            final Bundle result = new Bundle();
            result.putInt(AccountManager.KEY_ERROR_CODE,
                    AccountManager.ERROR_CODE_UNSUPPORTED_OPERATION);
            result.putString(AccountManager.KEY_ERROR_MESSAGE,
                    mContext.getString(R.string.error_allow_only_one_account));
            Toast.makeText(mContext, R.string.error_allow_only_one_account,
                    Toast.LENGTH_LONG).show();
            return result;
        }
        return super.addAccount(response, accountType, authTokenType,
                requiredFeatures, options);
    }

    @Override
    public Bundle getAccountRemovalAllowed(
            AccountAuthenticatorResponse response, Account account)
            throws NetworkErrorException {
        if (RootService.isRunning(mContext)) {
            mContext.stopService(RootService.createIntent(mContext));
        }
        return super.getAccountRemovalAllowed(response, account);
    }
}
