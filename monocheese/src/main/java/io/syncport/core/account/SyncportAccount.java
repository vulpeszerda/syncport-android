package io.syncport.core.account;

import io.syncport.utils.SyncportKeyBuilder;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.daftshady.superandroidkit.authentication.AbstractAccount;
import com.daftshady.superandroidkit.authentication.AccountUtils;

public class SyncportAccount extends AbstractAccount {

    private static final SyncportKeyBuilder builder = SyncportKeyBuilder
            .getInstance("auth");
    public static final String ACTION_SIGNIN = builder.getKey("signin");
    public static final String ACTION_SIGNOUT_SYNC = builder
            .getKey("signout_sync");
    public static final String ACTION_SIGNOUT_ASYNC = builder
            .getKey("signout_async");

    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_DEVICE_NAME = "device_name";
    public static final String KEY_DEVICE_ID = "device_id";

    public static final String ACCOUNT_TYPE = "io.syncport.account";
    public static final String TOKEN_TYPE = "syncport.token";

    public static SyncportAccount getAuthenticatedUser(Context context) {
        if (!AccountUtils.isThereAccount(context, ACCOUNT_TYPE)) {
            return null;
        }
        SyncportAccount account = new SyncportAccount(context, ACCOUNT_TYPE);
        return account.getCachedAuthToken() == null ? null : account;
    }

    /**
     * If account already exist, get existing account from AccountManager. Else
     * if context is instance of activity, show signin activity. Otherwise this
     * will return null. This method can be blocked when getAuthToken or
     * creating account, so this should not be called on MainUIThread
     *
     * @param context
     * @return
     */
    public static SyncportAccount getOrCreateAccount(final Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        boolean shouldNotify = false;
        if (getAuthenticatedUser(context) == null) {
            shouldNotify = true;
        }
        Account account = AccountUtils.getOrCreateAccount(context,
                (Activity) context, ACCOUNT_TYPE, TOKEN_TYPE);
        if (account != null) {
            SyncportAccount syncportAccount = new SyncportAccount(context,
                    account);
            String token = syncportAccount.getAuthToken();
            if ( TextUtils.isEmpty(token)) {
                return null;
            }
            if (shouldNotify) {
                sendBroadcast(context, ACTION_SIGNIN, false);
            }
            return syncportAccount;
        }
        return null;
    }

    public static interface OnSignoutFinishedListener {
        void onFinished();
    }

    /**
     * Sign out. Because android should notify 'user signed out' to server, this
     * method has OnSignoutFinishedListener callback (Blocking)
     *
     * @param context
     * @param listener
     */
    public static void signOut(final Context context,
            final OnSignoutFinishedListener listener) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SyncportAccount account = getAuthenticatedUser(context);
                if (account != null) {
                    // do all sign out notification jobs
                    sendBroadcast(context, ACTION_SIGNOUT_SYNC, true);
                    sendBroadcast(context, ACTION_SIGNOUT_ASYNC, false);
                    account.invalidateAuthToken();
                    AccountManager.get(context).setPassword(account.getAccount(), null);

                    AccountUtils.removeAllAccount(context, ACCOUNT_TYPE);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                listener.onFinished();
            }
        }.execute();
    }

    private static void sendBroadcast(Context context, String action,
            boolean sync) {
        Intent intent = new Intent(action);
        LocalBroadcastManager broadcastManager = LocalBroadcastManager
                .getInstance(context);
        if (sync) {
            broadcastManager.sendBroadcastSync(intent);
        } else {
            broadcastManager.sendBroadcast(intent);
        }
    }

    protected SyncportAccount(Context context, String accountType) {
        super(context, accountType);
    }

    protected SyncportAccount(Context context, Account account) {
        super(context, account);
    }

    @Override
    protected String accountType() {
        return ACCOUNT_TYPE;
    }

    /*
     * XXX: this function originally built for getting token type of android
     * account in various token types (like email signin token, facebook token,
     * etc.), but we use static final token type here because this application
     * has only one token type.
     */
    @Override
    protected String tokenType() {
        return TOKEN_TYPE;
    }

    public String getDeviceName() {
        Account account = getAccount();
        return getManager().getUserData(account, KEY_DEVICE_NAME);
    }

    public void setDeviceName(String name) {
        Account account = getAccount();
        getManager().setUserData(account, KEY_DEVICE_NAME, name);
    }

    public String getDeviceType() {
        Account account = getAccount();
        return getManager().getUserData(account, KEY_DEVICE_TYPE);
    }

    public String getDeviceId() {

        final Account account = getAccount();
        String savedDeviceId = getManager().getUserData(account,
                SyncportAccount.KEY_DEVICE_ID);
        if (savedDeviceId == null) {
            savedDeviceId = "null";
        }
        return getManager().getUserData(account, KEY_DEVICE_ID);
    }

}
