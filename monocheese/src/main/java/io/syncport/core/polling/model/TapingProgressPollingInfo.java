package io.syncport.core.polling.model;

import java.io.Serializable;

/**
 * Created by vulpes on 15. 2. 3..
 */
public class TapingProgressPollingInfo extends GeneralPollingInfo implements Serializable{

    private String sender;
    private String receiver;
    private float progress;
    private String path;
    private String id;

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getPath() {
        return path;
    }

    public String getId() {
        return id;
    }

    public float getProgress() {
        return progress;
    }
}
