package io.syncport.core.polling.model;

/**
 * Created by vulpes on 15. 2. 19..
 */
public class CancelTapingPollingInfo extends GeneralPollingInfo {

    private String receiver;
    private String sender;
    private String id;

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }

    public String getId() {
        return id;
    }
}
