package io.syncport.core.polling.model;

import java.io.Serializable;

/**
 * Created by vulpes on 15. 2. 3..
 */
public class RemoteTapingPollingInfo extends GeneralPollingInfo implements Serializable{

    private String receiver;
    private String sender;
    private String[] paths;
    private String filter;
    private String id;

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }

    public String[] getPaths() {
        return paths;
    }

    public String getId() {
        return id;
    }

    public String getFilter() {
        return filter;
    }
}
