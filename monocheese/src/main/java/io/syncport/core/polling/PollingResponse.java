package io.syncport.core.polling;

import io.syncport.utils.Log;
import io.syncport.utils.SyncportGson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class PollingResponse {

    private static final String TAG = "PollingResponse";
    private static final String PROTOCOL_CHARSET = "utf-8";

    private int mStatusCode;
    private byte[] mRawBody;
    private Map<String, String> mHeaders;

    public PollingResponse(HttpResponse httpResponse) {
        StatusLine statusLine = httpResponse.getStatusLine();

        mStatusCode = statusLine.getStatusCode();
        mHeaders = convertHeaders(httpResponse.getAllHeaders());

        if (httpResponse.getEntity() != null) {
            try {
                byte[] data = entityToBytes(httpResponse.getEntity());
                mRawBody = data;
            } catch (IOException e) {
                if (mRawBody == null) {
                    mRawBody = new byte[0];
                }
            }
        }
    }

    @Override
    public String toString() {
        try {
            return new String(mRawBody, PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new String(mRawBody);
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public byte[] getRawBody() {
        return mRawBody;
    }

    public <T> T getParseResponse(Class<T> type)
            throws UnsupportedEncodingException, JsonSyntaxException {
        Gson gson = SyncportGson.getInstance();
        String dataStr = new String(mRawBody, PROTOCOL_CHARSET);
        if (!TextUtils.isEmpty(dataStr)) {
            return gson.fromJson(dataStr, type);
        }
        else {
            System.out.println("Empty response");
        }
        return null;
    }

    private static Map<String, String> convertHeaders(Header[] headers) {
        HashMap<String, String> map = new HashMap<String, String>();
        for (Header header : headers) {
            map.put(header.getName(), header.getValue());
        }
        return map;
    }

    private static byte[] entityToBytes(HttpEntity entity) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        try {
            entity.writeTo(byteStream);
            return byteStream.toByteArray();
        } finally {
            try {
                // Close the InputStream and release the resources by
                // "consuming the content".
                entity.consumeContent();
            } catch (IOException e) {
                // This can happen if there was an exception above that left the
                // entity in
                // an invalid state.
                Log.d(TAG, "Error occured when calling consumingContent");
            }
            byteStream.close();
        }
    }

}
