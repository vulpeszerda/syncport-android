package io.syncport.core.polling.model;

import java.io.Serializable;

public class GeneralPollingInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String type;

    public String getType() {
        return type;
    }

    public Class<? extends GeneralPollingInfo> getKlass() {
        Class<? extends GeneralPollingInfo> klass = null;

        if (type.equals("device")) {
            klass = DevicePollingInfo.class;

        } else if (type.equals("thumbnail")) {
            klass = ThumbnailPollingInfo.class;

        } else if (type.equals("file_transfer")) {
            klass = FileTransferPollingInfo.class;

        } else if (type.equals("directory")) {
            klass = DirectoryPollingInfo.class;

        } else if (type.equals("collection")) {
            klass = CollectionPollingInfo.class;

        } else if (type.equals("negotiation")) {
            klass = NegotiationPollingInfo.class;

        } else if (type.equals("remote_adddel")) {
            klass = DirectoryRemoteAddDelPollingInfo.class;

        } else if (type.equals("remote_taping")) {
            klass = RemoteTapingPollingInfo.class;

        } else if (type.equals("taping_progress")) {
            klass = TapingProgressPollingInfo.class;

        } else if (type.equals("cancel_taping")) {
            klass = CancelTapingPollingInfo.class;

        }

        return klass;
    }
}
