package io.syncport.core.polling.model;

public class NegotiationPollingInfo extends GeneralPollingInfo{
    private String sender;
    private String protocol;
    
    public String getSender() {
        return sender;
    }
    
    public String getProtocol() {
        return protocol;
    }
}
