package io.syncport.core.polling.model;

/**
 * Created by vulpes on 15. 2. 27..
 */
public class ThumbnailPollingInfo extends GeneralPollingInfo{
    private String[] hashes;

    public String[] getHashes() {
        return hashes;
    }
}
