package io.syncport.core.polling.model;

import java.io.Serializable;

import android.text.TextUtils;

public class FileTransferPollingInfo extends GeneralPollingInfo {

    private static final int DEFAULT_RELAYNODE_PORT = 80;
    private static final long serialVersionUID = 1L;

    private Transaction transaction;

    public Transaction getTransaction() {
        return transaction;
    }

    public static class Transaction implements Serializable {
        private static final long serialVersionUID = 1L;
        private String id;
        private String sender;
        private String receiver;
        private String pathTo;
        private String pathFrom;
        private String filename;
        private String relayNode;

        public String getId() {
            return id;
        }

        public String getSender() {
            return sender;
        }

        public String getReceiver() {
            return receiver;
        }

        public String getRelayNode() {
            return relayNode;
        }

        public String getRelayNodeHost() {
            int portIdx = relayNode.lastIndexOf(":");
            if (portIdx > -1) {
                return relayNode.substring(0, portIdx);
            }
            return relayNode;
        }

        public int getRelayNodePort() {
            int portIdx = relayNode.lastIndexOf(":");
            if (portIdx > -1) {
                return Integer.valueOf(relayNode.substring(portIdx + 1));
            }
            return DEFAULT_RELAYNODE_PORT;
        }

        public String getPathFrom() {
            if (TextUtils.isEmpty(pathFrom)) {
                return "";
            }
            return !pathFrom.endsWith("/") ? pathFrom + "/" : pathFrom;
        }

        public String getPathTo() {
            if (TextUtils.isEmpty(pathTo)) {
                return "";
            }
            return !pathTo.endsWith("/") ? pathTo + "/" : pathTo;
        }

        public String getFilename() {
            return filename;
        }
    }
}
