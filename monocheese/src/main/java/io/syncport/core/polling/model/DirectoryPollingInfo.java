package io.syncport.core.polling.model;

public class DirectoryPollingInfo extends GeneralPollingInfo {
    private static final long serialVersionUID = 1L;

    private String commit;
    private String owner;

    public String getCommit() {
        return commit;
    }
    
    public String getOwner() {
        return owner;
    }

}
