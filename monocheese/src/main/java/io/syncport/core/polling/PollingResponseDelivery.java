package io.syncport.core.polling;

import io.syncport.core.polling.PollingClient.PollingException;
import io.syncport.core.polling.PollingClient.PollingStateListener;

import java.util.concurrent.Executor;

import android.os.Handler;

public class PollingResponseDelivery {

    public static PollingResponseDelivery getEmptyDelivery() {
        return new PollingResponseDelivery(null, null);
    }

    private final Executor mResponsePoster;
    private final PollingStateListener mListener;

    public PollingResponseDelivery(final PollingStateListener listener,
            final Handler handler) {
        mListener = listener;
        if (handler != null) {
            mResponsePoster = new Executor() {
                @Override
                public void execute(Runnable command) {
                    handler.post(command);
                }
            };
        } else {
            mResponsePoster = null;
        }
    }

    public void postStart(final PollingRequest request) {
        if (mResponsePoster != null) {
            mResponsePoster.execute(new Runnable() {
                @Override
                public void run() {
                    mListener.onStart(request);
                }
            });
        }
    }

    public void postStop(final int flag) {
        if (mResponsePoster != null) {
            mResponsePoster.execute(new Runnable() {
                @Override
                public void run() {
                    mListener.onStop(flag);
                }
            });
        }
    }

    public void postReceive(final PollingResponse response) {
        if (mResponsePoster != null) {
            mResponsePoster.execute(new Runnable() {
                @Override
                public void run() {
                    mListener.onReceive(response);
                }
            });
        }
    }

    public void postFailure(final int statusCode, final byte[] body,
            final PollingException e) {
        if (mResponsePoster != null) {
            mResponsePoster.execute(new Runnable() {
                @Override
                public void run() {
                    mListener.onFailure(statusCode, body, e);
                }
            });
        }
    }
}
