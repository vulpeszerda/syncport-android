package io.syncport.core.polling.model;

public class DevicePollingInfo extends GeneralPollingInfo{
    private static final long serialVersionUID = 1L;

    private String state;
    private String deviceId;
    
    public String getState() {
        return state;
    }
    
    public String getDeviceId() {
        return deviceId;
    }
}
