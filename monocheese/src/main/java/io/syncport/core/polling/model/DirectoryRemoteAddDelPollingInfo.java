package io.syncport.core.polling.model;

public class DirectoryRemoteAddDelPollingInfo extends GeneralPollingInfo{
    private static final long serialVersionUID = 1L;

    private String[] addition;
    private String[] deletion;
    
    public String[] getAddition() {
        return addition;
    }

    public String[] getDeletion() {
        return deletion;
    }
}
