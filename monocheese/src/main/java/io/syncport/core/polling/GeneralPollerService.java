package io.syncport.core.polling;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.daftshady.superandroidkit.authentication.AbstractAccount.OnGetAuthTokenListener;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import io.syncport.core.BaseChildService;
import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.polling.PollingClient.PollingException;
import io.syncport.core.polling.PollingClient.PollingStateListener;
import io.syncport.core.polling.PollingRequest.Method;
import io.syncport.core.polling.model.GeneralPollingInfo;
import io.syncport.core.remote.SyncPaths;
import io.syncport.core.remote.SyncResouces;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;

public class GeneralPollerService extends BaseChildService implements
        InternetConnectivityManager.Observer {

    private static final String TAG = "GeneralPollerService";

    private static final String POLLER_ACTION_PREFIX = "io.syncport.action.polling";
    private static final int POLLING_TIMEOUT = 30000;
    private static final int CONNECT_RETRY_INTERVAL = 10000;
    private static final int MAX_RETRY_COUNT = 3;

    public static final String PARAM_DATA = SyncportKeyBuilder.build("data");

    public static interface PollerAction {

        String DIRECTORY = buildPollerAction("directory");

        String COLLECTION = buildPollerAction("collection");

        String THUMBNAIL = buildPollerAction("thumbnail");

        String DEVICE = buildPollerAction("device");

        String FILE_TRANSFER = buildPollerAction("file_transfer");

        String NEGOTIATION = buildPollerAction("negotiation");

        String DIRECTORY_REMOTE_ADDDEL = buildPollerAction("remote_adddel");

        String TAPING_PROGRESS = buildPollerAction("taping_progress");

        String CANCEL_REMOTE_TAPING = buildPollerAction("cancel_taping");

        String REMOTE_TAPING = buildPollerAction("remote_taping");
    };


    public static final String ACTION_RESTART_POLLING = SyncportKeyBuilder
            .build("restart_polling");

    public static Intent createIntent(Context context) {
        return new Intent(context, GeneralPollerService.class);
    }

    private AtomicBoolean mIsPreparingToDestory = new AtomicBoolean(false);
    private AtomicBoolean mIsConnected = new AtomicBoolean(false);
    private AtomicInteger mRetryCount = new AtomicInteger(MAX_RETRY_COUNT);

    private AtomicBoolean mIsConnecting = new AtomicBoolean(false);
    private AtomicBoolean mIsDisconnecting = new AtomicBoolean(false);
    private List<Runnable> mOnAfterDisconnected = new ArrayList<>();

    private PollingClient mClient;
    private InternetConnectivityManager mConnManager;
    private Timer mReconnectTimer;

    private Timer mRestartCheckTimer;
    private BroadcastReceiver mRestartReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received restart polling broadcast");
            if (mClient != null && mClient.isRunning()) {
                if (mClient.isSleeping()) {
                    Log.d(TAG, "polling client is sleeping! wake it up");
                    mIsConnected.set(false);
                } else {
                    if (mRestartCheckTimer != null) {
                        mRestartCheckTimer.cancel();
                    }
                    // check after 10 sec
                    Log.d(TAG, "client is running.. check again after 10sec");
                    final Handler handler = new Handler(Looper.getMainLooper());
                    mRestartCheckTimer = new Timer();
                    TimerTask task = new TimerTask() {

                        @Override
                        public void run() {
                            if (mClient != null && mClient.isRunning()
                                    && mClient.isSleeping()) {
                                mIsConnected.set(false);
                            }
                            if (!mIsConnected.get()) {
                                Log.d(TAG, "send requestConnect");
                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        requestConnect();
                                    }

                                });
                            } else {
                                Log.d(TAG, "polling already connected");
                            }
                        }
                    };
                    mRestartCheckTimer.schedule(task, 15000);
                    return;
                }
            }

            if (!mIsConnected.get()) {
                Log.d(TAG, "send requestConnect");
                requestConnect();
            } else {
                Log.d(TAG, "polling already connected");
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "polling service is binded");
        IBinder binder = super.onBind(intent);
        mConnManager = InternetConnectivityManager.getInstance(this);
        mConnManager.addObserver(this);
        if (mConnManager.isConnected()) {
            Log.d(TAG, "onbind request connect");
            requestConnect();
        } else {
            Log.d(TAG, "onbind not request connect(internet disconnected)");
        }
        IntentFilter restartIntentFilter = new IntentFilter(
                ACTION_RESTART_POLLING);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mRestartReceiver, restartIntentFilter);
        return binder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "polling service is unbinded");
        return super.onUnbind(intent);
    }

    @Override
    protected void onPrepareDestroy(ReadyToDestroyListener listener) {
        if (!mIsPreparingToDestory.get()) {
            mIsPreparingToDestory.set(true);
            stopPolling(listener);
        } else if (listener != null) {
            listener.onReadyToDestroy();
        }
    }

    @Override
    public void onDestroy() {
        if (!mIsPreparingToDestory.get()) {
            stopPolling(null);
        }
        mConnManager.removeObserver(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mRestartReceiver);
        super.onDestroy();
    }

    private PollingRequest buildRequest(String token) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Cookie", String.format("sessionid=%s;", token));
        return new PollingRequest(SyncPaths.getPollingPath(this), Method.GET,
                headers);
    }

    private synchronized void startPolling(PollingRequest request) {
        PollingClient.Options options = new PollingClient.Options.Builder()
                .setPollingTimeout(POLLING_TIMEOUT).build();
        if (mClient == null || !mClient.isRunning() || mClient.isSleeping()) {
            if (mClient != null) {
                mClient.stopWithSlient();
            }
            Log.d(TAG, "PollingClient call & start");
            mClient = new PollingClient(request, options,
                    mPollingStateListener, new Handler(Looper.getMainLooper()));
            mClient.start();
        }
    }

    private void startAuthPolling() {
        Log.d(TAG, "startAuthPolling call");
        SyncportAccount account = SyncportAccount
                .getAuthenticatedUser(getApplicationContext());
        account.getAuthToken(new OnGetAuthTokenListener() {

            @Override
            public void onGetAuthToken(String token) {
                Log.d(TAG, "startPolling call");
                startPolling(buildRequest(token));

            }

            @Override
            public void onFailure(Exception e) {
                handleException(e);
            }
        });
    }

    /**
     * Stop currently running polling client.
     *
     * @return need to wait or not
     */
    private boolean stopPolling(ReadyToDestroyListener listener) {
        if (mClient != null && mClient.isRunning() && !mClient.isSleeping()) {
            Log.d(TAG, "polling client is running so go stop cleint");
            mClient.stopWithSlient();
        }
        if (mIsConnected.get()) {
            requestDisconnect(false, listener);
            return true;
        }
        if (listener != null) {
            listener.onReadyToDestroy();
        }
        return false;
    }

    private void handleException(Exception e) {
        e.printStackTrace();
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        Intent intent = new Intent(RootService.ACTION_STOP_SELF);
        manager.sendBroadcast(intent);
    }

    private synchronized void retryConnect() {
        final Handler handler = new Handler();
        if (mRetryCount.get() <= 0) {
            handleException(new Exception("retry connect exceed retry count"));
            return;
        }
        mRetryCount.decrementAndGet();
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
            mReconnectTimer = null;
        }
        mReconnectTimer = new Timer();
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        requestConnect();
                    }

                });
            }
        };
        mReconnectTimer.schedule(task, CONNECT_RETRY_INTERVAL);
    }

    private void requestConnect() {
        final SyncportAccount account = SyncportAccount
                .getAuthenticatedUser(this);
        if (account == null) {
            return;
        }
        if (mIsConnected.get() || mIsConnecting.get()) {
            Log.d(TAG, "already connected or connecting..");
            return;
        }
        if (!mConnManager.isConnected()) {
            Log.d(TAG, "internet is not connected");
            return;
        }

        mIsConnecting.set(true);
        final Handler handler = new Handler();
        Log.d(TAG, "go start request connect thread");
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Log.d(TAG, "request connect");
                    String connectionState = "unknown";
                    if (mConnManager.isMobileConnected()) {
                        connectionState = "charged";
                    } else if (mConnManager.isWifiConnected()) {
                        connectionState = "wifi";
                    }
                    RequestFuture<JSONObject> future = RequestFuture
                            .newFuture();
                    SyncResouces.connect(GeneralPollerService.this,
                            account.getDeviceType(), account.getDeviceName(),
                            connectionState,
                            future, future);

                    future.get();
                    mIsConnected.set(true);
                    mIsConnecting.set(false);
                    Log.d(TAG, "connected");
                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            startAuthPolling();
                        }
                    });
                } catch (InterruptedException e) {
                    mIsConnecting.set(false);
                } catch (ExecutionException e) {
                    mIsConnecting.set(false);
                    VolleyError error = (VolleyError) e.getCause();
                    error.printStackTrace();
                    NetworkResponse response = error.networkResponse;
                    String msg = null;
                    if (response != null && response.data != null) {
                        msg = new String(response.data);
                    }
                    if (msg == null) {
                        msg = error.getMessage();
                    }
                    if (msg != null) {
                        Log.e(TAG, msg);
                    }
                    if (error instanceof NetworkError || error instanceof
                            TimeoutError) {
                        handler.post(new Runnable() {

                            @Override
                            public void run() {
                                // retry request connect later;
                                retryConnect();
                            }
                        });
                        return;
                    }
                    handleException(e);
                }
            }
        }).start();
    }

    private void requestDisconnect(final boolean doReconnect,
            final ReadyToDestroyListener listener) {
        Log.d(TAG, "request disconnect");
        if (mIsDisconnecting.get() && listener != null) {
            synchronized (mOnAfterDisconnected) {
                mOnAfterDisconnected.add(new Runnable() {
                    @Override
                    public void run() {
                        listener.onReadyToDestroy();
                    }
                });
            }
            return;
        }
        mIsDisconnecting.set(true);
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        SyncResouces.disconnect(this, account.getDeviceType(),
                account.getDeviceName(), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        mIsDisconnecting.set(false);
                        mIsConnected.set(false);
                        Log.d(TAG, "set is connected false");
                        if (response.optBoolean("success")) {
                            Log.d(TAG, "successfully disconnected");
                            if (doReconnect && mConnManager.isConnected()) {
                                retryConnect();
                            }
                        }
                        if (listener != null) {
                            listener.onReadyToDestroy();
                        }
                        List<Runnable> runnables;
                        synchronized (mOnAfterDisconnected) {
                            runnables = new ArrayList<>(mOnAfterDisconnected);
                            mOnAfterDisconnected.clear();
                        }
                        for (Runnable runnable : runnables) {
                            runnable.run();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mIsDisconnecting.set(false);
                        mIsConnected.set(false);
                        Log.d(TAG, "set is connected false");
                        Log.d(TAG, "disconnected with failure");
                        if (doReconnect && mConnManager.isConnected()) {
                            retryConnect();
                        }
                        if (listener != null) {
                            listener.onReadyToDestroy();
                        }
                        List<Runnable> runnables;
                        synchronized (mOnAfterDisconnected) {
                            runnables = new ArrayList<>(mOnAfterDisconnected);
                            mOnAfterDisconnected.clear();
                        }
                        for (Runnable runnable : runnables) {
                            runnable.run();
                        }
                    }
                });
    }

    protected void onPollingStart(PollingRequest request) {
        Log.d(TAG, String.format("Polling start to '%s'", request.getUrl()));
    }

    protected void onPollingStop(int flag) {
        Log.d(TAG, String.format("Polling stopped"));
        if ((flag & PollingClient.FLAG_ONSTOP_CALL_DISCONNECT) > 0) {
            requestDisconnect(
                    (flag & PollingClient.FLAG_ONSTOP_DO_RECONNECT) > 0, null);
        } else {
            mIsConnected.set(false);
        }
    }

    protected void onPollingReceive(PollingResponse response) {
        GeneralPollingInfo info = null;
        try {
            info = (GeneralPollingInfo) response
                    .getParseResponse(GeneralPollingInfo.class);
            if (info != null) {
                String action = buildPollerAction(info.getType());

                Intent intent = new Intent(action);
                Class<? extends GeneralPollingInfo> dataClass = info.getKlass();
                if (dataClass != null) {
                    intent.putExtra(PARAM_DATA,
                            response.getParseResponse(dataClass));
                }
                Log.d(TAG, "sent broadcast action:" + action);
                Log.d(TAG, response.toString());

                LocalBroadcastManager.getInstance(this).sendBroadcastSync(
                        intent);
            } else {
                Log.e(TAG, "got poll response but cannot decode:'"
                        + new String(response.getRawBody()) + "'");
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    protected void onPollingFailure(int statusCode, byte[] body,
            PollingException e) {
        String msg = "undefined";
        if (body != null && body.length > 0) {
            try {
                msg = new String(body, "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
        Log.d(TAG,
                String.format("Polling failure (%1$d): %2$s", statusCode, msg));
        e.printStackTrace();
    }

    private PollingStateListener mPollingStateListener = new PollingStateListener() {

        @Override
        public void onStop(int flag) {
            Log.d(TAG, "stop with flag:" + flag);
            onPollingStop(flag);
        }

        @Override
        public void onStart(PollingRequest request) {
            onPollingStart(request);
        }

        @Override
        public void onReceive(PollingResponse response) {
            onPollingReceive(response);
        }

        @Override
        public void onFailure(int statusCode, byte[] body, PollingException e) {
            onPollingFailure(statusCode, body, e);
        }
    };

    public static String buildPollerAction(String action) {
        return POLLER_ACTION_PREFIX + "." + action;
    }

    @Override
    protected Service getService() {
        return this;
    }

    @Override
    public void onInternetConnected() {
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
            mReconnectTimer = null;
        }
        requestConnect();
    }

    @Override
    public void onInternetDisconnected() {
        if (mReconnectTimer != null) {
            mReconnectTimer.cancel();
            mReconnectTimer = null;
        }
    }

    @Override
    public void onInternetConnectTypeChanged(boolean isWifi) {
        if (mConnManager.isConnected()) {
            stopPolling(new ReadyToDestroyListener() {
                @Override
                public void onReadyToDestroy() {
                    Log.d(TAG, "polling stopped for restarting..");
                    requestConnect();
                }
            });
        }
    }

}
