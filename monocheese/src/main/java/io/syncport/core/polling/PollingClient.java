package io.syncport.core.polling;

import static java.net.HttpURLConnection.HTTP_ACCEPTED;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_GONE;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

import android.os.Handler;

import io.syncport.utils.Log;

public class PollingClient {
    public static interface PollingStateListener {
        void onStart(PollingRequest request);

        void onReceive(PollingResponse response);

        void onFailure(int statusCode, byte[] body, PollingException e);

        void onStop(int flag);
    }

    protected static final String HEADER_CONTENT_TYPE = "Content-Type";
    protected static final String HEADER_USER_AGENT = "User-Agent";

    public static final String USER_AGENT = "SyncportPollingClient";
    public static final int FLAG_ONSTOP_DO_RECONNECT = 1;
    public static final int FLAG_ONSTOP_CALL_DISCONNECT = 2;

    public static class PollingException extends Exception {
        private static final long serialVersionUID = 1L;

        public PollingException(String msg) {
            super(msg);
        }

        public PollingException(Throwable e) {
            super(e);
        }

        public PollingException(String msg, Throwable e) {
            super(msg, e);
        }
    }

    private PollingRequest mRequest;
    private Options mOptions;
    private PollingResponseDelivery mDelivery;
    private int mRetryCount;
    private Thread mPollingThread;
    private AtomicBoolean mIsRunning;
    private Date mLastPollRequestSentTime;

    public PollingClient(PollingRequest request, Options options,
            PollingStateListener listener, Handler handler) {
        mRequest = request;
        mOptions = options;
        mRetryCount = mOptions.getRetryCount();
        mDelivery = new PollingResponseDelivery(listener, handler);
        mPollingThread = new Thread(mPollingRunnable);
        mPollingThread.setName("polling client thread");
        mIsRunning = new AtomicBoolean(false);
        mLastPollRequestSentTime = new Date();
    }

    public void start() {
        try {
            mPollingThread.start();
        } catch (IllegalThreadStateException e) {
            if (mPollingThread.isAlive()) {
                stopWithSlient();
            }
            mPollingThread = new Thread(mPollingRunnable);
            mPollingThread.setName("polling client thread");
            start();
            return;
        }
    }

    public void stopWithSlient() {
        if (mPollingThread != null && mPollingThread.isAlive()) {
            mPollingThread.interrupt();
            mDelivery = PollingResponseDelivery.getEmptyDelivery();
        }
    }

    public boolean isRunning() {
        return mIsRunning.get();
    }

    public synchronized Date getLastPollRequestTime() {
        return mLastPollRequestSentTime;
    }

    public boolean isSleeping() {
        Date current = new Date();
        long timeDiff = current.getTime() - mLastPollRequestSentTime.getTime();
        Log.d("TEST", "timeDiff = " + timeDiff);
        if (timeDiff > mOptions.mPollTimeout + 10000) {
            return true;
        }
        return false;
    }

    private Runnable mPollingRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("TEST", "polling runnable run");
            mIsRunning.set(true);
            mDelivery.postStart(mRequest);

            int stopFlag = 0;
            boolean doReconnectOnNotInterrupt = true;
            boolean callDisconnectOnStop = true;
            boolean isThreadInterrupted = false;
            boolean stopPolling = false;
            HttpURLConnection conn = null;
            while (!(isThreadInterrupted = Thread.interrupted())
                    && !stopPolling) {
                System.out.println("waiting..");
                PollingResponse response = null;
                try {
                    synchronized (this) {
                        mLastPollRequestSentTime = new Date();
                    }
                    conn = createConnection(mRequest);
                    response = new PollingResponse(performRequest(conn));
                    // reset retryCount
                    mRetryCount = mOptions.getRetryCount();

                    if (isOk(response.getStatusCode())) {
                        mDelivery.postReceive(response);
                    } else {
                        throw new IOException();
                    }
                } catch (SocketTimeoutException e) {
                    // reset retryCount
                    mRetryCount = mOptions.getRetryCount();
                } catch (ConnectTimeoutException e) {
                    Log.d("PollingClient", "connection timeout. retry remain:"
                            + mRetryCount);
                    --mRetryCount;
                    if (mRetryCount <= 0) {
                        mDelivery.postFailure(-1, null, new PollingException(
                                "Failed to connecting sever", e));
                        stopPolling = true;
                    }
                } catch (MalformedURLException e) {
                    mDelivery.postFailure(-1, null, new PollingException(
                            "Bad URL " + mRequest.getUrl(), e));
                    stopPolling = true;
                } catch (IOException e) {
                    if (response == null) {
                        mDelivery.postFailure(-1, null, new PollingException(
                                "No connection error", e));
                    } else {
                        if (response.getStatusCode() != 504) {
                            mDelivery.postFailure(response.getStatusCode(),
                                    response.getRawBody(),
                                    new PollingException(e));
                        } else {
                            // XXX: 504 timeout will be thrown by nginx
                            // this failure should not call disconnect
                            doReconnectOnNotInterrupt = false;
                            callDisconnectOnStop = false;
                        }
                    }
                    stopPolling = true;
                } finally {
                    if (conn != null) {
                        conn.disconnect();
                    }
                }
            }
            mIsRunning.set(false);
            if (doReconnectOnNotInterrupt && !isThreadInterrupted) {
                stopFlag |= FLAG_ONSTOP_DO_RECONNECT;
            }
            if (callDisconnectOnStop) {
                stopFlag |= FLAG_ONSTOP_CALL_DISCONNECT;
            }
            mDelivery.postStop(stopFlag);
        }
    };

    protected boolean isError(final int code) {
        switch (code) {
        case HTTP_BAD_REQUEST:
        case HTTP_UNAUTHORIZED:
        case HTTP_FORBIDDEN:
        case HTTP_NOT_FOUND:
        case HTTP_CONFLICT:
        case HTTP_GONE:
        case HTTP_INTERNAL_ERROR:
            return true;
        default:
            return false;
        }
    }

    protected boolean isOk(final int code) {
        switch (code) {
        case HTTP_OK:
        case HTTP_CREATED:
        case HTTP_ACCEPTED:
            return true;
        default:
            return false;
        }
    }

    protected boolean isEmpty(final int code) {
        return HTTP_NO_CONTENT == code;
    }

    protected Map<String, String> getAdditionalHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(HEADER_USER_AGENT, USER_AGENT);
        return headers;
    }

    private HttpURLConnection openConnection(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(mOptions.getConnectTimeout());
        conn.setReadTimeout(mOptions.getPollTimeout());
        conn.setDoInput(true);
        return conn;
    }

    private HttpURLConnection createConnection(PollingRequest request)
            throws IOException {

        // create connection
        URL url = new URL(request.getUrl());
        HttpURLConnection conn = openConnection(url);
        conn.setUseCaches(false);
        conn.setReadTimeout(mOptions.getPollTimeout());

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.putAll(request.getHeaders());
        headers.putAll(getAdditionalHeaders());

        for (Entry<String, String> entry : headers.entrySet()) {
            conn.addRequestProperty(entry.getKey(), entry.getValue());
        }

        setConnectionParametersForRequest(conn, request);
        return conn;
    }

    private HttpResponse performRequest(HttpURLConnection conn)
            throws IOException {

        int responseCode = conn.getResponseCode();
        if (responseCode == -1) {
            throw new IOException(
                    "Could not retrieve response code from HttpUrlConnection");
        }
        ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
        StatusLine responseStatus = new BasicStatusLine(protocolVersion,
                conn.getResponseCode(), conn.getResponseMessage());
        BasicHttpResponse response = new BasicHttpResponse(responseStatus);
        response.setEntity(entityFromConnection(conn));
        for (Entry<String, List<String>> header : conn.getHeaderFields()
                .entrySet()) {
            if (header.getKey() != null) {
                Header h = new BasicHeader(header.getKey(), header.getValue()
                        .get(0));
                response.addHeader(h);
            }
        }
        return response;
    }

    private static void setConnectionParametersForRequest(
            HttpURLConnection conn, PollingRequest request)
            throws ProtocolException, IOException {
        switch (request.getMethod()) {
        case GET:
            conn.setRequestMethod("GET");
            break;
        case POST:
            conn.setRequestMethod("POST");
            addBodyIfExists(conn, request);
            break;
        case PUT:
            conn.setRequestMethod("PUT");
            addBodyIfExists(conn, request);
            break;
        }
    }

    private static void addBodyIfExists(HttpURLConnection conn,
            PollingRequest request) throws IOException {
        byte[] body = request.getBody();
        if (body != null) {
            conn.setDoOutput(true);
            conn.addRequestProperty(HEADER_CONTENT_TYPE,
                    request.getBodyContentType());
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.write(body);
            out.close();
        }
    }

    private static HttpEntity entityFromConnection(HttpURLConnection connection) {
        BasicHttpEntity entity = new BasicHttpEntity();
        InputStream inputStream;
        try {
            inputStream = connection.getInputStream();
        } catch (IOException ioe) {
            inputStream = connection.getErrorStream();
        }
        entity.setContent(inputStream);
        entity.setContentLength(connection.getContentLength());
        entity.setContentEncoding(connection.getContentEncoding());
        entity.setContentType(connection.getContentType());
        return entity;
    }

    public static class Options {
        private int mPollTimeout;
        private int mConnectTimeout;
        private int mRetryCount;

        private Options(int pollTimeout, int connectTimeout, int retryCount) {
            mPollTimeout = pollTimeout;
            mConnectTimeout = connectTimeout;
            mRetryCount = retryCount;
        }

        public int getPollTimeout() {
            return mPollTimeout;
        }

        public int getConnectTimeout() {
            return mConnectTimeout;
        }

        public int getRetryCount() {
            return mRetryCount;
        }

        public static class Builder {
            private int mPollingTimeout = 900000;
            private int mConnectTimeout = 30000;
            private int mRetryCount = 3;

            public Builder setPollingTimeout(int timeout) {
                mPollingTimeout = timeout;
                return this;
            }

            public Builder setConnectTimeout(int timeout) {
                mConnectTimeout = timeout;
                return this;
            }

            public Builder setConnectRetryCount(int count) {
                mRetryCount = count;
                return this;
            }

            public Options build() {
                return new Options(mPollingTimeout, mConnectTimeout,
                        mRetryCount);
            }
        }
    }
}
