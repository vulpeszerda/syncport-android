package io.syncport.core.polling;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class PollingRequest {

    public static enum Method {
        GET, POST, PUT
    }

    private static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE = String.format(
            "application/json; charset=%s", PROTOCOL_CHARSET);

    private String mUrl;
    private Method mMethod;
    private Map<String, String> mHeaders;
    private byte[] mBody;

    public PollingRequest(String url) {
        this(url, Method.GET);
    }

    public PollingRequest(String url, Method method) {
        this(url, Method.GET, null);
    }

    public PollingRequest(String url, Method method, Map<String, String> headers) {
        mUrl = url;
        mMethod = method;
        mHeaders = headers;
        if (mHeaders == null) {
            mHeaders = new HashMap<String, String>();
        }
    }

    public PollingRequest(String url, Method method,
            Map<String, String> header, JSONObject body) {
        this(url, method, header);
        setBody(body);
    }

    public PollingRequest(String url, Method method,
            Map<String, String> header, byte[] body) {
        this(url, method, header);
        setBody(body);
    }

    public String getUrl() {
        return mUrl;
    }

    public Method getMethod() {
        return mMethod;
    }

    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    public void setHeader(Map<String, String> headers) {
        mHeaders = headers;
    }

    public void addHeader(String key, String value) {
        mHeaders.put(key, value);
    }

    public void setBody(String body) {
        if (body != null) {
            try {
                mBody = body.getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public void setBody(JSONObject body) {
        if (body != null) {
            setBody(body.toString());
        }
    }

    public void setBody(byte[] body) {
        mBody = body;
    }

    public byte[] getBody() {
        return mBody;
    }

    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

}
