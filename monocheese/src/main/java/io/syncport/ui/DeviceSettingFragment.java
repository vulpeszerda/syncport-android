package io.syncport.ui;

import android.accounts.AuthenticatorException;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.VolleyError;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.ui.directory.AlbumActivity;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 2. 23..
 */
public class DeviceSettingFragment extends BaseFragment implements View
        .OnClickListener{

    public static DeviceSettingFragment getInstance(String deviceId) {
        DeviceSettingFragment fragment = new DeviceSettingFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String PARAM_DEVICE_ID = SyncportKeyBuilder.build
            ("device_id");

    private @InjectView(R.id.device_name) TextView mDeviceNameView;
    private @InjectView(R.id.button_cancel) Button mCancelBtn;
    private @InjectView(R.id.button_apply) Button mChangeBtn;

    private Device mDevice;
    private DeviceManager mDeviceManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            mDeviceManager = DeviceManager.from(getBaseActivity());
            fetchDeviceInfo();
        } catch (AuthenticatorException e) {
            e.printStackTrace();
            // not reach
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_setting, container,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mChangeBtn.setOnClickListener(this);
        mCancelBtn.setOnClickListener(this);
        mCancelBtn.setEnabled(false);
        mChangeBtn.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_apply) {
            CharSequence newDeviceName = mDeviceNameView.getText();
            if (TextUtils.isEmpty(newDeviceName)) {
                mDeviceNameView.setError(getString(R.string.error_value_required));
                return;
            }
            changeDeviceName(newDeviceName.toString());
        } else if (v.getId() == R.id.button_cancel) {
            mDeviceNameView.setText(mDevice.getName());
        }
    }

    private void fetchDeviceInfo() {
        String deviceId = getArguments().getString(PARAM_DEVICE_ID);

        final DialogInterface.OnClickListener onDialogClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            fetchDeviceInfo();
                        }
                    }
                };

        mDeviceManager.getDeviceWithId(deviceId, new DeviceManager.OnGetDeviceListener() {
            @Override
            public void onGetDevice(Device device) {
                mDevice = device;
                mDeviceNameView.setText(mDevice.getName());
                mCancelBtn.setEnabled(true);
                mChangeBtn.setEnabled(true);
            }

            @Override
            public void onFailure(Exception e) {
                mCancelBtn.setEnabled(false);
                mChangeBtn.setEnabled(false);
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_failed_to_load)
                        .setMessage(R.string.des_failed_to_fetch_device_info)
                        .setPositiveButton(R.string.button_retry, onDialogClickListener)
                        .setNegativeButton(R.string.button_confirm, null)
                        .show();
            }
        });
    }

    private void changeDeviceName(final String name) {
        RobustResponseListener responseListener = new RobustResponseListener
                (getBaseActivity(), this) {
            @Override
            public void onSkipped(Object response) {
                mDevice.setName(name);
            }

            @Override
            public void onSecureResponse(Object response) {
                showSuccessDialog(name);
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {
            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                showFailureDialog(name, msg);
            }

            @Override
            public void onSkipped(VolleyError error) {

            }
        };

        ApiResources.changeDeviceName(getActivity(), mDevice.getId(),
                name, responseListener, errorListener);
    }

    private void showSuccessDialog(final String name) {
        DialogInterface.OnClickListener afterDialogDismiss = new DialogInterface
                .OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDevice.setName(name);
            }
        };

        new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.title_applied)
                .setMessage(R.string.desc_device_name_changed)
                .setPositiveButton(R.string.button_confirm, afterDialogDismiss)
                .show();
    }

    private void showFailureDialog(final String name, String msg) {
        DialogInterface.OnClickListener afterDialogDismiss = new
                DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    changeDeviceName(name);
                }
            }
        };

        String message = getString(R.string.desc_failed_to_change_device_name);
        if (msg != null) {
            message += " (" + msg + ")";
        }
        new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.title_faild_to_apply)
                .setMessage(message)
                .setPositiveButton(R.string.button_retry, afterDialogDismiss)
                .setNegativeButton(R.string.button_close, null).show();

    }
}
