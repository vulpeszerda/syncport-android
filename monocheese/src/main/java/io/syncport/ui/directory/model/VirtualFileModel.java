package io.syncport.ui.directory.model;

import android.content.Context;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.syncport.core.device.Device;
import io.syncport.core.remote.ThumbnailPaths;

public class VirtualFileModel {

    static enum Type {
        DEVICE, DIRECTORY, FILE
    }

    private Device mDevice;
    private String mName;
    private Type mType;
    private String mAbsolutePath;
    private VirtualDirectoryModel mParent;
    private VirtualAlbumModel mAlbum;
    private Map<String, String> mMeta;

    protected VirtualFileModel(Type type, String name, String absolutePath,
                            Device device) {
        mType = type;
        mName = name;
        mMeta = new HashMap<>();
        mAbsolutePath = absolutePath;
        mDevice = device;
    }

    public VirtualFileModel(String name, String absolutePath, Device device) {
        this(Type.FILE, name, absolutePath, device);
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public String getAbsolutePath() {
        return mAbsolutePath;
    }

    public VirtualDirectoryModel getParent() {
        return mParent;
    }

    public VirtualAlbumModel getAlbum() {
        return mAlbum;
    }

    public VirtualDeviceModel getRoot() {
        if (this instanceof  VirtualDeviceModel) {
            return (VirtualDeviceModel) this;
        }
        return mParent != null ? mParent.getRoot() : null;
    }

    public VirtualDirectoryModel getSyncRoot() {
        VirtualDeviceModel root = getRoot();
        if (root == null) {
            return null;
        }
        String syncRootPath = root.getSyncRootPath();
        if (syncRootPath == null) {
            return null;
        }

        String[] subPaths = syncRootPath.split("/");
        VirtualDirectoryModel curr = root;
        for (int idx = 1; idx < subPaths.length; idx++) {
            String path = subPaths[idx];
            if (idx < subPaths.length -1 || syncRootPath.endsWith("/")) {
                path += "/";
            }
            VirtualFileModel file;
            file = curr.getChildByName(path);
            if (file == null || file.isFile()) {
                break;
            }
            curr = (VirtualDirectoryModel) file;
            if (idx == subPaths.length - 1) {
                return curr;
            }
        }
        return root;
    }

    public boolean isDirectory() {
        return mType == Type.DIRECTORY;
    }

    public boolean isFile() {
        return mType == Type.FILE;
    }

    public boolean isDevice() {
        return mType == Type.DEVICE;
    }

    public long getSize() {
        String val = mMeta.get("sz");
        if (val == null) {
            return -1L;
        }
        return Long.valueOf(val);
    }

    public Date getModifiedAt() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String val = mMeta.get("ma");

        if (val == null) {
            return null;
        }
        try {
            return formatter.parse(val);
        } catch (ParseException e) {
            return null;
        }
    }

    public String getExtraMetaByKey(String key) {
        return mMeta.get(key);
    }
    public boolean updateMeta(Map<String, String> info) {
//        MapDifference diff = Maps.difference(mMeta, info);
        mMeta.clear();
        mMeta.putAll(info);
//        return !diff.areEqual();
        return true;
    }


    public Device getDevice() {
        if (mDevice != null) {
            return mDevice;
        }
        else {
            return (mParent == null ? null : mParent.getDevice());
        }
    }

    public String getImageUrl() {
        String hash = getHash();
        String device_id = getDevice().getId();
        if (hash != null) {
            StringBuffer sb = new StringBuffer();
            sb.append(ThumbnailPaths.HOST).append("/");
            sb.append(device_id);
            sb.append("/");
            sb.append(hash);
            sb.append(".jpg");
            sb.append("?v=");
            sb.append(hashCode());
            return sb.toString();
        }
        return null;
    }

    public String getHash() {
        String deviceId = getDevice().getId();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((deviceId + ":" + getAbsolutePath()).getBytes());
            byte[] byteData = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            // not reach
        }
        return null;
    }

    public synchronized void clearParent() {
        String path = getAbsolutePath();
        String[] subPaths = path.split("/");
        if (subPaths.length < 2) {
            return;
        }
        String parentPath = "";
        VirtualDirectoryModel parent = null;
        for (int i = 0; i < subPaths.length - 1; i++) {
            parentPath += subPaths[i] + "/";
            VirtualDirectoryModel curr;
            if (i == 0) {
                curr = new VirtualDeviceModel(mDevice);
            } else {
                curr = new VirtualDirectoryModel(subPaths[i],
                        parentPath, mDevice);
            }
            if (parent != null) {
                parent.addChild(curr);
            }
            parent = curr;
        }
        parent.addChild(this);
    }

    public void loadParentDirectory(
            Context context,
            final VirtualDirectoryModel.LoadListener listener) {

        clearParent();
        if (mParent != null) {
            mParent.loadSubDirectories(context, 1, listener);
        }
    }



    Type getType() {
        return mType;
    }

    void setParent(VirtualDirectoryModel model) {
        mParent = model;
    }

    void setAlbum(VirtualAlbumModel model) {
        mAlbum = model;
    }
}
