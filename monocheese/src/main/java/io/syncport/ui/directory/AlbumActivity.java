package io.syncport.ui.directory;

import android.accounts.AuthenticatorException;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.CollectionPollingInfo;
import io.syncport.core.polling.model.DirectoryPollingInfo;
import io.syncport.core.polling.model.ThumbnailPollingInfo;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.SyncportApplication;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualAlbumModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 2. 23..
 */
@ContentView(R.layout.activity_album)
public class AlbumActivity extends BaseActivity implements AdapterView
        .OnItemClickListener, VirtualAlbumModel.LoadListener{

    public static Intent createIntent(Context context,
                                       VirtualAlbumModel album) {
        Intent intent = new Intent(context, AlbumActivity.class);
        intent.putExtra(PARAM_ALBUM_DEVICE_ID, album.getDevice().getId());
        intent.putExtra(PARAM_ALBUM_DIR_PATH, album.getPath());
        List<String> paths = new ArrayList<>();
        for (VirtualFileModel file : album.getFiles()) {
            paths.add(file.getAbsolutePath());
        }
        intent.putExtra(PARAM_ALBUM_FILE_PATHS, paths.toArray(new String[paths.size()]));
        intent.putExtra(PARAM_ALBUM_INCLUDE_NESTED_CHILDREN,
                album.isIncludeNestedChildren());
        return intent;
    }

    private static final String PARAM_ALBUM_DEVICE_ID = SyncportKeyBuilder
            .build("album_device_id");
    private static final String PARAM_ALBUM_DIR_PATH = SyncportKeyBuilder
            .build("album_dir_path");
    private static final String PARAM_ALBUM_FILE_PATHS = SyncportKeyBuilder
            .build("album_file_paths");
    private static final String PARAM_ALBUM_INCLUDE_NESTED_CHILDREN  =
            SyncportKeyBuilder.build("album_include_nested");


    private @InjectView(R.id.gridview) StickyGridHeadersGridView mGridView;
    private @InjectView(R.id.empty_view) View mEmptyView;
    private @InjectView(R.id.empty_text) TextView mEmptyTextView;
    private @InjectView(R.id.empty_progressbar) ProgressBar mEmptyProgressBar;

    private Device mDevice;
    private DeviceManager mDeviceManager;
    private VirtualAlbumModel mAlbum;
    private LocalBroadcastManager mBroadcastManager;
    private ArrayAdapter<VirtualFileModel> mAdapter;
    private Set<String> mThumbnailHashes;


    private BroadcastReceiver mPatchReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isFinishing()) {
                return;
            }

            String action = intent.getAction();
            String ownerId = null;
            if (action.equals(GeneralPollerService.PollerAction.DIRECTORY)) {
                DirectoryPollingInfo info = (DirectoryPollingInfo)
                        intent.getSerializableExtra(GeneralPollerService.PARAM_DATA);
                ownerId = info.getOwner();
            } else if (action.equals(GeneralPollerService.PollerAction
                    .COLLECTION)) {
                CollectionPollingInfo info = (CollectionPollingInfo)
                        intent.getSerializableExtra(GeneralPollerService
                                .PARAM_DATA);
                ownerId = info.getOwner();
            }
            if (mDevice != null && ownerId != null &&
                    ownerId.equals(mDevice.getId())) {
                refresh(true);
            }
        }

    };

    private BroadcastReceiver mThumbnailReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ThumbnailPollingInfo info = (ThumbnailPollingInfo)intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            if (isFinishing()) {
                return;
            }
            if (mThumbnailHashes != null) {
                String[] updatedHashes = info.getHashes();
                if (updatedHashes == null) {
                    return;
                }

                for (String hash : updatedHashes) {
                    if (mThumbnailHashes.contains(hash)) {
                        refresh(true);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mDeviceManager = DeviceManager.from(this);
        } catch(AuthenticatorException e) {
            // not reach
            finish();
            return;
        }

        String dirPath = getIntent().getStringExtra(PARAM_ALBUM_DIR_PATH);
        if (dirPath.endsWith("/")) {
            dirPath = dirPath.substring(0, dirPath.length() - 1);
        }
        int idx = dirPath.lastIndexOf("/");
        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle(dirPath.substring(idx + 1));
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);

        fetchDeviceInfo();

        if (mBroadcastManager == null) {
            mBroadcastManager = LocalBroadcastManager
                    .getInstance(this);
        }
        mBroadcastManager.registerReceiver(mPatchReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.DIRECTORY));
        mBroadcastManager.registerReceiver(mPatchReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.COLLECTION));
        mBroadcastManager.registerReceiver(mThumbnailReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.THUMBNAIL));
    }

    private void fetchDeviceInfo() {
        String deviceId = getIntent().getStringExtra(PARAM_ALBUM_DEVICE_ID);

        final DialogInterface.OnClickListener onDialogClickListener =
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    fetchDeviceInfo();
                }
            }
        };

        mDeviceManager.getDeviceWithId(deviceId, new DeviceManager.OnGetDeviceListener() {
            @Override
            public void onGetDevice(Device device) {
                if (isFinishing()) {
                    return;
                }
                mDevice = device;
                createAlbum();
                setupAdapter();
                refresh();
            }

            @Override
            public void onFailure(Exception e) {
                if (isFinishing()) {
                    return;
                }
                new AlertDialog.Builder(AlbumActivity.this)
                        .setTitle(R.string.title_failed_to_load)
                        .setMessage(R.string.des_failed_to_fetch_device_info)
                        .setPositiveButton(R.string.button_retry, onDialogClickListener)
                        .setNegativeButton(R.string.button_confirm, null)
                        .show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBroadcastManager.unregisterReceiver(mPatchReceiver);
        mBroadcastManager.unregisterReceiver(mThumbnailReceiver);
    }

    private void createAlbum() {
        Intent intent = getIntent();

        String dirPath = intent.getStringExtra(PARAM_ALBUM_DIR_PATH);
        boolean includeNestedChildren = intent.getBooleanExtra
                (PARAM_ALBUM_INCLUDE_NESTED_CHILDREN, false);

        mAlbum = new VirtualAlbumModel(dirPath, mDevice, includeNestedChildren);
    }

    private void setupAdapter() {
        if (mAdapter == null) {
            SyncportApplication app = (SyncportApplication) getApplication();
            ImageLoader imageLoader = app.getDefaultImageLoader();
            mAdapter = new ImageAdapter(this, imageLoader);
        }

        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
    }

    private void registerThumbnailHashes(List<VirtualFileModel> files) {
        if (mThumbnailHashes == null) {
            mThumbnailHashes = new HashSet<>();
        } else {
            mThumbnailHashes.clear();
        }
        for (VirtualFileModel file : files) {
            String path = file.getAbsolutePath();
            if (FileUtils.isVideoPath(path) || FileUtils.isAudioPath(path) ||
                    FileUtils.isImagePath(path)) {

                String hash = file.getHash();
                if (hash != null) {
                    mThumbnailHashes.add(hash);
                }
            }
        }
    }

    private void display() {
        new DisplayCollectionTask().execute();
    }

    private void refresh() {
        refresh(false);
    }

    private void refresh(boolean hideLoader) {
        mAlbum.load(this, this);
        if (!hideLoader) {
            display();
        }
    }

    private void showEmptyView(String message, boolean withProgress) {
        mEmptyTextView.setText(message);
        mEmptyProgressBar
                .setVisibility(withProgress ? View.VISIBLE : View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
    }

    private void hideEmptyView() {
        mEmptyView.setVisibility(View.GONE);
        mGridView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        VirtualFileModel model = mAdapter.getItem(position);

        if (!mAlbum.getDevice().isTurnOn()) {
            String format = getString(R.string
                    .desc_file_access_deneied__device_turned_off);
            String msg = String.format(format, mAlbum.getDevice().getName());

            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(msg)
                    .setPositiveButton(R.string.button_confirm, null).show();
            return;
        }
        Intent intent = ImageDetailActivity.createIntent(this, model);
        startActivity(intent);
    }

    @Override
    public void onAlbumLoaded(boolean isChanged) {
        if (isFinishing()) {
            return;
        }
        if (isChanged) {
            display();
        } else if (mEmptyProgressBar.getVisibility() == View.VISIBLE) {
            hideEmptyView();
        }
    }

    @Override
    public void onAlbumLoadException(Exception e) {
        if (isFinishing()) {
            return;
        }
        String errorString = "";
        Throwable cause = e.getCause();
        if (cause != null && VolleyError.class.isAssignableFrom(cause.getClass())) {
            VolleyError error = (VolleyError) cause;
            if (e instanceof NetworkError) {
                errorString = getString(R.string
                        .desc_failed_to_connect_internet);
            } else {
                NetworkResponse response = error.networkResponse;
                int statusCode = response.statusCode;
                if (statusCode == 400) {
                    String format = getString(R.string
                            .desc_failed_to_load_400);
                    String message = String.format(format, mDevice.getName(),
                            mDevice.getName());
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.title_failed_to_load_400)
                            .setMessage(message)
                            .setPositiveButton(R.string.button_retry,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            refresh();
                                        }
                                    }).setNegativeButton(R.string.button_confirm, null).show();
                    return;
                }

                if (TextUtils.isEmpty(errorString) &&
                        (response != null) &&
                        (response.data != null) &&
                        (response.data.length > 0)) {
                    errorString = new String(response.data);
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = error.getMessage();
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = getString(R.string.desc_undefined_error);
                }
            }
        } else {
            errorString = e.getMessage();
        }
        String message = getString(R.string.desc_failed_to_load_dirlist);
        if (!TextUtils.isEmpty(errorString)) {
            message += " (" + errorString + ")";
        }
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_failed_to_load)
                .setMessage(message)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                refresh();
                            }
                        }).setNegativeButton(R.string.button_confirm, null).show();
    }

    private class DisplayCollectionTask extends AsyncTask<Void, Void,
            List<VirtualFileModel>> {

        @Override
        protected List<VirtualFileModel> doInBackground(Void... params) {
            List<VirtualFileModel> files = mAlbum.getFiles();
            if (files.size() > 1) {
                Collections.sort(files, new Comparator<VirtualFileModel>() {
                    @Override
                    public int compare(VirtualFileModel lhs, VirtualFileModel rhs) {
                        Date lhsModifiedAt = lhs.getModifiedAt();
                        Date rhsModifiedAt = rhs.getModifiedAt();
                        if (lhsModifiedAt != null && rhsModifiedAt != null) {
                            return rhsModifiedAt.getTime() - lhsModifiedAt.getTime() > 0 ? 1 : -1;
                        } else if (lhsModifiedAt != null && rhsModifiedAt == null) {
                            return -1;
                        } else if (lhsModifiedAt == null && rhsModifiedAt != null) {
                            return 1;
                        } else {
                            return lhs.getName().compareTo(rhs.getName());
                        }
                    }
                });
            }
            return files;
        }

        @Override
        protected void onPostExecute(List<VirtualFileModel> files) {
            if (isFinishing()) {
                return;
            }
            registerThumbnailHashes(files);

            mAdapter.clear();
            mAdapter.addAll(files);
            mAdapter.notifyDataSetChanged();

            if (files.size() == 0 && mAlbum.isLoaded()) {
                showEmptyView(getString(R.string.desc_empty_album), false);
            } else if (files.size() == 0 && !mAlbum.isLoaded()) {
                showEmptyView(getString(R.string.desc_loading_directory),
                        true);
            } else {
                hideEmptyView();
            }
        }
    }
}
