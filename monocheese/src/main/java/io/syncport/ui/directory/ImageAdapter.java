package io.syncport.ui.directory;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersSimpleAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualFileModel;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class ImageAdapter extends ArrayAdapter<VirtualFileModel> implements
        StickyGridHeadersSimpleAdapter{

    private static final int ITEM_LAYOUT_ID = R.layout.item_image;
    private static final int HEADER_LAYOUT_ID = R.layout
            .item_collection__header__image;

    private ImageLoader mImageLoader;
    private DisplayImageOptions mPhotoFileOptions;

    public ImageAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mPhotoFileOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_default_picture)
                .showImageOnFail(R.drawable.img_default_picture)
//                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(ITEM_LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        VirtualFileModel item = getItem(position);
        ImageAware imageAware = new ImageViewAware(viewHolder.getThumb(),
                false);
        mImageLoader.displayImage(item.getImageUrl(),
                imageAware, mPhotoFileOptions);
        return row;
    }

    @Override
    public long getHeaderId(int i) {
        VirtualFileModel file = getItem(i);
        Date modifiedAt = file.getModifiedAt();
        if (modifiedAt != null) {
            Calendar date = new GregorianCalendar();
            date.setTime(modifiedAt);
            date.set(Calendar.DAY_OF_MONTH, 1);
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            return date.getTimeInMillis();
        }
        return 0;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup viewGroup) {
        View row = convertView;
        HeaderViewHolder viewHolder;
        VirtualFileModel file = getItem(i);

        if (row == null) {
            row = mInflater.inflate(HEADER_LAYOUT_ID, null);
            viewHolder = new HeaderViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (HeaderViewHolder) row.getTag();
        }

        Date modifiedAt = file.getModifiedAt();
        String date = getContext().getString(R.string.date_unknown);
        if (modifiedAt != null) {
            date = new SimpleDateFormat("yyyy.MM").format(modifiedAt);
        }
        viewHolder.getTitle().setText(date);
        return row;
    }

    private class ViewHolder {
        private ImageView mThumbView;

        public ViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.photo);
        }

        public ImageView getThumb() {
            return mThumbView;
        }
    }


    private class HeaderViewHolder {
        private TextView mTitleView;

        public HeaderViewHolder(View view) {
            mTitleView = (TextView) view.findViewById(R.id.title);
        }

        public TextView getTitle() {
            return mTitleView;
        }
    }
}

