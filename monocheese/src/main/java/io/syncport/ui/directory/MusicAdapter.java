package io.syncport.ui.directory;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualFileModel;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class MusicAdapter extends ArrayAdapter<VirtualFileModel> implements
        StickyListHeadersAdapter{

    private static final int ITEM_LAYOUT_ID = R.layout.item_music;
    private static final int HEADER_LAYOUT_ID = R.layout.item_music__header;

    /*ㄱ ㄲ ㄴ ㄷ ㄸ ㄹ ㅁ ㅂ ㅃ ㅅ ㅆ ㅇ ㅈ ㅉ ㅊ ㅋ ㅌ ㅍ ㅎ */
    private static final char[] CHO = {0x3131, 0x3132, 0x3134, 0x3137,
            0x3138, 0x3139, 0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147,
            0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e};


    private ImageLoader mImageLoader;
    private DisplayImageOptions mMusicFileOptions;

    public MusicAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mMusicFileOptions = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .showImageOnFail(R.drawable.ic_default_music_cover)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(ITEM_LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        VirtualFileModel item = getItem(position);
        viewHolder.getName().setText(item.getName());

        if (item.getExtraMetaByKey("dr") != null) {
            long m = Long.parseLong(item.getExtraMetaByKey("dr"));
            int s = (int) (m / 1000);
            String duration = String.format("%02d:%02d", s/60, (s%60));
            viewHolder.getMeta().setText(duration);
            viewHolder.getMeta().setVisibility(View.VISIBLE);
        } else {
            viewHolder.getMeta().setVisibility(View.GONE);
        }
        mImageLoader.displayImage(item.getImageUrl(),
                viewHolder.getIconView(), mMusicFileOptions);
        return row;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup viewGroup) {
        View row = convertView;
        HeaderViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(HEADER_LAYOUT_ID, null);
            viewHolder = new HeaderViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (HeaderViewHolder) row.getTag();
        }

        viewHolder.getTitle().setText(getHeaderTitle(i));
        return row;
    }

    @Override
    public long getHeaderId(int i) {
        return getHeaderTitle(i).charAt(0);
    }

    public String getHeaderTitle(int i) {
        String name = getItem(i).getName().toUpperCase();
        Pattern numberPattern = Pattern.compile("[0-9]");
        Pattern alphabetPattern = Pattern.compile("[A-Z]");
        Pattern koreanPattern = Pattern.compile("[ㄱ-ㅎㅏ-ㅣ가-힣]");
        String firstLetter = name.substring(0, 1);
        if (numberPattern.matcher(firstLetter).matches()) {
            return "#";
        }
        if (alphabetPattern.matcher(firstLetter).matches()) {
            return name.substring(0, 1);
        }
        if (koreanPattern.matcher(firstLetter).matches()) {
            char l = firstLetter.charAt(0);
            if (l >= 0xAC00) {
                char uniVal = (char) (l -  0xAC00);
                char cho = (char) (((uniVal - (uniVal % 28))/28)/21);
                return CHO[cho]+"";
            }
        }
        return "*";
    }

    private class ViewHolder {
        private TextView mNameView;
        private TextView mMetaView;
        private ImageView mIconView;

        public ViewHolder(View view) {
            mIconView = (ImageView) view.findViewById(R.id.icon);
            mNameView = (TextView) view.findViewById(R.id.name);
            mMetaView = (TextView) view.findViewById(R.id.metainfo);
        }

        public TextView getName() {
            return mNameView;
        }

        public TextView getMeta() {
            return mMetaView;
        }

        public ImageView getIconView() {
            return mIconView;
        }
    }


    private class HeaderViewHolder {
        private TextView mTitleView;

        public HeaderViewHolder(View view) {
            mTitleView = (TextView) view.findViewById(R.id.title);
        }

        public TextView getTitle() {
            return mTitleView;
        }
    }
}

