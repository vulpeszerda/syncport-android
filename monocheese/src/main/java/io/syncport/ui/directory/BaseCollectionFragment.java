package io.syncport.ui.directory;

import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.CollectionPollingInfo;
import io.syncport.core.polling.model.DirectoryPollingInfo;
import io.syncport.core.polling.model.ThumbnailPollingInfo;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.BaseFragment;
import io.syncport.ui.SyncportApplication;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualCollectionModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.InjectView;

/**
 * Abstract Class for common fragments for listing and streaming files in other devices.
 * Streaming means `You can use other devices' files like using it's own files`
 * It is different from DirectoryFragment, which has hierarchy for exploring directory.
 * It just lists bunch of files for images, videos, musics and so on.
 *
 * Subclasses extend this should override following methods.
 *
 * @Fragment.onCreateView       render fragment's layout
 * @Fragment.onActivityCreated  setup adapter to listview (or gridview)
 *
 * @createCollectionModel  create suitable collection model
 * @getCollectionModel      get created collection model
 * @getAdapter          get suitable Adapter
 * @setupAdapter        set adapter to given adapterView
 * @displayCollections  display file lists (in collection)
 *
 * (Optional)
 * @firstLaunched
 */
public abstract class BaseCollectionFragment<T> extends BaseFragment implements
        AdapterView.OnItemClickListener, DeviceManager.Observer,
        VirtualCollectionModel.LoadListener {

    /**
     * Variables
     */

    protected @InjectView(R.id.empty_view) View mEmptyView;
    protected @InjectView(R.id.empty_text) TextView mEmptyTextView;
    protected @InjectView(R.id.empty_progressbar) ProgressBar mEmptyProgressBar;

    protected static final String PARAM_DEVICE_ID = SyncportKeyBuilder.build
            ("device_id");


    private ImageLoader mImageLoader;
    private Device mDevice;
    private DeviceManager mDeviceManager;
    private VirtualCollectionModel mCollectionModel;
    private LocalBroadcastManager mBroadcastManager;
    protected Set<String> mThumbnailHashes;

    private boolean mIsFirstLaunch;
    private boolean mIsFragmentPaused;

    private BroadcastReceiver mPatchReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String ownerId = null;
            if (action.equals(GeneralPollerService.PollerAction.DIRECTORY)) {
                DirectoryPollingInfo info = (DirectoryPollingInfo)
                        intent.getSerializableExtra(GeneralPollerService.PARAM_DATA);
                ownerId = info.getOwner();
            } else if (action.equals(GeneralPollerService.PollerAction
                    .COLLECTION)) {
                CollectionPollingInfo info = (CollectionPollingInfo)
                        intent.getSerializableExtra(GeneralPollerService
                                .PARAM_DATA);
                ownerId = info.getOwner();
            }
            if (mDevice != null && ownerId != null &&
                    ownerId.equals(mDevice.getId())) {
                if (isAdded()) {
                    refresh(true);
                }
            }
        }
    };

    private BroadcastReceiver mThumbnailReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ThumbnailPollingInfo info = (ThumbnailPollingInfo)intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            if (mThumbnailHashes != null) {
                String[] updatedHashes = info.getHashes();
                if (updatedHashes == null) {
                    return;
                }

                for (String hash : updatedHashes) {
                    if (mThumbnailHashes.contains(hash) && isAdded()) {
                        refresh(true);
                    }
                }
            }
        }
    };

    protected abstract VirtualCollectionModel createCollectionModel(
            Device device);
    protected abstract ArrayAdapter<T> getAdapter();
    protected abstract void setupAdapter();
    protected abstract void displayCollections();

    /**
     * Overridden methods
     * Methods implementing fragment lifecycle
     */

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((BaseActivity) activity).supportInvalidateOptionsMenu();

        if (mBroadcastManager == null) {
            mBroadcastManager = LocalBroadcastManager
                    .getInstance(getActivity());
        }
        mBroadcastManager.registerReceiver(mPatchReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.DIRECTORY));
        mBroadcastManager.registerReceiver(mPatchReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.COLLECTION));
        mBroadcastManager.registerReceiver(mThumbnailReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.THUMBNAIL));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mIsFirstLaunch = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mDeviceManager == null) {
            try {
                mDeviceManager = DeviceManager.from(getActivity());
                mDeviceManager.addObserver(this);
            } catch (AuthenticatorException e) {
                // will not happen
                e.printStackTrace();
            }
        }
        if (mIsFirstLaunch) {
            mIsFirstLaunch = false;
            firstLaunched();
            fetchDeviceInfo();
        }
        setupAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsFragmentPaused = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        mIsFragmentPaused = true;
    }

    @Override
    public void onDestroy() {
        if (mDeviceManager != null) {
            mDeviceManager.removeObserver(this);
        }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mBroadcastManager != null) {
            mBroadcastManager.unregisterReceiver(mPatchReceiver);
            mBroadcastManager.unregisterReceiver(mThumbnailReceiver);
        }
    }

    /**
     * Overridden methods
     * Fragment options, etc
     */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.directory, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Overridden methods
     * DeviceManager.Observer
     */

    @Override
    public void onDeviceTurnOn(Device device) {

    }

    @Override
    public void onDeviceTurnOff(Device device) {

    }

    @Override
    public void onNewDeviceAdded(Device device) {

    }

    @Override
    public void onDeviceNameChanged(Device device, String name) {

    }

    @Override
    public void onCollectionLoaded(boolean isChanged) {
        if (!isAdded()) {
            return;
        }
        if (isChanged) {
            displayCollections();
        } else if (mEmptyProgressBar.getVisibility() == View.VISIBLE) {
            hideEmptyView();
        }
    }

    @Override
    public void onCollectionLoadException(Exception e) {
        if (!isAdded()) {
            return;
        }
        String errorString = "";
        Throwable cause = e.getCause();
        if (cause != null && VolleyError.class.isAssignableFrom(cause.getClass())) {
            VolleyError error = (VolleyError) cause;
            if (e instanceof NetworkError) {
                errorString = getString(R.string
                        .desc_failed_to_connect_internet);
            } else {
                NetworkResponse response = error.networkResponse;
                int statusCode = response.statusCode;
                if (statusCode == 400) {
                    String format = getString(R.string
                            .desc_failed_to_load_400);
                    String message = String.format(format, mDevice.getName(),
                            mDevice.getName());
                    new AlertDialog.Builder(getBaseActivity())
                            .setTitle(R.string.title_failed_to_load_400)
                            .setMessage(message)
                            .setPositiveButton(R.string.button_retry,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            refresh();
                                        }
                                    }).setNegativeButton(R.string.button_confirm, null).show();
                    return;
                }

                if (TextUtils.isEmpty(errorString) &&
                        (response != null) &&
                        (response.data != null) &&
                        (response.data.length > 0)) {
                    errorString = new String(response.data);
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = error.getMessage();
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = getString(R.string.desc_undefined_error);
                }
            }
        } else {
            errorString = e.getMessage();
        }
        String message = getString(R.string.desc_failed_to_load_dirlist);
        if (!TextUtils.isEmpty(errorString)) {
            message += " (" + errorString + ")";
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_failed_to_load)
                .setMessage(message)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                refresh();
                            }
                        }).setNegativeButton(R.string.button_confirm, null).show();
    }

    /**
     * Private methods
     */

    private void fetchDeviceInfo() {
        String deviceId = getArguments().getString(PARAM_DEVICE_ID);

        final DialogInterface.OnClickListener onDialogClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            fetchDeviceInfo();
                        }
                    }
                };

        mDeviceManager.getDeviceWithId(deviceId, new DeviceManager.OnGetDeviceListener() {
            @Override
            public void onGetDevice(Device device) {
                mDevice = device;
                if (isAdded()) {
                    refresh();
                }
            }

            @Override
            public void onFailure(Exception e) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.title_failed_to_load)
                        .setMessage(R.string.des_failed_to_fetch_device_info)
                        .setPositiveButton(R.string.button_retry, onDialogClickListener)
                        .setNegativeButton(R.string.button_confirm, null)
                        .show();
            }
        });
    }

    private void refresh() {
        refresh(false);
    }

    private void refresh(boolean hideLoader) {
        if (mCollectionModel == null) {
            mCollectionModel = createCollectionModel(mDevice);
        }
        if (!hideLoader) {
            showEmptyView(getString(R.string.desc_loading_directory), true);
        }
        mCollectionModel.load(getActivity(), this);
    }



    /**
     * Protected methods
     */


    /**
     * Processes for first launch of fragment.
     * it is called when BaseCollectionFragment.this.onActivityCreated is running.
     * Override this if you need.
     */
    protected void firstLaunched() {
    }

    protected VirtualCollectionModel getCollection() {
        return mCollectionModel;
    }

    protected ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            SyncportApplication app = (SyncportApplication) getActivity()
                    .getApplication();
            mImageLoader = app.getDefaultImageLoader();
        }
        return mImageLoader;
    }

    protected void showEmptyView(String message, boolean withProgress) {
        mEmptyTextView.setText(message);
        mEmptyProgressBar
                .setVisibility(withProgress ? View.VISIBLE : View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    protected void hideEmptyView() {
        mEmptyView.setVisibility(View.GONE);
    }

    protected void registerThumbnailHashes(List<VirtualFileModel> files) {
        if (mThumbnailHashes == null) {
            mThumbnailHashes = new HashSet<>();
        } else {
            mThumbnailHashes.clear();
        }
        for (VirtualFileModel file : files) {
            String path = file.getAbsolutePath();
            if (FileUtils.isVideoPath(path) || FileUtils.isAudioPath(path) ||
                    FileUtils.isImagePath(path)) {

                String hash = file.getHash();
                if (hash != null) {
                    mThumbnailHashes.add(hash);
                }
            }
        }
    }
}
