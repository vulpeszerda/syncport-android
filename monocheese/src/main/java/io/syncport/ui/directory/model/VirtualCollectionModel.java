package io.syncport.ui.directory.model;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

import io.syncport.core.device.Device;
import io.syncport.core.directory.DirectoryUtils;
import io.syncport.core.remote.DeltaResources;

public abstract class VirtualCollectionModel {

    public static interface LoadListener {
        void onCollectionLoaded(boolean isChanged);

        void onCollectionLoadException(Exception e);
    }

    protected Device mDevice;
    private boolean mIsLoaded;
    private Map<String, Map<String, VirtualFileModel>> mFiles;

    protected abstract String getType();

    public VirtualCollectionModel(Device device) {
        mFiles = new HashMap<>();
        mDevice = device;
    }

    public Device getDevice() {
        return mDevice;
    }

    public synchronized List<VirtualFileModel> getAll() {
        List<VirtualFileModel> all = new ArrayList<>();

        for (Map<String, VirtualFileModel> files : mFiles.values()) {
            all.addAll(files.values());
        }
        return all;
    }

    public synchronized void clear() {
        mFiles.clear();
        mIsLoaded = false;
    }

    public synchronized boolean isLoaded() {
        return mIsLoaded;
    }

    public void load(Context context, LoadListener listener) {
        new LoadTask(context, mDevice.getId(), getType(), listener).execute();
    }

    protected synchronized boolean buildChildrenFromJSON(
            Context context, JSONObject response) throws JSONException {

        boolean isChanged = false;

        JSONObject data = response.getJSONObject("data");
        Iterator<String> iter = data.keys();

        while (iter.hasNext()) {
            String parentPath = iter.next();
            JSONArray array = data.getJSONArray(parentPath);

            Map<String, VirtualFileModel> files = mFiles.get(parentPath);

            if (files == null) {
                files = new HashMap<>();
                mFiles.put(parentPath, files);
            }


            Set<VirtualFileModel> removeCandidates = new HashSet<>();
            removeCandidates.addAll(files.values());

            for (int idx = 0; idx < array.length(); idx++) {

                JSONObject fileObj = array.getJSONObject(idx);
                String name = fileObj.getString("n");


                JSONObject metaObj = fileObj.getJSONObject("m");

                Map<String, String> meta = new HashMap<>();
                if (metaObj != null) {
                    Iterator<String> metaKeyIter = metaObj.keys();
                    while (metaKeyIter.hasNext()) {
                        String key = metaKeyIter.next();
                        meta.put(key, metaObj.getString(key));
                    }
                }

                if (name == null) {
                    continue;
                }

                String path = parentPath + name;
                VirtualFileModel file = files.get(name);

                if (file == null) {
                    file = new VirtualFileModel(name, path, mDevice);
                    files.put(name, file);
                    isChanged = true;
                } else {
                    removeCandidates.remove(file);
                }
                isChanged |= file.updateMeta(meta);
            }
            for (VirtualFileModel file : removeCandidates) {
                files.remove(file.getName());
                isChanged = true;
            }
        }
        setIsLoaded(true);
        return isChanged;
    }

    synchronized void setIsLoaded(boolean isLoaed) {
        mIsLoaded = isLoaed;
    }

    private static class LoadResult {
        boolean isSuccess;
        boolean isChanged;
        Exception exception;

        public static LoadResult buildSuccessResult(boolean isChanged) {
            LoadResult result = new LoadResult();
            result.isSuccess = true;
            result.isChanged = isChanged;
            return result;
        }

        public static LoadResult buildFailureResult(Exception e) {
            LoadResult result = new LoadResult();
            result.isSuccess = false;
            result.exception = e;
            return result;
        }
    }

    private class LoadTask extends AsyncTask<Void, Void, LoadResult> {

        private Context mContext;
        private String mOwner;
        private String mFileType;
        private LoadListener mListener;


        public LoadTask(Context context, String owner, String fileType,
                        LoadListener listener) {
            mContext = context;
            mOwner = owner;
            mFileType = fileType;
            mListener = listener;
        }

        @Override
        protected LoadResult doInBackground(Void... args) {
            Map<String, String> params = new HashMap<>();
            params.put("owner", mOwner);
            params.put("file_type", mFileType);

            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            DeltaResources.pull(mContext, "collection", params, future, future);

            try {
                JSONObject response = future.get();
                boolean isChanged = buildChildrenFromJSON(mContext, response);
                return LoadResult.buildSuccessResult(isChanged);
            } catch (JSONException |
                    ExecutionException | InterruptedException e) {
                return LoadResult.buildFailureResult(e);
            }
        }

        @Override
        protected void onPostExecute(LoadResult result) {
            if (result.isSuccess) {
                mListener.onCollectionLoaded(result.isChanged);
            } else {
                mListener.onCollectionLoadException(result.exception);
            }
        }

    }

}
