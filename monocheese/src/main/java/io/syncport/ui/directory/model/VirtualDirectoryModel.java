package io.syncport.ui.directory.model;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.syncport.core.device.Device;
import io.syncport.core.remote.DeltaResources;

public class VirtualDirectoryModel extends VirtualFileModel implements
        Cloneable {


    public static interface LoadListener {
        void onLoaded(Set<VirtualDirectoryModel> changedDirs);
        void onException(Runnable retryRunnable, Exception e);
    }

    public static VirtualDirectoryModel copyAsParentDirectory(
            VirtualDirectoryModel model) throws CloneNotSupportedException {

        VirtualDirectoryModel parentModel = (VirtualDirectoryModel) model
                .clone();
        parentModel.mIsParentDirectory = true;
        return parentModel;
    }

    protected HashMap<String, VirtualFileModel> mChildren;

    private boolean mIsParentDirectory = false;
    private AtomicBoolean mIsLoaded;


    public VirtualDirectoryModel(String name, String absolutePath,
                                 Device device) {
        this(Type.DIRECTORY, name, absolutePath, device);
    }

    protected VirtualDirectoryModel(Type type, String name,
                                   String absolutePath, Device device) {
        super(type, name, absolutePath, device);
        mChildren = new HashMap<>();
        mIsLoaded = new AtomicBoolean(false);
    }

    public Map<String, VirtualFileModel> getChildrenMap() {
        return mChildren;
    }

    public List<VirtualFileModel> getChildren() {
        ArrayList<VirtualFileModel> children =
                new ArrayList<>(mChildren.values());
        if (children.size() > 1) {
            Collections.sort(children, new Comparator<VirtualFileModel>() {

                @Override
                public int compare(VirtualFileModel lhs, VirtualFileModel rhs) {
                    if (lhs.getType() == rhs.getType()) {
                        return lhs.getName().compareTo(rhs.getName());
                    }
                    if (lhs.isDevice()) {
                        return -1;
                    } else if (lhs.isDirectory()) {
                        return rhs.isDevice() ? 1 : -1;
                    } else {
                        return 1;
                    }
                }
            });
        }
        return children;
    }

    public VirtualFileModel getChildByName(String name) {
        synchronized (mChildren) {
            return mChildren.get(name);
        }
    }

    public boolean isParentDirectory() {
        return mIsParentDirectory;
    }

    public boolean isSyncRoot() {
        String syncRootPath = getSyncRootPath();
        return syncRootPath != null &&
                syncRootPath.equals(getAbsolutePath());
    }

    public String getSyncRootPath() {
        VirtualDeviceModel root = getRoot();
        if (root != null) {
            return root.getSyncRootPath();
        }
        return null;
    }

    public void setSyncRootPath(String path) {
        VirtualDeviceModel root = getRoot();
        if (root != null) {
            root.setSyncRootPath(path);
        }
    }


    public VirtualFileModel removeChild(String name) {
        synchronized (mChildren) {
            return mChildren.remove(name);
        }
    }


    public void clearChildren() {
        synchronized (mChildren) {
            mChildren.clear();
        }
        mIsLoaded.set(false);
    }

    public boolean isLoaded() {
        return mIsLoaded.get();
    }

    public void preLoadSubDirectories(Context context, final int depth,
                                      final LoadListener listener) {
        if (!isLoaded()) {
            loadSubDirectories(context, depth, listener);
            return;
        }
        if (depth > 1) {
            for (VirtualFileModel file : mChildren.values()) {
                if (file.isFile()) {
                    continue;
                }
                VirtualDirectoryModel dir = (VirtualDirectoryModel) file;
                dir.preLoadSubDirectories(context, depth - 1, listener);
            }
        }
    }

    public void loadSubDirectories(final Context context,
                                   final int depth,
                                   final LoadListener listener) {

        final Runnable retryRunnable = new Runnable() {

            @Override
            public void run() {
                loadSubDirectories(context, depth, listener);
            }
        };

        new LoadTask(context, getDevice().getId(), getAbsolutePath(), depth,
                retryRunnable, listener).execute();
    }

    public Set<VirtualFileModel> getSubFilesWithDepth(int depth) {
        Set<VirtualFileModel> files = new HashSet<>();
        for(VirtualFileModel file : mChildren.values()) {
            if (!file.isFile() && depth > 1) {
                VirtualDirectoryModel dir = (VirtualDirectoryModel) file;
                files.addAll(dir.getSubFilesWithDepth(depth - 1));
            }
            files.add(file);
        }
        return files;
    }

    protected synchronized Set<VirtualDirectoryModel> buildChildrenFromJSON
            (Context context, int depth, JSONObject response) throws
            JSONException{

        if (getSyncRootPath() == null) {
            try {
                setSyncRootPath(getDevice().getSyncRootPath(context, false));
            } catch(InterruptedException e) {
                // do nothing
                setSyncRootPath("/");
            } catch (ExecutionException e) {
                setSyncRootPath("/");
            }
        }

        Set<VirtualDirectoryModel> needUpdateDirs = new HashSet<>();
        Set<VirtualFileModel> removeCandidates = getSubFilesWithDepth(depth);

        JSONArray data = response.getJSONArray("data");
        for (int idx = 0; idx < data.length(); idx++) {
            JSONObject obj = data.getJSONObject(idx);

            String path = obj.getString("n");
            JSONObject metaObj = obj.optJSONObject("m");

            Map<String, String> meta = new HashMap<>();
            if (metaObj != null) {
                Iterator<String> metaKeyIter = metaObj.keys();
                while (metaKeyIter.hasNext()) {
                    String key = metaKeyIter.next();
                    meta.put(key, metaObj.getString(key));
                }
            }

            String[] subPaths = path.split("/");
            String childAbsPath = this.getAbsolutePath();
            VirtualDirectoryModel curr = this;
            VirtualFileModel file;
            for (int i = 0; i < subPaths.length; i++) {
                String subPath = subPaths[i];
                String key = subPath;

                if (i < subPaths.length - 1 || path.endsWith("/")) {
                    key += "/";
                }

                childAbsPath += key;

                synchronized (curr.mChildren) {
                    file = curr.getChildByName(key);
                    if (file == null) {
                        if (key.endsWith("/")) {
                            file = new VirtualDirectoryModel(subPath,
                                    childAbsPath, getDevice());
                        } else {
                            file = new VirtualFileModel(subPath, childAbsPath,
                                    getDevice());
                        }
                        curr.addChild(file);
                        curr.setIsLoaded(true);
                        needUpdateDirs.add(curr);
                    } else {
                        removeCandidates.remove(file);
                    }
                    if (file.updateMeta(meta)) {
                        needUpdateDirs.add(curr);
                    }
                    if (i < depth - 1 && !file.isFile()) {
                        ((VirtualDirectoryModel)file).setIsLoaded(true);
                    }
                }
                if (i < subPaths.length - 1 && file.isFile()) {
                    throw new RuntimeException("Invalid path & file match");
                } else if (i < subPaths.length - 1) {
                    curr = (VirtualDirectoryModel) file;
                }
            }
        }

        for (VirtualFileModel file : removeCandidates) {
            String name = file.getName();
            if (!file.isFile()) {
                name += "/";
            }
            VirtualDirectoryModel parent = file.getParent();
            needUpdateDirs.add(parent);
            parent.removeChild(name);
        }

        setIsLoaded(true);
        needUpdateDirs.add(VirtualDirectoryModel.this);

        return needUpdateDirs;
    }

    void addChild(VirtualFileModel model) {
        model.setParent(this);
        String name = model.getName();
        if (model.isDirectory()) {
            name += "/";
        }
        synchronized (mChildren) {
            mChildren.put(name, model);
        }
    }

    void setIsLoaded(boolean isLoaded) {
        mIsLoaded.set(isLoaded);
    }

    private static class LoadResult {
        boolean isSuccess;
        Set<VirtualDirectoryModel> changedDirs;
        Exception exception;

        public static LoadResult buildSuccessResult(
                Set<VirtualDirectoryModel> changedDirs) {
            LoadResult result = new LoadResult();
            result.isSuccess = true;
            result.changedDirs = changedDirs;
            return result;
        }

        public static LoadResult buildFailureResult(Exception e) {
            LoadResult result = new LoadResult();
            result.isSuccess = false;
            result.exception = e;
            return result;
        }
    }

    private class LoadTask extends AsyncTask<Void, Void, LoadResult> {

        private Context mContext;
        private String mOwner;
        private String mPath;
        private int mDepth;
        private Runnable mRetryRunnable;
        private LoadListener mListener;


        public LoadTask(Context context, String owner, String path,
                        int depth, Runnable retryRunnable,
                        LoadListener listener) {
            mContext = context;
            mOwner = owner;
            mPath = path;
            mDepth = depth;
            mListener = listener;
            mRetryRunnable = retryRunnable;
        }

        @Override
        protected LoadResult doInBackground(Void... args) {

            Map<String, String> param = new HashMap<>();
            param.put("owner", mOwner);
            param.put("plain", mPath);
            param.put("depth", String.valueOf(mDepth));

            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            DeltaResources.pull(mContext, "directory", param, future, future);

            try {
                JSONObject response = future.get();
                final Set<VirtualDirectoryModel> needUpdateDirs;
                needUpdateDirs = buildChildrenFromJSON(mContext, mDepth,
                        response);
                return LoadResult.buildSuccessResult(needUpdateDirs);
            } catch (JSONException |
                    ExecutionException | InterruptedException e) {
                return LoadResult.buildFailureResult(e);
            }
        }

        @Override
        protected void onPostExecute(LoadResult result) {
            if (result.isSuccess) {
                mListener.onLoaded(result.changedDirs);
            } else {
                mListener.onException(mRetryRunnable, result.exception);
            }
        }
    }
}
