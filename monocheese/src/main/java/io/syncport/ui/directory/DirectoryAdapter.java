package io.syncport.ui.directory;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualDirectoryModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class DirectoryAdapter extends ArrayAdapter<VirtualFileModel> {

    private static final int LAYOUT_ID = R.layout.item_directory;
    private DisplayImageOptions mMediaFileOptions;
    private DisplayImageOptions mPhotoFileOptions;
    private ImageLoader mImageLoader;

    public DirectoryAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mMediaFileOptions = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .showImageOnFail(R.drawable.ic_video)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();
        mPhotoFileOptions = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .showImageOnFail(R.drawable.ic_photo)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build();
    }

    protected void update(ViewHolder viewHolder, VirtualFileModel model) {
        if (model instanceof VirtualDirectoryModel) {
            VirtualDirectoryModel dirModel = (VirtualDirectoryModel) model;

            if (dirModel.isParentDirectory()) {
                updateAsParentDirectory(viewHolder, dirModel);
            } else {
                updateAsDirectory(viewHolder, dirModel);
            }
        } else {
            updateAsFile(viewHolder, model);
        }
    }

    private void updateAsParentDirectory(ViewHolder viewHolder, VirtualDirectoryModel model) {
        TextView nameView = viewHolder.getNameView();

        nameView.setText(model.getName());
        mImageLoader.displayImage("drawable://" + R.drawable.ic_parent,
                viewHolder.getIconView());
    }

    private void updateAsDirectory(ViewHolder viewHolder, VirtualDirectoryModel model) {
        TextView nameView = viewHolder.getNameView();

        nameView.setText(model.getName());
        mImageLoader.displayImage("drawable://" + R.drawable.ic_folder, viewHolder.getIconView());
    }

    private void updateAsFile(ViewHolder viewHolder, VirtualFileModel model) {
        TextView nameView = viewHolder.getNameView();
//        TextView metaView = viewHolder.getmMetaInfoView();
        String fileName = model.getName().toLowerCase(Locale.ENGLISH);

        nameView.setText(model.getName());
//        if (model.getSize() > -1 && model.getModifiedAt() != null) {
//
//            String size = FileUtils.readableFileSize(model.getSize());
//            String date = new SimpleDateFormat("yyyy.MM.dd a hh:mm:ss")
//                    .format(model.getModifiedAt());
//
//            metaView.setText(size + ", " + date);
//            metaView.setVisibility(View.VISIBLE);
//        } else {
//            metaView.setVisibility(View.GONE);
//        }

        DisplayImageOptions options;

        if (FileUtils.isVideoPath(fileName) || FileUtils.isAudioPath(fileName)) {
            options = mMediaFileOptions;
            mImageLoader.displayImage(model.getImageUrl(),
                    viewHolder.getIconView(), options);
        } else if (FileUtils.isImagePath(fileName)) {
            options = mPhotoFileOptions;
            mImageLoader.displayImage(model.getImageUrl(),
                    viewHolder.getIconView(), options);
        } else {
            mImageLoader.displayImage("drawable://" + R.drawable.ic_file,
                    viewHolder.getIconView());
        }
    }

    public void addParent(VirtualDirectoryModel parent) {
        try {
            add(0, VirtualDirectoryModel.copyAsParentDirectory(parent));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, null);

            TextView name = (TextView) row.findViewById(R.id.name);
            ImageView icon = (ImageView) row.findViewById(R.id.icon);
            TextView meta = (TextView) row.findViewById(R.id.metainfo);

            viewHolder = new ViewHolder(name, icon, meta);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }
        update(viewHolder, getItem(position));
        return row;
    }

    private class ViewHolder {
        private TextView mNameView;
        private TextView mMetaInfoView;
        private ImageView mIconView;

        public ViewHolder(TextView name, ImageView icon, TextView meta) {
            mNameView = name;
            mIconView = icon;
            mMetaInfoView = meta;
        }

        public TextView getNameView() {
            return mNameView;
        }

        public ImageView getIconView() {
            return mIconView;
        }

        public TextView getmMetaInfoView() {
            return mMetaInfoView;
        }
    }
}
