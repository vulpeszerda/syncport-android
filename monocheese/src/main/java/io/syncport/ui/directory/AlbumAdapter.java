package io.syncport.ui.directory;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersSimpleAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualAlbumModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class AlbumAdapter extends ArrayAdapter<VirtualAlbumModel> {

    private static final int LAYOUT_ID = R.layout.item_album;

    private ImageLoader mImageLoader;
    private DisplayImageOptions mPhotoFileOptions;

    public AlbumAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mPhotoFileOptions = new DisplayImageOptions.Builder()
//                .showImageOnLoading(new ColorDrawable(0x00ffffff))
                .showImageOnLoading(R.drawable.img_default_picture)
//                .displayer(new FadeInBitmapDisplayer(300))
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.img_default_picture).cacheInMemory(true)
                .build();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }
        VirtualAlbumModel album = getItem(position);

        viewHolder.getName().setText(album.getName());

        Resources res = getContext().getResources();
        int count = album.getCount();
        String countStr = res.getQuantityString(R.plurals
                .image_count__format, count, count);
        viewHolder.getCount().setText(countStr);

        VirtualFileModel thumbnailFile = album.getThumbnailFile();
        if (thumbnailFile == null) {
            mImageLoader.displayImage("drawable://" + R.drawable
                            .img_default_picture,
                    viewHolder.getThumb());
        } else {
            mImageLoader.displayImage(thumbnailFile.getImageUrl(),
                    viewHolder.getThumb(), mPhotoFileOptions);
        }
        return row;

    }

    @Override
    public void notifyDataSetChanged() {
        List<VirtualAlbumModel> albums = new ArrayList<>(getItems());
        if (albums.size() > 1) {
            Collections.sort(albums, new Comparator<VirtualAlbumModel>() {
                @Override
                public int compare(VirtualAlbumModel lhs, VirtualAlbumModel rhs) {
                    return rhs.getCount() - lhs.getCount();
                }
            });
        }
        clear();
        addAll(albums);
        super.notifyDataSetChanged();
    }

    private class ViewHolder {
        private TextView mNameView;
        private TextView mCountView;
        private ImageView mThumbView;

        public ViewHolder(View view) {
            mThumbView = (ImageView) view.findViewById(R.id.photo);
            mNameView = (TextView) view.findViewById(R.id.name);
            mCountView = (TextView) view.findViewById(R.id.count);
        }

        public TextView getName() {
            return mNameView;
        }

        public ImageView getThumb() {
            return mThumbView;
        }

        public TextView getCount() {
            return mCountView;
        }
    }
}

