package io.syncport.ui.directory.model;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import io.syncport.core.device.Device;
import io.syncport.core.remote.DeltaResources;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class VirtualAlbumModel {

    public static interface LoadListener {
        void onAlbumLoaded(boolean isChanged);

        void onAlbumLoadException(Exception e);
    }

    HashMap<String, VirtualFileModel> mChildren;

    private String mName;
    private String mPath;
    private Device mDevice;
    private String mSyncRootPath;
    private VirtualFileModel mCachedThumbnailFile;
    private boolean mIncludeNestedChildren;
    private boolean mIsLoaded;

    private Comparator<VirtualFileModel> mComparator = new
            Comparator<VirtualFileModel>() {
                @Override
                public int compare(VirtualFileModel lhs, VirtualFileModel rhs) {
                    Date lhsModifiedAt = lhs.getModifiedAt();
                    Date rhsModifiedAt = rhs.getModifiedAt();
                    if (lhsModifiedAt == null && rhsModifiedAt != null) {
                        return 1;
                    }
                    if (lhsModifiedAt != null && rhsModifiedAt == null) {
                        return -1;
                    }
                    if (lhsModifiedAt == null && rhsModifiedAt == null) {
                        return lhs.getName().compareTo(rhs.getName());
                    }
                    long val = rhsModifiedAt.getTime() - lhsModifiedAt.getTime();
                    if (val < 0) {
                        return -1;
                    } else if (val > 0) {
                        return 1;
                    }
                    return 0;
                }
            };

    public VirtualAlbumModel(String dirPath, Device device,
                             boolean includeNestedChildren) {

        String p = dirPath.endsWith("/") ? dirPath.substring(0,
                dirPath.length() - 1) : dirPath;
        int matchIdx = p.lastIndexOf("/");
        mName = p.substring(matchIdx + 1);

        mPath = dirPath;
        mDevice = device;
        mChildren = new HashMap<>();
        mIncludeNestedChildren = includeNestedChildren;
        mIsLoaded = false;
    }

    public synchronized void add(VirtualFileModel file) {
        if (!file.isFile()) {
            return;
        }
        file.setAlbum(this);
        mChildren.put(file.getAbsolutePath(), file);
    }

    public synchronized VirtualFileModel remove(VirtualFileModel file) {
        return mChildren.remove(file.getAbsolutePath());
    }

    public synchronized VirtualFileModel removeByPath(String path) {
        return mChildren.remove(path);

    }

    public synchronized void clear() {
        mChildren.clear();
        mIsLoaded = false;
    }

    public String getName() {
        return mName;
    }

    public int getCount() {
        return mChildren.size();
    }

    public boolean isIncludeNestedChildren() {
        return mIncludeNestedChildren;
    }

    public VirtualFileModel buildThumbnailFile() {
        if (mCachedThumbnailFile == null) {
            mCachedThumbnailFile = Collections.min(mChildren.values(),
                    mComparator);
        }
        return mCachedThumbnailFile;
    }
    public VirtualFileModel getThumbnailFile() {
        return mCachedThumbnailFile;
    }

    public String getPath() {
        return mPath;
    }

    public Device getDevice() {
        return mDevice;
    }

    public synchronized List<VirtualFileModel> getFiles() {
        return new ArrayList<>(mChildren.values());
    }

    public synchronized boolean isLoaded() {
        return mIsLoaded;
    }

    public void load(Context context, LoadListener listener) {
        new LoadTask(context, mDevice.getId(), VirtualAlbumCollectionModel
                .COLLECTION_TYPE, listener).execute();
    }

    protected synchronized boolean buildChildrenFromJSON(
            Context context, JSONObject response) throws JSONException {

        if (mSyncRootPath == null) {
            Device device = getDevice();
            try {
                mSyncRootPath =  device.getSyncRootPath(context, false);
            } catch (InterruptedException e) {
                // do nothing
                mSyncRootPath = "/";
            } catch (ExecutionException e) {
                mSyncRootPath = "/";
            }
        }

        boolean isChanged = false;

        JSONObject data = response.getJSONObject("data");
        Iterator<String> iter = data.keys();
        Set<VirtualFileModel> removeCandidates =
                new HashSet<>(mChildren.values());

        while (iter.hasNext()) {
            String parentPath = iter.next();
            if (!parentPath.startsWith(mPath)) {
                continue;
            }

            String subPath = parentPath.substring(mSyncRootPath.length());
            String[] subPaths = subPath.split("/");
            String albumPath = mSyncRootPath;

            for (int i = 0; i < subPaths.length && i <
                    VirtualAlbumCollectionModel.MAX_ALBUM_DEPTH; i++) {
                albumPath += subPaths[i] + "/";
            }

            if (!albumPath.equals(mPath)) {
                continue;
            }

            JSONArray array = data.getJSONArray(parentPath);

            for (int idx = 0; idx < array.length(); idx++) {
                JSONObject fileObj = array.getJSONObject(idx);
                String filename = fileObj.getString("n");

                Map<String, String> meta = new HashMap<>();
                JSONObject metaObj = fileObj.optJSONObject("m");
                Iterator<String> metaKeyIter = metaObj.keys();
                while (metaKeyIter.hasNext()) {
                    String key = metaKeyIter.next();
                    meta.put(key, metaObj.getString(key));
                }

                String path = parentPath + filename;

                VirtualFileModel file = mChildren.get(path);
                if (file == null) {
                    file = new VirtualFileModel(filename, path, mDevice);
                    add(file);
                    isChanged = true;
                } else {
                    removeCandidates.remove(file);
                }
                isChanged |= file.updateMeta(meta);
            }
        }

        for (VirtualFileModel file : removeCandidates) {
            VirtualAlbumModel album = file.getAlbum();
            album.remove(file);
            isChanged = true;
        }
        setIsLoaded(true);
        return isChanged;
    }

    synchronized void setIsLoaded(boolean isLoaded) {
        mIsLoaded = isLoaded;
    }

    private static class LoadResult {
        boolean isSuccess;
        boolean isChanged;
        Exception exception;

        public static LoadResult buildSuccessResult(boolean isChanged) {
            LoadResult result = new LoadResult();
            result.isSuccess = true;
            result.isChanged = isChanged;
            return result;
        }

        public static LoadResult buildFailureResult(Exception e) {
            LoadResult result = new LoadResult();
            result.isSuccess = false;
            result.exception = e;
            return result;
        }
    }

    private class LoadTask extends AsyncTask<Void, Void, LoadResult> {

        private Context mContext;
        private String mOwner;
        private String mFileType;
        private LoadListener mListener;


        public LoadTask(Context context, String owner, String fileType,
                        LoadListener listener) {
            mContext = context;
            mOwner = owner;
            mFileType = fileType;
            mListener = listener;

        }

        @Override
        protected LoadResult doInBackground(Void... args) {

            Map<String, String> param = new HashMap<>();
            param.put("owner", mOwner);
            param.put("file_type", mFileType);

            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            DeltaResources.pull(mContext, "collection", param, future, future);

            try {
                JSONObject response = future.get();
                boolean isChanged = buildChildrenFromJSON(mContext, response);
                return LoadResult.buildSuccessResult(isChanged);
            } catch (JSONException |
                    ExecutionException | InterruptedException e) {
                return LoadResult.buildFailureResult(e);
            }
        }

        @Override
        protected void onPostExecute(LoadResult result) {
            if (result.isSuccess) {
                mListener.onAlbumLoaded(result.isChanged);
            } else {
                mListener.onAlbumLoadException(result.exception);
            }
        }
    }
}
