package io.syncport.ui.directory;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class VideoAdapter extends ArrayAdapter<VirtualFileModel> {

    private static final int LAYOUT_ID = R.layout.item_video;

    private ImageLoader mImageLoader;
    private DisplayImageOptions mVideoFileOptions;

    public VideoAdapter(Context context, ImageLoader imageLoader) {
        super(context);
        mImageLoader = imageLoader;
        mVideoFileOptions = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading(true)
                .showImageOnFail(R.drawable.ic_video)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
    }

    private void update(ViewHolder holder, VirtualFileModel file) {
        holder.setName(file.getName());
        holder.setSize(file.getSize());
        holder.setLastModifed(file.getModifiedAt());

        mImageLoader.displayImage(file.getImageUrl(),
                holder.getIconView(), mVideoFileOptions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View row = convertView;

        if (row == null) {
            row = getLayoutInflater().inflate(LAYOUT_ID, null);
            ImageView icon = (ImageView) row.findViewById(R.id.icon);
            TextView name = (TextView) row.findViewById(R.id.name);
            TextView size = (TextView) row.findViewById(R.id.size);
            TextView modifiedAt = (TextView) row.findViewById(R.id.last_modified);

            viewHolder = new ViewHolder(icon, name, modifiedAt, size);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (viewHolder != null) {
            update(viewHolder, getItem(position));
        }

        return row;
    }

    private class ViewHolder {
        private TextView mNameView;
        private TextView mSizeView;
        private TextView mLastModifiedView;
        private ImageView mIconView;
        private SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");

        public ViewHolder(ImageView icon, TextView name, TextView lastModified, TextView size) {
            mIconView = icon;
            mNameView = name;
            mSizeView = size;
            mLastModifiedView = lastModified;
        }

        public void setName(String name) {
            mNameView.setText(name);
        }

        public void setSize(long size) {
            if (size >= 0) {
                String readableSize = FileUtils.readableFileSize(size);
                mSizeView.setText(readableSize);
                mSizeView.setVisibility(View.VISIBLE);
            } else {
                mSizeView.setVisibility(View.GONE);
            }
        }

        public void setLastModifed(Date date) {
            if (date == null) {
                mLastModifiedView.setVisibility(View.GONE);
            } else {
                mLastModifiedView.setText(format.format(date));
                mLastModifiedView.setVisibility(View.VISIBLE);
            }

        }

        public ImageView getIconView() {
            return mIconView;
        }
    }
}

