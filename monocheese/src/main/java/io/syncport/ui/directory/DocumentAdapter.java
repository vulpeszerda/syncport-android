package io.syncport.ui.directory;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import io.syncport.R;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualFileModel;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by vulpes on 15. 2. 22..
 */
public class DocumentAdapter extends ArrayAdapter<VirtualFileModel>
        implements StickyListHeadersAdapter{

    private static final int ITEM_LAYOUT_ID = R.layout.item_document;
    private static final int SECTION_LAYOUT_ID = R.layout.item_collection__header;

    public DocumentAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(ITEM_LAYOUT_ID, null);
            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        VirtualFileModel item = getItem(position);
        viewHolder.getName().setText(item.getName());
        return row;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup viewGroup) {
        View row = convertView;
        HeaderViewHolder viewHolder;
        VirtualFileModel file = getItem(i);

        if (row == null) {
            row = mInflater.inflate(SECTION_LAYOUT_ID, null);
            viewHolder = new HeaderViewHolder(row);
            row.setTag(viewHolder);
        } else {
            viewHolder = (HeaderViewHolder) row.getTag();
        }

        Date modifiedAt = file.getModifiedAt();
        String date = getContext().getString(R.string.date_unknown);
        if (modifiedAt != null) {
            date = new SimpleDateFormat("yyyy.MM.dd").format(modifiedAt);
            String format = getContext().getString(R.string
                    .document_header_format);
            date = String.format(format, date);
        }
        viewHolder.getDate().setText(date);
        return row;
    }

    @Override
    public long getHeaderId(int i) {
        VirtualFileModel file = getItem(i);
        Date modifiedAt = file.getModifiedAt();
        if (modifiedAt != null) {
            Calendar date = new GregorianCalendar();
            date.setTime(modifiedAt);
            date.set(Calendar.HOUR_OF_DAY, 0);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            return date.getTimeInMillis();
        }
        return 0;
    }

    private class ViewHolder {
        private TextView mNameView;
        private ImageView mIconView;

        public ViewHolder(View view) {
            mNameView = (TextView) view.findViewById(R.id.name);
            mIconView = (ImageView) view.findViewById(R.id.icon);
        }

        public TextView getName() {
            return mNameView;
        }

        public ImageView getIcon() {
            return mIconView;
        }
    }

    private class HeaderViewHolder {
        private TextView mDateView;

        public HeaderViewHolder(View view) {
            mDateView = (TextView) view.findViewById(R.id.title);
        }

        public TextView getDate() {
            return mDateView;
        }
    }
}

