package io.syncport.ui.directory.model;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import io.syncport.core.device.Device;

public class VirtualAlbumCollectionModel extends VirtualCollectionModel{

    public static final int MAX_ALBUM_DEPTH = 3;
    public static final String COLLECTION_TYPE = "image";

    private String mSyncRootPath;
    private Map<String, VirtualAlbumModel> mAlbums;

    @Override
    protected String getType() {
        return COLLECTION_TYPE;
    }

    public VirtualAlbumCollectionModel(Device device) {
        super(device);
        mAlbums = new HashMap<>();
    }

    @Override
    public synchronized List<VirtualFileModel> getAll() {
        List<VirtualFileModel> all = new ArrayList<>();
        for (VirtualAlbumModel album : mAlbums.values()) {
            all.addAll(album.mChildren.values());
        }
        return all;
    }

    public synchronized List<VirtualAlbumModel> getAllAlbums() {
        return new ArrayList<>(mAlbums.values());
    }

    @Override
    protected synchronized boolean buildChildrenFromJSON(
            Context context, JSONObject response) throws JSONException {

        if (mSyncRootPath == null) {
            Device device = getDevice();
            try {
                mSyncRootPath =  device.getSyncRootPath(context, false);
            } catch (InterruptedException e) {
                // do nothing
                mSyncRootPath = "/";
            } catch (ExecutionException e) {
                mSyncRootPath = "/";
            }
        }

        boolean isChanged = false;

        JSONObject data = response.getJSONObject("data");
        Iterator<String> iter = data.keys();
        Set<VirtualFileModel> removeCandidates = new HashSet<>(getAll());


        while (iter.hasNext()) {
            String parentPath = iter.next();


            String subPath = parentPath.substring(mSyncRootPath.length());
            String[] subPaths = subPath.split("/");
            String albumPath = mSyncRootPath;

            int albumDepth = 0;
            for (int i = 0; i < subPaths.length && i <
                    MAX_ALBUM_DEPTH; i++) {
                albumPath += subPaths[i] + "/";
                albumDepth = i;
            }

            boolean includeNestedChildren = albumDepth == MAX_ALBUM_DEPTH;

            VirtualAlbumModel album = mAlbums.get(albumPath);
            if (album == null) {
                album = new VirtualAlbumModel(
                        albumPath, mDevice, includeNestedChildren);
                mAlbums.put(albumPath, album);
            }

            JSONArray array = data.getJSONArray(parentPath);

            for (int idx = 0; idx < array.length(); idx++) {
                JSONObject fileObj = array.getJSONObject(idx);
                String filename = fileObj.getString("n");

                Map<String, String> meta = new HashMap<>();
                JSONObject metaObj = fileObj.optJSONObject("m");
                Iterator<String> metaKeyIter = metaObj.keys();
                while (metaKeyIter.hasNext()) {
                    String key = metaKeyIter.next();
                    meta.put(key, metaObj.getString(key));
                }

                String path = parentPath + filename;


                VirtualFileModel file = album.mChildren.get(path);
                if (file == null) {
                    file = new VirtualFileModel(filename, path, mDevice);
                    isChanged |= file.updateMeta(meta);
                    album.add(file);
                    isChanged = true;
                } else {
                    removeCandidates.remove(file);
                    isChanged |= file.updateMeta(meta);
                }
            }
        }

        for (VirtualFileModel file : removeCandidates) {
            VirtualAlbumModel album = file.getAlbum();
            album.remove(file);
            isChanged = true;
        }
        List<VirtualAlbumModel> albums = new ArrayList<>(mAlbums.values());
        for (VirtualAlbumModel album : albums) {
            if (album.mChildren.size() > 0) {
                album.buildThumbnailFile();
            }
            album.setIsLoaded(true);
            if (album.mChildren.size() == 0) {
                mAlbums.remove(album.getPath());
                isChanged = true;
            }
        }
        setIsLoaded(true);
        return isChanged;
    }

    @Override
    public synchronized void clear() {
        mAlbums.clear();
        super.clear();
    }
}

