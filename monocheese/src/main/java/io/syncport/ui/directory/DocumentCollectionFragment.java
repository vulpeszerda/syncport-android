package io.syncport.ui.directory;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualAlbumModel;
import io.syncport.ui.directory.model.VirtualCollectionModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


public class DocumentCollectionFragment extends BaseFileCollectionFragment {

    public static DocumentCollectionFragment getInstance(String deviceId) {
        DocumentCollectionFragment fragment = new DocumentCollectionFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String COLLECTION_TYPE = "document";

    private @InjectView(R.id.listview) StickyListHeadersListView mListView;

    private ArrayAdapter<VirtualFileModel> mAdapter;


    public DocumentCollectionFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_collection__document,
                container, false);
    }

    @Override
    protected VirtualCollectionModel createCollectionModel(Device device) {
        return new VirtualCollectionModel(device) {
                @Override
                protected String getType() {
                    return COLLECTION_TYPE;
                }
            };
    }

    @Override
    protected Comparator<VirtualFileModel> getComparator() {
        return new Comparator<VirtualFileModel>() {
            @Override
            public int compare(VirtualFileModel lhs, VirtualFileModel rhs) {
                Date lhsModifiedAt = lhs.getModifiedAt();
                Date rhsModifiedAt = rhs.getModifiedAt();
                if (lhsModifiedAt != null && rhsModifiedAt != null) {
                    return rhsModifiedAt.getTime() - lhsModifiedAt.getTime()
                            > 0 ? 1: -1;
                } else if (lhsModifiedAt != null) {
                    return -1;
                } else {
                    return 1;
                }
            }
        };
    }

    @Override
    protected void onDisplayReady(List<VirtualFileModel> files) {
        DocumentAdapter adapter = (DocumentAdapter) getAdapter();
        adapter.clear();
        adapter.addAll(files);
        adapter.notifyDataSetChanged();

        VirtualCollectionModel collectionModel = getCollection();

        if (files.size() == 0 && collectionModel.isLoaded()) {
            showEmptyView(getString(R.string.desc_empty_document), false);
        } else if (files.size() == 0 && !collectionModel.isLoaded()) {
            showEmptyView(getString(R.string.desc_loading_directory),
                    true);
        } else {
            hideEmptyView();
        }

    }

    @Override
    protected void setupAdapter() {
        if (mAdapter == null) {
            mAdapter = new DocumentAdapter(getActivity());
        }

        mListView.setAdapter((StickyListHeadersAdapter)mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    protected ArrayAdapter<VirtualFileModel> getAdapter() {
        return mAdapter;
    }

    @Override
    protected void showEmptyView(String message, boolean withProgress) {
        super.showEmptyView(message, withProgress);
        mListView.setVisibility(View.GONE);
    }

    @Override
    protected void hideEmptyView() {
        super.hideEmptyView();
        mListView.setVisibility(View.VISIBLE);
    }
}
