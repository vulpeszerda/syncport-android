package io.syncport.ui.directory.model;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.syncport.core.device.Device;

public class VirtualDeviceModel extends VirtualDirectoryModel {

    private static final String TAG = "SyncportDeviceModel";

    private Device mDevice;
    private String mSyncRootPath;

    public VirtualDeviceModel(Device device) {
        super(Type.DEVICE, device.getName(), "/", device);
        mDevice = device;
    }

    @Override
    public Device getDevice() {
        return mDevice;
    }

    @Override
    public synchronized void setSyncRootPath(String path) {
        mSyncRootPath = path;
    }

    @Override
    public synchronized String getSyncRootPath() {
        return mSyncRootPath;
    }
}
