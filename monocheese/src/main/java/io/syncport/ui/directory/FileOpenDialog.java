package io.syncport.ui.directory;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.BaseFileTask.FailureResult;
import io.syncport.core.transfer.BaseFileTask.Progress;
import io.syncport.core.transfer.BaseFileTask.Result;
import io.syncport.core.transfer.BaseFileTask.SuccessResult;
import io.syncport.core.transfer.BaseFileTask.TaskProgressListener;
import io.syncport.core.transfer.FileTaskManager;
import io.syncport.ui.BaseDialogFragment;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;

import java.io.File;

import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;

public class FileOpenDialog extends BaseDialogFragment implements
        FileTaskManager.Observer, TaskProgressListener, OnClickListener {

    public static final String PARAM_RECEIVED_FILE_PATH = SyncportKeyBuilder
            .build("received_file_path");

    private static final String TAG = "FileReceiveActivity";
    private static final String PARAM_FILE_PARENT_PATH = SyncportKeyBuilder
            .build("file_parent_path");
    private static final String PARAM_FILE_NAME = SyncportKeyBuilder
            .build("filename");
    private static final String PARAM_PEER_DEVICE_ID = SyncportKeyBuilder
            .build("peer_device_id");

    @SuppressLint("InlinedApi")
    public static FileOpenDialog getInstance(String peerDeviceId, String path,
            String filename) {
        Bundle args = new Bundle();

        args.putString(PARAM_PEER_DEVICE_ID, peerDeviceId);
        args.putString(PARAM_FILE_PARENT_PATH, path);
        args.putString(PARAM_FILE_NAME, filename);

        FileOpenDialog fragment = new FileOpenDialog();
        if (Build.VERSION.SDK_INT >= 14) {
            fragment.setStyle(
                    STYLE_NO_TITLE,
                    android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar_MinWidth);
        } else if (Build.VERSION.SDK_INT >= 11) {
            fragment.setStyle(
                    STYLE_NO_TITLE,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
        } else {
            fragment.setStyle(STYLE_NO_TITLE, android.R.style.Theme_Dialog);
        }
        fragment.setArguments(args);
        return fragment;
    }

    private @InjectView(R.id.filename) TextView mFileNameView;
    private @InjectView(R.id.progress) ProgressBar mProgressView;
    private @InjectView(R.id.status) TextView mStatusView;
    private @InjectView(R.id.button_cancel) Button mCancelBtn;

    private String mFileName;
    private String mParentFilePath;
    private String mPeerDeviceId;

    private String mTaskKey;

    private BaseFileTask mTask;
    private BaseFileTask.State mCurrentTaskState;
    private Transaction mTransaction;
    private FileTaskManager mManager;
    private boolean mIsViewCreated = false;

    @SuppressWarnings("unused")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        Bundle args = getArguments();
        mFileName = args.getString(PARAM_FILE_NAME);
        mParentFilePath = args.getString(PARAM_FILE_PARENT_PATH);
        if (!mParentFilePath.endsWith("/")) {
            mParentFilePath += "/";
        }
        mPeerDeviceId = args.getString(PARAM_PEER_DEVICE_ID);

        mTaskKey = BaseFileTask.generateTaskKey(mPeerDeviceId, mParentFilePath,
                mFileName);
        mManager = FileTaskManager.getInstance(getActivity()
                .getApplicationContext());
        BaseFileTask task = mManager.getTask(mTaskKey);

        if (task != null) {
            Log.d(TAG, "found existing task");
            onHookTask(task);
        } else {
            mManager.addObserver(this);
            requestStartTransaction();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                dialog.getWindow().setLayout(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
            } else {
                dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT);
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mIsViewCreated = true;
        mFileNameView.setText(mFileName);
        mStatusView.setText(null);
        mProgressView.setProgress(0);
        mCancelBtn.setOnClickListener(this);
        if (mTask != null) {
            onHookTask(mTask);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mIsViewCreated = false;
        return inflater.inflate(R.layout.dialog_file_open, container, false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mTask != null
                && (mTask.getState() != BaseFileTask.State.FINISHED && mTask
                        .getState() != BaseFileTask.State.CANCELLED)) {
            mTask.cancel();
        }
    }

    @Override
    public void onDestroy() {
        if (mManager != null) {
            mManager.removeObserver(this);
        }
        if (mTask != null) {
            if (mTask.getState() != BaseFileTask.State.FINISHED
                    && mTask.getState() != BaseFileTask.State.CANCELLED) {
                mTask.cancel();
            }
            mTask.unregisterTaskObserver(this);
        }
        super.onDestroy();
    }

    private void requestStartTransaction() {
        if (mIsViewCreated) {
            mStatusView.setText(R.string.desc_file_send_request);
            mProgressView.setIndeterminate(true);
        }

        DeltaResources
                .startFilePullTransaction(getActivity(), mParentFilePath,
                        mFileName, mPeerDeviceId,
                        new RobustResponseListener<JSONObject>(
                                getBaseActivity(), this) {

                            @Override
                            public void onSecureResponse(JSONObject response) {
                                if (mIsViewCreated) {
                                    mStatusView
                                            .setText(R.string.desc_file_wait_transaction);
                                }
                            }

                            @Override
                            public void onSkipped(JSONObject response) {

                            }
                        }, new RobustResponseErrorListener(getActivity()) {

                            @Override
                            public void onErrorResponse(VolleyError error,
                                    String msg) {
                                showFailureMessage(msg);
                            }

                            @Override
                            public void onSkipped(VolleyError error) {

                            }
                        });
    }

    private void onHookTask(BaseFileTask task) {
        if (task.getTaskType() != BaseFileTask.TaskType.RECEIVE) {
            dismiss();
        }
        mTask = task;
        mTransaction = mTask.getTransaction();

        if (!mIsViewCreated) {
            return;
        }

        mProgressView.setMax(100);

        switch (task.getState()) {
        case PREPARE:
            onPrepare();
            break;
        case PROGRESS:
            onProgressUpdated(task.getProgress());
            break;
        case FINISHED:
            onFinished(task.getResult());
            break;
        case CANCELLED:
            onCancelled();
        }
        mTask.registerTaskObserver(this);

    }

    private void showFailureMessage(String msg) {
        if (mIsViewCreated) {
            if (mStatusView.getVisibility() == View.GONE) {
                mStatusView.setVisibility(View.VISIBLE);
            }
            String failedToLoadFileFormat = getString(R.string.desc_failed_to_load_file__format);
            mStatusView.setText(String.format(failedToLoadFileFormat, msg));
            mProgressView.setIndeterminate(false);
            mProgressView.setProgress(0);
        }
    }

    @Override
    public void onAddTask(String tag, BaseFileTask task) {
        if (tag.equals(mTaskKey)) {
            onHookTask(task);
            mManager.removeObserver(this);
        }
    }

    @Override
    public void onRemoveTask(String tag, BaseFileTask task) {

    }

    @Override
    public void onPrepare() {
        if (!mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(true);
        }
        mStatusView.setText(R.string.desc_file_preparing);
        mProgressView.setProgress(0);
    }

    @Override
    public void onProgressUpdated(Progress prog) {
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        String progressFormat = getString(R.string.desc_file_receive_progress__format);
        String progressText = String.format(progressFormat,
                FileUtils.readableFileSize(prog.getDoneLength()),
                FileUtils.readableFileSize(prog.getFileLength()));
        mStatusView.setText(progressText);
        mProgressView.setProgress(prog.getPercent());
    }

    @Override
    public void onFinished(Result result) {
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        if (result.isSuccess()) {
            SuccessResult r = (SuccessResult) result;
            File receivedFile = r.getFile();
            Intent data = new Intent();
            data.putExtra(PARAM_RECEIVED_FILE_PATH,
                    receivedFile.getAbsolutePath());
            setResultCode(RESULT_OK);
            setResult(data);
            dismiss();
        } else {
            FailureResult r = (FailureResult) result;
            showFailureMessage(r.getException().getMessage());
        }
    }

    @Override
    public void onCancelled() {
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        mProgressView.setIndeterminate(false);
        mStatusView.setText(R.string.desc_file_transfer_cancelled);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_cancel) {
            dismiss();
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (IllegalStateException e) {
            // TODO
            // http://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa
        }
    }
}
