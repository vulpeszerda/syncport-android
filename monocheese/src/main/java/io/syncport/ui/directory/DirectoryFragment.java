package io.syncport.ui.directory;

import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.syncport.R;
import io.syncport.core.InternetConnectivityManager;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.polling.GeneralPollerService;
import io.syncport.core.polling.model.DirectoryPollingInfo;
import io.syncport.core.polling.model.ThumbnailPollingInfo;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.BaseDialogFragment;
import io.syncport.ui.BaseDialogFragment.DialogResultHandler;
import io.syncport.ui.BaseFragment;
import io.syncport.ui.SyncportApplication;
import io.syncport.ui.directory.model.VirtualDeviceModel;
import io.syncport.ui.directory.model.VirtualDirectoryModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.InjectView;

public class DirectoryFragment extends BaseFragment implements
        OnItemClickListener, DialogInterface.OnClickListener,
        DialogResultHandler{

    public static DirectoryFragment getInstance(String deviceId) {
        DirectoryFragment fragment = new DirectoryFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String TAG_PROGRESS_DIALOG__DIRECTORY_LOAD =
            "directory_load";
    protected static final String PARAM_DEVICE_ID = SyncportKeyBuilder.build
            ("device_id");

    private static final int PRELOAD_DIR_DEPTH = 4;
    private static final int REQUEST_CODE__DEVICE_NAME_CHANGE = 10;
    private static final int REQUEST_CODE__FILE_OPEN = 11;
    private static final int REQUEST_CODE__FILE_OPEN_ACTIVITY = 12;

    private @InjectView(R.id.listview) ListView mListView;
    private @InjectView(R.id.title) TextView mHeaderTitleView;
    private @InjectView(R.id.empty_view) View mEmptyView;
    private @InjectView(R.id.empty_text) TextView mEmptyTextView;
    private @InjectView(R.id.empty_progressbar) ProgressBar mEmptyProgressBar;

    private Device mDevice;
    private DeviceManager mDeviceManager;
    private LocalBroadcastManager mBroadcastManager;
    private AlertDialog mDeviceLoadErrorDialog;
    private AlertDialog mCurrDirectoryRemovedDialog;
    private VirtualDeviceModel mDeviceRoot;
    private VirtualDirectoryModel mCurrentWorkingDir;
    private InternetConnectivityManager mInternetManager;
    private Set<String> mThumbnailHashes;

    private boolean mIsFragmentPaused;
    private boolean mIsFirstLaunch;
    private boolean mFileOpenable;


    private BroadcastReceiver mDirectoryPatchReceiver;

    {
        mDirectoryPatchReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                DirectoryPollingInfo info = (DirectoryPollingInfo) intent
                        .getSerializableExtra
                        (GeneralPollerService.PARAM_DATA);
                if (mDevice != null && info != null &&
                        info.getOwner().equals(mDevice.getId())) {
                    if (isAdded()) {
                        refresh();
                    }
                }
            }

        };
    }

    private BroadcastReceiver mThumbnailReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ThumbnailPollingInfo info = (ThumbnailPollingInfo)intent
                    .getSerializableExtra(GeneralPollerService.PARAM_DATA);
            if (mThumbnailHashes != null) {
                String[] updatedHashes = info.getHashes();
                if (updatedHashes == null) {
                    return;
                }

                for (String hash : updatedHashes) {
                    if (mThumbnailHashes.contains(hash) && isAdded()) {
                        refresh();
                    }
                }
            }
        }
    };

    private VirtualDirectoryModel.LoadListener mDirLoadListener =
            new VirtualDirectoryModel.LoadListener() {

        @Override
        public void onLoaded(Set<VirtualDirectoryModel> changedDirs) {
            onDirLoaded(changedDirs);
        }

        @Override
        public void onException(Runnable runnable, Exception e) {
            onDirLoadException(runnable, e);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.title_filesystem);
        }
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mIsFirstLaunch = true;
        mFileOpenable = true;

    }

    @Override
    public void onPause() {
        super.onPause();
        mIsFragmentPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsFragmentPaused = false;
        if (mCurrentWorkingDir != null) {
            displayDirectory(mCurrentWorkingDir);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((BaseActivity) activity).supportInvalidateOptionsMenu();
        IntentFilter filter = new IntentFilter(GeneralPollerService
                .PollerAction.DIRECTORY);
        if (mBroadcastManager == null) {
            mBroadcastManager = LocalBroadcastManager
                    .getInstance(getActivity());
        }
        mBroadcastManager.registerReceiver(mDirectoryPatchReceiver, filter);
        mBroadcastManager.registerReceiver(mThumbnailReceiver,
                new IntentFilter(GeneralPollerService.PollerAction.THUMBNAIL));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mBroadcastManager != null) {
            mBroadcastManager.unregisterReceiver(mDirectoryPatchReceiver);
            mBroadcastManager.unregisterReceiver(mThumbnailReceiver);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_directory, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SyncportApplication app = (SyncportApplication) getActivity()
                .getApplication();

        ImageLoader loader = app.getDefaultImageLoader();
        mListView.setAdapter(buildAdapter(loader));
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mInternetManager = InternetConnectivityManager
                .getInstance(getActivity());

        try {
            mDeviceManager = DeviceManager.from(getActivity());
            fetchDeviceInfo();
        } catch (AuthenticatorException e) {
            // will not happen
            getBaseActivity().finish();
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.directory, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mCurrentWorkingDir.clearChildren();
                refresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__FILE_OPEN_ACTIVITY) {
            mFileOpenable = true;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        VirtualFileModel model = getAdapter().getItem(position);
        if (model.isFile()) {
            showDownloadConfirmDialog(model);
        } else {
            VirtualDirectoryModel dir = (VirtualDirectoryModel) model;
            dir.loadSubDirectories(
                    getActivity(), PRELOAD_DIR_DEPTH, mDirLoadListener);
            displayDirectory(dir);
        }
    }

    @Override
    public void onDialogResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__DEVICE_NAME_CHANGE
                && resultCode == BaseDialogFragment.RESULT_OK) {
            refresh();
            return;
        }
        if (requestCode == REQUEST_CODE__FILE_OPEN) {
            if (resultCode == BaseDialogFragment.RESULT_OK && data != null) {
                String path = data
                        .getStringExtra(FileOpenDialog.PARAM_RECEIVED_FILE_PATH);
                File receivedFile = new File(path);
                if (receivedFile.exists()) {
                    openDownloadedFile(receivedFile);
                }
            } else {
                mFileOpenable = true;
            }
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (dialog == mDeviceLoadErrorDialog) {
            fetchDeviceInfo();
        } else if (dialog == mCurrDirectoryRemovedDialog) {
            displayDirectory(mDeviceRoot);
        }
    }

    public void onDirLoaded(Set<VirtualDirectoryModel> changedDirs) {
        if (!isAdded()) {
            return;
        }
        if (changedDirs.contains(mCurrentWorkingDir)) {
            if (mCurrentWorkingDir.isDevice()) {
                VirtualDirectoryModel syncRoot = mCurrentWorkingDir
                        .getSyncRoot();
                if (syncRoot != null && mCurrentWorkingDir != syncRoot) {
                    mCurrentWorkingDir = mCurrentWorkingDir.getSyncRoot();
                    mCurrentWorkingDir.loadSubDirectories(getActivity(),
                            PRELOAD_DIR_DEPTH, mDirLoadListener);
                }
            }
            displayDirectory(mCurrentWorkingDir);
        } else if (mEmptyProgressBar.getVisibility() == View.VISIBLE) {
            hideEmptyView();
        }
    }

    public void onDirLoadException(final Runnable runnable, Exception e) {
        if (!isAdded()) {
            return;
        }
        String errorString = "";
        boolean showDialog = false;
        if (e instanceof VolleyError) {
            VolleyError error = (VolleyError) e;
            if (error instanceof NoConnectionError) {
                errorString = getString(
                        R.string.desc_failed_to_connect_internet);
                showDialog = true;
            } else {
                NetworkResponse response = error.networkResponse;
                int statusCode = response.statusCode;
                if (statusCode == 400) {
                    if (mCurrentWorkingDir.isDevice()) {
                        showEmptyView(R.string.desc_loading_directory, true);
                    }

                    showCurrDirectoryRemovedDialog();
                    return;
                }
                if (TextUtils.isEmpty(errorString) &&
                        (response != null) &&
                        (response.data != null) &&
                        (response.data.length > 0)) {
                    errorString = new String(response.data);
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = error.getMessage();
                }
                if (TextUtils.isEmpty(errorString)) {
                    errorString = getString(R.string.desc_undefined_error);
                }
            }
        }
        String message = getString(R.string.desc_failed_to_load_dirlist);
        if (!TextUtils.isEmpty(errorString)) {
            message += " (" + errorString + ")";
        }
        if (showDialog) {
            new AlertDialog.Builder(getBaseActivity())
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(message)
                    .setPositiveButton(R.string.button_retry,
                            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    runnable.run();
                }
            }).setNegativeButton(R.string.button_confirm, null).show();
        }
    }

    public boolean onBackPressed() {
        if (mCurrentWorkingDir != null
                && mCurrentWorkingDir.getParent() != null
                && !mCurrentWorkingDir.isSyncRoot()) {
            VirtualDirectoryModel parent = mCurrentWorkingDir.getParent();
            displayDirectory(parent);
            return true;
        }
        return false;
    }


    private void fetchDeviceInfo() {
        String deviceId = getArguments().getString(PARAM_DEVICE_ID);
        mDeviceManager.getDeviceWithId(deviceId,
                new DeviceManager.OnGetDeviceListener() {
            @Override
            public void onGetDevice(Device device) {
                if (!isAdded()) {
                    return;
                };
                mDevice = device;
                if (mIsFirstLaunch) {
                    mIsFirstLaunch = false;
                    refresh();
                }
            }

            @Override
            public void onFailure(Exception e) {
                if (!isAdded()) {
                    return;
                };
                if (mDeviceLoadErrorDialog == null) {
                    mDeviceLoadErrorDialog =
                            new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.title_failed_to_load)
                            .setMessage(R.string
                                    .des_failed_to_fetch_device_info)
                            .setPositiveButton(R.string.button_retry, DirectoryFragment.this).setNegativeButton
                                    (R.string.button_confirm, null).create();
                }
                mDeviceLoadErrorDialog.show();
            }
        });
    }

    private void showEmptyView(int msgId, boolean withProgress) {
        String msg = getString(msgId);
        mEmptyTextView.setText(msg);
        mEmptyProgressBar
                .setVisibility(withProgress ? View.VISIBLE : View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        if (withProgress) {
            mListView.setVisibility(View.GONE);
        }
    }

    private void hideEmptyView() {
        mEmptyView.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
    }

    private void refresh() {
        if (mCurrentWorkingDir == null) {
            mCurrentWorkingDir = new VirtualDeviceModel(mDevice);
        }
        mCurrentWorkingDir.loadParentDirectory(getActivity(), mDirLoadListener);
        mCurrentWorkingDir.loadSubDirectories(getActivity(),
                PRELOAD_DIR_DEPTH, mDirLoadListener);
        mDeviceRoot = mCurrentWorkingDir.getRoot();
        displayDirectory(mCurrentWorkingDir);
    }

    private void displayHeader() {
        if (mIsFragmentPaused) {
            return;
        }
        String headerTitle;
        if (mCurrentWorkingDir.isSyncRoot()) {
            headerTitle = mCurrentWorkingDir.getDevice().getName();
        } else {
            headerTitle = mCurrentWorkingDir.getName();
        }
        mHeaderTitleView.setText(headerTitle);
    }

    private void registerThumbnailHashes(List<VirtualFileModel> files) {
        // put all thumbnail hashes to check thumbnail polling response
        if (mThumbnailHashes == null) {
            mThumbnailHashes = new HashSet<>();
        } else {
            mThumbnailHashes.clear();
        }
        for (VirtualFileModel file : files) {
            String path = file.getAbsolutePath();
            if (FileUtils.isVideoPath(path) || FileUtils.isAudioPath(path) ||
                    FileUtils.isImagePath(path)) {

                String hash = file.getHash();
                if (hash != null) {
                    mThumbnailHashes.add(hash);
                }
            }
        }
    }

    private synchronized void displayDirectory(VirtualDirectoryModel model) {
        mCurrentWorkingDir = model;
        if (mIsFragmentPaused) {
            return;
        }
        DirectoryAdapter adapter = getAdapter();
        adapter.clear();

        if (model.getParent() != null && !model.isSyncRoot()) {
            adapter.addParent(model.getParent());
        }
        List<VirtualFileModel> files = model.getChildren();
        registerThumbnailHashes(files);

        adapter.addAll(files);
        adapter.notifyDataSetChanged();
        if (files.size() == 0) {
            if (!model.isLoaded()) {
                showEmptyView(R.string.desc_loading_directory, true);
            } else {
                showEmptyView(R.string.desc_empty_directory, false);
            }
        } else {
            hideEmptyView();
        }
        displayHeader();
    }

    private DirectoryAdapter buildAdapter(ImageLoader loader) {
        return new DirectoryAdapter(getActivity(), loader);
    }

    private DirectoryAdapter getAdapter() {
        return (DirectoryAdapter) mListView.getAdapter();
    }

    private void showErrorDialog(String title, String msg) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(R.string.button_confirm, null).show();
    }


    private void showDownloadConfirmDialog(final VirtualFileModel model) {
        File syncportDownloadDir = new File(
                Environment.getExternalStorageDirectory(), "Syncport/download");
        final File sameNameFile = new File(syncportDownloadDir, model.getName());
        if (!sameNameFile.exists()) {
            showDateUsageConfirmDialog(model);
            return;
        }

        DialogInterface.OnClickListener listener =
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            showDateUsageConfirmDialog(model);
                        } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                            openDownloadedFile(sameNameFile);
                        }
                    }
                };

        new AlertDialog.Builder(getActivity()).setTitle(R.string.title_caution)
                .setMessage(getString(R.string.already_have_file))
                .setPositiveButton(R.string.button_yes, listener)
                .setNeutralButton(
                        R.string.button_open_with_cache, listener).show();
    }

    private void showDateUsageConfirmDialog(final VirtualFileModel model) {
        if (!mInternetManager.isConnected()) {
            DialogInterface.OnClickListener listener =
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showDateUsageConfirmDialog(model);
                }
            };
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(R.string.desc_failed_to_connect_internet)
                    .setPositiveButton(R.string.button_retry, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;
        }
        if (!mInternetManager.isWifiConnected()) {
            DialogInterface.OnClickListener listener =
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showPeerDateUsageConfirmDialog(model);
                }
            };
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_are_you_sure)
                    .setMessage(R.string.desc_data_usage_confirm)
                    .setPositiveButton(R.string.button_proceed, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;
        }
        showPeerDateUsageConfirmDialog(model);
    }

    private void showPeerDateUsageConfirmDialog(final VirtualFileModel model) {
        if (model.getDevice().isChargedConnectedState()) {
            DialogInterface.OnClickListener listener =
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    openFile(model);
                }
            };
            String format = getString(R.string
                    .desc_peer_data_usage_confirm__format);
            String message = String.format(format, model.getDevice().getName());
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_are_you_sure)
                    .setMessage(message)
                    .setPositiveButton(R.string.button_proceed, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;

        }
        openFile(model);
    }

    private void showCurrDirectoryRemovedDialog() {
        String format = getString(R.string.title_curr_directory_removed);
        String message = String.format(format, mCurrentWorkingDir.getName());
        mCurrDirectoryRemovedDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_curr_directory_removed)
                .setMessage(message)
                .setPositiveButton(R.string.button_up_to_devicelist, this)
                .create();
        mCurrDirectoryRemovedDialog.show();
    }

    private void openFile(VirtualFileModel fileModel) {
        if (!mFileOpenable) {
            return;
        }
        mFileOpenable = false;
        if (!fileModel.getDevice().isTurnOn()) {
            String format = getString(
                    R.string.desc_file_access_deneied__device_turned_off);
            String msg = String.format(format, fileModel.getDevice().getName());

            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(msg)
                    .setPositiveButton(R.string.button_confirm,
                            new OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    mFileOpenable = true;
                                }
                            }).show();
            return;
        }

        String absPath = fileModel.getAbsolutePath();

        int splitPoint = absPath.lastIndexOf("/");
        final String parentPath = absPath.substring(0, splitPoint);
        final String filename = absPath.substring(splitPoint + 1);
        final String ownerId = fileModel.getDevice().getId();
        FileOpenDialog dialog = FileOpenDialog.getInstance(ownerId, parentPath,
                filename);
        dialog.setTargetFragment(this, REQUEST_CODE__FILE_OPEN);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "fileopen");
    }

    private void openDownloadedFile(File file) {
        try {
            startActivityForResult(
                    FileUtils.getOpenFileIntent(file),
                    REQUEST_CODE__FILE_OPEN_ACTIVITY);
        } catch (ActivityNotFoundException | IOException e) {
            e.printStackTrace();
            mFileOpenable = true;
            showErrorDialog(
                    getString(R.string.title_failed_to_open),
                    getString(R.string.desc_failed_to_find_openable_activity));
        }
    }
}
