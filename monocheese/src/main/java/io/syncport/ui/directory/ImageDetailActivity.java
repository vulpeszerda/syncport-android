package io.syncport.ui.directory;

import android.accounts.AuthenticatorException;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.polling.model.FileTransferPollingInfo;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.FileTaskManager;
import io.syncport.core.transfer.SingleMediaScanner;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

/**
 * Created by vulpes on 15. 2. 23..
 */
@ContentView(R.layout.activity_image_detail)
public class ImageDetailActivity extends BaseActivity implements
        FileTaskManager.Observer, BaseFileTask.TaskProgressListener,
        View.OnClickListener{


    public static Intent createIntent(Context context, VirtualFileModel model) {
        String absPath = model.getAbsolutePath();

        int splitPoint = absPath.lastIndexOf("/");
        String parentPath = absPath.substring(0, splitPoint + 1);
        String filename = absPath.substring(splitPoint + 1);
        String ownerId = model.getDevice().getId();

        Intent intent = new Intent(context, ImageDetailActivity.class);
        intent.putExtra(PARAM_PARENT_PATH, parentPath);
        intent.putExtra(PARAM_FILENAME, filename);
        intent.putExtra(PARAM_OWNERID, ownerId);
        intent.putExtra(PARAM_MODIFIED_AT, model.getModifiedAt().getTime());
        return intent;
    }

    private static final String TAG = "ImageDetailActivity";

    private static final String PARAM_PARENT_PATH = SyncportKeyBuilder.build
            ("parent_path");
    private static final String PARAM_FILENAME = SyncportKeyBuilder.build
            ("filename");
    private static final String PARAM_OWNERID = SyncportKeyBuilder.build
            ("owner_id");
    private static final String PARAM_MODIFIED_AT = SyncportKeyBuilder.build
            ("modified_at");

    private @InjectView(R.id.image) ImageView mImageView;
    private @InjectView(R.id.menus) LinearLayout mMenusView;
    private @InjectView(R.id.menu_delete) ImageButton mDeleteBtn;
    private @InjectView(R.id.menu_share) ImageButton mShareBtn;
    private @InjectView(R.id.menu_download) ImageButton mDownloadBtn;
    private @InjectView(R.id.status) TextView mStatusView;
    private @InjectView(R.id.progress) ProgressBar mProgressView;
    private @InjectView(R.id.progress_container) View mProgressContainer;

    private String mTaskKey;

    private BaseFileTask mTask;
    private Device mDevice;
    private DeviceManager mDeviceManager;
    private FileTransferPollingInfo.Transaction mTransaction;
    private FileTaskManager mFileTaskManager;
    private String mCacheFilename;
    private File mReceivedFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            mDeviceManager = DeviceManager.from(this);
        } catch(AuthenticatorException e) {
            // not reach
            finish();
            return;
        }

        Intent intent = getIntent();
        String filename = intent.getStringExtra(PARAM_FILENAME);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setTitle(filename);


        mDeleteBtn.setOnClickListener(this);
        mDownloadBtn.setOnClickListener(this);
        mShareBtn.setOnClickListener(this);

        fetchDeviceInfo();
    }

    @Override
    protected void onDestroy() {
        if (mFileTaskManager != null) {
            mFileTaskManager.removeObserver(this);
        }
        if (mTask != null) {
            if (mTask.getState() != BaseFileTask.State.FINISHED) {
                mTask.cancel();
            }
            mTask.unregisterTaskObserver(this);
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchDeviceInfo() {
        String deviceId = getIntent().getStringExtra(PARAM_OWNERID);

        final DialogInterface.OnClickListener onDialogClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            fetchDeviceInfo();
                        }
                    }
                };

        mDeviceManager.getDeviceWithId(deviceId, new DeviceManager.OnGetDeviceListener() {
            @Override
            public void onGetDevice(Device device) {
                if (isFinishing()) {
                    return;
                }
                mDevice = device;
                onAfterFetchDeviceInfo();
            }

            @Override
            public void onFailure(Exception e) {
                if (isFinishing()) {
                    return;
                }
                new AlertDialog.Builder(ImageDetailActivity.this)
                        .setTitle(R.string.title_failed_to_load)
                        .setMessage(R.string.des_failed_to_fetch_device_info)
                        .setPositiveButton(R.string.button_retry, onDialogClickListener)
                        .setNegativeButton(R.string.button_confirm, null)
                        .show();
            }
        });
    }

    private String getCacheFilename(String peerId, String path,
                                    String cacheKey) {
        String key = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(peerId.getBytes());
            md.update(path.getBytes());
            md.update(cacheKey.getBytes());
            StringBuffer hexString = new StringBuffer();
            byte[] hash = md.digest();

            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0"
                            + Integer.toHexString((0xFF & hash[i])));
                } else {
                    hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            key = hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (key != null) {
            int idx = path.lastIndexOf(".");
            String postfix = "";
            if (idx > -1) {
                postfix = path.substring(idx);
            }
            return key + postfix;
        }
        return key;
    }

    private void onAfterFetchDeviceInfo() {
        Intent intent = getIntent();
        String filename = intent.getStringExtra(PARAM_FILENAME);
        String parentPath = intent.getStringExtra(PARAM_PARENT_PATH);
        long modifiedAt = intent.getLongExtra(PARAM_MODIFIED_AT, -1);

        if (!parentPath.endsWith("/")) {
            parentPath += "/";
        }

        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        File accountDir = new File(getExternalCacheDir(), account.getName());
        final File cachedDownloadDir = new File(accountDir, "downloadCache");

        String key = String.valueOf(modifiedAt);
        mCacheFilename = getCacheFilename(mDevice.getId(),
                parentPath + filename, key);

        mReceivedFile = new File(cachedDownloadDir, mCacheFilename);

        if (mReceivedFile.exists()) {
            displayImage(mReceivedFile);
            return;
        }

        String destParentPath = mReceivedFile.getParent();
        if (!destParentPath.endsWith("/")) {
            destParentPath += "/";
        }

        mTaskKey = BaseFileTask.generateTaskKey(mDevice.getId(), parentPath,
                destParentPath, filename);

        mFileTaskManager = FileTaskManager.getInstance(this);
        BaseFileTask task = mFileTaskManager.getTask(mTaskKey);

        if (task != null) {
            Log.d(TAG, "found existing task");
            onHookTask(task);
        } else {
            mFileTaskManager.addObserver(this);

            requestStartTransaction(mDevice.getId(), parentPath,
                    destParentPath, filename);
        }

    }

    private void requestStartTransaction(String deviceId, String parentPath,
                                         String parentPathTo,
                                         String filename) {

        mStatusView.setText(R.string.desc_file_send_request);
        mProgressView.setIndeterminate(true);

        DeltaResources.startFilePullTransaction(this,
                parentPath, parentPathTo, filename, deviceId,
                new RobustResponseListener<JSONObject>(this) {

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mStatusView.setText(R.string.desc_file_wait_transaction);
                    }

                    @Override
                    public void onSkipped(JSONObject response) {

                    }
                }, new RobustResponseErrorListener(this) {

                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        showFailureMessage(msg);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.menu_delete:
                break;
            case R.id.menu_download:
                download();
                break;
            case R.id.menu_share:
                break;
        }
    }

    private void download() {
        File syncportDownloadDir = new File(
                Environment.getExternalStorageDirectory(), "Syncport/download");

        String filename = getIntent().getStringExtra(PARAM_FILENAME);
        File sameNameFile = new File(syncportDownloadDir, filename);
        if (sameNameFile.exists()) {
            sameNameFile = FileUtils.getAlternativeFile(sameNameFile);
        }
        final File file = sameNameFile;
        final Handler handler = new Handler();
        if (mReceivedFile.exists()) {
            try {
                publishDownloadStart(sameNameFile);
                copyFile(mReceivedFile, sameNameFile);
                SingleMediaScanner  scanner = new SingleMediaScanner(this,
                        sameNameFile) {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        super.onScanCompleted(path, uri);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                publishDownloadEnd(file);
                            }
                        });
                    }
                };
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void copyFile(File src, File dst) throws
            IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        byte[] buf = new byte[1024];
        long lastPublishTime = -1;
        long total = src.length();
        long copied = 0;
        int len;
        while ((len = in.read(buf)) > 0) {
            copied += len;
            out.write(buf, 0, len);
            long curr = (new Date()).getTime();
            if (lastPublishTime < 0 || curr - lastPublishTime > 500) {
                lastPublishTime = curr;
                int percent = (int) (copied * 100 / total);
                publishDownloadProgress(percent, copied, total);
            }
        }
        in.close();
        out.close();
    }

    private void publishDownloadStart(File file) {
        if (file.length() > 1 * 1000 * 1000) {
            String format = getString(R.string.desc_download_started__format);
            String message = String.format(format, file.getName());
            Toast.makeText(this, message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void publishDownloadProgress(int percent, long copied, long total) {

    }

    private void publishDownloadEnd(File file) {
        String format = getString(R.string.desc_download_end__format);
        String message = String.format(format, file.getName(),
                file.getParent());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void onHookTask(BaseFileTask task) {
        if (task.getTaskType() != BaseFileTask.TaskType.RECEIVE) {
            finish();
            return;
        }
        mTask = task;
        mTransaction = mTask.getTransaction();


        mProgressView.setMax(100);

        switch (task.getState()) {
            case PREPARE:
                onPrepare();
                break;
            case PROGRESS:
                onProgressUpdated(task.getProgress());
                break;
            case FINISHED:
                onFinished(task.getResult());
                break;
            case CANCELLED:
                onCancelled();
        }
        mTask.registerTaskObserver(this);

    }

    private void showFailureMessage(String msg) {
        String failedToLoadFileFormat = getString(
                R.string.desc_failed_to_load_file__format);
        mStatusView.setText(String.format(failedToLoadFileFormat, msg));
        mProgressView.setIndeterminate(false);
        mProgressView.setProgress(0);
    }


    @Override
    public void onAddTask(String tag, BaseFileTask task) {
        if (isFinishing()) {
            return;
        }
        if (tag.equals(mTaskKey)) {
            onHookTask(task);
            mFileTaskManager.removeObserver(this);
        }
    }

    @Override
    public void onRemoveTask(String tag, BaseFileTask task) {

    }

    @Override
    public void onPrepare() {
        if (isFinishing()) {
            return;
        }
        if (!mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(true);
        }
        mStatusView.setText(R.string.desc_file_preparing);
        mProgressView.setProgress(0);
    }

    @Override
    public void onProgressUpdated(BaseFileTask.Progress prog) {
        if (isFinishing()) {
            return;
        }
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        String progressFormat = getString(R.string.desc_file_receive_progress__format);
        String progressText = String.format(progressFormat,
                FileUtils.readableFileSize(prog.getDoneLength()),
                FileUtils.readableFileSize(prog.getFileLength()));
        mStatusView.setText(progressText);
        mProgressView.setProgress(prog.getPercent());
    }

    @Override
    public void onFinished(BaseFileTask.Result result) {
        if (isFinishing()) {
            return;
        }
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        if (result.isSuccess()) {
            BaseFileTask.SuccessResult r = (BaseFileTask.SuccessResult) result;
            File receivedFile = r.getFile();
            if (mReceivedFile.exists()) {
                mReceivedFile.delete();
            }
            receivedFile.renameTo(mReceivedFile);
            displayImage(mReceivedFile);
        } else {
            BaseFileTask.FailureResult r = (BaseFileTask.FailureResult) result;
            showFailureMessage(r.getException().getMessage());
        }
    }

    @Override
    public void onCancelled() {
        if (isFinishing()) {
            return;
        }
        if (mProgressView.isIndeterminate()) {
            mProgressView.setIndeterminate(false);
        }
        mProgressView.setIndeterminate(false);
        mStatusView.setText(R.string.desc_file_transfer_cancelled);
    }

    private void displayImage(File imageFile) {
        mProgressContainer.setVisibility(View.GONE);

        Uri receivedFileUri = Uri.fromFile(imageFile);
        mImageView.setImageURI(receivedFileUri);

        Animation anim = AnimationUtils.loadAnimation(
                this, android.R.anim.fade_in);
        anim.setFillAfter(true);
        mImageView.setAnimation(anim);

        mImageView.setVisibility(View.VISIBLE);
        mMenusView.setVisibility(View.VISIBLE);
    }
}
