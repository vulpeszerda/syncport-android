package io.syncport.ui.directory;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualCollectionModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;
import roboguice.inject.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class VideoCollectionFragment extends BaseFileCollectionFragment {

    public static VideoCollectionFragment getInstance(String deviceId) {
        VideoCollectionFragment fragment = new VideoCollectionFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String COLLECTION_TYPE = "video";

    private @InjectView(R.id.listview) ListView mListView;
    private ArrayAdapter<VirtualFileModel> mAdapter;

    public VideoCollectionFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_collection, container,
                false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected ArrayAdapter<VirtualFileModel> getAdapter() {
        return mAdapter;
    }

    @Override
    protected VirtualCollectionModel createCollectionModel(Device device) {
        return new VirtualCollectionModel(device) {
            @Override
            protected String getType() {
                return COLLECTION_TYPE;
            }
        };
    }

    @Override
    protected Comparator<VirtualFileModel> getComparator() {
        return new Comparator<VirtualFileModel>() {
            @Override
            public int compare(VirtualFileModel lhs, VirtualFileModel rhs) {
                Date lhsModifiedAt = lhs.getModifiedAt();
                Date rhsModifiedAt = rhs.getModifiedAt();
                if (lhsModifiedAt != null && rhsModifiedAt != null) {
                    return rhsModifiedAt.getTime() - lhsModifiedAt.getTime() > 0 ? 1 : -1;
                } else if (lhsModifiedAt != null && rhsModifiedAt == null) {
                    return -1;
                } else if (lhsModifiedAt == null && rhsModifiedAt != null) {
                    return 1;
                } else {
                    return lhs.getName().compareTo(rhs.getName());
                }
            }
        };
    }

    @Override
    protected void onDisplayReady(List<VirtualFileModel> files) {
        VideoAdapter adapter = (VideoAdapter) mAdapter;

        VirtualCollectionModel collectionModel = getCollection();
        registerThumbnailHashes(files);

        adapter.clear();
        adapter.addAll(files);
        adapter.notifyDataSetChanged();

        if (files.size() == 0 && collectionModel.isLoaded()) {
            showEmptyView(getString(R.string.desc_empty_video), false);
        } else if (files.size() == 0 && !collectionModel.isLoaded()) {
            showEmptyView(getString(R.string.desc_loading_directory), true);
        } else {
            hideEmptyView();
        }
    }

    @Override
    protected void setupAdapter() {
        if (mAdapter == null) {
            ImageLoader imageLoader = getImageLoader();
            mAdapter = new VideoAdapter(getActivity(), imageLoader);
        }

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    protected void showEmptyView(String message, boolean withProgress) {
        super.showEmptyView(message, withProgress);
        mListView.setVisibility(View.GONE);
    }

    @Override
    protected void hideEmptyView() {
        super.hideEmptyView();
        mListView.setVisibility(View.VISIBLE);
    }
}
