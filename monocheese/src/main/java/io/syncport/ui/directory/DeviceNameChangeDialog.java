package io.syncport.ui.directory;

import io.syncport.R;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.ui.BaseDialogFragment;
import io.syncport.utils.SyncportKeyBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

public class DeviceNameChangeDialog extends BaseDialogFragment implements
        OnClickListener {

    private String mDeviceName;
    private String mDeviceId;
    private String mErrorMsg;
    private String mNewName;

    private TextView mDescriptionView;
    private EditText mDeviceNameView;

    public static final String PARAM_NEW_NAME = SyncportKeyBuilder
            .build("new_name");

    public static DeviceNameChangeDialog getInstance(String deviceName,
            String deviceId) {
        DeviceNameChangeDialog dialog = new DeviceNameChangeDialog();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        args.putString(PARAM_DEVICE_NAME, deviceName);
        dialog.setArguments(args);
        return dialog;
    }

    private static final String PARAM_DEVICE_ID = SyncportKeyBuilder
            .build("device_id");
    private static final String PARAM_DEVICE_NAME = SyncportKeyBuilder
            .build("device_name");

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        Bundle args = getArguments();
        mDeviceId = args.getString(PARAM_DEVICE_ID);
        mDeviceName = args.getString(PARAM_DEVICE_NAME);
    }

    @SuppressLint("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_device_name_change, null);
        mDescriptionView = (TextView) view.findViewById(R.id.description);
        mDeviceNameView = (EditText) view.findViewById(R.id.input_device_name);

        String desc = String.format(
                getString(R.string.desc_type_new_device_name__format),
                mDeviceName);
        mDescriptionView.setText(desc);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_change_device_name).setView(view)
                .setNegativeButton(R.string.button_cancel, null)
                .setPositiveButton(R.string.button_change, null).create();
        dialog.setOnShowListener(new OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button btn = ((AlertDialog) dialog)
                        .getButton(AlertDialog.BUTTON_POSITIVE);
                btn.setOnClickListener(DeviceNameChangeDialog.this);
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onClick(View view) {
        if (TextUtils.isEmpty(mDeviceNameView.getText())) {
            mDeviceNameView.setError(getString(R.string.error_value_required));
            return;
        }
        mNewName = mDeviceNameView.getText().toString();
        ApiResources.changeDeviceName(getActivity(), mDeviceId, mNewName,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            FragmentManager manager = getFragmentManager();
                            manager.popBackStack(null, 0);
                            dismiss(false);

                            Fragment target = getTargetFragment();
                            if (target != null
                                    && target instanceof DialogResultHandler) {
                                DialogResultHandler handler = (DialogResultHandler) target;
                                Intent data = new Intent();
                                data.putExtra(PARAM_NEW_NAME, mNewName);
                                handler.onDialogResult(
                                        getTargetRequestCode(), RESULT_OK,
                                        data);
                            }
                        } catch (Exception e) {
                        }

                    }
                }, new RobustResponseErrorListener(getActivity()) {

                    @Override
                    public void onErrorResponse(VolleyError error,
                                                String msg) {
                        DeviceNameChangeDialog.this.onErrorResponse(error,
                                msg);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                });

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.addToBackStack(null);
        transaction.remove(DeviceNameChangeDialog.this);

        ProgressDialogFragment progressDialog = new ProgressDialogFragment();
        progressDialog.show(transaction, "progress");
    }

    public void onErrorResponse(VolleyError error, String errorStr) {
        FragmentManager manager = getFragmentManager();
        manager.popBackStack(null, 0);
        mErrorMsg = errorStr;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDeviceNameView.setError(mErrorMsg);
    }

    public static class ProgressDialogFragment extends BaseDialogFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setCancelable(false);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Request change..");
            dialog.setCanceledOnTouchOutside(false);
            return dialog;
        }
    }
}
