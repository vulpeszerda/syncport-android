package io.syncport.ui.directory;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.ui.directory.model.VirtualAlbumCollectionModel;
import io.syncport.ui.directory.model.VirtualAlbumModel;
import io.syncport.ui.directory.model.VirtualCollectionModel;
import io.syncport.ui.directory.model.VirtualFileModel;
import roboguice.inject.InjectView;


public class ImageCollectionFragment extends
        BaseCollectionFragment<VirtualAlbumModel> {

    public static ImageCollectionFragment getInstance(String deviceId) {
        ImageCollectionFragment fragment = new ImageCollectionFragment();
        Bundle args = new Bundle();
        args.putString(PARAM_DEVICE_ID, deviceId);
        fragment.setArguments(args);
        return fragment;
    }


    private @InjectView(R.id.gridview) GridView mGridView;
    private ArrayAdapter<VirtualAlbumModel> mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_collection__album, container,
                false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected VirtualCollectionModel createCollectionModel(Device device) {
        return new VirtualAlbumCollectionModel(device);
    }

    @Override
    protected ArrayAdapter<VirtualAlbumModel> getAdapter() {
        return mAdapter;
    }

    @Override
    protected void displayCollections() {
        final VirtualAlbumCollectionModel albumCollectionModel =
                (VirtualAlbumCollectionModel) getCollection();

        new AsyncTask<Void, Void, List<VirtualAlbumModel>>() {
            @Override
            protected List<VirtualAlbumModel> doInBackground(Void... params) {
                List<VirtualAlbumModel> albums = albumCollectionModel.getAllAlbums();
                List<VirtualFileModel> files = new ArrayList<>();
                for(VirtualAlbumModel album : albums) {
                    files.add(album.getThumbnailFile());
                }

                registerThumbnailHashes(files);
                if (albums.size() > 1) {
                    Collections.sort(albums, new Comparator<VirtualAlbumModel>() {
                        @Override
                        public int compare(VirtualAlbumModel lhs, VirtualAlbumModel rhs) {
                            return rhs.getCount() - lhs.getCount();
                        }
                    });
                }
                return albums;
            }

            @Override
            protected void onPostExecute(List<VirtualAlbumModel> albums) {
                if (!isAdded()) {
                    return;
                }
                AlbumAdapter adapter = (AlbumAdapter) getAdapter();
                adapter.clear();
                adapter.addAll(albums);
                adapter.notifyDataSetChanged();

                if (albums.size() == 0) {
                    if (albumCollectionModel.isLoaded()) {
                        showEmptyView(getString(R.string.desc_empty_image), false);
                    } else {
                        showEmptyView(getString(R.string.desc_loading_directory), true);
                    }
                } else {
                    hideEmptyView();
                }

            }
        }.execute();
    }

    @Override
    protected void setupAdapter() {
        if (mAdapter == null) {
            ImageLoader imageLoader = getImageLoader();
            mAdapter = new AlbumAdapter(getActivity(), imageLoader);
        }
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        VirtualAlbumModel model = (VirtualAlbumModel)parent.getAdapter()
                .getItem(position);
        Intent intent = AlbumActivity.createIntent(getBaseActivity(), model);
        startActivity(intent);
    }

    @Override
    protected void showEmptyView(String message, boolean withProgress) {
        super.showEmptyView(message, withProgress);
        mGridView.setVisibility(View.GONE);
    }

    @Override
    protected void hideEmptyView() {
        super.hideEmptyView();
        mGridView.setVisibility(View.VISIBLE);
    }
}
