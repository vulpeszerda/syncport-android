package io.syncport.ui.directory;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.syncport.R;
import io.syncport.core.InternetConnectivityManager;
import io.syncport.ui.BaseDialogFragment;
import io.syncport.ui.directory.model.VirtualFileModel;
import io.syncport.utils.FileUtils;

/**
 * Created by vulpes on 15. 2. 23..
 */
public abstract class BaseFileCollectionFragment extends
        BaseCollectionFragment<VirtualFileModel> implements
        BaseDialogFragment.DialogResultHandler {

    private static final int REQUEST_CODE__FILE_OPEN_ACTIVITY = 12;
    private static final int REQUEST_CODE__FILE_OPEN = 11;

    private InternetConnectivityManager mInternetManager;
    private boolean mFileOpenable;

    protected abstract Comparator<VirtualFileModel> getComparator();
    protected abstract void onDisplayReady(List<VirtualFileModel> files);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFileOpenable = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mInternetManager = InternetConnectivityManager.getInstance(getBaseActivity());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        // TODO change download to stremaing(open file)
        VirtualFileModel model = getAdapter().getItem(position);
        showDownloadConfirmDialog(model);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__FILE_OPEN_ACTIVITY) {
            mFileOpenable = true;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDialogResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__FILE_OPEN) {
            if (resultCode == BaseDialogFragment.RESULT_OK && data != null) {
                String path = data
                        .getStringExtra(FileOpenDialog.PARAM_RECEIVED_FILE_PATH);
                File receivedFile = new File(path);
                if (receivedFile.exists()) {
                    openDownloadedFile(receivedFile);
                }
            } else {
                mFileOpenable = true;
            }
        }
    }

    @Override
    protected void displayCollections() {
        new DisplayCollectionTask().execute();
    }

    private void showErrorDialog(String title, String msg) {
       new AlertDialog.Builder(getActivity())
               .setTitle(title)
               .setMessage(msg)
               .setPositiveButton(R.string.button_confirm, null).show();
    }

    private void showDownloadConfirmDialog(final VirtualFileModel model) {
        File syncportDownloadDir = new File(
                Environment.getExternalStorageDirectory(), "Syncport/download");
        final File sameNameFile = new File(syncportDownloadDir, model.getName());
        if (!sameNameFile.exists()) {
            showDateUsageConfirmDialog(model);
            return;
        }

        DialogInterface.OnClickListener listener =
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            showDateUsageConfirmDialog(model);
                        } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                            openDownloadedFile(sameNameFile);
                        }
                    }
                };

        new AlertDialog.Builder(getActivity()).setTitle(R.string.title_caution)
                .setMessage(getString(R.string.already_have_file))
                .setPositiveButton(R.string.button_yes, listener)
                .setNeutralButton(R.string.button_open_with_cache, listener).show();
    }

    private void showDateUsageConfirmDialog(final VirtualFileModel model) {
        if (!mInternetManager.isConnected()) {
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showDateUsageConfirmDialog(model);
                }
            };
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(R.string.desc_failed_to_connect_internet)
                    .setPositiveButton(R.string.button_retry, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;
        }
        if (!mInternetManager.isWifiConnected()) {
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showPeerDateUsageConfirmDialog(model);
                }
            };
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_are_you_sure)
                    .setMessage(R.string.desc_data_usage_confirm)
                    .setPositiveButton(R.string.button_proceed, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;
        }
        showPeerDateUsageConfirmDialog(model);
    }

    private void showPeerDateUsageConfirmDialog(final VirtualFileModel model) {
        if (model.getDevice().isChargedConnectedState()) {
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    openFile(model);
                }
            };
            String format = getString(R.string
                    .desc_peer_data_usage_confirm__format);
            String message = String.format(format, model.getDevice().getName());
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_are_you_sure)
                    .setMessage(message)
                    .setPositiveButton(R.string.button_proceed, listener)
                    .setNegativeButton(R.string.button_cancel, null)
                    .show();
            return;

        }
        openFile(model);
    }

    private void openFile(VirtualFileModel fileModel) {
        if (!mFileOpenable) {
            return;
        }
        mFileOpenable = false;
        if (!fileModel.getDevice().isTurnOn()) {
            String format = getString(R.string.desc_file_access_deneied__device_turned_off);
            String msg = String.format(format, fileModel.getDevice().getName());

            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_failed_to_load)
                    .setMessage(msg)
                    .setPositiveButton(R.string.button_confirm,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    mFileOpenable = true;
                                }
                            }).show();
            return;
        }

        String absPath = fileModel.getAbsolutePath();

        int splitPoint = absPath.lastIndexOf("/");
        final String parentPath = absPath.substring(0, splitPoint);
        final String filename = absPath.substring(splitPoint + 1);
        final String ownerId = fileModel.getDevice().getId();

        FileOpenDialog dialog = FileOpenDialog.getInstance(ownerId, parentPath,
                filename);
        dialog.setTargetFragment(this, REQUEST_CODE__FILE_OPEN);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "fileopen");
    }

    private void openDownloadedFile(File file) {
        try {
            startActivityForResult(
                    FileUtils.getOpenFileIntent(file),
                    REQUEST_CODE__FILE_OPEN_ACTIVITY);
        } catch (ActivityNotFoundException | IOException e) {
            e.printStackTrace();
            mFileOpenable = true;
            showErrorDialog(
                    getString(R.string.title_failed_to_open),
                    getString(R.string.desc_failed_to_find_openable_activity));
        }
    }

    private class DisplayCollectionTask
            extends AsyncTask<Void, Void, List<VirtualFileModel>> {

        @Override
        protected List<VirtualFileModel> doInBackground(Void... params) {

            List<VirtualFileModel> files = getCollection().getAll();
            registerThumbnailHashes(files);
            if (files.size() > 1) {
                Collections.sort(files, getComparator());
            }
            return files;
        }

        @Override
        protected void onPostExecute(List<VirtualFileModel> files) {
            if (isAdded()) {
                onDisplayReady(files);
            }
        }
    }
}
