package io.syncport.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.ui.addon.ArrayAdapter;

public class DeviceAdapter extends ArrayAdapter<Device> {
    /**
     * Adapter for listing devices in NavigationDrawerFragment
     */

    private static final int ITEM_LAYOUT_ID = R.layout.navigation_list__device_item;

    public DeviceAdapter(Context context) {
        super(context);
    }

    public int getPosition(Device device) {
        return getItems().indexOf(device);
    }

    protected void update(ViewHolder viewHolder, Device device) {
        String deviceType = device.getType();
        boolean isTurnOn = device.isTurnOn();

        int icon;
        if (deviceType.equals("ANDROID")) {
            icon = isTurnOn ? R.drawable.ic_phone : R.drawable.ic_phone_off;
        } else if (deviceType.equals("MAC") || deviceType.equals("WINDOWS")) {
            icon = isTurnOn ? R.drawable.ic_desktop : R.drawable.ic_desktop_off;
        } else {
            icon = isTurnOn ? R.drawable.ic_desktop : R.drawable.ic_desktop_off;
        }

        viewHolder.getName().setText(device.getName());
        viewHolder.getIcon().setImageResource(icon);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;

        if (row == null) {
            row = mInflater.inflate(ITEM_LAYOUT_ID, null);
            viewHolder = new ViewHolder(
                    (TextView) row.findViewById(R.id.text),
                    (ImageView) row.findViewById(R.id.img));

            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        if (viewHolder != null) {
            update(viewHolder, getItem(position));
        }

        return row;
    }

    private class ViewHolder {
        private TextView mDeviceName;
        private ImageView mDeviceIcon;

        public ViewHolder(TextView text, ImageView img) {
            mDeviceName = text;
            mDeviceIcon = img;
        }

        public TextView getName() {
            return mDeviceName;
        }

        public ImageView getIcon() {
            return mDeviceIcon;
        }
    }
}
