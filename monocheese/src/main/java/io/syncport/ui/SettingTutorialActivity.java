package io.syncport.ui;

import io.syncport.R;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

@ContentView(R.layout.activity_tutorial)
public class SettingTutorialActivity extends BaseActivity implements
        OnClickListener {

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, SettingTutorialActivity.class);
        return intent;
    }

    public static Intent createIntent(Context context, Intent nextIntent) {
        Intent intent = new Intent(context, SettingTutorialActivity.class);
        intent.putExtra(PARAM_NEXT_INTENT, nextIntent);
        return intent;
    }

    private static final String PARAM_NEXT_INTENT = SyncportKeyBuilder
            .build("next_intent");

    private @InjectView(R.id.button_skip) Button mSkipBtn;

    private Intent mNextIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNextIntent = getIntent().getParcelableExtra(PARAM_NEXT_INTENT);
        if (mNextIntent == null) {
            mNextIntent = NavigationDrawerActivity.createIntent(this);
        }

        mSkipBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_skip:
            if (mNextIntent != null) {
                startActivity(mNextIntent);
            }
            SharedPreferences pref = PreferenceManager
                    .getDefaultSharedPreferences(this);

            String connectTutorialKey = getString(
                    R.string.preference_key__did_connect_tutorial);
            pref.edit().putBoolean(connectTutorialKey, true).commit();
            finish();
            break;
        }
    }
}
