package io.syncport.ui;

import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.ui.addon.ErrorDialogFragment;
import io.syncport.ui.addon.ProgressDialogFragment;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.InjectView;

public class NavigationDrawerFragment extends BaseFragment implements
        DeviceManager.Observer, View.OnClickListener,
        AdapterView.OnItemClickListener {

    public static interface MenuType {
        static int PICTURE = 0;
        static int MUSIC = 1;
        static int VIDEO = 2;
        static int DOCUMENT = 3;
        static int FILE_BROWSER = 4;
        static int FILE_TRANSFER = 5;
        static int DEVICE_SETTING = 6;
        static int ACCOUNT_SETTING = 7;
        static int NO_DEVICE = 8;
    }

    private static final Integer[] DEVICE_MENUS = {
            MenuType.PICTURE,
            MenuType.MUSIC,
            MenuType.VIDEO,
            MenuType.DOCUMENT,
            MenuType.FILE_BROWSER
    };

    /**
     * Remember the position of the selected item.
     */
    public static final String STATE_SELECTED_MENU =
            SyncportKeyBuilder.build("state_selected_menu");
    public static final String TAG = "NavigationDrawerFragment";

    /**
     * Per the design guidelines, you should show the mDrawerLayout on launch
     * until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER =
            "navigation_mDrawerLayout_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallback mCallback;

    /**
     * Helper component that ties the action bar to the navigation mDrawerLayout.
     */
    private ActionBarDrawerToggle mDrawerToggle;
    private View mFragmentContainerView;
    private View mHeaderView;
    private LinearLayout mHeaderContainerView;

    private @InjectView(android.R.id.list) ListView mDrawerListView;
    private DrawerLayout mDrawerLayout;

    private NavigationAdapter mNavigationAdapter;
    private DeviceAdapter mDeviceAdapter;

    private DeviceManager mDeviceManager;
    private Device mWorkingDevice;

    private int mCurrentSelectedMenu = -1;
    private boolean mUserLearnedDrawer;
    private boolean mFirstLaunch = true;
    private boolean mHasDevices = false;

    public NavigationDrawerFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        // Read in the flag indicating whether or not the user has
        // demonstrated awareness of the
        // mDrawerLayout. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences
                (getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        try {
            mDeviceManager =  DeviceManager.from(getActivity());
        } catch (AuthenticatorException e) {
            // not reach
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of
        // actions in the action bar.
        setHasOptionsMenu(true);
;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_navigation_drawer, null,
                false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mNavigationAdapter == null) {
            mNavigationAdapter = new NavigationAdapter(getActivity());
        }
        if (mDeviceAdapter == null) {
            mDeviceAdapter = new DeviceAdapter(getActivity());
        }


        // set header container
        if (mHeaderContainerView == null) {
            mHeaderContainerView = new LinearLayout(getActivity());
            mHeaderContainerView.setOrientation(LinearLayout.VERTICAL);

            AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                    AbsListView.LayoutParams.MATCH_PARENT,
                    AbsListView.LayoutParams.WRAP_CONTENT);

            mHeaderContainerView.setLayoutParams(layoutParams);
        }
        mDrawerListView.addHeaderView(mHeaderContainerView);


        mDrawerListView.setOnItemClickListener(this);
        mDrawerListView.setAdapter(mNavigationAdapter);
    }

    private void addHeaderView(View view) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mHeaderContainerView.addView(view, params);
    }

    private void removeHeaderView(View view) {
        mHeaderContainerView.removeView(view);
    }

    private void initNoDeviceMenus() {
        List<Integer> deviceMenus = Arrays.asList(DEVICE_MENUS);
        if (deviceMenus.indexOf(mCurrentSelectedMenu) > -1) {
            mCurrentSelectedMenu = MenuType.NO_DEVICE;
        }
        mNavigationAdapter.clear();
        mNavigationAdapter.addAll(NavigationItem.getNoDeviceNavigationItems());
        mNavigationAdapter.notifyDataSetChanged();

        if (mHeaderView != null) {
            removeHeaderView(mHeaderView);
        }
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mHeaderView = inflater.inflate(R.layout.header_navi_drawer__nodevice,
                null, false);

        addHeaderView(mHeaderView);
        mWorkingDevice = null;
    }

    private void initDeviceMenus(Device initialDevice) {
        // called when device is added as first time
        if (mCurrentSelectedMenu == MenuType.NO_DEVICE) {
            mCurrentSelectedMenu = MenuType.FILE_BROWSER;
        }

        mNavigationAdapter.clear();
        mNavigationAdapter.addAll(NavigationItem.getNavigationItems());
        mNavigationAdapter.notifyDataSetChanged();

        if (mHeaderView != null) {
            removeHeaderView(mHeaderView);
        }
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        mHeaderView = inflater.inflate(R.layout.header_navi_drawer, null, false);
        mHeaderView.setOnClickListener(this);

        addHeaderView(mHeaderView);
        selectItem(initialDevice);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mDeviceManager != null) {
            mDeviceManager.addObserver(this);
        }
        fetchDeviceInfos();
    }


    private void fetchDeviceInfos() {
        if (mDeviceManager == null) {
            return;
        }
        if (mFirstLaunch) {
            ProgressDialogFragment.show(getBaseActivity(), "load_devices",
                    getString(R.string.title_loading_devices_list));
        }
        mDeviceManager.getAllDevices(true, new DeviceManager.OnGetDevicesListener() {
            @Override
            public void onGetDevices(List<Device> devices) {
                if (!isAdded()) {
                    return;
                }
                for (Device device : devices) {
                    onNewDeviceAdded(device);
                }
                if (mFirstLaunch) {
                    mFirstLaunch = false;
                    if (mDeviceAdapter.getCount() == 0) {
                        initNoDeviceMenus();
                    }
                    initDrawer();
                    ProgressDialogFragment.hide(getBaseActivity(), "load_devices");
                }
            }

            @Override
            public void onFailure(Exception e) {
                if (!isAdded()) {
                    return;
                }
                showRetryFetchDevicesDialog();
                if (mFirstLaunch) {
                    ProgressDialogFragment.hide(getBaseActivity(), "load_devices");
                }
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mDeviceManager != null) {
            mDeviceManager.removeObserver(this);
        }
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void setUp(int fragmentId, int drawerLayoutResId, int menu) {
        mCurrentSelectedMenu = menu;
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = (DrawerLayout)getActivity().findViewById
                (drawerLayoutResId);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation mDrawerLayout and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                getBaseActivity().getToolbar(),             /* nav mDrawerLayout image to replace  'Up' caret */
                R.string.navigation_drawer_open,  /* "open mDrawerLayout" description for accessibility */
                R.string.navigation_drawer_close  /* "close mDrawerLayout" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View mDrawerLayoutView) {
                super.onDrawerClosed(mDrawerLayoutView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View mDrawerLayoutView) {
                super.onDrawerOpened(mDrawerLayoutView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the mDrawerLayout; store
                    // this flag to prevent auto-showing
                    // the navigation mDrawerLayout automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void initDrawer() {
        NavigationItem item = mNavigationAdapter.getItemFromMenu
                (mCurrentSelectedMenu);
        if (item == null && mNavigationAdapter.getCount() > 0) {
            item = mNavigationAdapter.getItem(0);
        }
        if (item != null) {
            selectItem(item);
        }

        // If the user hasn't 'learned' about the mDrawerLayout,
        // open it to introduce them to the mDrawerLayout,
        // per the navigation mDrawerLayout design guidelines.
        if (!mUserLearnedDrawer) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
    }

    private void selectItem(NavigationItem item) {
        mCurrentSelectedMenu = item.getMenuType();
        if (mCallback != null) {
            mCallback.onNavigationDrawerItemSelected(item.getMenuType(),
                    mWorkingDevice);
            int position = mDrawerListView.getHeaderViewsCount() +
                    mNavigationAdapter.getPosition(item);
            mDrawerListView.setItemChecked(position, true);
        }
    }

    private void selectItem(Device device) {
        mWorkingDevice = device;
        updateHeader();

        NavigationItem item = mNavigationAdapter.getItemFromMenu(mCurrentSelectedMenu);
        if (item == null) {
            item = mNavigationAdapter.getItem(0);
        }
        mDeviceAdapter.notifyDataSetChanged();

        if (mCallback != null) {
            mCallback.onNavigationDrawerItemSelected(item.getMenuType(), mWorkingDevice);
        }
    }

    private void updateHeader() {
        TextView deviceName = (TextView) mHeaderView.findViewById(R.id.device_name);
        TextView deviceStatus = (TextView) mHeaderView.findViewById(R.id.device_status);
        ImageView deviceIcon = (ImageView) mHeaderView.findViewById(R.id.device_icon);

        String deviceType = mWorkingDevice.getType();
        boolean isTurnOn = mWorkingDevice.isTurnOn();
        int iconResId;


        if (deviceType.equals("ANDROID")) {
            iconResId = isTurnOn ? R.drawable.ic_phone : R.drawable.ic_phone_off;
        } else if (deviceType.equals("MAC") || deviceType.equals("WINDOWS")) {
            iconResId = isTurnOn ? R.drawable.ic_desktop : R.drawable.ic_desktop_off;
        } else {
            iconResId = isTurnOn ? R.drawable.ic_desktop : R.drawable.ic_desktop_off;
        }

        if (isTurnOn) {
            deviceStatus.setText("Turned on");
            deviceStatus.setTextColor(getResources().getColor(R.color.text_link));
        } else {
            deviceStatus.setText("Turned off");
            deviceStatus.setTextColor(getResources().getColor(R.color.text_light_gray));
        }

        deviceIcon.setImageResource(iconResId);
        deviceName.setText(mWorkingDevice.getName());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof NavigationDrawerCallback) {
            mCallback = (NavigationDrawerCallback) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the mDrawerLayout toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the mDrawerLayout is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId() == R.id.action_example) {
            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation mDrawerLayout design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
//        ActionBar actionBar = getActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallback {
        /**
         * Called when an item in the navigation mDrawerLayout is selected.
         */
        void onNavigationDrawerItemSelected(int menuType, Device device);
    }

    @Override
    public void onClick(View v) {
        // called when header of listview is clicked
        ImageView toggleButton = (ImageView) v.findViewById(R.id.button_toggle);
        ListAdapter adapter = mDrawerListView.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        }

        if (adapter instanceof  NavigationAdapter) {
            mDrawerListView.setAdapter(mDeviceAdapter);

            int position = mDeviceAdapter.getPosition(mWorkingDevice);
            if (position > -1) {
                position += mDrawerListView.getHeaderViewsCount();
                mDrawerListView.setItemChecked(position, true);
            }
            toggleButton.setImageResource(R.drawable.ic_carrot_up);
        } else if (adapter instanceof  DeviceAdapter) {
            mDrawerListView.setAdapter(mNavigationAdapter);

            NavigationItem item = mNavigationAdapter.getItemFromMenu
                    (mCurrentSelectedMenu);
            if (item == null && mNavigationAdapter.getCount() > 0) {
                item = mNavigationAdapter.getItem(0);
            }
            if (item != null) {
                int position = mNavigationAdapter.getPosition(item);
                if (position > -1) {
                    position += mDrawerListView.getHeaderViewsCount();
                    mDrawerListView.setItemChecked(position, true);
                }
            }
            toggleButton.setImageResource(R.drawable.ic_carrot_down);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /**
         * called when item in listview is clicked
         * item has 2 cases, NavigationItem and Device
         */
        Object item = parent.getItemAtPosition(position);
        if (item == null) {
            return;
        }
        if (item instanceof NavigationItem) {
            NavigationItem navItem = (NavigationItem) item;
            if (!navItem.isSection() && navItem.getMenuType() !=
                    mCurrentSelectedMenu) {
                selectItem(navItem);
            }
        } else if (item instanceof Device) {
            Device device = (Device) item;
            if (device != mWorkingDevice) {
                selectItem((Device) item);
            }
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }

    @Override
    public void onNewDeviceAdded(Device device) {
        if (!isAdded()) {
            return;
        }
        if (mDeviceAdapter.getPosition(device) == -1
                && !device.getType().equals(Device.Type.WEB)
                && !device.isMyDevice()) {
            int prevDeviceAdapterCount = mDeviceAdapter.getCount();
            mHasDevices = true;
            mDeviceAdapter.add(device);
            if (prevDeviceAdapterCount == 0) {
                initDeviceMenus(device);
            } else {
                mDeviceAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onDeviceTurnOn(Device device) {
        if (!isAdded()) {
            return;
        }
        if (mWorkingDevice != null && mWorkingDevice.equals(device)) {
            System.out.println("workingdevice: " + device.getId());
            updateHeader();
        }
        mDeviceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeviceTurnOff(Device device) {
        if (!isAdded()) {
            return;
        }
        if (mWorkingDevice != null && mWorkingDevice.equals(device)) {
            updateHeader();
        }
        mDeviceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeviceNameChanged(Device device, String name) {
        if (!isAdded()) {
            return;
        }
        if (mWorkingDevice != null && mWorkingDevice.equals(device)) {
            updateHeader();
        }
        mDeviceAdapter.notifyDataSetChanged();
    }

    private void showRetryFetchDevicesDialog() {

        AlertDialog dialog = new AlertDialog.Builder(getBaseActivity())
                .setTitle(R.string.failed_to_fetch_devices__title)
                .setMessage(R.string.failed_to_fetch_devices__desc)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                fetchDeviceInfos();
                            }
                        })
                .setNegativeButton(R.string.button_exit,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                getBaseActivity().finish();
                            }
                        }).create();

        ErrorDialogFragment fragment = new ErrorDialogFragment();
        fragment.setDialog(dialog);
        fragment.showAllowingStateLoss(getBaseActivity()
                        .getSupportFragmentManager(),
                "retry_dialog");
    }
}
