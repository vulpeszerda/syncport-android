package io.syncport.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import io.syncport.BuildConfig;
import io.syncport.R;
import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.remote.ApiResources;
import io.syncport.ui.addon.ErrorDialogFragment;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;
import io.syncport.utils.VersionComparator;
import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import roboguice.inject.ContentView;

@ContentView(R.layout.activity_startup)
public class StartupActivity extends BaseActivity {

    public static Intent createIntent(Context context) {
        return createIntent(context, null);
    }

    public static Intent createIntent(Context context, Intent nextIntent) {
        Intent intentToBeStart = new Intent(context, StartupActivity.class);
        intentToBeStart.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        intentToBeStart.putExtra(PARAM_NEXT_INTENT, nextIntent);
        return intentToBeStart;
    }

    private static final String TAG = "StartupActivity";
    private static final int GOOGLE_PLAY_SERVICE_REQUEST = 9000;
    private static final String PARAM_NEXT_INTENT = SyncportKeyBuilder .build("next_intent");

    private Intent mNextIntent;
    private StartupThread mThread;
    private Handler mResumeHandler;
    private Runnable mResumeRunnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!BuildConfig.DEBUG) {
            RaygunClient.Init(getApplicationContext());
            RaygunClient.AttachExceptionHandler();
        }
        else {
            Toast.makeText(this, "Developer mode enabled",
                    Toast.LENGTH_SHORT) .show();
        }

        Intent intent = getIntent();
        mNextIntent = intent.getParcelableExtra(PARAM_NEXT_INTENT);
        if (mNextIntent == null) {
            mNextIntent = NavigationDrawerActivity.createIntent(StartupActivity.this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mResumeHandler != null && mResumeRunnable != null) {
            mResumeHandler.removeCallbacks(mResumeRunnable);
        }
        if (mThread != null && mThread.isAlive()) {
            mThread.setPause();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (hasGooglePlayService()) {

            if (!isFinishing()) {
                if (mResumeRunnable == null) {
                    mResumeRunnable = new Runnable() {

                        @Override
                        public void run() {
                            if (mThread == null || !mThread.isAlive()) {
                                mThread = new StartupThread();
                                mThread.start();
                            } else {
                                mThread.setResume();
                            }
                        }
                    };
                }
                if (mResumeHandler == null) {
                    mResumeHandler = new Handler();
                }
                mResumeHandler.postDelayed(mResumeRunnable, 1000);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mThread != null && mThread.isAlive()) {
            mThread.interrupt();
        }
        super.onDestroy();
    }


    private class StartupThread extends Thread {
        private final AtomicBoolean mIsStarted = new AtomicBoolean(false);
        private Object mPauseLock;
        private boolean mPaused;

        public StartupThread() {
            mPauseLock = new Object();
            mPaused = false;
        }

        @Override
        public synchronized void start() {
            if (!mIsStarted.getAndSet(true)) {
                super.start();
            }
        }

        @Override
        public void run() {
            try {
                if (!BuildConfig.DEBUG && !checkAppVersion()) {
                    return;
                }
                SyncportAccount account = SyncportAccount
                        .getOrCreateAccount(StartupActivity.this);
                if (account == null) {
                    finish();
                    return;
                }

                String pushKey = null;
                String exceptionMsg = null;
                try {
                    pushKey = getOrRegisterGcmKey(account);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    VolleyError error = (VolleyError) e.getCause();
                    if (error instanceof NoConnectionError) {
                        exceptionMsg = getString(R.string.desc_failed_to_connect_internet);
                    } else {
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null
                                && response.data.length > 0) {
                            exceptionMsg = new String(response.data);
                        }
                        if (exceptionMsg == null) {
                            exceptionMsg = error.getMessage();
                        }
                    }
                }
                if (pushKey == null) {
                    String message = getString(R.string.failed_to_register_desc);
                    if (exceptionMsg != null) {
                        message += " (" + exceptionMsg + ")";
                    }
                    showRetryDialog(getString(R.string.failed_to_register),
                            message);
                    return;
                }

                // do long time tasks

                startSyncportBackgroundService();
                synchronized (mPauseLock) {
                    while (mPaused) {
                        mPauseLock.wait();
                    }
                }
                startNextActivity();
                finish();
            } catch (InterruptedException e) {
            }
        }

        public void setPause() {
            synchronized (mPauseLock) {
                mPaused = true;
            }
        }

        public void setResume() {
            synchronized (mPauseLock) {
                mPaused = false;
                mPauseLock.notifyAll();
            }
        }
    }

    private void startNextActivity() {
//        SharedPreferences pref = PreferenceManager
//                .getDefaultSharedPreferences(this);
//
//        String connectTutorialKey = getString(
//                R.string.preference_key__did_connect_tutorial);
//        boolean didConnectTutorial = pref.getBoolean(connectTutorialKey, false);
//
//        if (didConnectTutorial) {
//            mNextIntent = NavigationDrawerActivity.createIntent(this);
//            startActivity(mNextIntent);
//        } else {
//            startActivity(SettingTutorialActivity.createIntent(this));
//        }
        mNextIntent = NavigationDrawerActivity.createIntent(this);
        startActivity(mNextIntent);
    }

    private void startSyncportBackgroundService() {
        if (!RootService.isRunning(this)) {
            startService(RootService.createIntent(this.getApplicationContext()));
        }
    }

    private String getOrRegisterGcmKey(SyncportAccount account)
            throws IOException, InterruptedException, ExecutionException {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(this);

        String gcmPushIdKey = getString(R.string.preference_key__gcm);
        String appVersionKey = getString(R.string.preference_key__app_version);
        String gcmPushHolderKey = getString(R.string.perference_key__gcm_holder);

        String gcmPushId = pref.getString(gcmPushIdKey, null);
        String gcmHolder = pref.getString(gcmPushHolderKey, null);

        int currentAppVersion = getAppVersion(this);
        int appVersion = pref.getInt(appVersionKey, -1);
        boolean needServerPush = false;

        if (TextUtils.isEmpty(gcmPushId) || appVersion < 0
                || appVersion != currentAppVersion || gcmHolder == null
                || !gcmHolder.equals(account.getName())) {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            gcmPushId = gcm.register(getString(R.string.gcm_sender_id));
            needServerPush = true;
        }

        if (needServerPush) {
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            ApiResources.registerPushKey(this, gcmPushId, future, future);
            JSONObject result = future.get();

            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(appVersionKey, currentAppVersion)
                    .putString(gcmPushIdKey, gcmPushId)
                    .putString(gcmPushHolderKey, account.getName()).commit();

        }
        return gcmPushId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void showUpdateDialog() {
        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                final String appPackageName = getPackageName();
                Intent intent;
                try {
                    intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=" + appPackageName));
                    startActivity(intent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    intent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id="
                                    + appPackageName));
                    startActivity(intent);
                }
            }
        };
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(
                        StartupActivity.this)
                        .setTitle(R.string.get_new_version)
                        .setMessage(R.string.get_new_version_desc)
                        .setPositiveButton(R.string.button_get_new_version,
                                listener).create();

                ErrorDialogFragment fragment = new ErrorDialogFragment();
                fragment.setDialog(dialog);
                fragment.showAllowingStateLoss(getSupportFragmentManager(), "app_update");

            }

        });
    }

    private boolean checkAppVersion() {
        String message = null;
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            String currentVersion = packageInfo.versionName;
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            ApiResources.getClientVersion(this, future, future);
            JSONObject response = future.get();
            String recentVersion = response.getString("android_version");
            if (!recentVersion.matches("\\d+\\.\\d+\\.\\d+")) {
                throw new Exception("version is not proper format");
            }
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            String remoteAppVersionKey = getString(R.string
                    .preference_key__remote_app_version);
            editor.putString(remoteAppVersionKey, recentVersion);
            editor.commit();

            int cmp = VersionComparator.compare(currentVersion, recentVersion);
            if (cmp < 0) {
                // older version
                showUpdateDialog();
                return false;
            }
            return true;
        } catch (ExecutionException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            VolleyError error = (VolleyError) e.getCause();
            if (error instanceof NoConnectionError) {
                message = getString(R.string.desc_failed_to_connect_internet);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            message = getString(R.string.desc_failed_to_parse_json);
        } catch (NameNotFoundException e) {
            // skip
        } catch (InterruptedException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
            message = e.getMessage();
        }

        String text = getString(R.string.failed_to_version_compare_desc);
        if (message != null) {
            text += " (" + message + ")";
        }
        showRetryDialog(getString(R.string.failed_to_version_compare), text);
        return false;
    }

    public boolean hasGooglePlayService() {
        final int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        }
        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                ErrorDialogFragment dialogFragment = null;
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                            resultCode, StartupActivity.this,
                            GOOGLE_PLAY_SERVICE_REQUEST);
                    if (errorDialog != null) {
                        dialogFragment = new ErrorDialogFragment();
                        dialogFragment.setDialog(errorDialog);
                    }
                } else {
                    AlertDialog dialog = new AlertDialog.Builder(
                            StartupActivity.this)
                            .setTitle(R.string.critical_failure_on_start)
                            .setMessage(
                                    R.string.google_play_service_not_availiable)
                            .setPositiveButton(R.string.button_confirm,
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            finish();
                                        }
                                    }).create();
                    dialogFragment = new ErrorDialogFragment();
                    dialogFragment.setDialog(dialog);

                }
                if (!isFinishing() && dialogFragment != null) {
                    dialogFragment.showAllowingStateLoss(
                            getSupportFragmentManager(), "google playservice");
                }
            }
        });
        return false;
    }


    private void showRetryDialog(final String title, final String message) {

        this.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(
                        StartupActivity.this)
                        .setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(R.string.button_retry,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        if (mThread != null
                                                && mThread.isAlive()) {
                                            mThread.interrupt();
                                        }
                                        mThread = new StartupThread();
                                        mThread.start();
                                    }
                                })
                        .setNegativeButton(R.string.button_exit,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        finish();
                                    }
                                }).create();

                ErrorDialogFragment fragment = new ErrorDialogFragment();
                fragment.setDialog(dialog);
                if (!isFinishing()) {
                    fragment.showAllowingStateLoss(getSupportFragmentManager(), "retry_dialog");
                }
            }
        });
    }

}
