package io.syncport.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.Device;
import io.syncport.ui.directory.DirectoryFragment;
import io.syncport.ui.directory.DocumentCollectionFragment;
import io.syncport.ui.directory.ImageCollectionFragment;
import io.syncport.ui.directory.MusicCollectionFragment;
import io.syncport.ui.directory.VideoCollectionFragment;
import io.syncport.ui.transfer.FileTransferTaskListFragment;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;

@ContentView(R.layout.activity_navi_drawer)
public class NavigationDrawerActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallback {

    public static Intent createIntent(Context context) {
        return createIntent(context, -1);
    }

    public static Intent createIntent(Context context, int menu) {
        Intent intent = new Intent(context, NavigationDrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(PARAM_MENU, menu);
        return intent;
    }

    public static Intent createIntentWithAuthCheck(Context context, int position) {
        return StartupActivity.createIntent(context,
                createIntent(context, position));
    }

    private static final String PARAM_MENU = SyncportKeyBuilder.build("menu");
    private static final String PARAM_TITLE = SyncportKeyBuilder.build("title");
    private static final String TAG_CONTENT_FRAGMENT = "content_fragment";

    private FragmentManager mFragmentManager;
    private NavigationDrawerFragment mDrawerFragment;
    private Fragment mContentFragment;
    private CharSequence mTitle;
    private Device mSelectedDevice;
    private int mSelectedMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        mFragmentManager = getSupportFragmentManager();
        mDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        if (savedInstanceState == null) {
            mTitle = ""; // getTitle();
            mSelectedMenu = getIntent().getIntExtra(PARAM_MENU, -1);

            // Set up the drawer.
            mDrawerFragment.setUp(R.id.navigation_drawer, R.id.drawer_layout,
                    mSelectedMenu);
        } else {
            mTitle = savedInstanceState.getCharSequence(PARAM_TITLE);
            mSelectedMenu = savedInstanceState.getInt(PARAM_MENU, -1);

            // Set up the drawer.
            mDrawerFragment.setUp(R.id.navigation_drawer, R.id.drawer_layout,
                    mSelectedMenu);
            mDrawerFragment.initDrawer();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mSelectedMenu = intent.getIntExtra(PARAM_MENU, -1);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PARAM_MENU, mSelectedMenu);
        outState.putCharSequence(PARAM_TITLE, mTitle);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT < 11) {
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mSignoutReceiver, new IntentFilter(SyncportAccount
                            .ACTION_SIGNOUT_ASYNC));
        }
    }

    @Override
    protected void onDestroy() {
        if (Build.VERSION.SDK_INT < 11) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    mSignoutReceiver);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        boolean handled = false;
        if (mContentFragment != null && mContentFragment instanceof
                BaseFragment) {
            handled = ((BaseFragment) mContentFragment).onBackPressed();
        }
        if (!handled) {
            super.onBackPressed();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int menuType, Device device) {
        if (mSelectedMenu == menuType && mSelectedDevice == device) {
            return;
        }
        mSelectedMenu = menuType;
        mSelectedDevice = device;

        Fragment f = null;
        boolean useFadeAnim = false;
        int titleResId = -1;

        String deviceId = null;
        if (device != null) {
            deviceId = device.getId();
        }

        switch (menuType) {
            case NavigationDrawerFragment.MenuType.ACCOUNT_SETTING:
                f = AccountSettingFragment.getInstance();
                titleResId = R.string.title_setting;
                break;
            case NavigationDrawerFragment.MenuType.DEVICE_SETTING:
                f = DeviceSettingFragment.getInstance(deviceId);
                titleResId = R.string.title_device_settings;
                break;
            case NavigationDrawerFragment.MenuType.DOCUMENT:
                f = DocumentCollectionFragment.getInstance(deviceId);
                titleResId = R.string.title_documents;
                break;
            case NavigationDrawerFragment.MenuType.FILE_BROWSER:
                f = DirectoryFragment.getInstance(deviceId);
                titleResId = R.string.title_filesystem;
                break;
            case NavigationDrawerFragment.MenuType.FILE_TRANSFER:
                f = FileTransferTaskListFragment.getInstance();
                titleResId = R.string.title_file_transfer_tasks;
                break;
            case NavigationDrawerFragment.MenuType.MUSIC:
                f = MusicCollectionFragment.getInstance(deviceId);
                titleResId = R.string.title_musics;
                break;
            case NavigationDrawerFragment.MenuType.PICTURE:
                f = ImageCollectionFragment.getInstance(deviceId);
                titleResId = R.string.title_pictures;
                break;
            case NavigationDrawerFragment.MenuType.VIDEO:
                f = VideoCollectionFragment.getInstance(deviceId);
                titleResId = R.string.title_videos;
                break;
            case NavigationDrawerFragment.MenuType.NO_DEVICE:
                f = NoDeviceFragment.getInstance();
                useFadeAnim = true;
                mTitle = "";
                break;
        }

        if (f != null) {
            mContentFragment = f;
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            if (useFadeAnim) {
                ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            }
            ft.replace(R.id.container, f, TAG_CONTENT_FRAGMENT).commit();
            if (titleResId > -1) {
                mTitle = getString(titleResId);
            }
        }
        else {
            // handle for errors (or throw exception)
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    /**
     * Compatibility methods
     */
    private BroadcastReceiver mSignoutReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }

    };

}
