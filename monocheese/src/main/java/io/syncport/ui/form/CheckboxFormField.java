package io.syncport.ui.form;

import io.syncport.R;
import android.content.Context;
import android.widget.CheckBox;

public class CheckboxFormField implements FormFieldable {
    private String mName;
    private CheckBox mView;
    private boolean mIsRequired;

    public CheckboxFormField(String name, CheckBox view) {
        this(name, view, false);
    }

    public CheckboxFormField(String name, CheckBox view, boolean isRequired) {
        mName = name;
        mView = view;
        mIsRequired = isRequired;
    }

    @Override
    public boolean validate(Context context) {
        if (mIsRequired && !mView.isChecked()) {
            setError(context, context.getString(R.string.error_value_required));
            return false;
        }
        return true;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public void setError(Context context, String error) {
        mView.setError(error);
    }

    @Override
    public void clearError() {
        mView.setError(null);
    }

    @Override
    public Object getValue() {
        return String.valueOf(mView.isChecked());
    }

}
