package io.syncport.ui.form;

import io.syncport.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RadioFormField implements FormFieldable {
    private String mName;
    private boolean mIsRequired;
    private RadioGroup mGroup;
    private List<RadioButton> mRadioButtons;
    private String[] mValues;

    public RadioFormField(String name, RadioGroup radioGroup, boolean isRequired) {
        this(name, radioGroup, (String[]) null, isRequired);
    }

    public RadioFormField(String name, RadioGroup radioGroup,
            Collection<String> groupValues, boolean isRequired) {
        this(name, radioGroup, groupValues == null ? null : groupValues
                .toArray(new String[groupValues.size()]), isRequired);
    }

    public RadioFormField(String name, RadioGroup radioGroup,
            String[] groupValues, boolean isRequired) {
        mName = name;
        mGroup = radioGroup;
        mIsRequired = isRequired;
        mRadioButtons = new ArrayList<RadioButton>();
        mValues = groupValues;
        int childCount = mGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            mRadioButtons.add((RadioButton) mGroup.getChildAt(i));
        }

    }

    @Override
    public boolean validate(Context context) {
        boolean isValid = !mIsRequired
                || mGroup.getCheckedRadioButtonId() != -1;
        if (!isValid) {
            setError(context, context.getString(R.string.error_value_required));
        } else {

        }
        return isValid;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public void setError(Context context, String error) {
        for (RadioButton btn : mRadioButtons) {
            btn.setError(error);
        }
    }

    @Override
    public void clearError() {
        for (RadioButton btn : mRadioButtons) {
            btn.setError(null);
        }
    }

    @Override
    public Object getValue() {
        RadioButton selectedBtn = (RadioButton) mGroup.findViewById(mGroup
                .getCheckedRadioButtonId());
        int position = mRadioButtons.indexOf(selectedBtn);
        return mValues == null ? String.valueOf(position) : mValues[position];
    }
}
