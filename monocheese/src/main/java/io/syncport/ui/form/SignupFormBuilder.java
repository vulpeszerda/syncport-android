package io.syncport.ui.form;

import io.syncport.ui.form.TextFormField.FieldType;
import io.syncport.ui.form.TextFormField.Options;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.TextView;

public class SignupFormBuilder {
    public static final String FIELD_NAME_EMAIL = "email";
    public static final String FIELD_NAME_PASSWD = "password";
    public static final String FIELD_NAME_FIRSTNAME = "first_name";
    public static final String FIELD_NAME_LASTNAME = "last_name";

    private Context mContext;
    private List<FormFieldable> mFields;

    public SignupFormBuilder(Context context) {
        mContext = context;
        mFields = new ArrayList<FormFieldable>();
    }

    public SignupFormBuilder setEmailField(TextView view) {
        Options options = new Options.Builder().setRequired()
                .setFieldType(FieldType.EMAIL_FIELD).build();
        mFields.add(new TextFormField(FIELD_NAME_EMAIL, view, options));
        return this;
    }

    public SignupFormBuilder setPasswdField(TextView view) {
        Options options = new Options.Builder().setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_PASSWD, view, options));
        return this;
    }

    public SignupFormBuilder setFirstNameField(TextView view) {
        Options options = new Options.Builder().setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_FIRSTNAME, view, options));
        return this;
    }

    public SignupFormBuilder setLastNameField(TextView view) {
        Options options = new Options.Builder().setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_LASTNAME, view, options));
        return this;
    }
    
    public Form build() {
        return new Form(mContext, mFields);
    }
}
