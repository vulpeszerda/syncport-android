package io.syncport.ui.form;

import android.content.Context;

public interface FormFieldable {
    public boolean validate(Context context);

    public String getName();
    public Object getValue();
    public void setError(Context context, String error);
    public void clearError();

}
