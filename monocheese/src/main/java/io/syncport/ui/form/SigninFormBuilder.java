package io.syncport.ui.form;

import io.syncport.ui.form.TextFormField.FieldType;
import io.syncport.ui.form.TextFormField.Options;
import io.syncport.utils.DeviceUuidFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.widget.TextView;

public class SigninFormBuilder {
    public static final String FIELD_NAME_EMAIL = "email";
    public static final String FIELD_NAME_PASSWD = "password";
    public static final String FIELD_NAME_DEVICE_NAME = "device_name";
    public static final String FIELD_NAME_DEVICE_TYPE = "device_type";
    public static final String FIELD_NAME_DEVICE_KEY = "device_key";

    private Context mContext;
    private List<FormFieldable> mFields;
    private Map<String, Object> mExtraFields;

    public SigninFormBuilder(Context context) {
        mContext = context;
        mFields = new ArrayList<FormFieldable>();
        mExtraFields = new HashMap<String, Object>();

        String deviceType = "ANDROID";
        String deviceName = android.os.Build.MANUFACTURER + "_"
                + android.os.Build.PRODUCT;
        String deviceKey = new DeviceUuidFactory(context).getDeviceUuid()
                .toString();

        mExtraFields.put(FIELD_NAME_DEVICE_TYPE, deviceType);
        mExtraFields.put(FIELD_NAME_DEVICE_NAME, deviceName);
        mExtraFields.put(FIELD_NAME_DEVICE_KEY, deviceKey);
    }

    public SigninFormBuilder setEmailField(TextView view) {
        Options options = new Options.Builder().setRequired()
                .setFieldType(FieldType.EMAIL_FIELD).build();
        mFields.add(new TextFormField(FIELD_NAME_EMAIL, view, options));
        return this;
    }

    public SigninFormBuilder setEmailValue(String value) {
        mExtraFields.put(FIELD_NAME_EMAIL, value);
        return this;
    }

    public SigninFormBuilder setPasswdField(TextView view) {
        Options options = new Options.Builder().setRequired().build();
        mFields.add(new TextFormField(FIELD_NAME_PASSWD, view, options));
        return this;
    }

    public SigninFormBuilder setPasswdValue(String value) {
        mExtraFields.put(FIELD_NAME_PASSWD, value);
        return this;
    }

    public Form build() {
        return new Form(mContext, mFields, mExtraFields);
    }
}
