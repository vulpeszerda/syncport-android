package io.syncport.ui;

import java.util.ArrayList;
import java.util.List;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.ui.NavigationDrawerFragment.MenuType;

public class NavigationItem {

    public static List<NavigationItem> getNavigationItems() {
        return new ItemBuilder()
                .addItem(R.drawable.ic_nav_picture, R.string.title_pictures, MenuType.PICTURE)
            .addItem(R.drawable.ic_nav_music, R.string.title_musics, MenuType.MUSIC)
            .addItem(R.drawable.ic_nav_video, R.string.title_videos, MenuType.VIDEO)
            .addItem(R.drawable.ic_nav_document, R.string.title_documents, MenuType.DOCUMENT)
                .addItem(R.drawable.ic_nav_file, R.string.title_filesystem, MenuType.FILE_BROWSER)
            .addSection()
            .addItem(R.drawable.ic_nav_transfer, R.string.title_transfer, MenuType.FILE_TRANSFER)
            .addItem(R.drawable.ic_nav_device_setting, R.string.title_device_settings, MenuType.DEVICE_SETTING)
            .addSection()
            .addItem(R.drawable.ic_nav_account, R.string.title_setting, MenuType.ACCOUNT_SETTING, true)
            .build();
    }

    public static List<NavigationItem> getNoDeviceNavigationItems() {
        return new ItemBuilder()
            .addItem(R.drawable.ic_nav_file, R.string.title_no_device,
                    MenuType.NO_DEVICE)
            .addItem(R.drawable.ic_nav_account, R.string.title_setting, MenuType.ACCOUNT_SETTING, true)
            .build();
    }

    private Integer mImageResId;
    private Integer mTextResId;
    private Integer mMenuType;
    private boolean mIsSection;
    private boolean mIsNonSelectable;

    private NavigationItem(Integer imageResId, Integer textResId, int type) {
        this(imageResId, textResId, type, false);
    }
    private NavigationItem(Integer imageResId, Integer textResId, int type,
                           boolean isNonSelectable) {
        mImageResId = imageResId;
        mTextResId = textResId;
        mMenuType = type;
        mIsSection = false;
        mIsNonSelectable = isNonSelectable;
    }

    private NavigationItem() {
        mIsSection = true;
    }

    public int getImageResId() {
        return mImageResId;
    }

    public int getTextResId() {
        return mTextResId;
    }

    public int getMenuType() {
        return mMenuType;
    }

    public boolean isSection() {
        return mIsSection;
    }

    public boolean isIsSelectable() {
        return !mIsNonSelectable;
    }

    public static class ItemBuilder {

        private List<NavigationItem> mItems;

        public ItemBuilder() {
            mItems = new ArrayList<>();
        }

        public List<NavigationItem> build() {
            // TODO check a menu added twice or more, then throw error
            return mItems;
        }

        public ItemBuilder addItem(int imgResId, int textResId, int type) {
            mItems.add(new NavigationItem(imgResId, textResId, type));
            return this;
        }

        public ItemBuilder addItem(int imgResId, int textResId, int type,
                                   boolean isSelectable) {
            mItems.add(new NavigationItem(imgResId, textResId, type,
                    isSelectable));
            return this;
        }

        public ItemBuilder addSection() {
            mItems.add(new NavigationItem());
            return this;
        }
    }
}
