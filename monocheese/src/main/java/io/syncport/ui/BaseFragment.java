package io.syncport.ui;

import roboguice.fragment.RoboFragment;

public class BaseFragment extends RoboFragment {

    protected BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }

    public boolean onBackPressed() {
        return false;
    }
}
