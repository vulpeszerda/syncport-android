package io.syncport.ui;

import android.accounts.AuthenticatorException;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.ext.LruDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.syncport.core.RootService;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.remote.ApiResources;
import io.syncport.utils.Log;

public class SyncportApplication extends Application implements DeviceManager.Observer{

    private static final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;
    // send CPR every 5 min
    private static final long CPR_INTERVAL = 1 * 60 * 1000;
    private static final String TAG = "SyncportApplication";

    private ImageLoader mImageLoader;

    private boolean mWasAppInBackground = true;
    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;

    private Timer mCPRTimer;
    private TimerTask mCPRTimerTask;

    private DeviceManager mDeviceManager;
    private Thread.UncaughtExceptionHandler mDefaultExceptionHandler;

    private Handler mExceptionHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Thread.setDefaultUncaughtExceptionHandler
                    (mDefaultExceptionHandler);
            throw new RuntimeException((Throwable) msg.obj);
        }
    };


    private BroadcastReceiver mAuthReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SyncportAccount.ACTION_SIGNIN)) {
                registerCPRTimer();
                try {
                    mDeviceManager = DeviceManager.from(context);
                    mDeviceManager.addObserver(SyncportApplication.this);
                } catch (AuthenticatorException e) {
                    // not reach
                }
            } else {
                sendRCPR();
                unregisterCPRTimer();
                if (mDeviceManager != null) {
                    mDeviceManager.removeObserver(SyncportApplication.this);
                }
            }
        }
    };


    public SyncportApplication() {
        mDefaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));
                Log.exception(thread.getName(), errors.toString());
                mExceptionHandler.obtainMessage(0, ex).sendToTarget();
            }
        });
    }



    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter(SyncportAccount.ACTION_SIGNOUT_ASYNC);
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        if (account == null) {
            filter.addAction(SyncportAccount.ACTION_SIGNOUT_ASYNC);
        }
        registerReceiver(mAuthReceiver, filter);

        if (account != null) {
            try {
                mDeviceManager = DeviceManager.from(this);
                mDeviceManager.addObserver(SyncportApplication.this);
            } catch (AuthenticatorException e) {
                // not reach
            }
        }
    }

    private ImageLoader createDefaultImageLoader() {
        File cacheDir;
        cacheDir = new File(android.os.Environment
                .getExternalStorageDirectory(),"thumbCache");
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        DisplayImageOptions imageLoaderOptions = new DisplayImageOptions.Builder()
//              .displayer(new FadeInBitmapDisplayer(500))
                .cacheInMemory(true)
                .resetViewBeforeLoading(true).build();

        ImageLoaderConfiguration.Builder configBuilder =
                new ImageLoaderConfiguration.Builder(this)
                        .defaultDisplayImageOptions(imageLoaderOptions)
                        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                        .tasksProcessingOrder(QueueProcessingType.LIFO);

        DiskCache diskCache;
        try {
            diskCache = new LruDiscCache(cacheDir,
                    new HashCodeFileNameGenerator(), 50 * 1024 * 1024);
            configBuilder.diskCache(diskCache);
        } catch (IOException e) {
        }

        ImageLoaderConfiguration config = configBuilder.build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        return imageLoader;
    }

    public ImageLoader getDefaultImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = createDefaultImageLoader();
        }
        return mImageLoader;
    }

    public void startActivityTransitionTimer() {
        mActivityTransitionTimer = new Timer();
        mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                if (!mWasAppInBackground) {
                    synchronized (this) {
                        if (!mWasAppInBackground) {
                            SyncportApplication.this.mWasAppInBackground = true;
                            onAppBecomeBackground();
                        }
                    }
                }
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        if (mWasAppInBackground) {
            synchronized (this) {
                if (mWasAppInBackground) {
                    mWasAppInBackground = false;
                    onAppBecomeForeground();
                }
            }
        }
    }

    public synchronized boolean wasAppInBackground() {
        return mWasAppInBackground;
    }

    // Send rcpr to all wakable devices,
    // and release cpr timer
    private void onAppBecomeBackground() {
        Log.d(TAG, "app is background");
        unregisterCPRTimer();
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        if (account == null) {
            return;
        }
        sendRCPR();
    }

    // Send cpr to all wakable devices
    private void onAppBecomeForeground() {
        Log.d(TAG, "app is foreground");
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        if (account == null) {
            return;
        }
        registerCPRTimer();
    }

    private void registerCPRTimer() {
        unregisterCPRTimer();

        mCPRTimer = new Timer();
        mCPRTimerTask = new TimerTask() {
            @Override
            public void run() {
                sendCPR();
            }
        };
        mCPRTimer.schedule(mCPRTimerTask, 0, CPR_INTERVAL);
    }

    private void unregisterCPRTimer() {
        if (mCPRTimer != null) {
            mCPRTimer.cancel();
        }
        if (mCPRTimerTask != null) {
            mCPRTimerTask.cancel();
        }
    }

    private void sendCPR() {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        if (account == null) {
            return;
        }
        final List<String> deviceIds = new ArrayList<>();
        final List<String> deviceNames = new ArrayList<>();

        final Response.Listener<JSONObject> listener = new Response
                .Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "sent cpr to devices[" + TextUtils.join
                        (",", deviceNames) +
                        "]:" + response.toString());
                // do nothing
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener
                () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getStackTrace().toString());
                // do nothing
            }
        };
        try {
            DeviceManager manager = DeviceManager.from(this);
            manager.getAllDevices(false, new DeviceManager.OnGetDevicesListener() {

                @Override
                public void onGetDevices(List<Device> devices) {
                    for (Device device : devices) {
                        if (!device.isMyDevice() && device.getType().equals
                                (Device.Type.ANDROID)) {
                            deviceIds.add(device.getId());
                            deviceNames.add(device.getName());
                        }
                    }
                    if (deviceIds.size() > 0) {
                        ApiResources.requestCpr(
                                SyncportApplication.this,
                                deviceIds,
                                listener, errorListener);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    // do nothing
                }
            });
        } catch (AuthenticatorException e) {
            // do nothing
        }

    }


    private void sendCPR(final Device device) {
        final Response.Listener<JSONObject> listener = new Response
                .Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "sent cpr to devices[" + device.getName() +
                        "]:" + response.toString());
                // do nothing
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener
                () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getStackTrace().toString());
                // do nothing
            }
        };

        List<String> deviceIds = new ArrayList<>();
        deviceIds.add(device.getId());
        ApiResources.requestCpr(SyncportApplication.this, deviceIds,
                listener, errorListener);
    }

    private void sendRCPR() {
        SyncportAccount account = SyncportAccount.getAuthenticatedUser(this);
        if (account == null) {
            return;
        }

        final List<String> deviceIds = new ArrayList<>();
        final List<String> deviceNames = new ArrayList<>();

        final Response.Listener<JSONObject> listener = new Response
                .Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "sent rcpr to devices[" + TextUtils.join
                        (",", deviceNames) +
                        "]:" + response.toString());
                // do nothing
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener
                () {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.getStackTrace().toString());
                // do nothing
            }
        };

        try {
            DeviceManager manager = DeviceManager.from(this);
            manager.getAllDevices(false, new DeviceManager.OnGetDevicesListener() {
                @Override
                public void onGetDevices(List<Device> devices) {
                    for (Device device : devices) {
                        if (!device.isMyDevice() && device.getType().equals
                                (Device.Type.ANDROID) &&
                                device.isTurnOn()) {
                            deviceIds.add(device.getId());
                            deviceNames.add(device.getName());
                        }
                    }
                    if (deviceIds.size() > 0) {
                        ApiResources.requestRcpr(
                                SyncportApplication.this,
                                deviceIds, listener,
                                errorListener);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    // do nothing
                }
            });
        } catch (AuthenticatorException e) {
            // do nothing
        }
    }

    @Override
    public void onDeviceNameChanged(Device device, String name) {

    }

    @Override
    public void onDeviceTurnOff(Device device) {
        RootService.expireWatcher(this, device.getId());
    }

    @Override
    public void onDeviceTurnOn(final Device device) {
        if (!device.isMyDevice() && device.isTurnOn() &&
                device.getType().equals(Device.Type.ANDROID) &&
                !mWasAppInBackground) {
            registerCPRTimer();
        }
    }

    @Override
    public void onNewDeviceAdded(final Device device) {
        if (!device.isMyDevice() && device.isTurnOn() &&
                device.getType().equals(Device.Type.ANDROID) &&
            !mWasAppInBackground) {
            registerCPRTimer();
        }
    }
}
