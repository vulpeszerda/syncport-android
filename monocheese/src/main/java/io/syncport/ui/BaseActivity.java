package io.syncport.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import io.syncport.R;
import roboguice.activity.RoboActionBarActivity;

public class BaseActivity extends RoboActionBarActivity {

    private boolean mIsAlive;
    private Toolbar mToolbar;

    public boolean isAlive() {
        return mIsAlive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setElevation(0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mIsAlive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsAlive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SyncportApplication app = (SyncportApplication)getApplication();
        app.stopActivityTransitionTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SyncportApplication app = (SyncportApplication)getApplication();
        app.startActivityTransitionTimer();
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

}
