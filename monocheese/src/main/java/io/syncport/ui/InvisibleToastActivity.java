package io.syncport.ui;

import io.syncport.utils.SyncportKeyBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class InvisibleToastActivity extends BaseActivity {

    public static Intent createIntent(Context context, String message) {
        Intent intent = new Intent(context, InvisibleToastActivity.class);
        intent.putExtra(KEY_MESSAGE, message);
        return intent;
    }

    public static Intent createIntent(Context context, int resId) {
        Intent intent = new Intent(context, InvisibleToastActivity.class);
        intent.putExtra(KEY_MESSAGE_RES_ID, resId);
        return intent;
    }

    private static final String KEY_MESSAGE = SyncportKeyBuilder.build("message");
    private static final String KEY_MESSAGE_RES_ID = SyncportKeyBuilder
            .build("message_res_id");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String message = getIntent().getStringExtra(KEY_MESSAGE);
        if (message == null) {
            int resId = getIntent().getIntExtra(KEY_MESSAGE_RES_ID, -1);
            if (resId > 0) {
                message = getString(resId);
            }
        }
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
