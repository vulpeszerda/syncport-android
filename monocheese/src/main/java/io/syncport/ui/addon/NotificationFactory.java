package io.syncport.ui.addon;

import io.syncport.R;
import io.syncport.ui.StartupActivity;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

public class NotificationFactory {

    public static final int REQUEST_CODE_DEFAULT = 0;
    public static final int REQUEST_CODE_NOTI = 1;
    public static final int REQUEST_CODE_TRANSFER = 2;

    public static final int NOTI_ID_FOREGROUND = 1;

    private static final int NOTI_ID_MIN_TYPE_NOTI = 10;
    private static final int NOTI_ID_MIN_TYPE_TRANSFER = 100000;

    private static final int NOTI_ID_MAX_TYPE_NOTI = NOTI_ID_MIN_TYPE_TRANSFER - 1;
    private static final int NOTI_ID_MAX_TYPE_TRANSFER = 200000;

    private static int nextNotiId = NOTI_ID_MIN_TYPE_NOTI;
    private static int nextTransferId = NOTI_ID_MIN_TYPE_TRANSFER;

    @SuppressLint("NewApi")
    public static Notification createForegroundNoti(Context context) {
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                StartupActivity.createIntent(context), 0);

        Notification noti;
        noti = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setAutoCancel(true).setSmallIcon(R.drawable.ic_stat)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent).build();
        noti.flags |= Notification.FLAG_FOREGROUND_SERVICE;
        return noti;
    }

    public static synchronized int getNewTransferId() {
        if (nextTransferId == NOTI_ID_MAX_TYPE_TRANSFER - 1) {
            nextTransferId = NOTI_ID_MIN_TYPE_TRANSFER;
        }
        return nextTransferId++;
    }

    public static synchronized int getNewNotiId() {
        if (nextNotiId == NOTI_ID_MAX_TYPE_NOTI - 1) {
            nextNotiId = NOTI_ID_MIN_TYPE_NOTI;
        }
        return nextNotiId++;
    }

    public static void cancelAllTypeNoti(Context context) {
        NotificationManagerCompat manager = NotificationManagerCompat
                .from(context);
        for (int i = NOTI_ID_MIN_TYPE_NOTI; i < nextNotiId; i++) {
            manager.cancel(i);
        }
    }

    public static void cancelAllTypeTransfer(Context context) {
        NotificationManagerCompat manager = NotificationManagerCompat
                .from(context);
        for (int i = NOTI_ID_MIN_TYPE_TRANSFER; i < nextTransferId; i++) {
            manager.cancel(i);
        }
    }

    public static NotificationCompat.Builder createDefaultNotiBuilder(
            Context context, int requestCode, Intent intent, String title,
            String text, String ticker, boolean useSoundAndVibrate) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context).setContentTitle(title).setContentText(text)
                .setAutoCancel(true).setSmallIcon(R.drawable.ic_stat);
        if (intent != null) {
            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                    requestCode, intent, 0);
            builder.setContentIntent(pendingIntent);
        }
        if (ticker != null) {
            builder.setTicker(ticker);
        }
        if (useSoundAndVibrate) {
            builder.setDefaults(NotificationCompat.DEFAULT_SOUND);
        }
        return builder;
    }
}
