package io.syncport.ui.account;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.remote.ApiResources;
import io.syncport.ui.BaseAccountAuthenticatorActivity;
import io.syncport.ui.addon.ProgressDialogFragment;
import io.syncport.utils.SyncportKeyBuilder;

import java.util.concurrent.ExecutionException;

import org.json.JSONObject;

import roboguice.fragment.RoboFragment;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.daftshady.superandroidkit.authentication.AccountConstants;
import com.daftshady.superandroidkit.authentication.AccountUtils;
import com.google.inject.Inject;


@ContentView(R.layout.activity_empty)
public class AuthActivity extends BaseAccountAuthenticatorActivity implements
        OnBackStackChangedListener {

    private static SyncportKeyBuilder keyBuilder = SyncportKeyBuilder
            .getInstance("auth");
    public static final String PARAM_EMAIL = keyBuilder.getKey("email");
    public static final String PARAM_PASSWD = keyBuilder.getKey("passwd");
    public static final String PARAM_ACCOUNT_TYPE = keyBuilder
            .getKey("account_type");
    public static final String PARAM_TOKEN_TYPE = keyBuilder
            .getKey("token_type");
    public static final String PARAM_TOKEN = keyBuilder.getKey("token");

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, AuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    public static Intent generateSigninData(String email, String passwd,
            String token) {
        Intent intent = new Intent();
        intent.putExtra(PARAM_EMAIL, email);
        intent.putExtra(PARAM_PASSWD, passwd);
        intent.putExtra(PARAM_TOKEN, token);
        intent.putExtra(PARAM_ACCOUNT_TYPE, SyncportAccount.ACCOUNT_TYPE);
        intent.putExtra(PARAM_TOKEN_TYPE, SyncportAccount.TOKEN_TYPE);
        return intent;
    }

    private static final String TAG_PROGRESS_DIALOG__DEVICE_INFO = "device_info";

    private @Inject
    AuthFragment mAuthFragment;
    private FinishSigninThread mFinishSigninThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager manager = getSupportFragmentManager();
        manager.addOnBackStackChangedListener(this);
        if (savedInstanceState == null) {
            manager.beginTransaction()
                    .replace(R.id.container, mAuthFragment).commit();
        }

        actionbarConfigure();
    }

    @Override
    public void onBackStackChanged() {
        actionbarConfigure();
    }

    public void actionbarConfigure() {
        boolean isRootFragment = getSupportFragmentManager()
                .getBackStackEntryCount() == 0;
        ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeButtonEnabled(!isRootFragment);
        actionbar.setDisplayHomeAsUpEnabled(!isRootFragment);
        if (isRootFragment) {
            actionbar.hide();
        } else {
            actionbar.show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFinishSigninFailureDialog(final Intent intent, String msg) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_signin_failure)
                .setMessage(msg)
                .setNegativeButton(R.string.button_confirm, null)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                finishSignin(intent);
                            }
                        }).show();
    }

    synchronized void finishSignin(final Intent intent) {
        final Handler handler = new Handler();
        if (mFinishSigninThread != null && mFinishSigninThread.isAlive()) {
            mFinishSigninThread.interrupt();
        }
        mFinishSigninThread = new FinishSigninThread(intent, handler);
        mFinishSigninThread.start();
    }

    private class FinishSigninThread extends Thread {

        private Intent mIntent;
        private Handler mHandler;

        public FinishSigninThread(Intent intent, Handler handler) {
            mHandler = handler;
            mIntent = intent;
        }

        @Override
        public synchronized void start() {
            ProgressDialogFragment.show(AuthActivity.this,
                    TAG_PROGRESS_DIALOG__DEVICE_INFO, R.string.title_fetching_device_info,
                    false);
            super.start();
        }

        @Override
        public void run() {
            String userName = mIntent.getStringExtra(PARAM_EMAIL);
            String userPass = mIntent.getStringExtra(PARAM_PASSWD);
            String accountType = mIntent.getStringExtra(PARAM_ACCOUNT_TYPE);
            String tokenType = mIntent.getStringExtra(PARAM_TOKEN_TYPE);
            String token = mIntent.getStringExtra(PARAM_TOKEN);

            AccountManager manager = AccountManager.get(AuthActivity.this);

            Account account = new Account(userName, accountType);
            if (AccountUtils.isThereAccount(AuthActivity.this, accountType)) {
                AccountUtils.removeAllAccount(AuthActivity.this, accountType);
            }

            try {
                RequestFuture<JSONObject> future = RequestFuture.newFuture();
                ApiResources.getDeviceSelf(AuthActivity.this, token, future,
                        future);

                JSONObject response = future.get();

                String deviceName = response.optString("name");
                String deviceType = response.optString("type");
                String deviceId = response.optString("uid");

                Bundle userData = new Bundle();
                userData.putString(AccountConstants.ACCOUNT_SEQ_ID,
                        AccountUtils.getNextAccountSeqId(AuthActivity.this,
                                accountType));
                userData.putString(SyncportAccount.KEY_DEVICE_ID, deviceId);
                userData.putString(SyncportAccount.KEY_DEVICE_NAME, deviceName);
                userData.putString(SyncportAccount.KEY_DEVICE_TYPE, deviceType);

                manager.addAccountExplicitly(account, userPass, userData);
                manager.setAuthToken(account, tokenType, token);

                Intent result = new Intent();
                result.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName);
                result.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                result.putExtra(AccountManager.KEY_AUTHTOKEN, token);
                result.putExtra(AccountManager.KEY_PASSWORD, userPass);
                setAccountAuthenticatorResult(result.getExtras());
                setResult(RESULT_OK, mIntent);
                finish();

            } catch (ExecutionException e) {
                VolleyError error = (VolleyError) e.getCause();
                String msg = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null
                        && response.data.length > 0) {
                    msg = new String(response.data);
                }
                final String errorMsg = msg;
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        showFinishSigninFailureDialog(mIntent, errorMsg);
                    }

                });
            } catch (InterruptedException e) {
            } finally {

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        ProgressDialogFragment.hide(AuthActivity.this,
                                TAG_PROGRESS_DIALOG__DEVICE_INFO);
                    }
                });
            }
        }
    }

    public static class AuthFragment extends RoboFragment implements
            OnClickListener {
        private @InjectView(R.id.button_signup) Button mSignupBtn;
        private @InjectView(R.id.text_signin) TextView mSigninTextView;
        private @Inject SigninFragment mSigninFragment;
        private @Inject SignupFragment mSignupFragment;

        private FragmentManager mFragmentManager;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_auth, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            mSignupBtn.setOnClickListener(this);
            mSigninTextView.setOnClickListener(this);
            mFragmentManager = getActivity().getSupportFragmentManager();
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
            case R.id.button_signup:
                mFragmentManager.beginTransaction()
                        .replace(R.id.container, mSignupFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.text_signin:
                mFragmentManager.beginTransaction()
                        .replace(R.id.container, mSigninFragment)
                        .addToBackStack(null).commit();
                break;
            }
        }
    }
}
