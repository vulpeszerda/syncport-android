package io.syncport.ui.account;

import io.syncport.R;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RelayPaths;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.core.remote.SyncPaths;
import io.syncport.core.remote.SyncResouces;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.BaseFragment;
import io.syncport.ui.addon.ProgressDialogFragment;
import io.syncport.ui.form.Form;
import io.syncport.ui.form.SigninFormBuilder;
import io.syncport.ui.form.SignupFormBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

public class SignupFragment extends BaseFragment implements OnClickListener,
        DialogInterface.OnClickListener {

    private static final int REQUEST_CODE_TERM = 1;
    private static final String TAG_PROGRESS_DIALOG__SIGNIN = "signin";
    private static final String TAG_PROGRESS_DIALOG__SIGNUP = "signup";

    private @InjectView(R.id.input_email) EditText mInputEmailView;
    private @InjectView(R.id.input_fisrtname) EditText mInputFirstNameView;
    private @InjectView(R.id.input_lastname) EditText mInputLastNameView;
    private @InjectView(R.id.input_passwd) EditText mInputPasswdView;
    private @InjectView(R.id.button_signup) Button mSignupBtn;

    private AlertDialog mTermDialog;
    private AlertDialog mSignupErrorDialog;
    private AlertDialog mSigninErrorDialog;
    private CharSequence mPrevActionBarTitle;
    private Form mSigninForm;
    private Form mSignupForm;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mSignupBtn.setOnClickListener(this);

        mSignupForm = new SignupFormBuilder(getActivity())
                .setEmailField(mInputEmailView)
                .setPasswdField(mInputPasswdView)
                .setFirstNameField(mInputFirstNameView)
                .setLastNameField(mInputLastNameView).build();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        mPrevActionBarTitle = actionbar.getTitle();
        actionbar.setTitle(R.string.title_signup);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater
                .inflate(R.layout.fragment_auth_signup, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        actionbar.setTitle(mPrevActionBarTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_signup:
            if (mSignupForm.validate()) {
                //showTermDialog();
                requestSignup(mSignupForm.getParamsAsJson());
            }
            break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (dialog == mTermDialog) {
            if (which == DialogInterface.BUTTON_NEGATIVE) {
                startActivityForResult(
                        TermActivity.createIntent(getActivity(), true),
                        REQUEST_CODE_TERM);
            } else if (which == DialogInterface.BUTTON_POSITIVE) {
                requestSignup(mSignupForm.getParamsAsJson());
            }
        } else if (dialog == mSignupErrorDialog) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                requestSignup(mSignupForm.getParamsAsJson());
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNUP);
            }
        } else if (dialog == mSigninErrorDialog) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                requestSignin(mSigninForm.getParamsAsJson());
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
            }
        }
    }

    private void showTermDialog() {
        if (mTermDialog == null) {
            mTermDialog = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_terms)
                    .setMessage(R.string.desc_confirm_term_agree)
                    .setPositiveButton(R.string.button_agree, this)
                    .setNegativeButton(R.string.button_viewterms, this)
                    .create();
        }
        mTermDialog.show();
    }

    private void showSignupErrorDialog(String message) {
        mSignupErrorDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_signup_failure).setMessage(message)
                .setNegativeButton(R.string.button_confirm, this).create();
        mSignupErrorDialog.show();
    }

    private void showSigninErrorDialog(String message) {
        mSigninErrorDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_signin_failure).setMessage(message)
                .setNegativeButton(R.string.button_confirm, this)
                .setPositiveButton(R.string.button_retry, this).create();
        mSigninErrorDialog.show();
    }

    private void requestSignup(JSONObject param) {
        checkDebugModeAccount();
        Context context = getActivity();
        final RobustResponseErrorListener errorListener = new RobustResponseErrorListener(
                context) {
            @Override
            public void onErrorResponse(VolleyError error, String serverMsg) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNUP);
                showSignupErrorDialog(serverMsg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNUP);
            }
        };

        final RobustResponseListener<JSONObject> listener = new RobustResponseListener<JSONObject>(
                getBaseActivity(), this) {

            @Override
            public void onSecureResponse(JSONObject response) {
                if (!response.optBoolean("success")) {
                    String errorMsg = response.optString("error",
                            getString(R.string.error_undefined));
                    errorListener.onErrorResponse(null, errorMsg);
                    return;
                }
                String email = (String) mSignupForm
                        .getValue(SignupFormBuilder.FIELD_NAME_EMAIL);
                String passwd = (String) mSignupForm
                        .getValue(SignupFormBuilder.FIELD_NAME_PASSWD);
                mSigninForm = new SigninFormBuilder(getActivity())
                        .setEmailValue(email).setPasswdValue(passwd).build();
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNUP);
                requestSignin(mSigninForm.getParamsAsJson());

            }

            @Override
            public void onSkipped(JSONObject response) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNUP);
            }

        };

        ProgressDialogFragment.show(getBaseActivity(),
                TAG_PROGRESS_DIALOG__SIGNUP, R.string.desc_wait_signup);
        ApiResources.signup(context, param, listener, errorListener);
    }

    private void requestSignin(JSONObject param) {
        Context context = getActivity();
        final RobustResponseErrorListener errorListener = new RobustResponseErrorListener(
                context) {
            @Override
            public void onErrorResponse(VolleyError error, String serverMsg) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
                showSigninErrorDialog(serverMsg);
            }

            @Override
            public void onSkipped(VolleyError error) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
            }
        };
        final Response.Listener<JSONObject> listener = new RobustResponseListener<JSONObject>(
                getBaseActivity(), this) {

            @Override
            public void onSecureResponse(JSONObject response) {
                if (!response.optBoolean("success")) {
                    ProgressDialogFragment.hide(getBaseActivity(),
                            TAG_PROGRESS_DIALOG__SIGNIN);
                    String errorMsg = response.optString("error",
                            getString(R.string.error_undefined));
                    errorListener.onErrorResponse(null, errorMsg);
                    return;
                }
                try {
                    String token = response.getString("session_key");
                    String email = (String) mSigninForm
                            .getValue(SigninFormBuilder.FIELD_NAME_EMAIL);
                    String passwd = (String) mSigninForm
                            .getValue(SigninFormBuilder.FIELD_NAME_PASSWD);
                    Intent intent = AuthActivity.generateSigninData(email,
                            passwd, token);
                    ((AuthActivity) getActivity()).finishSignin(intent);
                } catch (JSONException e) {
                    ProgressDialogFragment.hide(getBaseActivity(),
                            TAG_PROGRESS_DIALOG__SIGNIN);
                    e.printStackTrace();
                }
            }

            @Override
            public void onSkipped(JSONObject response) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
            }

        };

        ProgressDialogFragment.show(getBaseActivity(),
                TAG_PROGRESS_DIALOG__SIGNIN, R.string.desc_wait_signin);
        ApiResources.signin(context, null, param, listener, errorListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_TERM
                && resultCode == Activity.RESULT_OK) {
            requestSignup(mSignupForm.getParamsAsJson());
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkDebugModeAccount() {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = pref.edit();
        String key = getString(R.string.preference_key__debug);
        String thumnailKey = getString(R.string.preference_key__thumbnail_upload);

        boolean isDebugMode = pref.getBoolean(key, false);

        String email = mInputEmailView.getText().toString();
        if (email.endsWith("@test.com")) {
            Toast.makeText(getActivity(), "signin as debug mode",
                    Toast.LENGTH_SHORT).show();

            String name = email.split("@")[0];
            if (!name.endsWith(".th")) {
                editor.putBoolean(thumnailKey, false);
            } else {
                editor.putBoolean(thumnailKey, true);
            }
            editor.putBoolean(key, true);
            editor.commit();
            if (!isDebugMode) {
                ApiResources.resetPathBuilder();
                DeltaResources.resetPathBuilder();
                SyncResouces.resetPathBuilder();
                SyncPaths.clearCache();
                RelayPaths.clearCache();
            }

        } else {
            if (isDebugMode) {
                Toast.makeText(getActivity(), "signin as prod mode",
                        Toast.LENGTH_SHORT).show();
            }
            editor.putBoolean(thumnailKey, true);
            editor.putBoolean(key, false).commit();
            if (isDebugMode) {
                ApiResources.resetPathBuilder();
                DeltaResources.resetPathBuilder();
                SyncResouces.resetPathBuilder();
                SyncPaths.clearCache();
                RelayPaths.clearCache();
            }
        }
    }

}
