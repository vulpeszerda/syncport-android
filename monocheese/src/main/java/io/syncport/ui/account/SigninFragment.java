package io.syncport.ui.account;

import io.syncport.R;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RelayPaths;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.core.remote.SyncPaths;
import io.syncport.core.remote.SyncResouces;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.BaseFragment;
import io.syncport.ui.addon.ProgressDialogFragment;
import io.syncport.ui.form.Form;
import io.syncport.ui.form.SigninFormBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.inject.Inject;


@ContentView(R.layout.fragment_auth_signin)
public class SigninFragment extends BaseFragment implements OnClickListener,
        DialogInterface.OnClickListener {

    private static final String TAG = "SigninFragment";
    private static final String TAG_PROGRESS_DIALOG__SIGNIN = "signin";

    private @InjectView(R.id.input_email) EditText mEmailInputView;
    private @InjectView(R.id.input_passwd) EditText mPwdInputView;
    private @InjectView(R.id.button_signin) Button mSigninBtn;
    private @InjectView(R.id.text_forgotpasswd) TextView mForgotPasswdView;
    private @Inject
    ForgotPasswdFragment mForgotPasswdFragment;

    private Form mForm;
    private CharSequence mPrevActionBarTitle;

    private AlertDialog mSigninErrorDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSigninBtn.setOnClickListener(this);
        mForgotPasswdView.setOnClickListener(this);

        mForm = new SigninFormBuilder(getActivity())
                .setEmailField(mEmailInputView).setPasswdField(mPwdInputView)
                .build();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        mPrevActionBarTitle = actionbar.getTitle();
        actionbar.setTitle(R.string.title_signin);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater
                .inflate(R.layout.fragment_auth_signin, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        actionbar.setTitle(mPrevActionBarTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_signin:
            if (mForm.validate()) {
                checkDebugModeAccount();
                requestSignin(mForm.getParamsAsJson());
            }
            break;
        case R.id.text_forgotpasswd:
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, mForgotPasswdFragment)
                    .addToBackStack(null).commit();
            break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (dialog == mSigninErrorDialog
                && which == DialogInterface.BUTTON_POSITIVE) {
            requestSignin(mForm.getParamsAsJson());
            return;
        }
    }

    private void checkDebugModeAccount() {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = pref.edit();

        String key = getString(R.string.preference_key__debug);
        String thumnailKey = getString(R.string.preference_key__thumbnail_upload);

        boolean isDebugMode = pref.getBoolean(key, false);

        String email = mEmailInputView.getText().toString();
        if (email.endsWith("@test.com")) {
            Toast.makeText(getActivity(), "signin as debug mode",
                    Toast.LENGTH_SHORT).show();

            String name = email.split("@")[0];
            if (!name.endsWith(".th")) {
                editor.putBoolean(thumnailKey, false);
            } else {
                editor.putBoolean(thumnailKey, true);
            }
            editor.putBoolean(key, true).commit();
            if (!isDebugMode) {
                ApiResources.resetPathBuilder();
                DeltaResources.resetPathBuilder();
                SyncResouces.resetPathBuilder();
                SyncPaths.clearCache();
                RelayPaths.clearCache();
            }

        } else {
            editor.putBoolean(thumnailKey, true);
            editor.putBoolean(key, false).commit();
            if (isDebugMode) {
                Toast.makeText(getActivity(), "signin as prod mode",
                        Toast.LENGTH_SHORT).show();
                ApiResources.resetPathBuilder();
                DeltaResources.resetPathBuilder();
                SyncResouces.resetPathBuilder();
                SyncPaths.clearCache();
                RelayPaths.clearCache();
            }
        }
    }

    private void showSigninErrorDialog(String message) {
        mSigninErrorDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_signin_failure).setMessage(message)
                .setNegativeButton(R.string.button_confirm, this)
                .setPositiveButton(R.string.button_retry, this).create();
        mSigninErrorDialog.show();
    }

    private void requestSignin(JSONObject params) {
        Context context = getActivity();

        final RobustResponseErrorListener errorListener = new RobustResponseErrorListener(
                context) {

            @Override
            public void onErrorResponse(VolleyError error, String msg) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
                showSigninErrorDialog(msg);
                return;
            }

            @Override
            public void onSkipped(VolleyError error) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
            }
        };
        final Response.Listener<JSONObject> listener = new RobustResponseListener<JSONObject>(
                getBaseActivity(), this) {

            @Override
            public void onSecureResponse(JSONObject response) {
                if (!response.optBoolean("success")) {
                    ProgressDialogFragment.hide(getBaseActivity(),
                            TAG_PROGRESS_DIALOG__SIGNIN);
                    String errorMsg = response.optString("error",
                            getString(R.string.error_undefined));
                    errorListener.onErrorResponse(null, errorMsg);
                    return;
                }
                try {
                    String token = response.getString("session_key");
                    String email = (String) mForm
                            .getValue(SigninFormBuilder.FIELD_NAME_EMAIL);
                    String passwd = (String) mForm
                            .getValue(SigninFormBuilder.FIELD_NAME_PASSWD);
                    Intent intent = AuthActivity.generateSigninData(email,
                            passwd, token);
                    ((AuthActivity) getActivity()).finishSignin(intent);
                } catch (JSONException e) {
                    ProgressDialogFragment.hide(getBaseActivity(),
                            TAG_PROGRESS_DIALOG__SIGNIN);
                    e.printStackTrace();
                }
            }

            @Override
            public void onSkipped(JSONObject response) {
                ProgressDialogFragment.hide(getBaseActivity(),
                        TAG_PROGRESS_DIALOG__SIGNIN);
            }
        };

        ProgressDialogFragment.show(getBaseActivity(),
                TAG_PROGRESS_DIALOG__SIGNIN, R.string.desc_wait_signin);

        ApiResources.signin(context, TAG, params, listener, errorListener);
    }
}
