package io.syncport.ui.account;

import io.syncport.R;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.ui.BaseActivity;
import io.syncport.ui.BaseFragment;
import io.syncport.ui.form.TextFormField;
import io.syncport.ui.form.TextFormField.FieldType;

import org.json.JSONObject;

import roboguice.inject.InjectView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

public class ForgotPasswdFragment extends BaseFragment implements
        OnClickListener {

    private @InjectView(R.id.input_email) EditText mEmailInputView;
    private @InjectView(R.id.button_resetpasswd) Button mResetPwdBtn;

    private CharSequence mPrevActionBarTitle;
    private TextFormField mEmailValidator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mResetPwdBtn.setOnClickListener(this);

        TextFormField.Options options = new TextFormField.Options.Builder()
                .setRequired().setFieldType(FieldType.EMAIL_FIELD).build();
        mEmailValidator = new TextFormField("email", mEmailInputView, options);

        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        mPrevActionBarTitle = actionbar.getTitle();
        actionbar.setTitle(R.string.title_forgotpasswd);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auth_forgotpwd, container,
                false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ActionBar actionbar = ((BaseActivity) getActivity())
                .getSupportActionBar();
        actionbar.setTitle(mPrevActionBarTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_resetpasswd:
          requestResetPasswd();
            break;
        }
    }

    private void showEmailSentDialog() {
        String format = getString(R.string.desc_reset_password_email_sent__format);
        String message = String.format(format, mEmailInputView.getText()
                .toString());
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_reset_password_email_sent)
                .setMessage(message)
                .setPositiveButton(R.string.button_confirm,
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                }).show();
    }

    private void showErrorDialog(String msg) {
        if (msg == null) {
            msg = getString(R.string.desc_undefined_error);
        }
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_submit_failure)
                .setMessage(msg)
                .setPositiveButton(R.string.button_confirm,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }
                        }).show();
    }

    private void requestResetPasswd() {
        RobustResponseListener<JSONObject> responseListener =
                new RobustResponseListener<JSONObject>(getBaseActivity(),
                        this) {

            @Override
            public void onSkipped(JSONObject response) {
                mEmailInputView.setEnabled(true);
                mResetPwdBtn.setEnabled(true);
            }

            @Override
            public void onSecureResponse(JSONObject response) {
                mEmailInputView.setEnabled(true);
                mResetPwdBtn.setEnabled(true);
                if (response.optBoolean("success")) {
                    showEmailSentDialog();
                    return;
                }
                showErrorDialog(response.optString("msg"));
            }
        };

        RobustResponseErrorListener errorListener = new
                RobustResponseErrorListener(getBaseActivity(), this) {

                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mEmailInputView.setEnabled(true);
                        mResetPwdBtn.setEnabled(true);
                        showErrorDialog(msg);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {
                        mEmailInputView.setEnabled(true);
                        mResetPwdBtn.setEnabled(true);
                    }
                };

        if (mEmailValidator.validate(getActivity())) {
            mEmailInputView.setEnabled(false);
            mResetPwdBtn.setEnabled(false);
            ApiResources.findpasswd(getActivity(),
                    (String) mEmailValidator.getValue(),
                    responseListener, errorListener);
        }
    }

}
