package io.syncport.ui.account;

import io.syncport.R;
import io.syncport.core.remote.WebPaths;
import io.syncport.ui.BaseActivity;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;

@ContentView(R.layout.activity_term)
public class TermActivity extends BaseActivity implements OnClickListener {

    public static Intent createIntent(Context context, boolean requireResult) {
        Intent intent = new Intent(context, TermActivity.class);
        intent.putExtra(PARAM_REQUIRE_RESULT, requireResult);
        return intent;
    }

    private static final String PARAM_REQUIRE_RESULT = SyncportKeyBuilder
            .build("require_result");

    private @InjectView(R.id.term) WebView mWebView;
    private @InjectView(R.id.footer) View mFooter;
    private @InjectView(R.id.button_back) Button mBackBtn;
    private @InjectView(R.id.button_agree) Button mAgreeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            ActionBar actionbar = getSupportActionBar();
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeButtonEnabled(true);
        }
        mWebView.loadUrl(WebPaths.getTermPath(this));
        setResult(RESULT_CANCELED);
        if (getIntent().getBooleanExtra(PARAM_REQUIRE_RESULT, false)) {
            mFooter.setVisibility(View.VISIBLE);
            mBackBtn.setOnClickListener(this);
            mAgreeBtn.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_agree:
            setResult(RESULT_OK);
        case R.id.button_back:
            finish();
            break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
