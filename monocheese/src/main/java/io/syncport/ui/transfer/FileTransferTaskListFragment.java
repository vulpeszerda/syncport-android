package io.syncport.ui.transfer;

import io.syncport.R;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.BaseFileTask.State;
import io.syncport.core.transfer.FileTaskManager;
import io.syncport.ui.BaseFragment;
//import io.syncport.ui.MainActivity.TabContainer;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class FileTransferTaskListFragment extends BaseFragment implements
        FileTaskManager.Observer, OnItemClickListener {

    public static FileTransferTaskListFragment getInstance() {
        FileTransferTaskListFragment fragment = new FileTransferTaskListFragment();
        return fragment;
    }

    private @InjectView(R.id.listview) ListView mListView;
    private @InjectView(R.id.empty_view) View mEmptyView;

    private FileTransferTaskAdapter mAdapter;
    private FileTaskManager mTaskManager;
    private boolean mIsFirstLaunch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mIsFirstLaunch = true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mIsFirstLaunch) {
            mIsFirstLaunch = false;
            mAdapter = new FileTransferTaskAdapter(getActivity(), mListView);
            mTaskManager = FileTaskManager.getInstance(getActivity());
            mTaskManager.addObserver(this);
            mAdapter.addAll(mTaskManager.getAllTasks());
        }
        if (mAdapter.getCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_file_transfer_tasks,
                container, false);
    }

    @Override
    public void onDestroy() {
        mTaskManager.removeObserver(this);
        mAdapter.clear();
        super.onDestroy();
    }

    @Override
    public void onAddTask(String tag, BaseFileTask task) {
        mAdapter.add(task);
        mAdapter.notifyDataSetChanged();
        if (mEmptyView.getVisibility() == View.VISIBLE) {
            mEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRemoveTask(String tag, final BaseFileTask task) {
        mListView.postDelayed(new Runnable() {

            @Override
            public void run() {
                mAdapter.remove(task);
                mAdapter.notifyDataSetChanged();
                if (mAdapter.getCount() == 0) {
                    mEmptyView.setVisibility(View.VISIBLE);
                }
            }

        }, 2000);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        final BaseFileTask task = mAdapter.getItem(position);
        if (task.getState() == State.PREPARE
                || task.getState() == State.PROGRESS) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.title_cancel_transfer_task)
                    .setMessage(R.string.desc_are_you_sure_cancel_task)
                    .setNegativeButton(R.string.button_cancel, null)
                    .setPositiveButton(R.string.button_ok,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    task.cancel();
                                }
                            }).show();
        }
    }
}
