package io.syncport.ui.transfer;

import io.syncport.R;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.BaseFileTask.State;
import io.syncport.core.transfer.FileTaskManager;
import io.syncport.ui.BaseActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

@ContentView(R.layout.activity_file_transfer_tasks)
public class FileTransferTaskListActivity extends BaseActivity implements
        FileTaskManager.Observer, OnItemClickListener {

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, FileTransferTaskListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    private @InjectView(R.id.listview) ListView mListView;
    private @InjectView(R.id.empty_view) View mEmptyView;

    private FileTransferTaskAdapter mAdapter;
    private FileTaskManager mTaskManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeButtonEnabled(true);

        mAdapter = new FileTransferTaskAdapter(this, mListView);
        mListView.setAdapter(mAdapter);
        mTaskManager = FileTaskManager.getInstance(this);
        mTaskManager.addObserver(this);
        mAdapter.addAll(mTaskManager.getAllTasks());
        if (mAdapter.getCount() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        mListView.setOnItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mTaskManager.removeObserver(this);
        mAdapter.clear();
        super.onDestroy();
    }

    @Override
    public void onAddTask(String tag, BaseFileTask task) {
        mAdapter.add(task);
        mAdapter.notifyDataSetChanged();
        if (mEmptyView.getVisibility() == View.VISIBLE) {
            mEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRemoveTask(String tag, final BaseFileTask task) {
        mListView.postDelayed(new Runnable() {

            @Override
            public void run() {
                mAdapter.remove(task);
                mAdapter.notifyDataSetChanged();
                if (mAdapter.getCount() == 0) {
                    mEmptyView.setVisibility(View.VISIBLE);
                }
            }

        }, 2000);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        final BaseFileTask task = mAdapter.getItem(position);
        if (task.getState() == State.PREPARE
                || task.getState() == State.PROGRESS) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_cancel_transfer_task)
                    .setMessage(R.string.desc_are_you_sure_cancel_task)
                    .setNegativeButton(R.string.button_cancel, null)
                    .setPositiveButton(R.string.button_ok,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which) {
                                    task.cancel();
                                }
                            }).show();
        }
    }
}
