package io.syncport.ui.transfer;

import io.syncport.R;
import io.syncport.core.device.Device;
import io.syncport.core.device.DeviceManager;
import io.syncport.core.device.DeviceManager.OnGetDeviceListener;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.BaseFileTask.Progress;
import io.syncport.core.transfer.BaseFileTask.Result;
import io.syncport.core.transfer.BaseFileTask.TaskProgressListener;
import io.syncport.core.transfer.FileWebP2PReceiveTask;
import io.syncport.core.transfer.FileWebP2PSendTask;
import io.syncport.ui.addon.ArrayAdapter;
import io.syncport.utils.FileUtils;

import java.util.HashMap;
import java.util.Map;

import android.accounts.AuthenticatorException;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FileTransferTaskAdapter extends ArrayAdapter<BaseFileTask> {
    private static final int LAYOUT_ID = R.layout.item_transfer_task;

    private ListView mListView;
    private Map<String, TaskProgressListener> mListeners;

    public FileTransferTaskAdapter(Context context, ListView listview) {
        super(context);
        mListView = listview;
        mListeners = new HashMap<>();
    }

    private void registerTaskListener(final ViewHolder viewHolder,
                                      final BaseFileTask item,
            final int position) {
        String key = BaseFileTask.generateTaskKey(item.getTransaction(),
                item.getTaskType());
        TaskProgressListener listener = mListeners.get(key);
        if (listener != null) {
            item.unregisterTaskObserver(listener);
        }
        listener = new TaskProgressListener() {

            @Override
            public void onPrepare() {
                if (isItemVisible(position)) {
                    updateProgress(viewHolder, item);
                }
            }

            @Override
            public void onProgressUpdated(Progress prog) {
                if (isItemVisible(position)) {
                    updateProgress(viewHolder, item);
                }
            }

            @Override
            public void onFinished(Result result) {
                if (isItemVisible(position)) {
                    updateProgress(viewHolder, item);
                }
            }

            @Override
            public void onCancelled() {
                if (isItemVisible(position)) {
                    updateProgress(viewHolder, item);
                }
            }
        };
        item.registerTaskObserver(listener);
        mListeners.put(key, listener);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder viewHolder;
        if (row == null) {
            row = getLayoutInflater().inflate(LAYOUT_ID, null);

            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder);

            viewHolder.progressView.setMax(100);
        } else {
            viewHolder = (ViewHolder) row.getTag();
        }

        update(viewHolder, getItem(position), position);
        return row;
    }

    public void update(ViewHolder viewHolder, BaseFileTask item,
                       final int position) {
        registerTaskListener(viewHolder, item, position);

        final TextView typeView = viewHolder.typeView;
        final TextView peerNameView = viewHolder.peerNameView;
        TextView methodView = viewHolder.methodView;
        TextView nameView = viewHolder.nameView;

        if (item instanceof FileWebP2PReceiveTask
                || item instanceof FileWebP2PSendTask) {
            methodView.setText("P2P");
        } else {
            methodView.setText("RELAY");
        }

        final String typeStr;
        String peerId;
        String filePath;
        if (item.getTaskType() == BaseFileTask.TaskType.SEND) {
            typeStr = "Send to";
            peerId = item.getTransaction().getReceiver();
            filePath = item.getTransaction().getPathFrom();
        } else {
            typeStr = "Receive from";
            peerId = item.getTransaction().getSender();
            filePath = item.getTransaction().getPathTo();
        }
        try {
            DeviceManager.from(getContext()).getDeviceWithId(peerId,
                    new OnGetDeviceListener() {

                        @Override
                        public void onGetDevice(Device device) {
                            if (isItemVisible(position)) {
                                typeView.setText(typeStr);
                                peerNameView.setText(device.getName());
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            if (isItemVisible(position)) {
                                typeView.setText(typeStr + "unkown");
                            }
                        }
                    });
        } catch (AuthenticatorException e) {
            // not reach
        }
        filePath += item.getTransaction().getFilename();
        nameView.setText(filePath);

        updateProgress(viewHolder, item);
    }

    private void updateProgress(ViewHolder viewHolder, BaseFileTask task) {
        TextView stateView = viewHolder.stateView;
        ProgressBar progressView = viewHolder.progressView;

        String stateText = "";
        Progress progress = task.getProgress();
        switch (task.getState()) {
        case PREPARE:
            stateText = "prepare ";
            stateText += task.getTaskType() == BaseFileTask.TaskType.SEND ? "sending.."
                    : "receiving..";
            progressView.setIndeterminate(true);
            progressView.setVisibility(View.VISIBLE);
            break;
        case PROGRESS:
            stateText = task.getTaskType() == BaseFileTask.TaskType.SEND ? "sending.."
                    : "receiving..";
            stateText += String.format(" (%1$s / %2$s)",
                    FileUtils.readableFileSize(progress.getDoneLength()),
                    FileUtils.readableFileSize(progress.getFileLength()));
            progressView.setIndeterminate(false);
            progressView.setProgress(progress.getPercent());
            progressView.setVisibility(View.VISIBLE);
            break;
        case CANCELLED:
            stateText = "cancelled";
            progressView.setIndeterminate(false);
            progressView.setVisibility(View.GONE);
            break;
        case FINISHED:
            stateText = "finished";
            progressView.setVisibility(View.GONE);
            progressView.setIndeterminate(false);
            break;
        }
        stateView.setText(stateText);
    }

    @Override
    public void notifyDataSetChanged() {
        for (BaseFileTask task : getItems()) {
            String key = BaseFileTask.generateTaskKey(task.getTransaction(),
                    task.getTaskType());
            TaskProgressListener listener = mListeners.remove(key);
            if (listener != null) {
                task.unregisterTaskObserver(listener);
            }
        }
        super.notifyDataSetChanged();
    }

    @Override
    public boolean remove(BaseFileTask item) {
        String key = BaseFileTask.generateTaskKey(item.getTransaction(),
                item.getTaskType());
        TaskProgressListener listener = mListeners.remove(key);
        if (listener != null) {
            item.unregisterTaskObserver(listener);
        }
        return super.remove(item);
    }

    @Override
    public BaseFileTask remove(int position) {
        BaseFileTask task = super.remove(position);
        String key = BaseFileTask.generateTaskKey(task.getTransaction(),
                task.getTaskType());
        TaskProgressListener listener = mListeners.remove(key);
        if (listener != null) {
            task.unregisterTaskObserver(listener);
        }
        return task;
    }

    @Override
    public void clear() {
        for (BaseFileTask task : getItems()) {
            String key = BaseFileTask.generateTaskKey(task.getTransaction(),
                    task.getTaskType());
            TaskProgressListener listener = mListeners.remove(key);
            if (listener != null) {
                task.unregisterTaskObserver(listener);
            }
        }
        super.clear();
    }

    private boolean isItemVisible(int position) {
        int firstPosition = mListView.getFirstVisiblePosition();
        int lastPosition = mListView.getLastVisiblePosition();
        return position >= firstPosition && position <= lastPosition;
    }

    private class ViewHolder {
        TextView stateView;
        TextView typeView;
        TextView methodView;
        TextView nameView;
        TextView peerNameView;
        ProgressBar progressView;

        public ViewHolder(View view) {
            stateView = (TextView) view.findViewById(R.id.state);
            typeView = (TextView) view.findViewById(R.id.type);
            peerNameView = (TextView) view.findViewById(R.id.peer_name);
            methodView = (TextView) view.findViewById(R.id.transfer_method);
            nameView = (TextView) view.findViewById(R.id.name);
            progressView = (ProgressBar) view.findViewById(R.id.progress);
        }
    }
}
