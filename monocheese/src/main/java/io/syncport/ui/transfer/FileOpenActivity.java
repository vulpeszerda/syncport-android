package io.syncport.ui.transfer;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.syncport.R;
import io.syncport.core.polling.model.FileTransferPollingInfo.Transaction;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RobustResponseErrorListener;
import io.syncport.core.remote.RobustResponseListener;
import io.syncport.core.transfer.BaseFileTask;
import io.syncport.core.transfer.BaseFileTask.FailureResult;
import io.syncport.core.transfer.BaseFileTask.Progress;
import io.syncport.core.transfer.BaseFileTask.Result;
import io.syncport.core.transfer.BaseFileTask.SuccessResult;
import io.syncport.core.transfer.BaseFileTask.TaskProgressListener;
import io.syncport.core.transfer.FileRelayReceiveTask;
import io.syncport.core.transfer.FileTaskManager;
import io.syncport.ui.BaseActivity;
import io.syncport.utils.FileUtils;
import io.syncport.utils.Log;
import io.syncport.utils.SyncportKeyBuilder;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_file_open)
public class FileOpenActivity extends BaseActivity implements
        FileTaskManager.Observer, TaskProgressListener, OnClickListener {

    public static Intent createIntent(Context context, String peerDeviceId,
            String path, String filename) {
        Intent intent = new Intent(context, FileOpenActivity.class);
        intent.putExtra(PARAM_PEER_DEVICE_ID, peerDeviceId);
        intent.putExtra(PARAM_FILE_PARENT_PATH, path);
        intent.putExtra(PARAM_FILE_NAME, filename);
        return intent;
    }

    private static final String TAG = "FileReceiveActivity";
    private static final String PARAM_FILE_PARENT_PATH = SyncportKeyBuilder
            .build("file_parent_path");
    private static final String PARAM_FILE_NAME = SyncportKeyBuilder
            .build("filename");
    private static final String PARAM_PEER_DEVICE_ID = SyncportKeyBuilder
            .build("peer_device_id");

    private static final boolean USE_CACHE = false;

    private @InjectView(R.id.filename) TextView mFileNameView;
    private @InjectView(R.id.progress) ProgressBar mProgressView;
    private @InjectView(R.id.button_open) Button mOpenBtn;
    private @InjectView(R.id.status) TextView mStatusView;

    private String mFileName;
    private String mParentFilePath;
    private String mPeerDeviceId;

    private String mTaskKey;

    private BaseFileTask mTask;
    private BaseFileTask.State mCurrentTaskState;
    private Transaction mTransaction;
    private FileTaskManager mManager;

    private File mReceivedFile;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("unused")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionbar = getSupportActionBar();
        if (!isTaskRoot()) {
            actionbar.setHomeButtonEnabled(true);
            actionbar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        mFileName = intent.getStringExtra(PARAM_FILE_NAME);
        mParentFilePath = intent.getStringExtra(PARAM_FILE_PARENT_PATH);
        if (!mParentFilePath.endsWith("/")) {
            mParentFilePath += "/";
        }
        mPeerDeviceId = intent.getStringExtra(PARAM_PEER_DEVICE_ID);

        actionbar.setTitle(mFileName);
        mFileNameView.setText(mFileName);
        mStatusView.setText(null);
        mProgressView.setProgress(0);
        mOpenBtn.setEnabled(false);
        mOpenBtn.setOnClickListener(this);

        mTaskKey = BaseFileTask.generateTaskKey(mPeerDeviceId,
                mParentFilePath, mFileName);
        mManager = FileTaskManager.getInstance(getApplicationContext());
        BaseFileTask task = mManager.getTask(mTaskKey);

        if (task != null) {
            Log.d(TAG, "found existing task");
            onHookTask((FileRelayReceiveTask) task);
        } else {
            mManager.addObserver(this);
            requestStartTransaction();
        }
    }

    @Override
    protected void onDestroy() {
        if (mManager != null) {
            mManager.removeObserver(this);
        }
        if (mTask != null) {
            if (mTask.getState() != BaseFileTask.State.FINISHED) {
                mTask.cancel();
            }
            mTask.unregisterTaskObserver(this);
        }
        super.onDestroy();
    }

    private void requestStartTransaction() {

        mStatusView.setText("Send request..");
        mProgressView.setIndeterminate(true);

        DeltaResources.startFilePullTransaction(this, mParentFilePath,
                mFileName, mPeerDeviceId,
                new RobustResponseListener<JSONObject>(this) {

                    @Override
                    public void onSecureResponse(JSONObject response) {
                        mStatusView.setText("Wait transaction..");
                    }

                    @Override
                    public void onSkipped(JSONObject response) {

                    }
                }, new RobustResponseErrorListener(this) {

                    @Override
                    public void onErrorResponse(VolleyError error, String msg) {
                        mStatusView.setText("Failed to load file");
                        mProgressView.setIndeterminate(false);
                        mProgressView.setProgress(0);
                        showRequestFailureDialog(msg);
                    }

                    @Override
                    public void onSkipped(VolleyError error) {

                    }
                });
    }

    private void onHookTask(BaseFileTask task) {
        if (task.getTaskType() != BaseFileTask.TaskType.RECEIVE) {
            finish();
        }
        mTask = task;
        mTransaction = mTask.getTransaction();

        mProgressView.setMax(100);

        switch (task.getState()) {
        case PREPARE:
            onPrepare();
            break;
        case PROGRESS:
            onProgressUpdated(task.getProgress());
            break;
        case FINISHED:
            onFinished(task.getResult());
            break;
        case CANCELLED:
            onCancelled();
        }
        mTask.registerTaskObserver(this);

    }

    private void updateView() {
        if (mCurrentTaskState == mTask.getState()) {
            return;
        }
        mCurrentTaskState = mTask.getState();
        switch (mCurrentTaskState) {
        case PREPARE:
            mStatusView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.setIndeterminate(true);
            mOpenBtn.setEnabled(false);
            break;
        case PROGRESS:
            mStatusView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.setIndeterminate(false);
            mOpenBtn.setEnabled(false);
            break;
        case FINISHED:
            mStatusView.setVisibility(View.GONE);
            mProgressView.setVisibility(View.GONE);
            mOpenBtn.setEnabled(true);
            break;
        case CANCELLED:
            mStatusView.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
            mOpenBtn.setEnabled(false);
            break;
        }
    }

    @Override
    public void onAddTask(String tag, BaseFileTask task) {
        if (tag.equals(mTaskKey)) {
            onHookTask(task);
            mManager.removeObserver(this);
        }
    }

    @Override
    public void onRemoveTask(String tag, BaseFileTask task) {

    }

    @Override
    public void onPrepare() {
        updateView();
        mStatusView.setText("Preparing..");
        mProgressView.setProgress(0);
    }

    @Override
    public void onProgressUpdated(Progress prog) {
        updateView();
        String progressFormat = getString(R.string.desc_file_receive_progress__format);
        String progressText = String.format(progressFormat,
                FileUtils.readableFileSize(prog.getDoneLength()),
                FileUtils.readableFileSize(prog.getFileLength()));
        mStatusView.setText(progressText);
        mProgressView.setProgress(prog.getPercent());
    }

    @Override
    public void onFinished(Result result) {
        if (result.isSuccess()) {
            SuccessResult r = (SuccessResult) result;
            mReceivedFile = r.getFile();
            updateView();
        } else {
            FailureResult r = (FailureResult) result;
            showReceiveFailureDialog(r.getException().getMessage());
        }
    }

    @Override
    public void onCancelled() {
        updateView();
        mProgressView.setIndeterminate(false);
        mStatusView.setText("Cancelled");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_open) {
            try {
                startActivity(FileUtils.getOpenFileIntent(mReceivedFile));
            } catch (IOException e) {
                showReceiveFailureDialog(e.getMessage());
            } catch (ActivityNotFoundException e) {
                showOpenFailureDialog(e.getMessage());
            }
        }
    }

    private void showRequestFailureDialog(String msg) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_failed_to_load)
                .setMessage(msg)
                .setNegativeButton(R.string.button_confirm, null)
                .setPositiveButton(R.string.button_retry,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                requestStartTransaction();
                            }
                        }).show();
    }

    private void showReceiveFailureDialog(String msg) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_failed_to_download)
                .setMessage(msg)
                .setPositiveButton(R.string.button_back,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                finish();
                            }
                        }).show();
    }

    private void showOpenFailureDialog(String msg) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_failed_to_open)
                .setMessage(msg)
                .setPositiveButton(R.string.button_back,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                finish();
                            }
                        }).show();
    }

}
