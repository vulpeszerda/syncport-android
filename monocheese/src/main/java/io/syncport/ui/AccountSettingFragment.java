package io.syncport.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.Fragment;
import android.support.v4.preference.PreferenceFragment;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import io.syncport.R;
import io.syncport.core.account.SyncportAccount;
import io.syncport.core.remote.ApiResources;
import io.syncport.core.remote.DeltaResources;
import io.syncport.core.remote.RelayPaths;
import io.syncport.core.remote.SyncPaths;
import io.syncport.core.remote.SyncResouces;
import io.syncport.ui.account.TermActivity;
import io.syncport.ui.addon.ProgressDialogFragment;
import io.syncport.ui.directory.DeviceNameChangeDialog;

/**
 * Created by vulpes on 15. 2. 8..
 */
public class AccountSettingFragment extends PreferenceFragment implements
        BaseDialogFragment.DialogResultHandler {


    private static final String TAG_PROGRESS_DIALOG_SIGNOUT = "signout";
    private static final int REQUEST_CODE__DEVICE_NAME_CHANGE = 10;

    public static Fragment getInstance() {
        return new AccountSettingFragment();
    }

    private static final int SYNC_PREF_ORDER = 10;

    private boolean mIsDebugMode;
    private SharedPreferences mDefaultPreference;
    private SyncportAccount mAccount;

    @Override
    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);

        setRetainInstance(true);
        addPreferencesFromResource(R.xml.preferences);

        mDefaultPreference = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        mIsDebugMode = mDefaultPreference.getBoolean(
                getString(R.string.preference_key__debug), false);

        // set version code
        Preference versionPref = findPreferenceByKeyResId(R.string
                .preference_key__version);
        String version = getString(R.string.app_version);
        if (mIsDebugMode) {
            version += "(DEBUG)";
        }
        versionPref.setSummary(version);

        // set account row info
        mAccount = SyncportAccount.getAuthenticatedUser(getActivity());
        if (mAccount == null) {
            getActivity().finish();
            return;
        }
        Preference accountPref = findPreferenceByKeyResId(R.string
                .preference_key__email);
        accountPref.setSummary(mAccount.getName());

        // set device name info
        int myDeviceNameResId = R.string.preference_key__current_device_name;
        Preference myDeviceNamePref = findPreferenceByKeyResId
                (myDeviceNameResId);
        myDeviceNamePref.setSummary(mAccount.getDeviceName());

        // PreferenceScreen root = getPreferenceScreen();
        //
        // PreferenceCategory syncPrefCategory = new PreferenceCategory(
        // getActivity());
        // syncPrefCategory.setTitle(R.string.preference_title__syncdevices);
        // syncPrefCategory.setOrder(SYNC_PREF_ORDER);
        // root.addPreference(syncPrefCategory);

        // String[] devices = new String[] { "android", "teksu-pc" };
        // int order = SYNC_PREF_ORDER;
        // for (String deviceName : devices) {
        // PreferenceScreen deviceSyncPref = getPreferenceManager()
        // .createPreferenceScreen(getActivity());
        // deviceSyncPref.setTitle(deviceName);
        // deviceSyncPref.setOrder(++order);
        //
        // String notiKey = String.format(
        // getString(R.string.preference_key__notification),
        // deviceName);
        // String fileKey = String.format(
        // getString(R.string.preference_key__filesystem),
        // deviceName);
        //
        // CheckBoxPreference notiSyncPref = new CheckBoxPreference(
        // getActivity());
        // notiSyncPref.setTitle(R.string.preference_title__notification);
        // notiSyncPref.setKey(notiKey);
        // deviceSyncPref.addPreference(notiSyncPref);
        //
        // CheckBoxPreference fileSyncPref = new CheckBoxPreference(
        // getActivity());
        // fileSyncPref.setTitle(R.string.preference_title__filesystem);
        // fileSyncPref.setKey(fileKey);
        // deviceSyncPref.addPreference(fileSyncPref);
        //
        // root.addPreference(deviceSyncPref);
        // }
    }

    private Preference findPreferenceByKeyResId(int resId) {
        return getPreferenceManager().findPreference(getString(resId));
    }

    private boolean isEqualKey(String key, int resourceId) {
        return TextUtils.equals(key, getString(resourceId));
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                                         Preference preference) {
        String key = preference.getKey();
        if (isEqualKey(key, R.string.preference_key__signout)) {
            showSignoutConfirmDialog();
            return true;
        } else if (isEqualKey(key, R.string.preference_key__privacy)) {
            startActivity(TermActivity.createIntent(getActivity(), false));
            return true;
        } else if (isEqualKey(key,
                R.string.preference_key__current_device_name)) {
            DeviceNameChangeDialog dialog = DeviceNameChangeDialog
                    .getInstance(mAccount.getDeviceName(),
                            mAccount.getDeviceId());
            dialog.setTargetFragment(this, REQUEST_CODE__DEVICE_NAME_CHANGE);
            dialog.show(getFragmentManager(), "device_name_change");
        }
        if (preference instanceof PreferenceScreen) {
            initializeActionBar((PreferenceScreen) preference);
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @SuppressLint("NewApi")
    private void initializeActionBar(PreferenceScreen screen) {
        final Dialog dialog = screen.getDialog();
        if (dialog == null
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return;
        }

        dialog.getActionBar().setDisplayHomeAsUpEnabled(true);
        View homeBtn = dialog.findViewById(android.R.id.home);
        if (homeBtn != null) {
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            };
            ViewParent homeBtnContainer = homeBtn.getParent();
            if (homeBtnContainer instanceof FrameLayout) {
                ViewGroup containerParent = (ViewGroup) homeBtnContainer
                        .getParent();

                if (containerParent instanceof LinearLayout) {
                    ((LinearLayout) containerParent)
                            .setOnClickListener(listener);
                } else {
                    ((FrameLayout) homeBtnContainer)
                            .setOnClickListener(listener);
                }
            } else {
                homeBtn.setOnClickListener(listener);
            }
        }
    }

    private void showSignoutConfirmDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.title_are_you_sure)
                .setMessage(R.string.desc_confirm_signout)
                .setNegativeButton(R.string.button_no, null)
                .setPositiveButton(R.string.button_yes,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                processSignout();
                            }
                        }).show();
    }

    private void processSignout() {
        final BaseActivity activity = (BaseActivity) getActivity();
        ProgressDialogFragment.show((BaseActivity) getActivity(),
                TAG_PROGRESS_DIALOG_SIGNOUT, R.string.desc_wait_signout);
        SyncportAccount.signOut(getActivity(),
                new SyncportAccount.OnSignoutFinishedListener() {

                    @Override
                    public void onFinished() {
                        ProgressDialogFragment.hide(
                                (BaseActivity) getActivity(),
                                TAG_PROGRESS_DIALOG_SIGNOUT);
                        activity.finish();
                        startActivity(new Intent(StartupActivity
                                .createIntent(getActivity())));

                    }
                });
    }

    @Override
    public void onDialogResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE__DEVICE_NAME_CHANGE
                && resultCode == BaseDialogFragment.RESULT_OK
                && data != null) {
            String newName = data
                    .getStringExtra(DeviceNameChangeDialog.PARAM_NEW_NAME);
            int myDeviceNameResId = R.string.preference_key__current_device_name;
            Preference myDeviceNamePref = findPreferenceByKeyResId(myDeviceNameResId);
            myDeviceNamePref.setSummary(newName);
            mAccount.setDeviceName(newName);
        }
    }
}
