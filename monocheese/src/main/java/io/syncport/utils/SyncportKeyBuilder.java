package io.syncport.utils;

import java.util.Locale;

import android.text.TextUtils;

public class SyncportKeyBuilder {

    public static SyncportKeyBuilder getInstance() {
        return new SyncportKeyBuilder(BUNDLE_KEY_PREFIX);
    }

    public static SyncportKeyBuilder getInstance(String additionalPrefix) {
        return new SyncportKeyBuilder(BUNDLE_KEY_PREFIX, additionalPrefix);
    }
    
    public static String build(String key) {
        return SyncportKeyBuilder.getInstance().getKey(key);
    }
    
    private static final String BUNDLE_KEY_PREFIX = "io.syncport";

    private String mPrefix;

    public SyncportKeyBuilder(String... prefix) {
        mPrefix = TextUtils.join(".", prefix);
    }

    public String getKey(String key) {
        return mPrefix + "." + key.toLowerCase(Locale.ENGLISH);
    }
}
