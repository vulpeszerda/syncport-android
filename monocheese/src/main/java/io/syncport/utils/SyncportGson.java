package io.syncport.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SyncportGson {
    private static final String REMOTE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static Gson getInstance() {
        return new GsonBuilder()
                .setDateFormat(REMOTE_DATE_FORMAT)
                .setFieldNamingPolicy(
                        FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    }
}
