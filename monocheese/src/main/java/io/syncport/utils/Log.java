package io.syncport.utils;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.syncport.BuildConfig;

/**
 * Created by vulpes on 15. 3. 14..
 */
public class Log {

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]D/" + tag + " : " + msg);
        }
        android.util.Log.d(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]E/" + tag + " : " + msg);
        }
        android.util.Log.e(tag, msg);
    }

    public static void i(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]I/" + tag + " : " + msg);
        }
        android.util.Log.i(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]V/" + tag + " : " + msg);
        }
        android.util.Log.v(tag, msg);
    }

    public static void w(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]W/" + tag + " : " + msg);
        }
        android.util.Log.w(tag, msg);
    }

    public static void exception(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss");
            appendLog("[" + format.format(date) + "]Exception/" + tag + " : " +
                    msg);
            android.util.Log.e("Exception[" + tag + "]", msg);
        }
    }

    private static void appendLog(String text) {
        File syncportDir = new File(Environment.getExternalStorageDirectory()
                , "Syncport");
        syncportDir.mkdirs();
        File logFile = new File(syncportDir, "syncport.log");
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
